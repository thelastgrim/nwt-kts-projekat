import { HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { PictureBase64 } from 'src/app/components/model/pictureBase64';

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  private readonly path = 'http://localhost:8080';

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  getPicture(id:number) : Observable<HttpResponse<PictureBase64>>{
    return this.http.get<PictureBase64>(this.path + "/pictures/"+id, {observe : 'response'})
  }
}
