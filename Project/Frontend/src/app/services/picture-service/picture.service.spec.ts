import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';

import { Comment } from 'src/app/components/model/comment';
import { Picture } from 'src/app/components/model/picture';
import { PictureService } from './picture.service';
import { PictureBase64 } from 'src/app/components/model/pictureBase64';


describe('PictureService', () => {
    let injector;
    let pictureService: PictureService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {

        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
           providers:    [PictureService ]
        });
    
        injector = getTestBed();
        pictureService = TestBed.inject(PictureService);
        httpClient = TestBed.inject(HttpClient);
        httpMock = TestBed.inject(HttpTestingController);
      });

      afterEach(() => {
        httpMock.verify();
      });


      it('getPicture() should query url and return picture', fakeAsync(() => {

        let newPicture : PictureBase64 = new PictureBase64()

        const mockPictureBase64: PictureBase64 = 
        {
          picture : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o"
        };
    
        pictureService.getPicture(1).subscribe(res => newPicture = res.body);
        
        const req = httpMock.expectOne('http://localhost:8080/pictures/1');
        expect(req.request.method).toBe('GET');
        req.flush(mockPictureBase64);
    
        tick();
    
        expect(newPicture).toBeDefined();
        expect(newPicture.picture).toEqual('aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o');
      }));

      it('getPicture() should query url and return 404, if picture doesnt exist', fakeAsync(() => {

        let newPicture : PictureBase64 = new PictureBase64()

        let newError : HttpHeaderResponse
    
        pictureService.getPicture(-1).subscribe(null, error => newError = error);
        
        const req = httpMock.expectOne('http://localhost:8080/pictures/-1');
        expect(req.request.method).toBe('GET');

        req.flush("Something went wrong" , {status:404, statusText: "NOT_FOUND"});

        expect(newError.status).toBe(404);
        expect(newError.statusText).toBe("NOT_FOUND");
    
      }));

});
