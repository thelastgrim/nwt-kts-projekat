import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { Category } from 'src/app/components/model/category';
import { AuthService } from './auth.service';


describe('CategoryServiceService', () => {
  let injector;
  let authService: AuthService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
       providers:    [AuthService]
  });

  injector = getTestBed();
  authService = TestBed.inject(AuthService);
  httpClient = TestBed.inject(HttpClient);
  httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('setSession() should put user info into current session', fakeAsync(() => {

    let authResult = {
        expiresIn : '180000',
        accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6IlVTRVIiLCJkaXNwbGF5TmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.k2nTg0l3ZSjE3ao7cD0YxKMMYppeXQKdbO247ZP0ESA"
    }

    authService.setSession(authResult)

    expect(localStorage.getItem('id_token')).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6IlVTRVIiLCJkaXNwbGF5TmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.k2nTg0l3ZSjE3ao7cD0YxKMMYppeXQKdbO247ZP0ESA')
    expect(localStorage.getItem('logged')).toEqual('true');
    expect(localStorage.getItem('role')).toEqual('USER');
  }));

  it('getToken() should return token', fakeAsync(() => {

    let authResult = {
        expiresIn : '180000',
        accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6IlVTRVIiLCJkaXNwbGF5TmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.k2nTg0l3ZSjE3ao7cD0YxKMMYppeXQKdbO247ZP0ESA"
    }

    authService.setSession(authResult)

    expect(authService.getToken()).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6IlVTRVIiLCJkaXNwbGF5TmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.k2nTg0l3ZSjE3ao7cD0YxKMMYppeXQKdbO247ZP0ESA')
    
  }));

  it('isLoggedIn() should return if user is logged in', fakeAsync(() => {

    let authResult = {
        expiresIn : '180000',
        accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6IlVTRVIiLCJkaXNwbGF5TmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.k2nTg0l3ZSjE3ao7cD0YxKMMYppeXQKdbO247ZP0ESA"
    }

    authService.setSession(authResult)

    expect(authService.isLoggedIn()).toEqual(true)
  }));

  it('logout() should clear session', fakeAsync(() => {

    authService.logout()

    expect(localStorage.removeItem("id_token")).toEqual(undefined)
    expect(localStorage.removeItem("expires_at")).toEqual(undefined)
    expect(localStorage.removeItem('displayName')).toEqual(undefined)
    expect(localStorage.removeItem('logged')).toEqual(undefined)
    expect(localStorage.removeItem('role')).toEqual(undefined)
  }));

  it('isLoggedOut() should return if user is logged out', fakeAsync(() => {
    authService.logout()
    expect(authService.isLoggedOut()).toEqual(true)

  }));

  

  
});
