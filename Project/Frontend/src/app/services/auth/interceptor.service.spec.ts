import { getTestBed, TestBed } from '@angular/core/testing';
import {HttpClientTestingModule,HttpTestingController, } from '@angular/common/http/testing';
import { InterceptorService } from './interceptor.service';
import { HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CategoryServiceService } from '../category-service/category-service.service';


describe('InterceptorService', () => {
    let injector;
    let service: CategoryServiceService;
    let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CategoryServiceService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: InterceptorService,
          multi: true,
        },
      ],
    });

    injector = getTestBed()
    service = injector.get(CategoryServiceService)
    httpMock = injector.get(HttpTestingController);
  });

  afterEach( ()=>{
    httpMock.verify()
  })


  it('should add an Authorization header', () => {

    service.getAllCategories().subscribe()
    const httpReq = httpMock.expectOne("http://localhost:8080/categories")
    expect(httpReq.request.headers.has('Authorization')).toEqual(true)
  
   
  });
  
});