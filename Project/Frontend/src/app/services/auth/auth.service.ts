import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ReplaySubject } from 'rxjs';
import { UserLogIn } from 'src/app/components/model/userLogIn';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  

  private helper = new JwtHelperService();


  constructor() { }

    setSession(authResult:any) {
      
    const expiresAt = moment().add(authResult.expiresIn,'second');
    let decodedToken = this.helper.decodeToken(authResult.accessToken);
   
    localStorage.setItem('id_token', authResult.accessToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    localStorage.setItem('displayName', decodedToken.displayName)
    localStorage.setItem('logged', 'true')
    localStorage.setItem('role', decodedToken.role)

  }
  
  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem('displayName');
    localStorage.removeItem('logged');
    localStorage.removeItem('role');
  }
  getToken() {
    return localStorage.getItem('id_token');
  }
  public isLoggedIn() {
      
      return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
      return !this.isLoggedIn();
  }

  getExpiration() {
      const expiration = localStorage.getItem("expires_at");
      const expiresAt = JSON.parse(expiration || '{}');
      return moment(expiresAt);
  }    
}
