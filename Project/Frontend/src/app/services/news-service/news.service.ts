import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { News } from 'src/app/components/model/news';
import { NewsAdd } from 'src/app/components/model/newsAdd';
import { Constants} from 'src/app/app.constants'
import { Observable, OperatorFunction, Subject } from 'rxjs';
import { Page } from 'src/app/components/model/page';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  private path = 'http://localhost:8080/categories';

  pageSize = new Constants().PAGE_SIZE_NEWS;

  constructor(private http: HttpClient) {}



  getNews(offerId: number, page: number) : Observable<HttpResponse<Page<News>>> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('size', this.pageSize.toString())
      .set('sort', 'dateAdded,desc');
    return this.http.get<Page<News>>(
      `${this.path}/0/culturalOffers/${offerId}/news/by-page`,
      { params: params, observe : 'response' }
    );
  }

  createNews(newsDTO:NewsAdd, categoryId:number) : Observable<HttpResponse<NewsAdd>>{
    console.log("SERVICE NEWS: LINK" + this.path + "/" + categoryId+ "/culturalOffers/" + newsDTO.culturalOfferId + "/news")
    console.log("SERVICE NEWS DTO " + newsDTO.culturalOfferId + " "+ newsDTO.dateAdded + " " + newsDTO.text + " "+ newsDTO.title)

    return this.http.post<NewsAdd>(this.path + "/" + categoryId+ "/culturalOffers/" + newsDTO.culturalOfferId + "/news", newsDTO, {observe : 'response'})
  }
}
