import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { Page } from 'src/app/components/model/page';
import { NewsService } from './news.service';
import { News } from 'src/app/components/model/news';
import { NewsAdd } from 'src/app/components/model/newsAdd';

describe('NewsService', () => {
  let injector;
  let newsService: NewsService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

	beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
       providers:    [NewsService ]
    });

    injector = getTestBed();
    newsService = TestBed.inject(NewsService);
    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('getNews() should return news page and display it', fakeAsync(() => {

    let newsPage : Page<News>;
    let mockNewsPage : Page<News> = {
      "content": [
        {
            "dateAdded": new Date(1610406000000),
            "text": "Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.",
            "title": "Eddie",
        },
        {
            "dateAdded": new Date(1608764400000),
            "text": "Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.",
            "title": "Fubar",
        },
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false,
        },
        "offset": 0,
        "pageSize": 10,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalElements": 480,
    "totalPages": 48,
    "last": false,
    "size": 10,
    "number": 0,
    "sort": {
        "sorted": true,
        "unsorted": false,
    },
    "numberOfElements": 10,
    "first": true,
    };

    newsService.getNews(1, 0).subscribe(res => {
        newsPage = res.body;
    });

    const req = httpMock.expectOne('http://localhost:8080/categories/0/culturalOffers/1/news/by-page?page=0&size=10&sort=dateAdded,desc');
    expect(req.request.method).toBe('GET');
    req.flush(mockNewsPage);

    tick();

    expect(newsPage).toBeDefined();
    expect(newsPage.sort.sorted).toEqual(true);

    expect(newsPage.content[0].title).toEqual("Eddie");
    expect(newsPage.content[0].dateAdded).toEqual(new Date(1610406000000));
    expect(newsPage.content[0].text).toEqual( "Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.");

    expect(newsPage.content[1].title).toEqual("Fubar");
    expect(newsPage.content[1].dateAdded).toEqual(new Date(1608764400000));
    expect(newsPage.content[1].text).toEqual("Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.");

    expect(newsPage.content[0].dateAdded >= newsPage.content[1].dateAdded).toBeTruthy();

  }));


  it('createNews() should save news ', fakeAsync(() => {

    let newsResponse = new NewsAdd();
    let news = new NewsAdd();
    news.title = "Nova ponuda";
    news.text = "Nesto novo";
    news.culturalOfferId = 1;

    const mockNews = new NewsAdd();
    mockNews.culturalOfferId = 1;
    mockNews.text = "Nesto novo";
    mockNews.title = "Nova ponuda";

    newsService.createNews(news,1).subscribe(res => {
        newsResponse  = res.body;
    });

    const req = httpMock.expectOne('http://localhost:8080/categories/1/culturalOffers/1/news');
    expect(req.request.method).toBe('POST');
    req.flush(mockNews);

    tick();

    expect(newsResponse).toBeDefined();
    expect(newsResponse.title).toEqual("Nova ponuda");
    expect(newsResponse.text).toEqual("Nesto novo");
    expect(newsResponse.culturalOfferId).toEqual(1);


  }));

  it('getNews() should return 404 since Page<News> is null', fakeAsync(()=>{

    let error : HttpHeaderResponse;

    newsService.getNews(58, 0).subscribe(res =>{}, err => {
      error = err;
  });

  const req = httpMock.expectOne('http://localhost:8080/categories/0/culturalOffers/58/news/by-page?page=0&size=10&sort=dateAdded,desc');
  expect(req.request.method).toBe('GET');
  req.flush("Something went wrong" , {status:404, statusText: "NOT_FOUND"});

  tick();

  expect(error.status).toBe(404);

  }));

});

