import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/components/model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryServiceService {


  private readonly path = 'http://localhost:8080/categories';
  constructor(private http: HttpClient) { }

  getAllCategories():Observable<HttpResponse<Category[]>>{
    return this.http.get<Category[]>(this.path, {observe: 'response'});
  }

  saveCategory(category : Category): Observable<HttpResponse<Category>>{
      return this.http.post<Category>(this.path + "/" , category,{observe: 'response'});
  }

  deleteCategory(id: number):Observable<HttpResponse<any>> {
    return this.http.delete<any>(this.path + "/" + id);
  }

}
