import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { CategoryServiceService } from './category-service.service';
import { Category } from 'src/app/components/model/category';


describe('CategoryServiceService', () => {
  let injector;
  let categoryService: CategoryServiceService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
       providers:    [CategoryServiceService]
  });

  injector = getTestBed();
  categoryService = TestBed.inject(CategoryServiceService);
  httpClient = TestBed.inject(HttpClient);
  httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should pass simple test', () => {
    expect(true).toBe(true);
});

  it('createCategory() should query url and save a category', fakeAsync(() => {

    let newCategory: Category = new Category();
    newCategory.name = "TEST_NAME"
    newCategory.deleted = false
    newCategory.culturalOffers = null
    newCategory.id = 5

    const mockCategory: Category =
    {
      id: 5,
      name : "TEST_NAME",
      deleted : false,
      culturalOffers : null
    };
    categoryService.saveCategory(newCategory).subscribe(res => newCategory = res.body);

    const req = httpMock.expectOne('http://localhost:8080/categories/');
    expect(req.request.method).toBe('POST');
    req.flush(mockCategory);

    tick();

    expect(newCategory).toBeDefined();
    expect(newCategory.id).toEqual(5);
    expect(newCategory.name).toEqual('TEST_NAME');
    expect(newCategory.deleted).toBeFalse;
    expect(newCategory.culturalOffers).toBeNull;
  }));

  it('createCategory() should return 404 BAD_REQUEST', fakeAsync(() => {

    let newCategory: Category = new Category();
    newCategory.name = "Museums"
    newCategory.deleted = false
    newCategory.culturalOffers = null
    newCategory.id = 5

    let error : HttpHeaderResponse;

    categoryService.saveCategory(newCategory).subscribe(null, data=>{
      error = data;});

    const req = httpMock.expectOne('http://localhost:8080/categories/');
    expect(req.request.method).toBe('POST');

    req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

    tick();
   expect(error.status).toBe(400);
  }));

  it('getCategories() should return all categories', fakeAsync(()=>{
    let categories : Category[];
    const mockCategories: Category[] = [
      {
        id: 1,
        name : "TEST_NAME1",
        deleted : false,
        culturalOffers : null
      },
      {
        id: 2,
        name : "TEST_NAME2",
        deleted : false,
        culturalOffers : null
      }
    ];

    categoryService.getAllCategories().subscribe(data => {
      categories = data.body;
    })

    const req = httpMock.expectOne('http://localhost:8080/categories');
    expect(req.request.method).toBe('GET');

    req.flush(mockCategories);

    tick();

    expect(categories.length).toEqual(2,'shoud contain given amount of categories');
    expect(categories[0].id).toEqual(1);
    expect(categories[0].deleted).toBeFalse();
    expect(categories[0].name).toBe("TEST_NAME1");
    expect(categories[0].culturalOffers).toBeNull;

    expect(categories[1].id).toEqual(2);
    expect(categories[1].deleted).toBeFalse();
    expect(categories[1].name).toBe("TEST_NAME2");
    expect(categories[1].culturalOffers).toBeNull;

  }));

  it('delete() should query url and delete category offer', fakeAsync(() => {

    categoryService.deleteCategory(1).subscribe(res => { });

    const req = httpMock.expectOne('http://localhost:8080/categories/1');
    expect(req.request.method).toBe('DELETE');
    req.flush({});

  }));

});
