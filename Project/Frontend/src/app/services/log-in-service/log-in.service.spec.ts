import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';

import { Comment } from 'src/app/components/model/comment';
import { Picture } from 'src/app/components/model/picture';
import { LogInService } from './log-in.service';
import { UserLogIn } from 'src/app/components/model/userLogIn';
import { LoginResponse } from 'src/app/components/model/loginResponse';
import { UserTokenState } from 'src/app/components/model/userTokenState';

describe('LogInService', () => {
    let injector;
    let logInService: LogInService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {

        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
           providers:    [LogInService ]
        });

        injector = getTestBed();
        logInService = TestBed.inject(LogInService);
        httpClient = TestBed.inject(HttpClient);
        httpMock = TestBed.inject(HttpTestingController);
      });

      afterEach(() => {
        httpMock.verify();
      });


      it('logIn() should query url and log in user', fakeAsync(() => {

        let userLogIn: UserLogIn = new UserLogIn();
        userLogIn.username = "username"
        userLogIn.password = "password"

        let newloginResponse = new LoginResponse()

        const userTokenState : UserTokenState ={
              accessToken : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lIiwicGFzc3dvcmQiOiJwYXNzd29yZCIsImV4cGlyZXNJbiI6MTgwMDAwMH0.Oxa26QBHCR7Q6KlXryQpIiVJWmYyrDS-FU-jwrRzEX8",
              expiresIn : 1800000

        }

        const mockLoginReponse: LoginResponse =
        {
          subbs : [1, 2],
          dtoUserTokenState: userTokenState
        };

        logInService.logIn(userLogIn).subscribe(res => newloginResponse = res.body);

        const req = httpMock.expectOne('http://localhost:8080/auth/log-in');
        expect(req.request.method).toBe('POST');
        req.flush(mockLoginReponse);

        tick();

        expect(newloginResponse).toBeDefined();
        expect(newloginResponse.subbs).toEqual([1, 2]);
        expect(newloginResponse.dtoUserTokenState.accessToken).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lIiwicGFzc3dvcmQiOiJwYXNzd29yZCIsImV4cGlyZXNJbiI6MTgwMDAwMH0.Oxa26QBHCR7Q6KlXryQpIiVJWmYyrDS-FU-jwrRzEX8');
        expect(newloginResponse.dtoUserTokenState.expiresIn).toEqual(1800000)
      }));

      it('logIn() should query url and return 400, if user doesnt exist', fakeAsync(() => {

        let userLogIn: UserLogIn = new UserLogIn();
        userLogIn.username = "username"
        userLogIn.password = "password"

        let newError : HttpHeaderResponse


        logInService.logIn(userLogIn).subscribe(null, error => newError = error);

        const req = httpMock.expectOne('http://localhost:8080/auth/log-in');
        expect(req.request.method).toBe('POST');
        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

       
        expect(newError.status).toBe(400);
        expect(newError.statusText).toBe("BAD_REQUEST");
  
      }));
      


});
