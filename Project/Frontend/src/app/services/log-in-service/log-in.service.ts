import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoginResponse } from 'src/app/components/model/loginResponse';
import { User } from 'src/app/components/model/user';
import { UserLogIn } from 'src/app/components/model/userLogIn'

@Injectable({
  providedIn: 'root'
})
export class LogInService {
  
  private readonly path = 'http://localhost:8080/';

  


  constructor(private http: HttpClient) { }

  logIn(_user: UserLogIn) : Observable<HttpResponse<LoginResponse>>{

    return this.http.post<LoginResponse>(this.path+"auth/log-in",_user, {observe : 'response'})
    .pipe(
      catchError( err => {
        console.log(err)
        alert("Wrong credentials :(")
        return throwError(err);
      })
      );;
  }
}
