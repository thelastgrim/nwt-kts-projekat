import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { CommentService } from './comment.service';
import { Comment } from 'src/app/components/model/comment';
import { Picture } from 'src/app/components/model/picture';
import { Page } from 'src/app/components/model/page';
import {Constants} from 'src/app/app.constants';
import { constants } from 'buffer';

describe('CommentService', () => {
  let injector;
  let commentService: CommentService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

	beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
       providers:    [CommentService ]
    });

    injector = getTestBed();
    commentService = TestBed.inject(CommentService);
    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('createComment() should query url and save a comment', fakeAsync(() => {

    let newComment: Comment = new Comment();
    newComment.date = new Date()
    newComment.rating = 2
    newComment.text = "TEST"
    newComment.username = "TEST"

    const mockStudent: Comment =
    {
      id: 1,
      rating : 2,
      text : "TEST",
      username : "TEST" ,
      date : new Date()
    };

    commentService.createComment(newComment, 1).subscribe(res => newComment = res.body);

    const req = httpMock.expectOne('http://localhost:8080/culturalOffers/1/comments');
    expect(req.request.method).toBe('POST');
    req.flush(mockStudent);

    tick();

    expect(newComment).toBeDefined();
    expect(newComment.id).toEqual(1);
    expect(newComment.text).toEqual('TEST');
    expect(newComment.rating).toEqual(2);
    expect(newComment.username).toEqual('TEST');
    expect(newComment.date).toEqual(new Date());
  }));

  it('uploadPicture() should query url and return 406 not accepted if there is no title', fakeAsync(() => {

    let base64 = Constants.IMAGE_FILE
    let newError : HttpHeaderResponse;
    const imageBlob = commentService.dataURItoBlob(base64);

    const imageFile = new File([imageBlob], "NASLOV", { type: 'image/png' });
    let formData:FormData = new FormData();
    //formData.append("title", "NASLOV")
    formData.append('file', imageFile);

    let newPicture: Picture = new Picture()

    const mockPicture: Picture =
    {
      id: 1,
      title: "NASLOV",
      path : "/resources/NASLOV.png"
    };

    commentService.uploadPicture(formData, 1, 1).subscribe(null, error => newError=error);

    const req = httpMock.expectOne('http://localhost:8080/pictures/culturalOffers/1/comments/1');
    expect(req.request.method).toBe('POST');

    req.flush("Something went wrong" , {status:406, statusText: "NOT_ACCEPTABLE"});

    expect(newError.status).toBe(406);
    expect(newError.statusText).toBe("NOT_ACCEPTABLE");

  }));

  it('uploadPicture() should query url and return 406 not accepted if there is no file', fakeAsync(() => {

    let base64 = Constants.IMAGE_FILE
    let newError : HttpHeaderResponse;
    const imageBlob = commentService.dataURItoBlob(base64);

    const imageFile = new File([imageBlob], "NASLOV", { type: 'image/png' });
    let formData:FormData = new FormData();
    formData.append("title", "NASLOV")
    //formData.append('file', imageFile);

    let newPicture: Picture = new Picture()

    const mockPicture: Picture =
    {
      id: 1,
      title: "NASLOV",
      path : "/resources/NASLOV.png"
    };

    commentService.uploadPicture(formData, 1, 1).subscribe(null, error => newError=error);

    const req = httpMock.expectOne('http://localhost:8080/pictures/culturalOffers/1/comments/1');
    expect(req.request.method).toBe('POST');

    req.flush("Something went wrong" , {status:406, statusText: "NOT_ACCEPTABLE"});

    expect(newError.status).toBe(406);
    expect(newError.statusText).toBe("NOT_ACCEPTABLE");

  }));

  it('uploadPicture() should query url and upload a picture for comment', fakeAsync(() => {

    let base64 = Constants.IMAGE_FILE

    const imageBlob = commentService.dataURItoBlob(base64);

    const imageFile = new File([imageBlob], "NASLOV", { type: 'image/png' });
    let formData:FormData = new FormData();
    formData.append("title", "NASLOV")
    formData.append('file', imageFile);

    let newPicture: Picture = new Picture()

    const mockPicture: Picture =
    {
      id: 1,
      title: "NASLOV",
      path : "/resources/NASLOV.png"
    };

    commentService.uploadPicture(formData, 1, 1).subscribe(res => newPicture = res.body);

    const req = httpMock.expectOne('http://localhost:8080/pictures/culturalOffers/1/comments/1');
    expect(req.request.method).toBe('POST');
    req.flush(mockPicture);

    tick();

    expect(newPicture).toBeDefined();
    expect(newPicture.id).toEqual(1);
    expect(newPicture.title).toEqual('NASLOV');
    expect(newPicture.path).toEqual('/resources/NASLOV.png');

  }));

  it('getComments() should get page comments for culturalOffer and display it', fakeAsync(() => {

    let commentPage : Page<Comment>;

    let mockCommentPage : Page<Comment> = {
        "content": [
            {
                "id": 224,
                "date": new Date(1610406000000),
                "text": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.",
                "username": "test2",
                "rating": 4.0,
                "pictures": []
            },
            {
                "id": 4,
                "date": new Date(1607727600000),
                "text": "Pellentesque viverra pede ac diam.",
                "username": "test",
                "rating": 2.0,
                "pictures": []
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
            },
            "offset": 0,
            "pageSize": 10,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 512,
        "last": false,
        "totalPages": 52,
        "size": 10,
        "number": 0,
        "sort": {
            "sorted": true,
            "unsorted": false,

        },
        "numberOfElements": 10,
        "first": true,
    }


    commentService.getComments(1 , 0).subscribe(res =>{
      commentPage = res.body;
    });

    const req = httpMock.expectOne('http://localhost:8080/culturalOffers/1/comments/by-page?page=0&size=10&sort=date,desc');
    expect(req.request.method).toBe('GET');
    req.flush(mockCommentPage);

    tick();

    expect(commentPage).toBeDefined();
    expect(commentPage.sort.sorted).toEqual(true);

    expect(commentPage.content[0].id).toEqual(224);
    expect(commentPage.content[0].date).toEqual(new Date(1610406000000));
    expect(commentPage.content[0].text).toEqual( "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.");
    expect(commentPage.content[0].rating).toEqual(4);
    expect(commentPage.content[0].pictures).toEqual([]);
    expect(commentPage.content[0].username).toEqual("test2");

    expect(commentPage.content[1].id).toEqual(4);
    expect(commentPage.content[1].date).toEqual(new Date(1607727600000));
    expect(commentPage.content[1].text).toEqual("Pellentesque viverra pede ac diam.");
    expect(commentPage.content[1].rating).toEqual(2);
    expect(commentPage.content[1].pictures).toEqual([]);
    expect(commentPage.content[1].username).toEqual("test");

    expect(commentPage.content[0].date >= commentPage.content[1].date).toBeTruthy();

  }));

  it('getComments() should return 404 not found if culturalOffer id doesnt exist', fakeAsync(() => {

    let commentPage : Page<Comment>;

    let mockCommentPage : Page<Comment> = {
        "content": [
            {
                "id": 224,
                "date": new Date(1610406000000),
                "text": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.",
                "username": "test2",
                "rating": 4.0,
                "pictures": []
            },
            {
                "id": 4,
                "date": new Date(1607727600000),
                "text": "Pellentesque viverra pede ac diam.",
                "username": "test",
                "rating": 2.0,
                "pictures": []
            }
        ],
        "pageable": {
            "sort": {
                "sorted": true,
                "unsorted": false,
            },
            "offset": 0,
            "pageSize": 10,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 512,
        "last": false,
        "totalPages": 52,
        "size": 10,
        "number": 0,
        "sort": {
            "sorted": true,
            "unsorted": false,

        },
        "numberOfElements": 10,
        "first": true,
    }

    let newError : HttpHeaderResponse;

    commentService.getComments(-1 , 0).subscribe(null, error => newError =error);

    const req = httpMock.expectOne('http://localhost:8080/culturalOffers/-1/comments/by-page?page=0&size=10&sort=date,desc');
    expect(req.request.method).toBe('GET');

    req.flush("Something went wrong" , {status:404, statusText: "NOT_FOUND"});

    expect(newError.status).toBe(404);
    expect(newError.statusText).toBe("NOT_FOUND");

  }));

  it('getComents() should return 404 since Page<Comment> is null', fakeAsync(()=>{

    let error : HttpHeaderResponse;

    commentService.getComments(58, 0).subscribe(res =>{}, err => {
      error = err;
  });

  const req = httpMock.expectOne('http://localhost:8080/culturalOffers/58/comments/by-page?page=0&size=10&sort=date,desc');
  expect(req.request.method).toBe('GET');
  req.flush("Something went wrong" , {status:404, statusText: "NOT_FOUND"});

  tick();

  expect(error.status).toBe(404);

  }));

});

