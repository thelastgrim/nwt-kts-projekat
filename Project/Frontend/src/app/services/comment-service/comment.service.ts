import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Comment } from 'src/app/components/model/comment';
import { Constants } from 'src/app/app.constants';
import { constants } from 'buffer';
import { catchError } from 'rxjs/operators';
import { Observable, OperatorFunction, Subject } from 'rxjs';
import { Page } from 'src/app/components/model/page';
import { Picture } from 'src/app/components/model/picture';


@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private path = "http://localhost:8080/culturalOffers";
  pageSize :number = new Constants().PAGE_SIZE_COMMENTS;
  constructor(private http: HttpClient) { }

  /*
  private RegenerateData = new Subject<void>();

    RegenerateData$ = this.RegenerateData.asObservable();

    announceChange() {
        this.RegenerateData.next();
    }
*/


  createComment(commentDTO:Comment, id:number): Observable<HttpResponse<Comment>>{

    return this.http.post<Comment>(this.path+"/"+id+"/comments", commentDTO, {observe: 'response'})
  }


  uploadPicture(formData:FormData, offerId:number, commentId:number) : Observable<HttpResponse<Picture>>{
    return this.http.post<Picture>("http://localhost:8080/pictures/culturalOffers/"+offerId+"/comments/"+commentId, formData, {observe :'response'});
  }

  getComments(offerId : number, page : number) : Observable<HttpResponse<Page<Comment>>>{
    const params = new HttpParams().set('page',page.toString()).set("size", this.pageSize.toString()).set('sort', 'date,desc');
    return this.http.get<Page<Comment>>(this.path + "/" + offerId + "/comments/by-page",{params : params, observe : 'response'});
  }

  dataURItoBlob(dataURI:string) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
 }

}
