import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';

import { UserLogIn } from 'src/app/components/model/userLogIn';
import { LoginResponse } from 'src/app/components/model/loginResponse';
import { UserTokenState } from 'src/app/components/model/userTokenState';
import { UserSignUp } from 'src/app/components/model/userSignUp';
import { User } from 'src/app/components/model/user';
import { ChangeProfileService } from './change-profile.service';
import { UserChange } from 'src/app/components/model/userChange';


describe('changeProfileService', () => {
    let injector;
    let changeProfileService: ChangeProfileService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {

        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
           providers:    [ChangeProfileService ]
        });

        injector = getTestBed();
        changeProfileService = TestBed.inject(ChangeProfileService);
        httpClient = TestBed.inject(HttpClient);
        httpMock = TestBed.inject(HttpTestingController);
      });

      afterEach(() => {
        httpMock.verify();
      });


      it('change() should query url and change password' , fakeAsync(() => {

        let changeResponse : User;

        let userChange: UserChange = new UserChange();
        userChange.oldPassword = "user";
        userChange.newPassword= "nova"



        const mockChangeReponse: User = new User();
        mockChangeReponse.ativated = true;
        mockChangeReponse.password = "nova"
        

        changeProfileService.change(userChange).subscribe(res => changeResponse = res.body);

        const req = httpMock.expectOne('http://localhost:8080/users');
        expect(req.request.method).toBe('PUT');
        req.flush(mockChangeReponse);

        tick();

        expect(changeResponse).toBeDefined();
        expect(changeResponse.ativated).toEqual(true);
        expect(changeResponse.password).toEqual("nova");


      }));

      it('change() should return error when old password is wrong', fakeAsync(() => {

        let error: HttpHeaderResponse;

        let userChange: UserChange = new UserChange();
        userChange.oldPassword = "pogresnaLozinka";
        userChange.newPassword= "nova"

        changeProfileService.change(userChange).subscribe(null,res => error = res);

        const req = httpMock.expectOne('http://localhost:8080/users');
        expect(req.request.method).toBe('PUT');
        req.flush("Wrong password", {status:400, statusText: "BAD_REQUEST"});

        tick();

        expect(error.status).toBe(400);
        expect(error.statusText).toEqual("BAD_REQUEST");
       

      }));
      

});
