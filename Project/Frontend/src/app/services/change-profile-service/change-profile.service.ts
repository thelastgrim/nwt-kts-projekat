import { HttpClient,HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserChange } from 'src/app/components/model/userChange';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from 'src/app/components/model/user';

@Injectable({
  providedIn: 'root'
})
export class ChangeProfileService {

  private readonly path = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  change(_user: UserChange) : Observable<HttpResponse<User>>{   
 //   console.log("  USER in service: " + _user.oldPassword + " "+ _user.newPassword + " "+ _user.rePassword)
 //   console.log(localStorage.getItem('id_token'))
    return this.http.put<User>(this.path+"users",_user, {observe: 'response'}) 
    .pipe( 
      catchError( err => {
        console.log("SERVICE -----")
        console.log(err.error)
        console.log(err.error.message)
        console.log("---------------")
        if(err.error.message==="Wrong password"){
          alert("The old password you have entered is incorrect!")
        }else{
          alert("Sorry! Something went wrong :(")
        }
        
        return throwError(err);
      })
      );;
  }
}
