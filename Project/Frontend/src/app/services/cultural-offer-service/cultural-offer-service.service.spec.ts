import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { CulturalOfferServiceService } from './cultural-offer-service.service';
import { CulturalOffer} from 'src/app/components/model/cultural-offer';
import { CulturalOfferAdd } from 'src/app/components/model/cultural-offer-add';
import { Location } from 'src/app/components/model/location';
import { Picture } from 'src/app/components/model/picture';
import { Page } from 'src/app/components/model/page';
import { Pageable } from 'src/app/components/model/pageable';
import { Sort } from 'src/app/components/model/sort';
import { transformWithProjections } from 'ol/proj';
import { OffersForUser } from 'src/app/components/model/offersForUser';



describe('CulturalOfferService', () => {
    let injector;
    let culturalOfferService: CulturalOfferServiceService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {

        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
           providers:    [CulturalOfferServiceService]
        });

        injector = getTestBed();
        culturalOfferService = TestBed.inject(CulturalOfferServiceService);
        httpClient = TestBed.inject(HttpClient);
        httpMock = TestBed.inject(HttpTestingController);
      });

      afterEach(() => {
        httpMock.verify();
      });

      it('delete() should query url and delete cultural offer', fakeAsync(() => {

        culturalOfferService.delete(1).subscribe(res => { });

        const req = httpMock.expectOne('http://localhost:8080/offers/1');
        expect(req.request.method).toBe('DELETE');
        req.flush({});

      }));

      it('delete() should query url and return 404 if  cultural offer doesnt exist', fakeAsync(() => {

        let newError : HttpHeaderResponse

        culturalOfferService.delete(-11).subscribe(null, error => newError = error);

        const req = httpMock.expectOne('http://localhost:8080/offers/-11');
        expect(req.request.method).toBe('DELETE');
        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});


        expect(newError.status).toBe(400);
        expect(newError.statusText).toBe("BAD_REQUEST");

      }));

      it('subscribe() should query url and subscribe user', fakeAsync(() => {

        culturalOfferService.subscribe(1).subscribe(res => { });

        const req = httpMock.expectOne('http://localhost:8080/offers/1/subscribe');
        expect(req.request.method).toBe('POST');
        req.flush({});

      }));

      it('unsubscribe() should query url and unsubscribe user', fakeAsync(() => {

        culturalOfferService.unsubscribe(1).subscribe(res => { });

        const req = httpMock.expectOne('http://localhost:8080/offers/1/unsubscribe');
        expect(req.request.method).toBe('POST');
        req.flush({});

      }));

      it('saveCulturalOffer() should save cultural offer and display details', fakeAsync(()=>{

        let formData:FormData = new FormData();
        let response : CulturalOfferAdd;
        let mockResponse : CulturalOfferAdd = {
          id : 1,
          name : "TEST",
          about  :"TEST",
          lng : "50",
          lat :"51",
          country  : "Serbia",
          city : "Novi Sad",
          street : "Vojvode Bojovica 3",
          picture : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o",
          categoryName: "Muesums"
        }


        formData.append('name',"TEST");
        formData.append('about',"TEST");
        formData.append("lng", "50");
        formData.append('lat', "51");
        formData.append('country',"Serbia");
        formData.append('city',"Novi Sad");
        formData.append('street',"Vojvode Bojovica 3");
        formData.append('category',"Muesums");
        formData.append('picture', "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o");

        culturalOfferService.saveCulturalOffer(formData).subscribe(data=>{
          response=data.body;
        })
        const req = httpMock.expectOne('http://localhost:8080/offers');
        expect(req.request.method).toBe('POST');
        req.flush(mockResponse);

        tick();

        expect(response).toBeDefined;
        expect(response.id).toBeDefined;
        expect(response.name).toBe("TEST");
      }));

      it('saveCulturalOffer() should return 400 BAD_REQUEST', fakeAsync(()=>{

        let formData:FormData = new FormData();

        let error : HttpHeaderResponse;

        formData.append('name',"Gallery1");
        formData.append('about',"TEST");
        formData.append("lng", "50");
        formData.append('lat', "51");
        formData.append('country',"Serbia");
        formData.append('city',"Novi Sad");
        formData.append('street',"Vojvode Bojovica 3");
        formData.append('category',"Muesums");
        formData.append('picture', "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o");

        culturalOfferService.saveCulturalOffer(formData).subscribe(null, data=>{
          error = data;
        });

        let req = httpMock.expectOne('http://localhost:8080/offers');
        expect(req.request.method).toBe('POST');

        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

        tick();
       expect(error.status).toBe(400);

      }));

      it('getOneOffer() should get one offer and display it', fakeAsync(()=>{

        let culturalOffer : CulturalOffer;


        const mockOffer : CulturalOffer = {
          about : "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
          averageRating : 3,
          categoryName : "Museums",
          comments : [],
          id : 2,
          name : "McLaughlin-Nader",
          news : [],
          deleted : false,
          location : {
            lng: 10.7533605,
            lat: 59.9384597,
            deleted : false,
            country: "Norway",
            city: "Kristiansand S",
            street: "18 Maple Wood Crossing"},
          cover : {
            id: 1,
            path: "src//main//resources//pictures//museum.jpg",
            title: "Thrive"},

        };

        culturalOfferService.getOneOffer(2).subscribe(res => {
           culturalOffer = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/2');
        expect(req.request.method).toBe('GET');
        req.flush(mockOffer);

        expect(culturalOffer.name).toEqual("McLaughlin-Nader");
        expect(culturalOffer.id).toEqual(2);
        expect(culturalOffer.about).toEqual("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
        expect(culturalOffer.categoryName).toEqual("Museums");
        expect(culturalOffer.averageRating).toEqual(3);
        expect(culturalOffer.location.city).toEqual("Kristiansand S");
        expect(culturalOffer.location.country).toEqual("Norway");
        expect(culturalOffer.location.street).toEqual("18 Maple Wood Crossing");
      }));

      it('getOneOffer() should return 400 since offer with that id does not exist', fakeAsync(()=>{

        let error : HttpHeaderResponse;


        culturalOfferService.getOneOffer(-1).subscribe(res => {}, err => {
          error = err;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/-1');
        expect(req.request.method).toBe('GET');
        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

        expect(error.status).toBe(400);
      }));



      it('getAllUniqueCities() should get all unique cities and add them to filter options', fakeAsync(()=>{

         let cities : String[];

         let mockCities : String[] = [
          "Abaza",
          "Baiyushan",
          "Chalamarca",
          "Chambly",
          "Drezdenko",
          "Drien Rampak",
          "Granja",
          "Grootfontein",
          "Igpit",
          "Ikar",
          "Lewolang",
          "Monte Alto",
          "Xiangcheng",
          "Zykovo",
          "城郊"
         ];

         culturalOfferService.getAllUniqueCities().subscribe(res => {
           cities = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/cities?sort=city,asc');
        expect(req.request.method).toBe('GET');
        req.flush(mockCities);

        tick();

        expect(cities).toBeDefined();
        expect(cities.length).toEqual(15, 'should contain given amount of cities');
        expect(cities[0]).toEqual("Abaza");
        expect(cities[1]).toEqual("Baiyushan");
        expect(cities[2]).toEqual("Chalamarca");
        expect(cities[3]).toEqual("Chambly");
        expect(cities[4]).toEqual("Drezdenko");
        expect(cities[5]).toEqual("Drien Rampak");
        expect(cities[6]).toEqual("Granja");
        expect(cities[7]).toEqual("Grootfontein");
        expect(cities[8]).toEqual("Igpit");
        expect(cities[9]).toEqual("Ikar");
        expect(cities[10]).toEqual("Lewolang");
        expect(cities[11]).toEqual("Monte Alto");
        expect(cities[12]).toEqual("Xiangcheng");
        expect(cities[13]).toEqual("Zykovo");
        expect(cities[14]).toEqual( "城郊");

      }));

      it('updateOffer() should update cultural offer', fakeAsync(()=>{

        let culturalOffer : CulturalOffer;

        const updateOffer : CulturalOffer= {
          about : "New description",
          averageRating : 3,
          categoryName : "Museums",
          comments : [],
          id : 2,
          name : "New Name",
          news : [],
          deleted : false,
          location : {
            lng: 10.7533605,
            lat: 59.9384597,
            deleted : false,
            country: "Norway",
            city: "Kristiansand S",
            street: "18 Maple Wood Crossing"},
          cover : {
            id: 1,
            path: "src//main//resources//pictures//museum.jpg",
            title: "Thrive"},

        };

        const mockOffer : CulturalOffer= {
          about : "New description",
          averageRating : 3,
          categoryName : "Museums",
          comments : [],
          id : 2,
          name : "New Name",
          news : [],
          deleted : false,
          location : null,
          cover : null,
        };

        culturalOfferService.updateOffer(updateOffer).subscribe(res =>{
            culturalOffer = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/2');
        expect(req.request.method).toBe('PUT');
        req.flush(mockOffer);

        expect(culturalOffer.name).toEqual("New Name");
        expect(culturalOffer.about).toEqual("New description");
      }));

      it('updateOffer() should return 400 since offer with that id does not exist', fakeAsync(()=>{

        let error : HttpHeaderResponse;

        const updateOffer : CulturalOffer= {
          about : "New description",
          averageRating : 3,
          categoryName : "Museums",
          comments : [],
          id : -1,
          name : "Hoeger LLC",
          news : [],
          deleted : false,
          location : {
            lng: 10.7533605,
            lat: 59.9384597,
            deleted : false,
            country: "Norway",
            city: "Kristiansand S",
            street: "18 Maple Wood Crossing"},
          cover : {
            id: 1,
            path: "src//main//resources//pictures//museum.jpg",
            title: "Thrive"},

        };

        culturalOfferService.updateOffer(updateOffer).subscribe(res =>{}, err=>{
          error = err;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/-1');
        expect(req.request.method).toBe('PUT');
        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

        expect(error.status).toBe(400);
      }));

      it('updateOffer() should return 400 since offer with that name already exists', fakeAsync(()=>{

        let error : HttpHeaderResponse;

        const updateOffer : CulturalOffer= {
          about : "New description",
          averageRating : 3,
          categoryName : "Museums",
          comments : [],
          id : 2,
          name : "Hoeger LLC",
          news : [],
          deleted : false,
          location : {
            lng: 10.7533605,
            lat: 59.9384597,
            deleted : false,
            country: "Norway",
            city: "Kristiansand S",
            street: "18 Maple Wood Crossing"},
          cover : {
            id: 1,
            path: "src//main//resources//pictures//museum.jpg",
            title: "Thrive"},

        };

        culturalOfferService.updateOffer(updateOffer).subscribe(res =>{}, err=>{
          error = err;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/2');
        expect(req.request.method).toBe('PUT');
        req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});

        expect(error.status).toBe(400);
      }));

      it('search() should get all offers that contains inputed search value ', fakeAsync(()=>{
        let name : string = "Gallery";
        let page : number = 0;

        let response : Page<CulturalOffer> = new Page<CulturalOffer>();

        const mockSort : Sort = {
          sorted : true,
          unsorted : false,
        }

        const mockPageable : Pageable = {
          offset : 0,
          pageNumber :0,
          pageSize : 10,
          paged : true,
          sort : mockSort,
          unpaged : false
        }

        let mockOffers : Page<CulturalOffer> ={
         "content": [
          {
              id: 1,
              name : "Gallery1",
              about : "about1",
              averageRating : 4,
              deleted : false,
              comments: null,
              news :null,
              location : {
                lng : 25,
                lat : 25,
                deleted : false,
                country : "Srbija",
                city : "Beograd",
                street : "Karadjordjeva"
              },
            cover : {
                id : 1,
                title: "naslov",
                path : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o"
              },
              categoryName: "Gallery"
        },
          {
              id: 2,
              name : "Gallery2",
              about : "about2",
              averageRating : 4,
              deleted : false,
              comments: null,
              news :null,
              location : {
                lng : 25,
                lat : 25,
                deleted : false,
                country : "Srbija",
                city : "Novi Sad",
                street : "Karadjordjeva"
              },
              cover : {
                  id : 1,
                  title: "naslov",
                  path : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o"
                },
                categoryName: "Gallery"
         }],
         first :true,
         last : false,
         number:0,
         numberOfElements:10,
         pageable: mockPageable,
         size : 10,
         sort : mockSort,
         totalElements : 2,
         totalPages:2
        }

        culturalOfferService.search(name, page).subscribe(data=>{response.content = data.body.content});
        const req = httpMock.expectOne('http://localhost:8080/offers/search?page=0&size=6&sort=id,asc&name=Gallery');
        expect(req.request.method).toBe('GET');
        req.flush(mockOffers);

        tick();

        expect(response).toBeDefined;

        expect(response.content.length).toBe(2);

        expect(response.content[0].name).toBe("Gallery1")
        expect(response.content[0].averageRating).toBe(4)

        expect(response.content[1].name).toBe("Gallery2")
        expect(response.content[1].averageRating).toBe(4)
      }));

      // it('search() should 404 BAD_REQUEST return there is no offer name that contains that input asdfasdfa', fakeAsync(()=>{
      //   let name : string = "asdfasdfa";
      //   let page : number = 0;

      //   let error : HttpHeaderResponse= new HttpHeaderResponse();

      //   culturalOfferService.search(name, page).subscribe(null, data=>{
      //     error = data;
      //   });
      //   const req = httpMock.expectOne('http://localhost:8080/offers/search?page=0&size=6&sort=id,asc&name=asdfasdfa');
      //   expect(req.request.method).toBe('GET');
      //   req.flush("Something went wrong" , {status:400, statusText: "BAD_REQUEST"});
      //   tick();

      //   expect(error.status).toBe(400);

      // }));

      it('searchByCategory() should get all offers by category', fakeAsync(()=>{
        let categoryId : number = 4;

        let response : Page<CulturalOffer> = new Page<CulturalOffer>();

        const mockSort : Sort = {
          sorted : true,
          unsorted : false,
        }

        const mockPageable : Pageable = {
          offset : 0,
          pageNumber :0,
          pageSize : 10,
          paged : true,
          sort : mockSort,
          unpaged : false
        }

        let mockOffers : Page<CulturalOffer> ={
         "content": [
          {
              id: 1,
              name : "Gallery1",
              about : "about1",
              averageRating : 4,
              deleted : false,
              comments: null,
              news :null,
              location : {
                lng : 25,
                lat : 25,
                deleted : false,
                country : "Srbija",
                city : "Beograd",
                street : "Karadjordjeva"
              },
            cover : {
                id : 1,
                title: "naslov",
                path : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o"
              },
              categoryName: "Gallery"
        },
          {
              id: 2,
              name : "Gallery2",
              about : "about2",
              averageRating : 4,
              deleted : false,
              comments: null,
              news :null,
              location : {
                lng : 25,
                lat : 25,
                deleted : false,
                country : "Srbija",
                city : "Novi Sad",
                street : "Karadjordjeva"
              },
              cover : {
                  id : 1,
                  title: "naslov",
                  path : "aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o"
                },
                categoryName: "Gallery"
         }],
         first :true,
         last : false,
         number:0,
         numberOfElements:6,
         pageable: mockPageable,
         size : 6,
         sort : mockSort,
         totalElements : 2,
         totalPages:2
        }

        culturalOfferService.getPageOffersByCategory(categoryId,0).subscribe(data=>{response.content = data.body.content});
        const req = httpMock.expectOne('http://localhost:8080/offers/page-by-category/4?page=0&size=6&sort=id,asc');
        expect(req.request.method).toBe('GET');
        req.flush(mockOffers);

        tick();

        expect(response).toBeDefined;

        expect(response.content.length).toBe(2);

        expect(response.content[0].name).toBe("Gallery1")
        expect(response.content[0].averageRating).toBe(4)

        expect(response.content[1].name).toBe("Gallery2")
        expect(response.content[1].averageRating).toBe(4)
      }));

      it('getAllByPage() should get all offers by page', fakeAsync(() => {

        let offerPage : Page<CulturalOffer>;

        let mockPageOffer : Page<CulturalOffer> = {
          "content": [
            {
                "id": 1,
                "name": "Willms, Sanford and Toy",
                "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
                "averageRating": 4.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Penang",
                    "country": "Indonesia",
                    "street": "913 Sycamore Point",
                    "lng": 100.3091,
                    "lat": 5.4356367
                },
            },
            {
                "id": 2,
                "name": "McLaughlin-Nader",
                "about": "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
                "averageRating": 3.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Kristiansand S",
                    "country": "Norway",
                    "street": "18 Maple Wood Crossing",
                    "lng": 10.7533605,
                    "lat": 59.9384597
                },
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
            },
            "offset": 0,
            "pageSize": 2,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1014,
        "last": false,
        "totalPages": 507,
        "size": 2,
        "number": 0,
        "sort": {
            "sorted": false,
            "unsorted": true,
        },
        "numberOfElements": 2,
        "first": true,
        }

        culturalOfferService.getAllByPage(0).subscribe(res =>{
          offerPage = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/all-by-page?page=0&size=6');
        expect(req.request.method).toBe('GET');
        req.flush(mockPageOffer);

        tick();

        expect(offerPage).toBeDefined;

        expect(offerPage.content.length).toBe(2);

        expect(offerPage.content[0].name).toBe("Willms, Sanford and Toy");
        expect(offerPage.content[0].averageRating).toBe(4);
        expect(offerPage.content[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
        expect(offerPage.content[0].categoryName).toBe("Museums");
        expect(offerPage.content[0].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.content[0].location.city).toBe("Penang");
        expect(offerPage.content[0].location.country).toBe("Indonesia");
        expect(offerPage.content[0].location.street).toBe("913 Sycamore Point");

        expect(offerPage.content[1].name).toBe("McLaughlin-Nader");
        expect(offerPage.content[1].averageRating).toBe(3)
        expect(offerPage.content[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
        expect(offerPage.content[1].categoryName).toBe("Museums");
        expect(offerPage.content[1].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.content[1].location.city).toBe("Kristiansand S");
        expect(offerPage.content[1].location.country).toBe("Norway");
        expect(offerPage.content[1].location.street).toBe("18 Maple Wood Crossing");

      }));

      it('getPageOffersByCategoryAndUser() should get all offers by page ordered by user subscriptions', fakeAsync(() => {

        let offerPage : OffersForUser;
        let categoryId : number = 4;
        let mockPageOffer : OffersForUser = {
          offers: [
            {
                "id": 1,
                "name": "Willms, Sanford and Toy",
                "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
                "averageRating": 4.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Penang",
                    "country": "Indonesia",
                    "street": "913 Sycamore Point",
                    "lng": 100.3091,
                    "lat": 5.4356367
                },
            },
            {
                "id": 2,
                "name": "McLaughlin-Nader",
                "about": "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
                "averageRating": 3.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Kristiansand S",
                    "country": "Norway",
                    "street": "18 Maple Wood Crossing",
                    "lng": 10.7533605,
                    "lat": 59.9384597
                },
            }
        ],
        collectionSize : 2
        }

        culturalOfferService.getPageOffersByCategoryAndUser(categoryId, 0).subscribe(res =>{
          
          offerPage = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/page-by-category-and-user/4?page=0&size=6');
        expect(req.request.method).toBe('GET');
        req.flush(mockPageOffer);

        tick();

        expect(offerPage).toBeDefined;

        expect(offerPage.collectionSize).toEqual(2);

        expect(offerPage.offers[0].name).toBe("Willms, Sanford and Toy");
        expect(offerPage.offers[0].averageRating).toBe(4);
        expect(offerPage.offers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
        expect(offerPage.offers[0].categoryName).toBe("Museums");
        expect(offerPage.offers[0].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.offers[0].location.city).toBe("Penang");
        expect(offerPage.offers[0].location.country).toBe("Indonesia");
        expect(offerPage.offers[0].location.street).toBe("913 Sycamore Point");
        expect(offerPage.offers[1].name).toBe("McLaughlin-Nader");
        expect(offerPage.offers[1].averageRating).toBe(3)
        expect(offerPage.offers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
        expect(offerPage.offers[1].categoryName).toBe("Museums");
        expect(offerPage.offers[1].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.offers[1].location.city).toBe("Kristiansand S");
        expect(offerPage.offers[1].location.country).toBe("Norway");
        expect(offerPage.offers[1].location.street).toBe("18 Maple Wood Crossing");

      }));

      it('getPageOffersByCity() should get all offers in the city by page', fakeAsync(() => {

        let offerPage : Page<CulturalOffer>;

        let mockPageOffer : Page<CulturalOffer> = {
          "content": [
            {
                "id": 1,
                "name": "Willms, Sanford and Toy",
                "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
                "averageRating": 4.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Penang",
                    "country": "Indonesia",
                    "street": "913 Sycamore Point",
                    "lng": 100.3091,
                    "lat": 5.4356367
                },
            },
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
            },
            "offset": 0,
            "pageSize": 2,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1014,
        "last": false,
        "totalPages": 507,
        "size": 2,
        "number": 0,
        "sort": {
            "sorted": false,
            "unsorted": true,
        },
        "numberOfElements": 2,
        "first": true,
        }

        culturalOfferService.getPageOffersByCity('Penang', 0).subscribe(res =>{
          offerPage = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/page-by-city/Penang?page=0&size=6');
        expect(req.request.method).toBe('GET');
        req.flush(mockPageOffer);

        tick();

        expect(offerPage).toBeDefined;

        expect(offerPage.content.length).toBe(1);

        expect(offerPage.content[0].name).toBe("Willms, Sanford and Toy");
        expect(offerPage.content[0].averageRating).toBe(4);
        expect(offerPage.content[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
        expect(offerPage.content[0].categoryName).toBe("Museums");
        expect(offerPage.content[0].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.content[0].location.city).toBe("Penang");
        expect(offerPage.content[0].location.country).toBe("Indonesia");
        expect(offerPage.content[0].location.street).toBe("913 Sycamore Point");

      }));

      it('getPageOfferByCityAndCat() should get all offers in the city under given category by page', fakeAsync(() => {

        let offerPage : Page<CulturalOffer>;

        let mockPageOffer : Page<CulturalOffer> = {
          "content": [
            {
                "id": 1,
                "name": "Willms, Sanford and Toy",
                "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
                "averageRating": 4.0,
                "categoryName": "Museums",
                "deleted": false,
                "comments": [],
                "news": [],
                "cover": {
                    "id": 1,
                    "title": "Thrive",
                    "path": "src//main//resources//pictures//museum.jpg",
                },
                "location": {
                    "city": "Penang",
                    "country": "Indonesia",
                    "street": "913 Sycamore Point",
                    "lng": 100.3091,
                    "lat": 5.4356367
                },
            },
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
            },
            "offset": 0,
            "pageSize": 2,
            "pageNumber": 0,
            "unpaged": false,
            "paged": true
        },
        "totalElements": 1014,
        "last": false,
        "totalPages": 507,
        "size": 2,
        "number": 0,
        "sort": {
            "sorted": false,
            "unsorted": true,
        },
        "numberOfElements": 2,
        "first": true,
        }

        culturalOfferService.getPageOfferByCityAndCat('Penang', 1, 0).subscribe(res =>{
          offerPage = res.body;
        });

        const req = httpMock.expectOne('http://localhost:8080/offers/by-page/categories/1/location/Penang?page=0&size=6');
        expect(req.request.method).toBe('GET');
        req.flush(mockPageOffer);

        tick();

        expect(offerPage).toBeDefined;

        expect(offerPage.content.length).toBe(1);

        expect(offerPage.content[0].name).toBe("Willms, Sanford and Toy");
        expect(offerPage.content[0].averageRating).toBe(4);
        expect(offerPage.content[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
        expect(offerPage.content[0].categoryName).toBe("Museums");
        expect(offerPage.content[0].cover.path).toBe("src//main//resources//pictures//museum.jpg");
        expect(offerPage.content[0].location.city).toBe("Penang");
        expect(offerPage.content[0].location.country).toBe("Indonesia");
        expect(offerPage.content[0].location.street).toBe("913 Sycamore Point");

      }));



});
