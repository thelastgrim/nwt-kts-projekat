import { ErrorHandler, Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { CulturalOffer } from 'src/app/components/model/cultural-offer';
import { Observable, throwError } from 'rxjs';
import {catchError, retry } from 'rxjs/operators';
import { Constants } from 'src/app/app.constants';
import { CulturalOfferAdd } from 'src/app/components/model/cultural-offer-add';
import { Page } from 'src/app/components/model/page';
import { OffersForUser } from 'src/app/components/model/offersForUser';


@Injectable()
export class CulturalOfferServiceService {



  private readonly path = 'http://localhost:8080/offers';

  pageSize :number = new Constants().PAGE_SIZE;

  constructor(private http: HttpClient) { }

  ngOnInit(){

  }
  delete(id: number) : Observable<HttpResponse<any>>{
    console.log("brise se")
    return this.http.delete<any>(this.path+"/"+id)
    /* kad ima ovo ne radi negativan test za delete, error bude undefined
    .pipe(catchError (err =>{
      console.log(err);
      return throwError("NOOO")
    }))
    */
  }

  subscribe(id: number) : Observable<HttpResponse<null>>{
    return this.http.post<null>(this.path+"/"+id+"/subscribe", null);
  }
  unsubscribe(id: number) : Observable<HttpResponse<any>>{
    return this.http.post<any>(this.path+"/"+id+"/unsubscribe", null);
  }

  getAll(id : number):Observable<CulturalOffer[]>{
    if (id === undefined){id = 0;}
    return this.http.get<CulturalOffer[]>(this.path + "/categories" + `/${id}` );
  }

  search(name : string, page : number) :Observable<HttpResponse<Page<CulturalOffer>>>{
    const params = new HttpParams().set('page',page.toString()).set("size", this.pageSize.toString()).set('sort', 'id,asc').set("name", name);
    return this.http.get<Page<CulturalOffer>>(this.path + "/search", {params:params, observe: 'response'})
    /*
    .pipe(
      catchError( err => {
        console.log("Error" + err)
        console.log("OVBDE")
        return throwError(this.errorHandler(err));
      })
      );*/
   }

   saveCulturalOffer(formData : FormData): Observable<HttpResponse<CulturalOfferAdd>>{
      return this.http.post<CulturalOfferAdd>(this.path, formData, {observe:'response'});
   }

   errorHandler(err:any){
   alert("Cultural offer with that name or adress already exists! Check input!");
   }

  getAllUniqueCities() : Observable<HttpResponse<String[]>>{
    const params = new HttpParams().set('sort', 'city,asc');
    return this.http.get<String[]>(`${this.path}/cities`,  {params : params, observe:'response'});
  }

  getAllByPage(page : number = 0) : Observable<HttpResponse<Page<CulturalOffer>>>{
    const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString());
    return this.http.get<Page<CulturalOffer>>(this.path + "/all-by-page", {params : params, observe : 'response'});
  }

  getPageOffersByCity(cityName : string, page : number) : Observable<HttpResponse<Page<CulturalOffer>>>{
    const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString());
    return this.http.get<Page<CulturalOffer>>(this.path + "/page-by-city/" + cityName, {params : params, observe : 'response'});
  }

  getPageOffersByCategory(categoryId : number, page:number=0) : Observable<HttpResponse<Page<CulturalOffer>>>{
   //const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString());
    const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString()).set('sort', 'id,asc');
    console.log("QUERY SA ID== " + categoryId)
    return this.http.get<Page<CulturalOffer>>(this.path + "/page-by-category/" + categoryId, {params:params, observe: 'response'});
  }

  getPageOfferByCityAndCat(cityName : string, categoryId : number, page : number) : Observable<HttpResponse<Page<CulturalOffer>>>{
    const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString());
    return this.http.get<Page<CulturalOffer>>(this.path + "/by-page/categories/" + categoryId + "/location/" + cityName , {params : params, observe: 'response'});
  }

  getPageOffersByCategoryAndUser(categoryId : number, page:number=0):Observable<HttpResponse<OffersForUser>>{
    const params = new HttpParams().set('page',page.toString()).set("size",this.pageSize.toString());

    return this.http.get<OffersForUser>(this.path+ "/page-by-category-and-user/" + categoryId, {params:params, observe: 'response'});

  }

  getOneOffer(offerId : number) :  Observable<HttpResponse<CulturalOffer>>{
    return this.http.get<CulturalOffer>(`${this.path}/${offerId}`, {observe : 'response'});
  }

  updateOffer(culturalOffer : CulturalOffer) : Observable<HttpResponse<CulturalOffer>>{
    return this.http.put<CulturalOffer>(`${this.path}/${culturalOffer.id}`, culturalOffer, {observe : 'response'});
  }

}
