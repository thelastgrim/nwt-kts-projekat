import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {fakeAsync, tick} from '@angular/core/testing';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';

import { UserLogIn } from 'src/app/components/model/userLogIn';
import { LoginResponse } from 'src/app/components/model/loginResponse';
import { UserTokenState } from 'src/app/components/model/userTokenState';
import { RegisterService } from './register.service';
import { UserSignUp } from 'src/app/components/model/userSignUp';
import { User } from 'src/app/components/model/user';


describe('registerService', () => {
    let injector;
    let registerService: RegisterService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {

        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule],
           providers:    [RegisterService ]
        });

        injector = getTestBed();
        registerService = TestBed.inject(RegisterService);
        httpClient = TestBed.inject(HttpClient);
        httpMock = TestBed.inject(HttpTestingController);
      });

      afterEach(() => {
        httpMock.verify();
      });


      it('register() should query url and register user', fakeAsync(() => {

        let registerResponse : User;

        let userRegister: UserSignUp = new UserSignUp();
        userRegister.email = "ana.jordanovic@hotmail.com"
        userRegister.password = "lozinka123"
        userRegister.rePassword = "lozinka123"
        userRegister.username = "ana"



        const mockRegisterReponse: User =
        {
            ativated : false, //jer nije posetio link jos uvek
            username : "ana",
            email :"ana.jordanovic@hotmail.com",
            password: "lozinka123",
            commentsId: [],
            culturalOffersId: []

        };

        registerService.register(userRegister).subscribe(res => registerResponse = res.body);

        const req = httpMock.expectOne('http://localhost:8080/auth/sign-up');
        expect(req.request.method).toBe('POST');
        req.flush(mockRegisterReponse);

        tick();

        expect(registerResponse).toBeDefined();
        expect(registerResponse.email).toEqual("ana.jordanovic@hotmail.com");
        expect(registerResponse.ativated).toEqual(false);
        expect(registerResponse.commentsId).toEqual([]);
        expect(registerResponse.culturalOffersId).toEqual([]);
        expect(registerResponse.password).toEqual("lozinka123");
        expect(registerResponse.username).toEqual("ana");


      }));

      it('register() should return error when username or email already exist', fakeAsync(() => {

        let error: HttpHeaderResponse;

        let userRegister: UserSignUp = new UserSignUp();
        userRegister.email = "ana.jordanovic@hotmail.com"
        userRegister.password = "lozinka123"
        userRegister.rePassword = "lozinka123"
        userRegister.username = "ana"

        registerService.register(userRegister).subscribe(null,res => error = res);

        const req = httpMock.expectOne('http://localhost:8080/auth/sign-up');
        expect(req.request.method).toBe('POST');
        req.flush("Username or email already exists", {status:400, statusText: "BAD_REQUEST"});

        tick();

        expect(error.status).toBe(400);
        expect(error.statusText).toEqual("BAD_REQUEST");


      }));
      

});
