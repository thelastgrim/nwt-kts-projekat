import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { UserSignUp } from 'src/app/components/model/userSignUp';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from 'src/app/components/model/user';




@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private readonly path = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  register(_user: UserSignUp) : Observable<HttpResponse<User>>{
    console.log("  USER: " + _user.email + " "+ _user.password + " "+ _user.username)

    return this.http.post<User>(this.path+"auth/sign-up",_user, {observe: 'response'})
    .pipe(
      catchError( err => {
        console.log(err.error)
        console.log(err.error.message)
        if(err.error.message==="Username or email already exists"){
          alert("Username or email already exists. Try again")
        }else{
          alert("Wrong credentials :(")
        }
        return throwError(err);
      })
      );;
  }
}
