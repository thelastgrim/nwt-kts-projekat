import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ContentComponent } from './components/content/content.component';
import { CulturalOfferAboutComponent } from './components/cultural-offer/cultural-offer-about/cultural-offer-about.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { RegisterComponent } from './components/register/register.component';
import { ChangeProfileComponent } from './components/change-profile/change-profile.component';
import { CulturalOfferFilterComponent } from './components/cultural-offer/cultural-offer-filter/cultural-offer-filter.component';



const routes: Routes = [
  { path: '',   redirectTo: 'content', pathMatch: 'full' },
  { path: 'content', component: ContentComponent},
  { path: 'log-in', component:LogInComponent},
  { path: 'map', component: CulturalOfferFilterComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'changeProfile', component: ChangeProfileComponent },
  { path: 'offer/:offerId', component: CulturalOfferAboutComponent},
  { path: '**', redirectTo: 'content'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
