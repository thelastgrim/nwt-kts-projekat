import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { CommentComponent } from './components/comment/comment.component';
import { NewsComponent } from './components/news/news.component';
import { CommentsFormComponent } from './components/comments-form/comments-form.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CulturalOfferServiceService } from './services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOfferListComponent } from './components/cultural-offer/cultural-offer-list/cultural-offer-list.component';
import { CategoryServiceService } from './services/category-service/category-service.service';
import {CulturalOfferSearchComponent} from './components/cultural-offer/cultural-offer-search/cultural-offer-search.component';
import { CulturalOfferAddComponent } from './components/cultural-offer/cultural-offer-add/cultural-offer-add.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule} from '@angular/forms';
import {CategoryComponent} from './components/category/category.component';
import { FilterPipe } from './pipes/filter/filter.pipe';
import { HighlightDirective } from './directives/highlight/highlight.directive';
import { CulturalOfferFilterComponent } from './components/cultural-offer/cultural-offer-filter/cultural-offer-filter.component';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import { MapComponent } from './components/map/map.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { InterceptorService } from './services/auth/interceptor.service';
import { AuthService } from './services/auth/auth.service';
import { CulturalOfferAboutComponent } from './components/cultural-offer/cultural-offer-about/cultural-offer-about.component';
import { RegisterComponent } from './components/register/register.component';
import { ChangeProfileComponent } from './components/change-profile/change-profile.component';
import { NewsAddComponent } from './components/news-add/news-add.component';
import { UnsubscribeComponent } from './components/unsubscribe/unsubscribe.component';
import { CulturalOfferCoverComponent } from './components/cultural-offer/cultural-offer-cover/cultural-offer-cover.component';
import { CulturalOfferUpdateComponent } from './components/cultural-offer/cultural-offer-update/cultural-offer-update/cultural-offer-update.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CulturalOfferDeleteComponent } from './components/cultural-offer/cultural-offer-delete/cultural-offer-delete/cultural-offer-delete.component';
import { CategoryDeleteComponent } from './components/category/category-delete/category-delete/category-delete.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    CommentComponent,
    NewsComponent,
    CommentsFormComponent,
    CulturalOfferListComponent,
    CulturalOfferSearchComponent,
    CulturalOfferAddComponent,
    CategoryComponent,
    HighlightDirective,
    FilterPipe,
    CulturalOfferFilterComponent,
    CulturalOfferAboutComponent,
    MapComponent,
    LogInComponent,
    RegisterComponent,
    ChangeProfileComponent,
    NewsAddComponent,
    UnsubscribeComponent,
    CulturalOfferCoverComponent,
    CulturalOfferUpdateComponent,
    CulturalOfferDeleteComponent,
    CategoryDeleteComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    ReactiveFormsModule,
    NgbTypeaheadModule,
    NgbRatingModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:InterceptorService,
      multi:true
    },
    CulturalOfferServiceService, CategoryServiceService, AuthService],

  bootstrap: [AppComponent]
})
export class AppModule { }
