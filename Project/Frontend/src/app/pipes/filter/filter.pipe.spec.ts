/* tslint:disable:no-unused-variable */

import { analyzeAndValidateNgModules } from '@angular/compiler';
import { TestBed, async } from '@angular/core/testing';
import { FilterPipe } from './filter.pipe';

describe('Pipe: Filtere', () => {
    // This pipe is a pure, stateless function so no need for BeforeEach
    const pipe = new FilterPipe();

  it('create an instance', () => {
    let pipe = new FilterPipe();
    expect(pipe).toBeTruthy();
  });

  it('["Museum","Gallery", null] tranforms to ["Museum","Gallery"]', () => {

    expect(pipe.transform(['Museums','Gallery'],null)).toEqual(['Museums','Gallery']);

  });

  it('["Museum", null] tranforms to ["Museum"]', () => {

    expect(pipe.transform(['Museums'],null)).toEqual(['Museums']);

  });

  it('[[],TEXT] tranforms to []', () => {

    expect(pipe.transform([],"TEXT")).toEqual([]);

  });

  it('[[],TEXT] tranforms to []', () => {

    expect(pipe.transform([],"TEXT")).toEqual([]);

  });

  it('[[],TEXT] tranforms to []', () => {

    expect(pipe.transform([],"TEXT")).toEqual([]);

  });

  it('[[],TEXT] tranforms to []', () => {

    expect(pipe.transform([],"TEXT")).toEqual([]);

  });

  it('[[],null] tranforms to []', () => {

    expect(pipe.transform([],null)).toEqual([]);

  });

  it('[["Museums"], TEXT] tranforms to [["museums"], text]', () => {
    let text : string = "TEXT";
    expect(pipe.transform(["Museums"],text)).toEqual([]);
  });

  it('[["Museums"], TEXT] tranforms to [["museums"], text] check individual', () => {
    let text : string = "TEXT";
    let items : any[];
    items = pipe.transform(["Museums"],text);
    items.filter(it => {
        expect(it[0]).toEqual("museums");

        expect(it[1]).toEqual("text");
    });
  });





});
