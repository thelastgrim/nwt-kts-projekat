import { Picture } from "./picture";

export class Comment {
  id? : number;
  date : Date;
  text : string;
  rating : number;
  username : string;
  pictures? : Picture[];

   constructor(obj?: any){
     this.id = obj && obj.date || null;
     this.date = obj && obj.date || null;
     this.text = obj && obj.text || null;
     this.rating = obj && obj.rating || null;
     this.username = obj && obj.username || null;
     this.pictures = obj && obj.picutres || null;

   }
 }
