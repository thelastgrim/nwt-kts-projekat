
export class NewsAdd {
    title : string;
    text : string;
    dateAdded : Date;
    culturalOfferId: number;

     constructor(obj?: any){
       this.title = obj && obj.title || null;
       this.text = obj && obj.text || null;
       this.dateAdded = obj && obj.dateAdded || null;
       this.culturalOfferId = obj && obj.culturalOfferId || null;

     }
   }
