import {CulturalOffer} from "./cultural-offer";

export class Category{
  id : number;
  name : string;
  deleted : boolean;
  culturalOffers : CulturalOffer[];

	constructor(obj?: any){
    this.id = obj && obj.name || -1;
		this.name = obj && obj.name || null;
    this.deleted = obj && obj.deleted || true;
    this.culturalOffers = obj && obj.culturalOffers || null;
  }
}
