export class Picture {
  id : number
  title: string
  path : string;


  constructor(obj?: any){
    this.path = obj && obj.path || null;
    this.id = obj && obj.id || null;
    this.title = obj && obj.title || null;
	}
}
