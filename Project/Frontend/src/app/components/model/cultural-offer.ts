import {News} from "./news";
import { Picture } from "./picture";
import {Comment} from "./comment";
import {Location} from "./location";

export class CulturalOffer {
 id: number;
 name : string;
 about : string;
 averageRating : number;
 deleted : boolean;
 comments: Comment[];
 news : News[];
 location : Location;
 cover : Picture;
 categoryName: string;

	constructor(obj?: any){
    this.id = obj && obj.name || null;
		this.name = obj && obj.name || null;
		this.about = obj && obj.about || null;
    this.deleted = obj && obj.deleted || true;
    this.averageRating = obj && obj.averageRating || 0;
    this.comments = obj && obj.comments || null;
    this.news = obj && obj.news || null;
    this.location = obj && obj.location || null;
    this.cover = obj && obj.pictures || null;
    this.categoryName = obj && obj.pictures || null;
  }
  // constructor(name : string,
  //   about : string,
  //   averageRating : number,
  //   deleted : boolean,
  //   comments: Comment[],
  //   news : News[],
  //   location : Location,
  //   pictures : Picture[]){
	// 	this.name = name || "";
	// 	this.about = about || "";
  //   this.deleted = deleted || false;
  //   this.averageRating =averageRating || 0;
  //   this.comments = comments || [];
  //   this.news = news || [];
  //   this.location = location || new Location(0,0,false,"","","");
  //   this.pictures = pictures || [];
	// }
}
