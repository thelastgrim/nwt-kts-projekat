
export class News {
  title : string;
  text : string;
  deleted? : boolean;
  dateAdded : Date;

   constructor(obj?: any){
     this.dateAdded = obj && obj.date || null;
     this.title = obj && obj.title || null;
     this.text = obj && obj.text || null;
     this.deleted = obj && obj.deleted || true;
   }
 }
