export class Location {
  lng : number;
  lat : number;
  deleted? : boolean;
  country : string;
  city : string;
  street : string;

  constructor(lng: number, lat : number,
    deleted : boolean,
    country : string,
    city : string,
    street : string ){
		this.lng = lng || 0;
		this.lat = lat || 0;
    this.deleted =deleted || false;
    this.country =country || "Serbia";
    this.city = city || "Novi Sad";
    this.street = street || "Vojvode Bojovica";
	}
}
