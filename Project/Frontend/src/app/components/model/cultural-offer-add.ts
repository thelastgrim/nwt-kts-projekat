import {News} from "./news";
import { Picture } from "./picture";
import {Comment} from "./comment";
import {Location} from "./location";

export class CulturalOfferAdd {
 id: number;
 name : string;
 about : string;
 lng : string;
 lat : string;
 country : string;
 city : string;
 street : string;
 picture : string;
 categoryName: string;

	constructor(obj?: any){
    this.id = obj && obj.name || null;
		this.name = obj && obj.name || null;
		this.about = obj && obj.about || null;
    this.categoryName = obj && obj.pictures || null;
  }
  // constructor(name : string,
  //   about : string,
  //   averageRating : number,
  //   deleted : boolean,
  //   comments: Comment[],
  //   news : News[],
  //   location : Location,
  //   pictures : Picture[]){
	// 	this.name = name || "";
	// 	this.about = about || "";
  //   this.deleted = deleted || false;
  //   this.averageRating =averageRating || 0;
  //   this.comments = comments || [];
  //   this.news = news || [];
  //   this.location = location || new Location(0,0,false,"","","");
  //   this.pictures = pictures || [];
	// }
}
