/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CategoryDeleteComponent } from './category-delete.component';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { Category } from 'src/app/components/model/category';

describe('CategoryDeleteComponent', () => {
  let component: CategoryDeleteComponent;
  let fixture: ComponentFixture<CategoryDeleteComponent>;

  let categoryService : any;

  beforeEach(() => {

    spyOn(window, 'confirm').and.callFake(function () {
      return true;
  });

  let categoryServiceMock = {
    deleteCategory: jasmine.createSpy('deleteCategory')
        .and.returnValue(of()),
  }

  TestBed.configureTestingModule({
    declarations: [ CategoryDeleteComponent ],
    imports: [FormsModule],
    providers:    [ {provide:CategoryServiceService, useValue: categoryServiceMock }]
 });

    fixture = TestBed.createComponent(CategoryDeleteComponent);
    component = fixture.componentInstance;
    categoryService = TestBed.inject(CategoryServiceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should prompt admin before deleted an category ', fakeAsync(() => {
    let category = new Category();
    category.name = "Museums";
    category.id = 1;

    component.category = category;
    component.deleteCategory();
    expect(window.confirm).toHaveBeenCalledWith('Are you sure you want delete category Museums? note : All offers under this category will be deleted if you confirm deletion. To cancel deletion press button Cancel.');
}));

it('should delete selected category', fakeAsync(() => {
  let category = new Category();
  category.name = "Museums";
  category.id = 1;

  component.category = category;
  component.deleteCategory();

  expect(categoryService.deleteCategory).toHaveBeenCalled()

}));

});
