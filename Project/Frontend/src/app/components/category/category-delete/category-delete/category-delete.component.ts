import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/app/components/model/category';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';

@Component({
  selector: 'app-category-delete',
  templateUrl: './category-delete.component.html',
  styleUrls: ['./category-delete.component.scss']
})
export class CategoryDeleteComponent implements OnInit {



  @Input() category: Category;

  @Output() refreshEvent = new EventEmitter<boolean>();

  loading : Boolean = false;
  constructor(private categoryService : CategoryServiceService) { }

  ngOnInit() {
  }

  deleteCategory(){
    if(confirm("Are you sure you want delete category "+this.category.name+"? note : All offers under this category will be deleted if you confirm deletion. To cancel deletion press button Cancel.")) {
      this.loading = true;
      this.categoryService.deleteCategory(this.category.id).subscribe(()=>{
        this.loading = false;
        this.refreshEvent.emit(true);
      });
    }
  }

}
