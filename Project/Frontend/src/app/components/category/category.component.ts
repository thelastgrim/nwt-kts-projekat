import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { Category } from '../model/category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  userRole = localStorage.getItem('role')||'none';

  @Input ()
  categories! : Category[];

  @Output() newCategoryEvent = new EventEmitter<Category>();

  category : Category = new Category();

  authorization : any;
  closeResult = '';
  formImport!: FormGroup;
  forbiddenName : boolean = false;

  constructor(private modalService : NgbModal, private categoryService: CategoryServiceService) {
    this.formImport = new FormGroup({
      importFile: new FormControl('', Validators.required)
    });

  }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe( data =>{
      this.categories = data.body;
    });
  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})/*.result.then((result) => {
      console.log(result)
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    */
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  checkInputName(){
    let sameInput : String;
    this.categories.forEach(currCategory => {
     if(currCategory.name.toUpperCase() === this.category.name.toUpperCase()){
       sameInput = currCategory.name.toUpperCase();
     this.forbiddenName = true;
     }else if(this.category.name.toUpperCase() != sameInput){
       this.forbiddenName = false;
     }
    });
  }

  save(){
    this.categoryService.saveCategory(this.category).subscribe(
      (data) =>{
        console.log("before;");
        this.newCategoryEvent.emit(this.category);
        console.log("after;");
      },(error : HttpErrorResponse)=>{
        alert("ne valja");
      });
      this.category.name = null;
  }

}
