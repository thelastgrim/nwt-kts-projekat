import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement, TemplateRef } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';

import { CategoryComponent } from './category.component';
import { NgbModal,  NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Category } from '../model/category';

describe('CategoryComponent', () => {
  let component: CategoryComponent;
  let fixture: ComponentFixture<CategoryComponent>;

  let categoryService : any;
  let modalService : any;


  beforeEach(() => {
    let categoryServiceMock =
     {
      getAllCategories : jasmine.createSpy('getAllStudents')
        .and.returnValue(of({body:[{}, {}, {}, {}, {}]})),
      save : jasmine.createSpy('save')
        .and.returnValue(of()),
      checkInputName : jasmine.createSpy('checkInputName')
        .and.returnValue(of()),
      saveCategory : jasmine.createSpy('createCategory')
        .and.returnValue(of({body : new Category ({
          name: "newCategory1",
          id: 1,
          deleted : false,
        culturalOffers : []
        })}))
    }

    let modalServiceMock  = {
      open: jasmine.createSpy('open')
        .and.returnValue(of()),
      dismissAll : jasmine.createSpy('dismissAll')
        .and.returnValue(of())
    }

    TestBed.configureTestingModule({
       declarations: [ CategoryComponent ],
       imports: [FormsModule],
       providers:    [ {provide: CategoryServiceService, useValue: categoryServiceMock },
        { provide : NgbModal, useValue : modalServiceMock}
       ]
    });

    fixture = TestBed.createComponent(CategoryComponent);
    component = fixture.componentInstance;
    categoryService = TestBed.inject(CategoryServiceService);
    modalService = TestBed.inject(NgbModal)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal dialog', fakeAsync(() => {
    let contet = TemplateRef
    component.open(contet);
    expect(modalService.open).toHaveBeenCalled();
  }));

  it('should fetch the categories list on init', fakeAsync(() => {
    component.ngOnInit();

    expect(categoryService.getAllCategories).toHaveBeenCalled();
    fixture.whenStable()
      .then(()=>{
        expect(component.categories.length).toBe(5);
      })
  }));

  it('should check input name for new category with different name', fakeAsync(() => {
    let category1 = new Category();
      category1.name = "category1";
      category1.id = 1;
      category1.deleted = false;
      category1.culturalOffers = [];

    let category2 = new Category();
      category2.name = "category2";
      category2.id = 1;
      category2.deleted = false;
      category2.culturalOffers = [];

    component.category.name = "newCategory";
    component.category.id = 1
    component.category.deleted = false;
    component.category.culturalOffers = [];

     component.forbiddenName = false;

     component.categories = [category1, category2];
     component.checkInputName()

    expect(component.forbiddenName).toBeFalse();
  }));

  it('should check input name for new category with same name', fakeAsync(() => {
    let category1 = new Category();
      category1.name = "category1";
      category1.id = 1;
      category1.deleted = false;
      category1.culturalOffers = [];

    let category2 = new Category();
      category2.name = "category2";
      category2.id = 1;
      category2.deleted = false;
      category2.culturalOffers = [];

    component.category.name = "category1";
    component.category.id = 1
    component.category.deleted = false;
    component.category.culturalOffers = [];

     component.forbiddenName = false;

     component.categories = [category1, category2];
     component.checkInputName()

    expect(component.forbiddenName).toBeTrue();
  }));

  it('should check save category', fakeAsync(() => {
    spyOn(component.newCategoryEvent, 'emit');
    let category1 = new Category();
      category1.name = "category1";
      category1.id = 1;
      category1.deleted = false;
      category1.culturalOffers = [];

    let category2 = new Category();
      category2.name = "category2";
      category2.id = 1;
      category2.deleted = false;
      category2.culturalOffers = [];

    component.category.name = "newCategory1";
    component.category.id = 1
    component.category.deleted = false;
    component.category.culturalOffers = [];

     component.forbiddenName = false;
     component.categories = [category1, category2];
     component.save()

     expect(categoryService.saveCategory).toHaveBeenCalledWith(component.category)
     expect(component.newCategoryEvent.emit).toHaveBeenCalled()
  }));
});
