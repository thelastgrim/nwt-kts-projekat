import { Element } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Target from 'ol/events/Target';
import { RegisterService } from 'src/app/services/register-service/register.service';
import { UserSignUp } from '../model/userSignUp';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private registerService:RegisterService, private router:Router) { }
  user: UserSignUp = new UserSignUp;
  isShown: boolean = false


  ngOnInit(): void {
  }

  signUp(){
    if(this.user.password.length >= 8 && this.user.password.length <= 16 ){

    
      if(this.user.password === this.user.rePassword){
        if(this.user.username ==null ||this.user.username =="" || this.user.email ==null ||this.user.email =="" || this.user.password ==null ||this.user.password =="" || this.user.rePassword ==null ||this.user.rePassword ==""){
          this.isShown = true;
        }else{
          this.registerService.register(this.user).subscribe((data) =>{
            alert("Please click on the link we sent to your email to activate your account!")
            this.router.navigate(['app-log-in'])
          });
        }

      }else{
        alert("The passwords does not match! Please try again!")
      }

  }else{
    alert("Your password must be between 8 and 16 characters")
  }
  }

  onChange(value: string): void {
    if(value == ""){
      this.isShown = true;
    }else{
      this.isShown = false
    }
  }

}
