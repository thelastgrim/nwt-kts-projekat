import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { of } from 'rxjs';
import { RegisterComponent } from './register.component';
import { RegisterService } from 'src/app/services/register-service/register.service';
import { FormsModule } from '@angular/forms';
import { UserSignUp } from '../model/userSignUp';

describe('registersComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let registerService: any;
  let router: any;

  beforeEach(() => {
   spyOn(window, 'alert');
    let registerServiceMock = {
        register: jasmine.createSpy('register')
          .and.returnValue(of({body: {}  }))
    //    onChange : jasmine.createSpy('onChange').and.returnValue(of())     
    };

    let routerMock = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
       declarations: [ RegisterComponent ],
       imports: [FormsModule],
       providers:    [ {provide: RegisterService, useValue: registerServiceMock },
                       { provide: Router, useValue: routerMock } ]
    });

    fixture = TestBed.createComponent(RegisterComponent);
    component    = fixture.componentInstance;
    registerService = TestBed.inject(RegisterService);
    router = TestBed.inject(Router);
  });


 

  it('should register user', () => {
    let user: UserSignUp = new UserSignUp();
    user.email= "user@gmail.com"
    user.password="test12345"
    user.rePassword = "test12345"
    user.username = "newUser"

    component.user = user;
    component.signUp();
    expect(registerService.register).toHaveBeenCalled();
    expect(window.alert).toHaveBeenCalledWith('Please click on the link we sent to your email to activate your account!');
  });

  it('should alert when passwords does not match', () => {
    let user: UserSignUp = new UserSignUp();
    user.email= "user@gmail.com"
    user.password="test12345"
    user.rePassword = "test"
    user.username = "newUser"

    component.user = user;
    component.signUp();
    expect(window.alert).toHaveBeenCalledWith('The passwords does not match! Please try again!');
  });


  it('should display error when input is empty', () => {
    let user: UserSignUp = new UserSignUp();
    user.email= null
    user.password="test12345"
    user.rePassword = "test12345"
    user.username = "newUser"

    component.user = user;
    component.signUp();
    expect(component.isShown).toBe(true);
  });

  it('should change error message visibility on true', () => {
    let usernameInput = "";
    component.onChange(usernameInput);
  //  expect(RegisterComponent.prototype.onChange).toHaveBeenCalled();
    expect(component.isShown).toBe(true);
  });

  it('should change error message visibility on false', () => {
    let usernameInput: string = "someUsername";
    component.onChange(usernameInput);
    expect(component.isShown).toBe(false);
  });

  it('should alert when the password is shorter than 8 characters ', () => {
    let user: UserSignUp = new UserSignUp();
    user.email= "user@gmail.com"
    user.password="test"
    user.rePassword = "test"
    user.username = "newUser"

    component.user = user;
    component.signUp();
    expect(window.alert).toHaveBeenCalledWith('Your password must be between 8 and 16 characters');
  });

  it('should alert when the password is longer than 16 characters ', () => {
    let user: UserSignUp = new UserSignUp();
    user.email= "user@gmail.com"
    user.password="test"
    user.rePassword = "lozinkalozinka123"
    user.username = "lozinkalozinka123"

    component.user = user;
    component.signUp();
    expect(window.alert).toHaveBeenCalledWith('Your password must be between 8 and 16 characters');
  });


});
