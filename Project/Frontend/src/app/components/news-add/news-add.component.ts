import { formatDate } from '@angular/common';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal,ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NewsService } from 'src/app/services/news-service/news.service';
import { NewsAdd } from '../model/newsAdd';

@Component({
  selector: 'app-news-add',
  templateUrl: './news-add.component.html',
  styleUrls: ['./news-add.component.scss']
})
export class NewsAddComponent implements OnInit {

  constructor(private modalService: NgbModal, private newsService: NewsService,private router:Router) { }

  isShown: boolean = false

  closeResult = '';
  @Input()
  currentOffer: any;

  @Input()
  currentCategory : number =0;

  @Output()
  newItemEvent = new EventEmitter<boolean>();

  newsText!: string;
  newsTitle!: string;


  newsDTO: NewsAdd = {
    title: "",
    text : "",
    dateAdded : null,
    culturalOfferId : 0,
  };

  ngOnInit(): void {
  }

  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})/*.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });*/
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  createNews(){

    if( this.newsTitle ==null ||this.newsTitle =="" || this.newsText ==null ||this.newsText ==""){
      this.isShown = true;
    }else{
      this.newsDTO.dateAdded = new Date();
      console.log("DATE in news add component: " +this.newsDTO.dateAdded);
      this.newsDTO.title = this.newsTitle;
      this.newsDTO.text = this.newsText;
      this.newsDTO.culturalOfferId = this.currentOffer.id;
  
      this.newsService.createNews(this.newsDTO, this.currentCategory).subscribe((response) =>{
  
        console.log("NEWS RESPONSE: " + response);
        this.newItemEvent.emit(true);
        alert("News has been successfully added!");
        //this.router.navigateByUrl('/').then(() => window.location.reload())
  
  
      });
      this.modalService.dismissAll('Dismissed after saving data');
     // window.location.reload();
    }
   
  }

  onChange(value: string): void {   
   // console.log("NEEEWSL "+value);
  //  console.log(this)
    if(value == ""){
      this.isShown = true;
    }else{
      this.isShown = false
    }
  }

}
