import {ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement, TemplateRef } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';

import { CommentService } from 'src/app/services/comment-service/comment.service';
import { ActivatedRouteStub } from 'src/app/testing/router-stubs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Comment } from '../model/comment';
import { NewsAddComponent } from './news-add.component';
import { News } from '../model/news';
import { NewsService } from 'src/app/services/news-service/news.service';
import { NewsAdd } from '../model/newsAdd';
import { CulturalOffer } from '../model/cultural-offer';


describe('NewsAddComponent', () => {
  let component: NewsAddComponent;
  let fixture: ComponentFixture<NewsAddComponent>;
  let router: any;

  let newsAddService : any;
  let modalService : any;
 
  beforeEach(() => {
    spyOn(window, 'alert');
      let news = new News();
      news.title = "Novo desavanje"
      news.text = "Svi ste dobrodosli!";
    let newsAddServiceMock =
     {
        createNews : jasmine.createSpy('createNews')
        .and.returnValue(of({body:{news}}))
    
    }

    let modalServiceMock =
     {
        open : jasmine.createSpy('open')
            .and.returnValue(of()),
        dismissAll : jasmine.createSpy('dismissAll')
            .and.returnValue(of())
    }

    let routerMock = {
        navigate: jasmine.createSpy('navigate')
      };

    TestBed.configureTestingModule({
       declarations: [ NewsAddComponent],
       imports: [FormsModule],
       providers:    [ {provide: NewsService, useValue: newsAddServiceMock },
                      {provide: NgbModal, useValue: modalServiceMock } ,
                      { provide: Router, useValue: routerMock }  ]
    });

    fixture = TestBed.createComponent(NewsAddComponent);
    component = fixture.componentInstance;
    newsAddService = TestBed.inject(NewsService);
    modalService = TestBed.inject(NgbModal);
    router = TestBed.inject(Router);

  });

  it('should open modal dialog', fakeAsync(() => {
        let contet = TemplateRef
        component.open(contet);
        expect(modalService.open).toHaveBeenCalled();

  }));

  it('should create news', fakeAsync(() => {

    let newsDTO = new NewsAdd();
    newsDTO.title = "Novo desavanje"
    newsDTO.text = "Svi ste dobrodosli!";
    newsDTO.culturalOfferId = 1;

    component.newsDTO = newsDTO;
    component.newsText = "Svi ste dobrodosli!"
    component.newsTitle = "Novo desavanje"
    component.currentCategory = 1;

    let offer = new CulturalOffer();
    offer.id=1
    component.currentOffer = offer;

    component.createNews();
    
    expect(newsAddService.createNews).toHaveBeenCalled();
    expect(modalService.dismissAll).toHaveBeenCalled()
    expect(window.alert).toHaveBeenCalledWith('News has been successfully added!');


    }));

it('should display error when input is empty', fakeAsync(() => {
   
    component.newsText = null;

    component.createNews();

    expect(component.isShown).toBe(true);


    }));

it('should change error message visibility on true', () => {
    let usernameInput = "";
    component.onChange(usernameInput);
    //  expect(RegisterComponent.prototype.onChange).toHaveBeenCalled();
    expect(component.isShown).toBe(true);
    });

    it('should change error message visibility on false', () => {
    let usernameInput: string = "someUsername";
    component.onChange(usernameInput);
    expect(component.isShown).toBe(false);
    });
 

});
