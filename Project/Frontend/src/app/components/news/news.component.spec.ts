import {ComponentFixture, flush, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { By, DomSanitizer } from '@angular/platform-browser';
import { DebugElement, Sanitizer } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { NewsComponent } from './news.component';
import { Page } from '../model/page';
import { News } from '../model/news';
import { NewsService } from 'src/app/services/news-service/news.service';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


describe('NewsComponent', () => {
    let component: NewsComponent;
    let fixture: ComponentFixture<NewsComponent>;
    ///servisi koje komponenta koristi
    let newsService: any;
    let route: any;

    let currentDate = new Date()

    beforeEach(() => {
        let page = {
            content : [{
                title: "new news",
                text : "new text",
                dateAdded : currentDate
            },{
                title: "new news2",
                text : "new text2",
                dateAdded : currentDate
            }],
            pageable:{
                sort: {unsorted:true, sorted:false},
                pageSize: 10,
                pageNumber: 0,
                offset:0,
                unpaged:false,
                paged:true,
            },
            last: false,
            totalPages: 100,
            totalElements: 1000,
            first: true,
            sort: {unsorted:true, sorted:false},
            numberOfElements: 2,
            size: 1,
            number: 1,

        }
        let newsServiceMock = {
            getNews : jasmine.createSpy('getNews')
                .and.returnValue(of({body :page}))
        }

        TestBed.configureTestingModule({
            declarations: [ NewsComponent ],
            imports: [FormsModule, NgbPaginationModule],
            providers:    [ 
                            {provide: NewsService, useValue: newsServiceMock},
                            {provide: ActivatedRoute, useValue: { snapshot: 
                                { paramMap: convertToParamMap({offerId: '1'})}}}
                            ]
         });

         fixture = TestBed.createComponent(NewsComponent);
         component = fixture.componentInstance;
         newsService = TestBed.inject(NewsService)      

    });

    it('should show existing news on init', fakeAsync(() => {
    
        component.ngOnInit()
      

        expect(newsService.getNews).toHaveBeenCalled()
        expect(component.collectionSize).toEqual(1000)
        expect(component.news.length).toEqual(2)
        expect(component.news[0].title).toEqual("new news")
        expect(component.news[0].text).toEqual("new text")
        expect(component.news[0].dateAdded).toEqual(currentDate)
            
        expect(component.news[1].title).toEqual("new news2")
        expect(component.news[1].text).toEqual("new text2")
        expect(component.news[1].dateAdded).toEqual(currentDate)

        fixture.detectChanges()
        tick()

        let news = fixture.debugElement.queryAll(By.css('.newsPanel'))
        expect(news.length).toEqual(2)
        expect(news[0].query(By.css("#newsTitle")).nativeElement.textContent.trim()).toEqual("new news")
        expect(news[0].query(By.css("#newsText")).nativeElement.textContent.trim()).toEqual("new text")

        expect(news[1].query(By.css("#newsTitle")).nativeElement.textContent.trim()).toEqual("new news2")
        expect(news[1].query(By.css("#newsText")).nativeElement.textContent.trim()).toEqual("new text2")

    
    }));

    it('should show different news on page change', fakeAsync(() => {
        expect(component.news).toBe(undefined)
        component.changePage()
      

        expect(newsService.getNews).toHaveBeenCalled()

        expect(component.news.length).toEqual(2)
        expect(component.news[0].title).toEqual("new news")
        expect(component.news[0].text).toEqual("new text")
        expect(component.news[0].dateAdded).toEqual(currentDate)
            
        expect(component.news[1].title).toEqual("new news2")
        expect(component.news[1].text).toEqual("new text2")
        expect(component.news[1].dateAdded).toEqual(currentDate)

        fixture.detectChanges()
        tick()

        let news = fixture.debugElement.queryAll(By.css('.newsPanel'))
        expect(news.length).toEqual(2)
        expect(news[0].query(By.css("#newsTitle")).nativeElement.textContent.trim()).toEqual("new news")
        expect(news[0].query(By.css("#newsText")).nativeElement.textContent.trim()).toEqual("new text")

        expect(news[1].query(By.css("#newsTitle")).nativeElement.textContent.trim()).toEqual("new news2")
        expect(news[1].query(By.css("#newsText")).nativeElement.textContent.trim()).toEqual("new text2")

    
    }));

    it('should reload page', fakeAsync(() => {
        component.reload(true)
        expect(newsService.getNews).toHaveBeenCalled()
    }));

    


});
