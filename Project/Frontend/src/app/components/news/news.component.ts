import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NewsService } from 'src/app/services/news-service/news.service';
import { CulturalOffer } from '../model/cultural-offer';
import { News } from '../model/news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  offerId : number;
  news : News[];
  userRole = localStorage.getItem('role')||'none';

  @Input() culturalOffer : CulturalOffer;

  page : number;
  collectionSize : number;

  constructor(private route : ActivatedRoute, private newsService : NewsService) {
    this.page = 1;
    this.offerId = +route.snapshot.paramMap.get('offerId');

  }

  ngOnInit(): void {
    this.page = 1;

    this.newsService.getNews(this.offerId, this.page - 1)

    .pipe(
      catchError(err => {
        console.log('Handling error locally and rethrowing it...', err);
        return throwError(err);
    })
    )
    .subscribe(

      data => {
        
      //this.collectionSize = data.body.totalElements / data.body.size * 10 ;
      this.collectionSize = data.body.totalElements;
      this.news = data.body.content;

    });
  }

  reload(added : boolean){
    if(added){
      //console.log("NEWS IS ADDED " + added);
      this.ngOnInit();
    }
  }

  changePage(){
    this.newsService.getNews(this.offerId, this.page - 1).subscribe(data => {
      this.news = data.body.content;
    });
  }



}
