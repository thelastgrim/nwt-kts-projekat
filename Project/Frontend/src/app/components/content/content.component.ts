import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { Category } from '../model/category';
import { CulturalOffer } from '../model/cultural-offer';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})

export class ContentComponent implements OnInit {


  userRole = localStorage.getItem('role')||'none';

  categoriesObserv!: Observable<Category[]>;
  newCategory! : Observable<Category>;

  categories: Category[] = [];

  @Output()
  currentCategory! : number;

  @Output()
  nameForSearch : string = "";

  constructor(
     private categoryService : CategoryServiceService,  private router: Router, ratingConfig: NgbRatingConfig
    ) {
      ratingConfig.max = 5;
      ratingConfig.readonly = true;
      this.currentCategory = 0 // = 0 trigeruje ngOnChanges u culutral-offer-list (child)
    }

  ngOnInit(): void  {
    console.log(this.nameForSearch);

    this.categoryService.getAllCategories().subscribe( data =>{

    console.log("getAllCat2");
      this.categories = data.body;
      if(this.currentCategory != null && data.body.length > 0){
        this.currentCategory = this.categories[0].id;
      }else{this.currentCategory = null;}
    },(error : HttpErrorResponse)=>{
      alert("There is no offers");
    });
  }

 changeCategory(id : number){
   this.currentCategory = id;
 }

 addItem(name : string){
   this.nameForSearch = name;
   console.log(this.nameForSearch);

 }

 addCategory(newCategory: Category){
   this.categories.push(newCategory);
   this.categories.forEach(element => {
    console.log(element.name);
  });
  this.ngOnInit();
 }

 showMap(){
  this.router.navigate(['/map']);
 }

 refreshCategories(emitted : boolean){
  if(emitted){
    if(this.categories.length>0){
      this.ngOnInit();

    }
    else{
      console.log("current category  = Null");
      this.currentCategory = null;
    }
  }
}

}
