import {ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement, TemplateRef } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';

import { CommentService } from 'src/app/services/comment-service/comment.service';
import { ActivatedRouteStub } from 'src/app/testing/router-stubs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Comment } from '../model/comment';
import { ContentComponent } from './content.component';
import { Category } from '../model/category';


describe('ContentComponent', () => {
  let component: ContentComponent;
  let fixture: ComponentFixture<ContentComponent>;

  let categoryService : any;
  let router : any;
 
  beforeEach(() => {
    
    let categoryServiceMock =
     {
        getAllCategories : jasmine.createSpy('getAllCategories')
        .and.returnValue(of({body:[{
            id:1,
            name : "CAT1",
            deleted : false,
            culturalOffers : [{
                id: 1,
                name : "CU1",
                about : "ABOUTCU1",
                averageRating : 1,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 45,
                    lat : 46,
                    country : "SERBIA",
                    city : "NOVI SAD",
                    street : "GOGOLJEVA"
                },
                cover : {
                    id : 1,
                    title:"PIC1",
                    path : "/PIC1",

                },
                categoryName: "CAT1"
            }]  
        },{
            id:2,
            name : "CAT2",
            deleted : false,
            culturalOffers : [{
                id: 2,
                name : "CU2",
                about : "ABOUTCU2",
                averageRating : 2,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 46,
                    lat : 47,
                    country : "SERBIA",
                    city : "NIS",
                    street : "GOGOLJEVA"
                },
                cover : {
                    id : 2,
                    title:"PIC2",
                    path : "/PIC2",

                },
                categoryName: "CAT2"
            }]  
        },{
            id:3,
            name : "CAT3",
            deleted : false,
            culturalOffers : [{
                id: 3,
                name : "CU3",
                about : "ABOUTCU3",
                averageRating : 3,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 48,
                    lat : 49,
                    country : "SERBIA",
                    city : "UZICE",
                    street : "GOGOLJEVA"
                },
                cover : {
                    id : 3,
                    title:"PIC3",
                    path : "/PIC3",

                },
                categoryName: "CAT3"
            }]  
        }]}))
    
    }

    let routerMock = {
        navigate: jasmine.createSpy('navigate')
      };

    TestBed.configureTestingModule({
       declarations: [ ContentComponent],
       imports: [FormsModule],
       providers:    [ {provide: CategoryServiceService, useValue: categoryServiceMock },
                      {provide: Router, useValue: routerMock }  ]
    });

    fixture = TestBed.createComponent(ContentComponent);
    component = fixture.componentInstance;
    categoryService = TestBed.inject(CategoryServiceService);
    router = TestBed.inject(Router)
  });

  it('should populate categories and selected category on init', fakeAsync(() => {
        component.ngOnInit()

        expect(categoryService.getAllCategories).toHaveBeenCalled()
        expect(component.categories.length).toBe(3)
        expect(component.currentCategory).toBe(1)
        expect(component.categories[0].id).toBe(1)
        expect(component.categories[0].deleted).toBe(false)
        expect(component.categories[0].name).toBe("CAT1")
        expect(component.categories[0].culturalOffers[0].id).toBe(1)
        expect(component.categories[0].culturalOffers[0].name).toBe("CU1")
        expect(component.categories[0].culturalOffers[0].about).toBe("ABOUTCU1")
        expect(component.categories[0].culturalOffers[0].averageRating).toBe(1)
        expect(component.categories[0].culturalOffers[0].deleted).toBe(false)
        expect(component.categories[0].culturalOffers[0].comments.length).toBe(0)
        expect(component.categories[0].culturalOffers[0].news.length).toBe(0)
        expect(component.categories[0].culturalOffers[0].location.lng).toBe(45)
        expect(component.categories[0].culturalOffers[0].location.lat).toBe(46)
        expect(component.categories[0].culturalOffers[0].location.country).toBe("SERBIA")
        expect(component.categories[0].culturalOffers[0].location.city).toBe("NOVI SAD")
        expect(component.categories[0].culturalOffers[0].location.street).toBe("GOGOLJEVA")
        expect(component.categories[0].culturalOffers[0].cover.id).toBe(1)
        expect(component.categories[0].culturalOffers[0].cover.title).toBe("PIC1")
        expect(component.categories[0].culturalOffers[0].cover.path).toBe('/PIC1')
        expect(component.categories[0].culturalOffers[0].categoryName).toBe('CAT1')

        expect(component.categories[1].id).toBe(2)
        expect(component.categories[1].deleted).toBe(false)
        expect(component.categories[1].name).toBe("CAT2")
        expect(component.categories[1].culturalOffers[0].id).toBe(2)
        expect(component.categories[1].culturalOffers[0].name).toBe("CU2")
        expect(component.categories[1].culturalOffers[0].about).toBe("ABOUTCU2")
        expect(component.categories[1].culturalOffers[0].averageRating).toBe(2)
        expect(component.categories[1].culturalOffers[0].deleted).toBe(false)
        expect(component.categories[1].culturalOffers[0].comments.length).toBe(0)
        expect(component.categories[1].culturalOffers[0].news.length).toBe(0)
        expect(component.categories[1].culturalOffers[0].location.lng).toBe(46)
        expect(component.categories[1].culturalOffers[0].location.lat).toBe(47)
        expect(component.categories[1].culturalOffers[0].location.country).toBe("SERBIA")
        expect(component.categories[1].culturalOffers[0].location.city).toBe("NIS")
        expect(component.categories[1].culturalOffers[0].location.street).toBe("GOGOLJEVA")
        expect(component.categories[1].culturalOffers[0].cover.id).toBe(2)
        expect(component.categories[1].culturalOffers[0].cover.title).toBe("PIC2")
        expect(component.categories[1].culturalOffers[0].cover.path).toBe('/PIC2')
        expect(component.categories[1].culturalOffers[0].categoryName).toBe('CAT2')

        expect(component.categories[2].id).toBe(3)
        expect(component.categories[2].deleted).toBe(false)
        expect(component.categories[2].name).toBe("CAT3")
        expect(component.categories[2].culturalOffers[0].id).toBe(3)
        expect(component.categories[2].culturalOffers[0].name).toBe("CU3")
        expect(component.categories[2].culturalOffers[0].about).toBe("ABOUTCU3")
        expect(component.categories[2].culturalOffers[0].averageRating).toBe(3)
        expect(component.categories[2].culturalOffers[0].deleted).toBe(false)
        expect(component.categories[2].culturalOffers[0].comments.length).toBe(0)
        expect(component.categories[2].culturalOffers[0].news.length).toBe(0)
        expect(component.categories[2].culturalOffers[0].location.lng).toBe(48)
        expect(component.categories[2].culturalOffers[0].location.lat).toBe(49)
        expect(component.categories[2].culturalOffers[0].location.country).toBe("SERBIA")
        expect(component.categories[2].culturalOffers[0].location.city).toBe("UZICE")
        expect(component.categories[2].culturalOffers[0].location.street).toBe("GOGOLJEVA")
        expect(component.categories[2].culturalOffers[0].cover.id).toBe(3)
        expect(component.categories[2].culturalOffers[0].cover.title).toBe("PIC3")
        expect(component.categories[2].culturalOffers[0].cover.path).toBe('/PIC3')
        expect(component.categories[2].culturalOffers[0].categoryName).toBe('CAT3')

  }));

  it('should change category', fakeAsync(() => {
    component.changeCategory(155);
    expect(component.currentCategory).toBe(155)

    }));

    it('should change name for search', fakeAsync(() => {
        component.addItem("NEW CAT")
        expect(component.nameForSearch).toBe("NEW CAT")
    
    }));

    it('should navigate to map', fakeAsync(() => {
        component.showMap()
        expect(router.navigate).toHaveBeenCalledWith(["/map"])
    
    }));

    
});
