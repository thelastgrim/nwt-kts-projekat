import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { LoginResponse } from '../model/loginResponse';
import { UserTokenState } from '../model/userTokenState';
import { LogInService } from 'src/app/services/log-in-service/log-in.service';
import { UserLogIn } from '../model/userLogIn';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    ///servisi koje komponenta koristi
    let authService: any;
    let router: any;

    beforeEach(() => {
        //spyOn(window, "prompt").and.returnValue("Java Script Object Notation");
        //spyOn(window, "confirm");

        spyOn(window, 'confirm').and.callFake(function () {
            return true;
        });

        const one = new Promise<boolean>((resolve, reject) => {});

        let authServiceMock = {
            logout : jasmine.createSpy('logout')
                .and.returnValue(of(null))
        }

        let routerMock = {
            navigate: jasmine.createSpy('navigate')
            .and.returnValue(one)

        };

        TestBed.configureTestingModule({
            declarations: [ HeaderComponent ],
            imports: [FormsModule],
            providers:    [ 
                            {provide: AuthService, useValue: authServiceMock},
                            { provide: Router, useValue: routerMock } ]
         });

         fixture = TestBed.createComponent(HeaderComponent);
         component = fixture.componentInstance;
         authService = TestBed.inject(AuthService)
         router = TestBed.inject(Router);

    });

    it('should read and set data from local storage upon creation', fakeAsync(() => {
        localStorage.setItem('logged', 'true')
        localStorage.setItem('displayName', "test")
        localStorage.setItem('role', 'user');
        component = TestBed.createComponent(HeaderComponent).componentInstance;
        expect(component.userDisplat).toEqual('test')
        expect(component.userRole).toEqual('user')
        expect(component.isAuthenticated).toEqual(true)
    
    }));

    it('should prompt user before logout ', fakeAsync(() => {
        
        component.logOut()
        expect(window.confirm).toHaveBeenCalledWith('Are you sure you want to log out?');
        
    
    }));

    it('should logout user', fakeAsync(() => {
        
        component.logOut()
       
        expect(authService.logout).toHaveBeenCalled()
        expect(router.navigate).toHaveBeenCalledWith(['/logout'])
        expect(router.navigate).toHaveBeenCalledWith(['/'])
        
    
      }));

    it('should show map', fakeAsync(() => {
        component.showMap()
        expect(router.navigate).toHaveBeenCalledWith(['/map'])
    }));

    it('should show content', fakeAsync(() => {
        component.showList()
        expect(router.navigate).toHaveBeenCalledWith(['/content'])
    }));

    it('should show login form', fakeAsync(() => {
        component.showLogInForm()
        expect(router.navigate).toHaveBeenCalledWith(['/log-in'])
    }));

    it('should show register form', fakeAsync(() => {
        component.showRegisterForm()
        expect(router.navigate).toHaveBeenCalledWith(['/register'])
    }));

    it('should show change profile form', fakeAsync(() => {
        component.changeProfile()
        expect(router.navigate).toHaveBeenCalledWith(['/changeProfile'])
    }));




});
