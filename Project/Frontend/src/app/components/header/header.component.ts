import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  
  isAuthenticated : boolean = false;
  userDisplat : string = ""
  userRole : string = ""

  constructor(private router : Router, private auth:AuthService) {
    
    this.isAuthenticated = (/true/i).test(localStorage.getItem('logged')|| 'false');
    this.userDisplat = localStorage.getItem('displayName')||'none';
    this.userRole = localStorage.getItem('role')||'none';
    
  }

  ngOnInit(): void {
  
  }

  showMap(){
    this.router.navigate(['/map']);
  }


   showList(){
    this.router.navigate(['/content']);
  }

  showLogInForm(){
    this.router.navigate(['/log-in'])
    
  }

  logOut(){
    if(confirm("Are you sure you want to log out?")) {
      this.router.navigate(['/logout'])
      this.auth.logout()
      this.router.navigate(['/']).then(() => window.location.reload())
    }
   
  }

  showRegisterForm(){
    this.router.navigate(["/register"])
    
  }

  changeProfile(){
    this.router.navigate(['/changeProfile'])
  }

}
