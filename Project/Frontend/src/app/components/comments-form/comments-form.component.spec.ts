import {ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement, TemplateRef } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';

import { CommentService } from 'src/app/services/comment-service/comment.service';
import { ActivatedRouteStub } from 'src/app/testing/router-stubs';
import { CommentsFormComponent } from './comments-form.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Comment } from '../model/comment';


describe('CommentsFormComponent', () => {
  let component: CommentsFormComponent;
  let fixture: ComponentFixture<CommentsFormComponent>;

  let commentService : any;
  let modalService : any;
 
  beforeEach(() => {
    spyOn(window, 'alert');
      let sts : NgbModalRef
    let commentServiceMock =
     {
      createComment : jasmine.createSpy('createComment')
        .and.returnValue(of({body:{sts}}))
    
    }

    let modalServiceMock =
     {
        open : jasmine.createSpy('open')
            .and.returnValue(of()),
        dismissAll : jasmine.createSpy('dismissAll')
            .and.returnValue(of())
    }

    TestBed.configureTestingModule({
       declarations: [ CommentsFormComponent],
       imports: [FormsModule],
       providers:    [ {provide: CommentService, useValue: commentServiceMock },
                      {provide: NgbModal, useValue: modalServiceMock }  ]
    });

    fixture = TestBed.createComponent(CommentsFormComponent);
    component = fixture.componentInstance;
    commentService = TestBed.inject(CommentService);
    modalService = TestBed.inject(NgbModal)
  });

  it('should open modal dialog', fakeAsync(() => {
        let contet = TemplateRef
        component.open(contet);
        expect(modalService.open).toHaveBeenCalled();

  }));

  it('should create comment', fakeAsync(() => {
    let commentDTO = new Comment()
    commentDTO.date = new Date()
    commentDTO.rating = 3
    commentDTO.text = "TEST"

    let currnetOffer = {
        id:1
    }

    component.commentDTO = commentDTO;
    component.currentOffer = currnetOffer

    component.createComment()
    expect(commentService.createComment).toHaveBeenCalled();
    expect(modalService.dismissAll).toHaveBeenCalled()

    }));

    it('should alert when number of pictures is more than 5', fakeAsync(() => {
        let event ={
            target : {
                files : ["fajl1", "fajl2", "fajl3","fajl2", "fajl3","fajl2", "fajl3"]
            }
        }
    
        component.onFileChange(event)
        expect(window.alert).toHaveBeenCalledWith('Maximum number of pictures is 5.');
    
    }));

});
