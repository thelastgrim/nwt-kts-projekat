import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common'
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { Comment } from '../model/comment';
import { Picture } from '../model/picture';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { LogInService } from 'src/app/services/log-in-service/log-in.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-comments-form',
  templateUrl: './comments-form.component.html',
  styleUrls: ['./comments-form.component.scss']
})
export class CommentsFormComponent implements OnInit{

  alert = false
  images = [];
  names = []
   myForm = new FormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    file: new FormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });

  commentDTO: Comment = {
    text : "",
    date : new Date(),
    rating : 5,
    username: ""
  };

  closeResult = '';

  currentRate =5 ;


  commentText!: string;

  @Output() newItemEvent = new EventEmitter<boolean>();

  @Input()
  events!: Observable<number>;

  @Input()
  currentOffer: any;

  @Input()
  currentCategory : number =0;

  constructor(private modalService: NgbModal, private commentService:CommentService) {

    //this.isSubbed = this.subbs.indexOf(this.currentOffer) > 0
  }

  ngOnInit(){
  }

  open(content: any) {
    
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})/*.result.then((result) => {
      console.log(result)
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    */
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  createComment(){

    this.commentDTO.date = new Date()
    this.commentDTO.rating = this.currentRate;
    this.commentDTO.text = this.commentText;

    for (let index = 0; index < this.images.length; index++) {
      let base64 = this.images[index].split(",", "2")[1]
      const imageBlob = this.dataURItoBlob(base64);
      if (imageBlob.size > 5242880){
        alert("File too large.");
        return;
      }
    }



    this.commentService.createComment(this.commentDTO, this.currentOffer.id).subscribe((response) =>{

      for (let index = 0; index < this.images.length; index++) {
        console.log(this.images.length);
        const imageName = this.names[index];

        let base64 = this.images[index].split(",", "2")[1]
        const imageBlob = this.dataURItoBlob(base64);
        const imageFile = new File([imageBlob], imageName, { type: 'image/png' });
        let formData:FormData = new FormData();
        formData.append("title", "NASLOV")
        formData.append('file', imageFile);

        this.commentService.uploadPicture(formData, this.currentCategory, response.body.id).subscribe((data)=>{
          console.log(data);
          if(index === this.images.length - 1)
            this.newItemEvent.emit(true);
        })

      }
    })

    //this.alert = true
    this.modalService.dismissAll('Dismissed after saving data');
    //this.alert = false

    if(this.images.length === 0){
      this.newItemEvent.emit(true);
    }

    alert("Comment created.")



    //this.router.navigateByUrl("/offer/"+this.currentOffer.id)
  }

  get f(){
    return this.myForm.controls;
  }

  onFileChange(event) {
    console.log("event")
    console.log(event)
    this.images = [];
    this.names = []
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;

        for (let index = 0; index < filesAmount; index++) {
          this.names.push(event.target.files[index].name)
        }

        if(filesAmount > 5){
          alert("Maximum number of pictures is 5.")
        }else{


          for (let i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = (event:any) => {

                    this.images.push(event.target.result);

                    this.myForm.patchValue({
                        fileSource: this.images
                    });
                  }

                  reader.readAsDataURL(event.target.files[i]);
          }
      }
    }
  }

  private dataURItoBlob(dataURI:string) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
 }

}
