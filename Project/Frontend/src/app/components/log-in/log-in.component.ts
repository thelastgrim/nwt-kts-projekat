import { Component, OnInit } from '@angular/core';
import { LogInService } from 'src/app/services/log-in-service/log-in.service';
import { UserLogIn } from '../model/userLogIn';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  constructor(private logInService:LogInService, private authService:AuthService, private router:Router) {
    
  }

  user: UserLogIn = new UserLogIn;

  ngOnInit(): void {

  }

  logIn(){
    this.logInService.logIn(this.user).subscribe((data) =>{
     
      localStorage.setItem('subbs', data.body.subbs==null?"null":data.body.subbs.toString()) ;
    
      this.authService.setSession(data.body.dtoUserTokenState);
      this.router.navigate(['/']).then(() => window.location.reload())
      console.log("TEXXXXX")
      console.log(this.router.navigate(['/']))
    });
    
  }
  

  
}
