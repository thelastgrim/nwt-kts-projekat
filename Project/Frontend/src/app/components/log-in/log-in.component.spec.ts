import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { LogInComponent } from './log-in.component';
import { LoginResponse } from '../model/loginResponse';
import { UserTokenState } from '../model/userTokenState';
import { LogInService } from 'src/app/services/log-in-service/log-in.service';
import { UserLogIn } from '../model/userLogIn';
import { AuthService } from 'src/app/services/auth/auth.service';

describe('LogInComponent', () => {
    let component: LogInComponent;
    let fixture: ComponentFixture<LogInComponent>;

    ///servisi koje komponenta koristi
    let logInService: any;
    let authService: any;
    let router: any;

    beforeEach(() => {
        const one = new Promise<boolean>((resolve, reject) => {});
        let loginResponse = new LoginResponse()
        loginResponse.subbs = [1, 2]
        let userTokenState = new UserTokenState()
        userTokenState.accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lIiwicGFzc3dvcmQiOiJwYXNzd29yZCIsImV4cGlyZXNJbiI6MTgwMDAwMH0.Oxa26QBHCR7Q6KlXryQpIiVJWmYyrDS-FU-jwrRzEX8"
        userTokenState.expiresIn = 1800000
        loginResponse.dtoUserTokenState = userTokenState

        let logInServiceMock = {
          logIn: jasmine.createSpy('logIn')
              .and.returnValue(of({body: loginResponse})),    
        }

        let authServiceMock = {
            setSession : jasmine.createSpy('setSessuin')
                .and.returnValue(of(null))
        }
        
        let routerMock = {
            navigate: jasmine.createSpy('navigate')
            .and.returnValue(one),   
        };

        TestBed.configureTestingModule({
            declarations: [ LogInComponent ],
            imports: [FormsModule],
            providers:    [ {provide: LogInService, useValue: logInServiceMock },
                            {provide: AuthService, useValue: authServiceMock},
                            { provide: Router, useValue: routerMock } ]
         });

         fixture = TestBed.createComponent(LogInComponent);
         component = fixture.componentInstance;
         logInService = TestBed.inject(LogInService);
         authService = TestBed.inject(AuthService)
         router = TestBed.inject(Router);

    });

    it('should log in user', fakeAsync(() => {
        
        let user: UserLogIn = new UserLogIn();
        user.username = "test"
        user.password = "test"
        component.user = user;

        component.logIn();
        tick()
        
        expect(logInService.logIn).toHaveBeenCalled();
        expect(localStorage.getItem("subbs")).toEqual("1,2");
        expect(localStorage.getItem("subbs")).toEqual("1,2");
        expect(authService.setSession).toHaveBeenCalled()
        expect(router.navigate).toHaveBeenCalledWith(['/']);
    
      }));


});
