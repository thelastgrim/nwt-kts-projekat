import { Component, Input, OnInit } from '@angular/core';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements OnInit {

  isSubbed :boolean;
  subbs : number[];
  buttonText : string; 

  @Input()
  currentOffer;

  constructor(private culturalOfferService:CulturalOfferServiceService) {
    this.subbs = localStorage.getItem("subbs").split(",").map(Number);
    
  }
  ngOnInit(): void {
    this.isSubbed = this.subbs.indexOf(this.currentOffer.id) >-1
    this.buttonText = this.isSubbed?"Unsubscribe":"Subscribe"
  }

  private subscribe(){
    console.log("metoda sub")
    this.culturalOfferService.subscribe(this.currentOffer.id).subscribe((data)=>{
      this.subbs = localStorage.getItem("subbs").split(",").map(Number);
      this.subbs.push(this.currentOffer.id);
      localStorage.setItem("subbs", this.subbs.toString());
      this.buttonText = "Unsubscribe"
      this.isSubbed = true
    })
  }

  subUnsub(){
    this.isSubbed?this.unsubscribe():this.subscribe()
    
  }

  private unsubscribe(){
    console.log("metoda unsub")
    this.culturalOfferService.unsubscribe(this.currentOffer.id).subscribe((data)=>{
      this.subbs = localStorage.getItem("subbs").split(",").map(Number);
      this.subbs = this.subbs.filter(item => item !== this.currentOffer.id);
      localStorage.setItem("subbs", this.subbs.toString());
      this.buttonText = "Subscribe"
      this.isSubbed = false
    })
  }

}
