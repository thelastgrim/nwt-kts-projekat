import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { UnsubscribeComponent } from './unsubscribe.component';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOfferDeleteComponent } from '../cultural-offer/cultural-offer-delete/cultural-offer-delete/cultural-offer-delete.component';
import { CulturalOffer } from '../model/cultural-offer';



describe('UnsubscribeComponent', () => {
    let component: UnsubscribeComponent;
    let fixture: ComponentFixture<UnsubscribeComponent>;

    ///servisi koje komponenta koristi
    let culturalOfferService: any;

    beforeEach(() => {
        localStorage.setItem("subbs", "1,2,3")
        let culturalOfferServiceMock = {
            unsubscribe: jasmine.createSpy('unsubscribe')
                .and.returnValue(of()),
            subscribe: jasmine.createSpy('subscribe')
                .and.returnValue(of()),        
        }

        TestBed.configureTestingModule({
            declarations: [ UnsubscribeComponent ],
            imports: [FormsModule],
            providers:    [ {provide:CulturalOfferServiceService, useValue: culturalOfferServiceMock }]
         });

         fixture = TestBed.createComponent(UnsubscribeComponent);
         component = fixture.componentInstance;
         culturalOfferService = TestBed.inject(CulturalOfferServiceService);

    });

  it('should unsubscribe subscribed user', fakeAsync(() => {
        let culturalOffer = new CulturalOffer()
        culturalOffer.id = 1
        

        component.currentOffer = culturalOffer
        tick()
        component.ngOnInit()
        component.subUnsub()
    
        expect(culturalOfferService.unsubscribe).toHaveBeenCalled()
       
  }));
  
  it('should subscribe user', fakeAsync(() => {
    let culturalOffer = new CulturalOffer()
        culturalOffer.id = 1
        
        component.currentOffer = culturalOffer
        tick()
        component.ngOnInit()
        component.isSubbed = false
        component.subUnsub()
    
        expect(culturalOfferService.subscribe).toHaveBeenCalled()


}));

});
