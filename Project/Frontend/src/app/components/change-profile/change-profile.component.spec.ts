import {async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';

import { of } from 'rxjs';
import { RegisterService } from 'src/app/services/register-service/register.service';
import { FormsModule } from '@angular/forms';
import { UserSignUp } from '../model/userSignUp';
import { ChangeProfileComponent } from './change-profile.component';
import { UserChange } from '../model/userChange';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangeProfileService } from 'src/app/services/change-profile-service/change-profile.service';

describe('changeProfileComponent', () => {
  let component: ChangeProfileComponent;
  let fixture: ComponentFixture<ChangeProfileComponent>;
  let changeProfileService: any;
  let router: any;

  beforeEach(() => {
    spyOn(window, 'alert');
    let changeProfileMock = {
      change: jasmine.createSpy('change')
          .and.returnValue(of({body: {} }))
    };

    let routerMock = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
       declarations: [ ChangeProfileComponent ],
       imports: [FormsModule],
       providers:    [ {provide: ChangeProfileService, useValue: changeProfileMock },
                       { provide: Router, useValue: routerMock } ]
    });

    fixture = TestBed.createComponent(ChangeProfileComponent);
    component = fixture.componentInstance;
    changeProfileService = TestBed.inject(ChangeProfileService);
    router = TestBed.inject(Router);
  });


  it('should change password', () => {
    let user: UserChange = new UserChange();
    user.oldPassword="test12345"
    user.rePassword = "novaLozinka"
    user.newPassword = "novaLozinka"

    component.user = user;
    component.changeProfile();

    expect(changeProfileService.change).toHaveBeenCalledWith(user);
    expect(window.alert).toHaveBeenCalledWith('Password updated!');
    expect(router.navigate).toHaveBeenCalledWith(['app-content']);
  });

  
  it('should alert when passwords does not match ', () => {
    let user: UserChange = new UserChange();
    user.oldPassword="test12345"
    user.rePassword = "nova"
    user.newPassword = "novaLozinka"

    component.user = user;
    component.changeProfile();

    expect(window.alert).toHaveBeenCalledWith('The passwords does not match! Repeat password again!');
  });
 

  it('should show passwords', () => {
    let event ={
      target:{
        checked: true
      }
    }
    component.show(event);
    component.show1(event);
    component.show2(event);
    expect(component.type).toBe("input")
    expect(component.type1).toBe("input")
    expect(component.type2).toBe("input")


  });

  it('should hide passwords', () => {
    let event ={
      target:{
        checked: false
      }
    }
    component.show(event);
    component.show1(event);
    component.show2(event);
    expect(component.type).toBe("password")
    expect(component.type1).toBe("password")
    expect(component.type2).toBe("password")


  });

   
  it('should alert when the password is shorter than 8 characters', () => {
    let user: UserChange = new UserChange();
    user.oldPassword="test12345"
    user.rePassword = "test"
    user.newPassword = "test"

    component.user = user;
    component.changeProfile();

    expect(window.alert).toHaveBeenCalledWith('Your password must be between 8 and 16 characters');
  });


  it('should alert when the password is longer than 16 characters', () => {
    let user: UserChange = new UserChange();
    user.oldPassword="test12345"
    user.rePassword = "lozinkaLozinka123"
    user.newPassword = "lozinkaLozinka123"

    component.user = user;
    component.changeProfile();

    expect(window.alert).toHaveBeenCalledWith('Your password must be between 8 and 16 characters');
  });
});
