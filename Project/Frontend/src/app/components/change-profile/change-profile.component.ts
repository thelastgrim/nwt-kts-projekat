import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChangeProfileService } from 'src/app/services/change-profile-service/change-profile.service';
import { UserChange } from '../model/userChange';

@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.scss']
})
export class ChangeProfileComponent implements OnInit {
  userDisplay : string = ""
  user: UserChange;
  type: string = "password"
  type1: string = "password"
  type2: string = "password"



  constructor(private changeService:ChangeProfileService, private router:Router) {
    this.userDisplay = localStorage.getItem('displayName')||'none';
      this.user = new UserChange;


   }

  ngOnInit(): void {
  }

  changeProfile(){
    if(this.user.newPassword.length >=8 && this.user.newPassword.length <=16){

      if(this.user.newPassword === this.user.rePassword){
        this.changeService.change(this.user).subscribe((data) =>{
          console.log("  USER in component: " + this.user.newPassword + " "+ this.user.oldPassword )
          alert("Password updated!")
          this.router.navigate(['app-content'])
        });
      }else{
        alert("The passwords does not match! Repeat password again!")
      }
  }else{
    alert("Your password must be between 8 and 16 characters")
  }
}

  show(event) {
    if ( event.target.checked ) {
        this.type = "input";
   }else{
     this.type="password"
   }
}
  show1(event) {
    if ( event.target.checked ) {
        this.type1 = "input";
  }else{
    this.type1="password"
  }
  }
  show2(event) {
    if ( event.target.checked ) {
        this.type2 = "input";
  }else{
    this.type2="password"
  }
}

}
