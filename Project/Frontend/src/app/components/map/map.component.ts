import { Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import { CulturalOffer } from '../model/cultural-offer';
import Map from 'ol/Map';
import View, { createCenterConstraint } from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import { fromLonLat, transform } from 'ol/proj';
import Overlay from 'ol/Overlay';
import Text from 'ol/style/Text';
import { toStringHDMS } from 'ol/coordinate';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import { SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnChanges {

  @Input() culturalOffers! : CulturalOffer[];

  map! : Map;
  markerSource : VectorSource;
  markerStyle! : Style;
  popup! : Overlay;
  view : View;
  activeRoute : string;

  constructor( private route: ActivatedRoute) {
    this.activeRoute = this.route.snapshot.url.toString();
      this.markerSource =  new VectorSource({});
      this.view = new View({
        center: olProj.fromLonLat([7.55, 52]),
        zoom: 3,
        maxZoom: 15,
      });
   }

  ngOnChanges() {

    this.map = new Map({
      target: 'map',
      layers: [
        new TileLayer({
          source: new OSM()
        }),
        new VectorLayer({
          source : this.markerSource,
        })
      ],
      view: this.view,
    });

    if(this.culturalOffers != undefined)
        this.writeOffers()

}


  writeOffers(){

    this.markerSource.forEachFeature(value =>{
      this.markerSource.removeFeature(value);
    });

    if(this.culturalOffers.length !== 0){
      this.view.setCenter(fromLonLat([this.culturalOffers[0].location.lng, this.culturalOffers[0].location.lat]));
    }

    if(this.culturalOffers.length === 1){
      this.view.setZoom(10);
    }else{
      this.view.setZoom(3);
    }

    this.culturalOffers.forEach(data =>{
      let feature =  new Feature({
                      geometry: new Point(fromLonLat([data.location.lng, data.location.lat])),
                      name : data.name
                  });
      let style =  new Style({
        image: new Icon({
          anchor: [0.5, 1],
          src: 'https://openlayers.org/en/latest/examples/data/icon.png'
        }),
        text: new Text({
          text: data.name,
          fill: new Fill({color: 'white'}),
          stroke: new Stroke({color: 'black', width: 4}),
          offsetX: 20,
        })
      });
      feature.setId(data.id);
      feature.setGeometryName(data.name);
      feature.setStyle(style);
      this.markerSource.addFeature(feature);

    });
  }
}



