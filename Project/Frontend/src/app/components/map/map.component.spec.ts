import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MapComponent } from './map.component';
import { CulturalOfferAboutComponent } from '../cultural-offer/cultural-offer-about/cultural-offer-about.component';
import { CulturalOffer } from '../model/cultural-offer';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import TileLayer from 'ol/layer/Tile';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  let route : any;

  beforeEach(() => {

    //mockSnapshot.url = '/protected';

    let routeMock = {
      route : jasmine.createSpy('route.snapshot.url'),
      snapshot : jasmine.createSpy('snapshot.url'),
    }
    TestBed.configureTestingModule({
       declarations: [ CulturalOfferAboutComponent ],
       imports: [],
       providers: [{provide : ActivatedRoute, useValue : { snapshot: { url: {get:(url:string)=>{url:'/map'}}}}}]
    });

    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize and add features on map', () => {

    let culturalOffers : CulturalOffer[] =
    [
      {
        "id": 1,
        "name": "Willms, Sanford and Toy",
        "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
        "averageRating": 4.0,
        "categoryName": "Museums",
        "deleted": false,
        "comments": [],
        "news": [],
        "cover": {
            "id": 1,
            "title": "Thrive",
            "path": "src//main//resources//pictures//museum.jpg",
        },
        "location": {
            "city": "Penang",
            "country": "Indonesia",
            "street": "913 Sycamore Point",
            "lng": 100.3091,
            "lat": 5.4356367
        },
      },
      {
        "id": 2,
        "name": "McLaughlin-Nader",
        "about": "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
        "averageRating": 3.0,
        "categoryName": "Museums",
        "deleted": false,
        "comments": [],
        "news": [],
        "cover": {
            "id": 1,
            "title": "Thrive",
            "path": "src//main//resources//pictures//museum.jpg",
        },
        "location": {
            "city": "Kristiansand S",
            "country": "Norway",
            "street": "18 Maple Wood Crossing",
            "lng": 10.7533605,
            "lat": 59.9384597
          },
        }
    ];

    component.culturalOffers = culturalOffers;

    component.ngOnChanges();

    expect(component.markerSource).toBeDefined();
    expect(component.view).toBeDefined();
    expect(component.activeRoute).toBeDefined();

    expect(component.map.getView().getMaxZoom()).toEqual(15);

    expect(component.map.getView().getZoom()).toEqual(3);

    expect(component.markerSource.getFeatures().length).toEqual(2);
    expect(component.markerSource.getFeatureById(1).getGeometryName()).toBe("Willms, Sanford and Toy");
    expect(component.markerSource.getFeatureById(2).getGeometryName()).toBe("McLaughlin-Nader");

  });






});
