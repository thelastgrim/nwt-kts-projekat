import {ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { CommentComponent } from './comment.component';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { ActivatedRouteStub } from 'src/app/testing/router-stubs';
import { NgbModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';


describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;

  let commentService : any;
  let activatedRoute: any;

  let currentDate = new Date()

  beforeEach(() => {

    

    let page = {
      content : [{
        date : currentDate,
          text : "NEW COMMENT",
          rating : 1,
          username : "USER1",
          pictures:[]
      },{
        date : currentDate,
        text : "NEW COMMENT2",
        rating : 2,
        username : "USER2",
        pictures:[]
      }],
      pageable:{
          sort: {unsorted:true, sorted:false},
          pageSize: 10,
          pageNumber: 0,
          offset:0,
          unpaged:false,
          paged:true,
      },
      last: false,
      totalPages: 100,
      totalElements: 1000,
      first: true,
      sort: {unsorted:true, sorted:false},
      numberOfElements: 2,
      size: 1,
      number: 1,

  }

    let commentServiceMock =
     {
      getComments : jasmine.createSpy('getComments')
        .and.returnValue(of({body :page}))
    
    }

    TestBed.configureTestingModule({
       declarations: [ CommentComponent ],
       imports: [FormsModule, NgbRatingModule, NgbModule],
       providers:    [ {provide: CommentService, useValue: commentServiceMock },
        {provide: ActivatedRoute, useValue: { snapshot: { paramMap: convertToParamMap({offerId: '1'})}}} ]
    });

    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    commentService = TestBed.inject(CommentService);
    activatedRoute = TestBed.inject(ActivatedRoute);

  });

  it('should fetch comments on init', fakeAsync(() => {
    component.offerId = 1;
    component.ngOnInit()
    expect(commentService.getComments).toHaveBeenCalled()
    expect(component.comments.length).toEqual(2)
    expect(component.collectionSize).toEqual(1000)

    expect(component.comments[0].date).toEqual(currentDate)
    expect(component.comments[0].rating).toEqual(1)
    expect(component.comments[0].text).toEqual("NEW COMMENT")
    expect(component.comments[0].username).toEqual("USER1")

    expect(component.comments[1].date).toEqual(currentDate)
    expect(component.comments[1].rating).toEqual(2)
    expect(component.comments[1].text).toEqual("NEW COMMENT2")
    expect(component.comments[1].username).toEqual("USER2")

    fixture.detectChanges()
    tick()

    let comments = fixture.debugElement.queryAll(By.css('.head'))
 
        expect(comments.length).toEqual(2)

        expect(comments[0].query(By.css(".user")).nativeElement.textContent.trim()).toEqual("USER1")
        //expect(comments[0].query(By.css("#commentRating")).nativeElement.attributes["ng-reflect-rate"]).toEqual(1)
        expect(comments[0].query(By.css("#commentText")).nativeElement.textContent.trim()).toEqual("NEW COMMENT")

       expect(comments[1].query(By.css(".user")).nativeElement.textContent.trim()).toEqual("USER2")
       //expect(comments[0].query(By.css("#commentRating")).nativeElement.attributes["ng-reflect-rate"]).toEqual(2)
       expect(comments[1].query(By.css("#commentText")).nativeElement.textContent.trim()).toEqual("NEW COMMENT2")
        
   

  }));

  it('should call init on reload', fakeAsync(() => {
    component.offerId = 1;
    component.reload(true)
    expect(commentService.getComments).toHaveBeenCalled()
    expect(component.comments.length).toEqual(2)
    expect(component.collectionSize).toEqual(1000)

    expect(component.comments[0].date).toEqual(currentDate)
    expect(component.comments[0].rating).toEqual(1)
    expect(component.comments[0].text).toEqual("NEW COMMENT")
    expect(component.comments[0].username).toEqual("USER1")

    expect(component.comments[1].date).toEqual(currentDate)
    expect(component.comments[1].rating).toEqual(2)
    expect(component.comments[1].text).toEqual("NEW COMMENT2")
    expect(component.comments[1].username).toEqual("USER2")

    fixture.detectChanges()
    tick()

    let comments = fixture.debugElement.queryAll(By.css('.head'))
     
        expect(comments.length).toEqual(2)
        expect(comments[0].query(By.css(".user")).nativeElement.textContent.trim()).toEqual("USER1")
        //ne moze da se dobije rejting iz ngb-rating
        //expect(comments[0].query(By.css("#commentRating")).nativeElement.value).toEqual(1)
        expect(comments[0].query(By.css("#commentText")).nativeElement.textContent.trim()).toEqual("NEW COMMENT")

       expect(comments[1].query(By.css(".user")).nativeElement.textContent.trim()).toEqual("USER2")
       //expect(comments[1].query(By.css("#commentRating")).nativeElement.textContent.trim()).toEqual(2)
       expect(comments[1].query(By.css("#commentText")).nativeElement.textContent.trim()).toEqual("NEW COMMENT2")
        
   

  }));
  
});
