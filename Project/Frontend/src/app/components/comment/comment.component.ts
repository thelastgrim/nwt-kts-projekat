import { Component, OnChanges, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { Comment } from 'src/app/components/model/comment'
import { DomSanitizer } from '@angular/platform-browser';
import { Input } from '@angular/core';
import { CulturalOffer } from '../model/cultural-offer';
import { PictureService } from 'src/app/services/picture-service/picture.service';
import { Constants } from 'src/app/app.constants';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { EventEmitter } from '@angular/core';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() culturalOffer : CulturalOffer;

  showModal: boolean;
  offerId : number;
  comments! : Comment[];

  cover : any;

  page : number;
  collectionSize : number;

  userRole = localStorage.getItem('role')||'none';
  display = false;
  readonly pageSize = new Constants().PAGE_SIZE

  constructor(private commentService : CommentService, private route : ActivatedRoute){
    this.offerId = + route.snapshot.paramMap.get('offerId');
    this.page = 1;
    this.cover = "http://placehold.it/700x400";
}

  ngOnInit(){
    this.commentService.getComments(this.offerId, this.page - 1)
    .pipe(
      catchError(err => {
        console.log('Handling error locally and rethrowing it...', err);
        return throwError(err);
    })
    )
    .subscribe(data => {
      this.comments = data.body.content;
      this.collectionSize = data.body.totalElements ;
  });

  }

  reload(data: boolean){
    
    if(data){
      this.page = 1;
      this.ngOnInit();
    }
  }



}
