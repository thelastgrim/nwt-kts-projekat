import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitter } from '@angular/core';
import { Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CulturalOffer } from 'src/app/components/model/cultural-offer';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';

@Component({
  selector: 'app-cultural-offer-update',
  templateUrl: './cultural-offer-update.component.html',
  styleUrls: ['./cultural-offer-update.component.scss']
})
export class CulturalOfferUpdateComponent implements OnInit {

  @Input() culturalOffer : CulturalOffer;

  @Output() refreshEvent = new EventEmitter<boolean>();

  constructor(private modalService: NgbModal, private culturalOfferService : CulturalOfferServiceService) {
  }

  ngOnInit() {
  }

  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
    // .result.then((result) => {
    //   this.closeResult = `Closed with: ${result}`;
    // }, (reason) => {
    //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //   this.refreshEvent.emit(true);
    // });
  }

  update(){
    console.log(this.culturalOffer);
    this.culturalOfferService.updateOffer(this.culturalOffer).subscribe(data => {
      console.log("Nova ponuda " + data.body);
      alert("Offer " + data.body.name + " has been updated");
    },
    (error : HttpErrorResponse)=>{
      alert("Cultural offer with that name already exists!");
      this.refreshEvent.emit(true);
    });
  }

  cancel(){
    this.refreshEvent.emit(true);
  }

}
