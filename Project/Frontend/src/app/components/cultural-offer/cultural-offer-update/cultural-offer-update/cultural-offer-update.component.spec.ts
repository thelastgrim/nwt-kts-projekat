import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef, TemplateRef } from '@angular/core';

import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ngbCarouselTransitionOut } from '@ng-bootstrap/ng-bootstrap/carousel/carousel-transition';
import { CulturalOfferUpdateComponent } from './cultural-offer-update.component';
import { CulturalOffer } from 'src/app/components/model/cultural-offer';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

describe('CulturalOfferUpdateComponent', () => {
  let component: CulturalOfferUpdateComponent;
  let fixture: ComponentFixture<CulturalOfferUpdateComponent>;

  let culturalOfferService : any;
  let modalService : any;

  beforeEach(() => {
    spyOn(window, 'alert');
    let culturalOffer : CulturalOffer = {
          "id": 1,
          "name": "New Offer",
          "about": "New Description",
          "averageRating": 5.0,
          "categoryName": "Museums",
          "deleted": false,
          "comments": [],
          "news": [],
          "cover": {
              "id": 1,
              "title": "Thrive",
              "path": "src//main//resources//pictures//museum.jpg",
          },
          "location": {
              "city": "Penang",
              "country": "Indonesia",
              "street": "913 Sycamore Point",
              "lng": 100.3091,
              "lat": 5.4356367
          },
    }
    let culturalOfferServiceMock = {
      updateOffer : jasmine.createSpy('updateOffer').and.returnValue(of({body : culturalOffer})),
    }

    let modalServiceMock =
     {
        open : jasmine.createSpy('open')
            .and.returnValue(of()),
        dismissAll : jasmine.createSpy('dismissAll')
            .and.returnValue(of())
    }

    TestBed.configureTestingModule({
       declarations: [ CulturalOfferUpdateComponent ],
       imports: [FormsModule],
       providers:  [ {provide: CulturalOfferServiceService, useValue: culturalOfferServiceMock },
        {provide: NgbModal, useValue: modalServiceMock },
      ]
    });

    fixture = TestBed.createComponent(CulturalOfferUpdateComponent);
    component = fixture.componentInstance;
    culturalOfferService = TestBed.inject(CulturalOfferServiceService);
    modalService = TestBed.inject(NgbModal)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal dialog', fakeAsync(() => {
    let contet = TemplateRef;
    component.open(contet);
    expect(modalService.open).toHaveBeenCalled();


  }));

  it('should update cultural offer,', fakeAsync(()=>{
    let updateOffer : CulturalOffer = {
      "id": 1,
      "name": "New Offer",
      "about": "New Description",
      "averageRating": 5.0,
      "categoryName": "Museums",
      "deleted": false,
      "comments": [],
      "news": [],
      "cover": {
          "id": 1,
          "title": "Thrive",
          "path": "src//main//resources//pictures//museum.jpg",
      },
      "location": {
          "city": "Penang",
          "country": "Indonesia",
          "street": "913 Sycamore Point",
          "lng": 100.3091,
          "lat": 5.4356367
      },
    };

    component.culturalOffer = updateOffer;


    fixture.detectChanges();
    tick();

    component.update();

    expect(culturalOfferService.updateOffer).toHaveBeenCalled();
    expect(window.alert).toHaveBeenCalledWith('Offer '+ component.culturalOffer.name +' has been updated');



  }));

  it('should listen for form changes', () => {

    spyOn(component.refreshEvent, 'emit');
    component.cancel();
    expect(component.refreshEvent.emit).toHaveBeenCalled();
    expect(component.refreshEvent.emit).toHaveBeenCalledWith(true);
  });
});
