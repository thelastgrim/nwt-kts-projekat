import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PictureService } from 'src/app/services/picture-service/picture.service';

@Component({
  selector: 'app-cultural-offer-cover',
  templateUrl: './cultural-offer-cover.component.html',
  styleUrls: ['./cultural-offer-cover.component.scss']
})
export class CulturalOfferCoverComponent implements OnInit, OnChanges {

  @Input()
  culturalOffer;

  @Input() comment : any;

  cover;
  commentCover : Object;
  helper = [];

  showModal : boolean;
  picture : File;

  constructor(private pictureService:PictureService, private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.commentCover = {};
    //this.cover = changes.currentValue
  }

  ngOnInit(): void {
  
    if(this.culturalOffer){
      //if(this.culturalOffer.cover == null){
        
        this.pictureService.getPicture(this.culturalOffer.cover.id).subscribe((res) => {
         
          this.cover = this.sanitizer.bypassSecurityTrustResourceUrl(
            'data:image/jpeg;base64,' + res.body.picture);
           
        },
        response =>{
          console.log("ERROR ", response);
        },
        () =>{
          console.log("picture set")

        })
      //}else{
        //this.cover = "http://placehold.it/700x400"
      //}

    }else if(this.comment){
      if(this.comment.pictures.length != 0){
        this.comment.pictures.forEach(pic => {
          this.pictureService.getPicture(pic.id).subscribe((res) => {


            this.helper.push(this.sanitizer.bypassSecurityTrustResourceUrl(
              'data:image/jpeg;base64,' + res.body.picture));


          },
          response =>{
            console.log("ERROR ", response);
          },
          () =>{
            console.log("picture set")

          })
        });
        this.commentCover[this.comment.name] = this.helper;
      }

    }
  }

  show(picture : any)
  {
    this.showModal = true;
    this.picture = picture;
  }

  hide()
  {
    this.showModal = false;
  }

}



