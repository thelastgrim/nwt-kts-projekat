import {ComponentFixture, flush, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By, DomSanitizer } from '@angular/platform-browser';
import { DebugElement, Sanitizer } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CulturalOfferCoverComponent } from './cultural-offer-cover.component';
import { Constants } from 'src/app/app.constants';
import { PictureService } from 'src/app/services/picture-service/picture.service';


describe('CulturalOfferCoverComponent', () => {
    let component: CulturalOfferCoverComponent;
    let fixture: ComponentFixture<CulturalOfferCoverComponent>;
    ///servisi koje komponenta koristi
    let pictureService: any;



    beforeEach(() => {
        let pic = Constants.IMAGE_FILE;
        
        let pictureServiceMock = {
            getPicture : jasmine.createSpy('getPicture')
                .and.returnValue(of({body :{pic}}))
        }

        TestBed.configureTestingModule({
            declarations: [ CulturalOfferCoverComponent ],
            imports: [FormsModule],
            providers:    [ 
                            {provide: PictureService, useValue: pictureServiceMock}]
         });

         fixture = TestBed.createComponent(CulturalOfferCoverComponent);
         component = fixture.componentInstance;
         pictureService = TestBed.inject(PictureService)      

    });

    it('should initialize image on init', fakeAsync(() => {
    
       component.culturalOffer = {cover:{id:1}}
       fixture.detectChanges();
       component.ngOnInit()
       flush()
       expect(pictureService.getPicture).toHaveBeenCalled()

    
    }));

    it('should show image', fakeAsync(() => {
        let f : File 
        component.show(f)
        fixture.detectChanges()
        expect(component.picture).toEqual(f)
        expect(component.showModal).toEqual(true)

     }));

     it('should hide image', fakeAsync(() => {
        component.hide()
        fixture.detectChanges()
        expect(component.showModal).toEqual(false)

     }));


});
