import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from '../../model/category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CulturalOffer } from '../../model/cultural-offer';
import { Picture } from '../../model/picture';
import { Location} from '../../model/location';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CulturalOfferAdd } from '../../model/cultural-offer-add';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cultural-offer-add',
  templateUrl: './cultural-offer-add.component.html',
  styleUrls: ['./cultural-offer-add.component.scss']
})

export class CulturalOfferAddComponent{

  data:any;
  keyword = 'Title';
  errorMsg: string;
  isLoadingResult: boolean;

  public imagePath;
  imgURL: any;
  public message: string;

  alert = false
  images = [];
  names = []

  myForm = new FormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    file: new FormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });

  userRole = localStorage.getItem('role')||'none';

  authorization : any;
  closeResult = '';

  @Input ()
  categories!: Category[];

  @ViewChild('labelImport')
  labelImport!: ElementRef;

  selectedValue = 'Museums';
  selectedCategory = new Category();

  formImport!: FormGroup;
  fileToUpload: File = new File(["foo"],"Noimage.jpg");

  newCulturalOffer = new CulturalOfferAdd();

  constructor(private modalService : NgbModal,
    private culturalOfferService: CulturalOfferServiceService,
    private httpClient : HttpClient,
    private router : Router
    ) {
    this.formImport = new FormGroup({
      importFile: new FormControl('', Validators.required)
    });

    }

  ngOnInit(): void {

  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  selectCategory(selectedCategory: Category){
    this.selectedCategory = selectedCategory;
    this.selectedValue = selectedCategory.name;
  }

  preview(files) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  onFileChange(event) {
    this.images = [];
    this.names = []
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;

        for (let index = 0; index < filesAmount; index++) {
          this.names.push(event.target.files[index].name)
        }

        if(filesAmount > 5){
          alert("Maximum number of pictures is 5.")
        }else{


          for (let i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = (event:any) => {

                    this.images.push(event.target.result);

                    this.myForm.patchValue({
                        fileSource: this.images
                    });
                  }

                  reader.readAsDataURL(event.target.files[i]);
          }
      }
    }
  }

  public dataURItoBlob(dataURI:string) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
 }

  save(){

    this.httpClient.get("https://nominatim.openstreetmap.org/search/" +
    this.newCulturalOffer.street + "%20" +
    this.newCulturalOffer.city + "%20" +
    this.newCulturalOffer.country +
    "?format=json&addressdetails=1&limit=1&polygon_svg=1"
    )
    .subscribe(data => {
    
   
      if (data['Search'] == undefined) {
        this.data = [];
        this.errorMsg = data['Error'];

      this.newCulturalOffer.lat = data[0].lat;
      this.newCulturalOffer.lng = data[0].lon; //LON?????????
      
     

    // for (let index = 0; index < 1; index++) {
      const imageName = this.names[0];

      let base64 = this.images[0].split(",", "2")[1]
      const imageBlob = this.dataURItoBlob(base64);
      const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

      let formData:FormData = new FormData();

      formData.append('name',this.newCulturalOffer.name);
      formData.append('about',this.newCulturalOffer.about);
      formData.append("lng", this.newCulturalOffer.lng.toString());
      formData.append('lat', this.newCulturalOffer.lat.toString());
      formData.append('country',this.newCulturalOffer.country);
      formData.append('city',this.newCulturalOffer.city);
      formData.append('street',this.newCulturalOffer.street);
      formData.append('category', this.selectedValue);
      formData.append('picture', imageFile);
     
      this.culturalOfferService.saveCulturalOffer(formData).subscribe(
        (data) =>{

          this.showOffer(data.body.id);
        },(error : HttpErrorResponse)=>{
          alert("Cultural offer with that name, or on that location already exists. Try again!");
        });
    // } if adding more than one picture
      } else {
        console.log(data);
      }
      this.isLoadingResult = false;
    });
  }

  showOffer(offerId : number){
    this.router.navigate(['/offer/'+ offerId]);
  }
}
