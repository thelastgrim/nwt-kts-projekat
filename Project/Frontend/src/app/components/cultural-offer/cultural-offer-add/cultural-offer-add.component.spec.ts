import { HttpClient } from '@angular/common/http';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { DebugElement } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { ComponentFixture, discardPeriodicTasks, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { Constants } from 'src/app/app.constants';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { Category } from '../../model/category';
import { CulturalOffer } from '../../model/cultural-offer';
import { CulturalOfferAdd } from '../../model/cultural-offer-add';
import { Location } from '../../model/location';
import { Picture } from '../../model/picture';

import { CulturalOfferAddComponent } from './cultural-offer-add.component';

describe('CulturalOfferAddComponent', () => {
  let component: CulturalOfferAddComponent;
  let fixture: ComponentFixture<CulturalOfferAddComponent>;
  let httpClient : any;;
  let culuralOfferService : any;
  let modalService : any;
  let router: any;

  beforeEach(() => {
    spyOn(window, 'alert');

    const blob = new Blob(['aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o'], { type: 'image/png' });
    blob[`lastModifiedDate`] = '';
    blob[`name`] = 'photo';
    const fakeFile = blob as File;


    let offerMock = new CulturalOffer();
    let locationMock = new Location(19.8471935, 45.2444398, false, "Srbija",  "Novi Sad", "Fruskogorska" );
    let pictureMock = new Picture(fakeFile);
    offerMock.id = 1;
    offerMock.name = "Gallery1";
    offerMock.about = "adfas";
    offerMock.averageRating = 0;
    offerMock.categoryName = "Gallery";
    offerMock.location = locationMock;
    offerMock.comments = [];
    offerMock.cover = pictureMock;
    offerMock.news = [];

    let cultrualOfferServiceMock = {
      selectCategory: jasmine.createSpy('selectCategory')
        .and.returnValue(of()),
      saveCulturalOffer : jasmine.createSpy('saveCulturalOffer')
        .and.returnValue(of({body :{id:1}}))
    };



   

    let routerMock = {
      navigate: jasmine.createSpy('navigate')
    };


    let mockLocation = [
      {
      lat:45.2444398,
      lon:19.8471935
    }, 
    {
      lat:45.2444398,
      lon:19.8471935
    }];

    let httpServiceMock = {
      get : jasmine.createSpy('get')
        .and.returnValue(of(mockLocation))
    };

    let modalServiceMock  = {
      open: jasmine.createSpy('open')
        .and.returnValue(of()),
      dismissAll : jasmine.createSpy('dismissAll')
        .and.returnValue(of())
    };

    TestBed.configureTestingModule({
       declarations: [ CulturalOfferAddComponent ],
       imports: [FormsModule],
       providers:    [
         {provide: CulturalOfferServiceService, useValue: cultrualOfferServiceMock },
         { provide : NgbModal, useValue : modalServiceMock},
         { provide : HttpClient, useValue : httpServiceMock},
         { provide: Router, useValue: routerMock }
       ]
    });

    fixture = TestBed.createComponent(CulturalOfferAddComponent);
    component = fixture.componentInstance;
    culuralOfferService = TestBed.inject(CulturalOfferServiceService);
    modalService = TestBed.inject(NgbModal);
    httpClient = TestBed.inject(HttpClient);
    router = TestBed.inject(Router);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal dialog', fakeAsync(() => {
    let contet = TemplateRef
    component.open(contet);
    expect(modalService.open).toHaveBeenCalled();
  }));

  it('should change category', fakeAsync(() => {
    let choosedCategory = new Category();
      choosedCategory.name = "Gallery";
      choosedCategory.id = 2;
      choosedCategory.deleted = false;
      choosedCategory.culturalOffers = [];

    component.selectedCategory.name = "Museums";
    component.selectedCategory.id = 1;
    component.selectedCategory.deleted = false;
    component.selectedCategory.culturalOffers = [];

    component.selectedValue = 'Museums';

    component.selectCategory(choosedCategory);

    expect(component.selectedCategory.name).toBe("Gallery");
    expect(component.selectedCategory.id).toBe(2);
    expect(component.selectedCategory.deleted).toBeFalsy;
    expect(component.selectedCategory.culturalOffers).toBeNull;

    expect(component.selectedValue).toBe("Gallery");
  }));

  it('should preview upload photo', fakeAsync(() => {
    const blob = new Blob(['aq2je90dja2xgnxcixdhghsz9032rjuwgha9e4yt8o'], { type: 'image/png' });
    blob[`lastModifiedDate`] = '';
    blob[`name`] = 'photo';
    const fakeFile = blob as File;

    let event ={
      target : {
          files : [fakeFile]
      }
  }
  component.preview(event.target.files);

  tick();

  fixture.detectChanges();
  expect(component.imagePath).toBe(event.target.files);

  }));

  it('should alert when number of pictures is more than 5', fakeAsync(() => {
    let event ={
        target : {
            files : ["fajl1", "fajl2", "fajl3","fajl2", "fajl3","fajl2", "fajl3"]
        }
    }

    component.onFileChange(event)
    expect(window.alert).toHaveBeenCalledWith('Maximum number of pictures is 5.');

  }));

  it('should save cultural offer and get on page /offer/:id', fakeAsync(() => {
    let offer = new CulturalOfferAdd();
    offer.id = 1015;
    offer.name = "Gallery1";
    offer.about = "adfas";
    offer.categoryName = "Gallery";
    offer.country = "Srbija";
    offer.city = "Novi Sad";
    offer.street = "Fruskogorska";
    component.newCulturalOffer = offer;

    // let base64 = Constants.IMAGE_FILE
    // const imageBlob = component.dataURItoBlob(base64);
    // const imageFile = new File([imageBlob], "NASLOV", { type: 'image/png' });

    // let event ={
    //   target : {
    //       files : [imageFile]
    //   }
    // }

    

    // component.names = [];
    // component.names.push(event.target.files[0].name);
    component.images.push("data:image/jpeg;base64,"+Constants.IMAGE_FILE)
    component.save();
    flush()
     expect(httpClient.get).toHaveBeenCalledWith("https://nominatim.openstreetmap.org/search/" +
     component.newCulturalOffer.street + "%20" +
     component.newCulturalOffer.city + "%20" +
     component.newCulturalOffer.country +
     "?format=json&addressdetails=1&limit=1&polygon_svg=1");

    let offerMock = new CulturalOffer();
    let locationMock = new Location(19.8471935, 45.2444398, false, "Srbija",  "Novi Sad", "Fruskogorska" );
    let pictureMock = new Picture();

    pictureMock.id = 1;
    pictureMock.title = "Gallery1";
    pictureMock.path = "/resources/NASLOV.png";
    offerMock.id = 1015;
    offerMock.name = "Gallery1";
    offerMock.about = "adfas";
    offerMock.averageRating = 0;
    offerMock.categoryName = "Gallery";
    offerMock.location = locationMock;
    offerMock.comments = [];
    offerMock.cover = pictureMock;
    offerMock.news = [];
    
    expect(culuralOfferService.saveCulturalOffer).toHaveBeenCalled();

    fixture.whenStable()
    .then(() => {
        fixture.detectChanges();
        //expect(culuralOfferService.saveCulturalOffer).toEqual(offerMock);
        fixture.detectChanges(); // synchronize HTML with component data
        expect(router.navigate).toHaveBeenCalled();
    });

  }));
});
