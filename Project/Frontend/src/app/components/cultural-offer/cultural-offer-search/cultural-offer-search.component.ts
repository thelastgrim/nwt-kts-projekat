import { HttpErrorResponse } from '@angular/common/http';
import { Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOffer } from '../../model/cultural-offer';

@Component({
  selector: 'app-cultural-offer-search',
  templateUrl: './cultural-offer-search.component.html',
  styleUrls: ['./cultural-offer-search.component.scss']
})

export class CulturalOfferSearchComponent implements OnInit {

  name : string;
  @Output() newItemEvent = new EventEmitter<string>();

  constructor(){}

  ngOnInit(): void {

  }

  reset(){
    this.newItemEvent.emit('');
    this.name = null;
  }
}
