import {ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { ActivatedRouteStub } from 'src/app/testing/router-stubs';
import { NgbModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { CulturalOfferSearchComponent } from './cultural-offer-search.component';


describe('CulturalOfferSearchComponent', () => {
  let component: CulturalOfferSearchComponent;
  let fixture: ComponentFixture<CulturalOfferSearchComponent>;

  beforeEach(() => {


    TestBed.configureTestingModule({
       declarations: [ CulturalOfferSearchComponent ],
       imports: [FormsModule],
       providers:    [  ]
    });

    fixture = TestBed.createComponent(CulturalOfferSearchComponent);
    component = fixture.componentInstance;

  });

  it('should emit on click', () => {
    spyOn(component.newItemEvent, 'emit');
    component.reset()
    expect(component.newItemEvent.emit).toHaveBeenCalled();
    expect(component.newItemEvent.emit).toHaveBeenCalledWith('');
});
});
