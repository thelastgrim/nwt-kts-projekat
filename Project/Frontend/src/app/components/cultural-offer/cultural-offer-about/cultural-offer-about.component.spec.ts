/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';

import { CulturalOfferAboutComponent } from './cultural-offer-about.component';
import { CulturalOffer } from '../../model/cultural-offer';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ngbCarouselTransitionOut } from '@ng-bootstrap/ng-bootstrap/carousel/carousel-transition';

describe('CulturalOfferAboutComponent', () => {
  let component: CulturalOfferAboutComponent;
  let fixture: ComponentFixture<CulturalOfferAboutComponent>;

  let culturalOfferService : any;
  let router : any;
  let route : any;

  beforeEach(() => {
    let culturalOffer : CulturalOffer = {
          "id": 1,
          "name": "Willms, Sanford and Toy",
          "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
          "averageRating": 4.0,
          "categoryName": "Museums",
          "deleted": false,
          "comments": [],
          "news": [],
          "cover": {
              "id": 1,
              "title": "Thrive",
              "path": "src//main//resources//pictures//museum.jpg",
          },
          "location": {
              "city": "Penang",
              "country": "Indonesia",
              "street": "913 Sycamore Point",
              "lng": 100.3091,
              "lat": 5.4356367
          },
    }
    let culturalOfferServiceMock = {
      getOneOffer : jasmine.createSpy('getOneOffer').and.returnValue(of({body : culturalOffer})),
    }

    let routerMock = {
      navigate : jasmine.createSpy('navigate'),
      routeReuseStrategy : jasmine.createSpy('routeReuseStrategy.shouldReuseRoute'),
      onSameUrlNavigation : jasmine.createSpy('onSameUrlNavigation'),
    }

    TestBed.configureTestingModule({
       declarations: [ CulturalOfferAboutComponent ],
       imports: [],
       providers:    [ {provide: CulturalOfferServiceService, useValue: culturalOfferServiceMock },
        {provide : Router, useValue: routerMock},
        {provide : ActivatedRoute, useValue : { snapshot: { paramMap: {get:(offerId:number)=>{offerId:1}}}}},
       ]
    });

    fixture = TestBed.createComponent(CulturalOfferAboutComponent);
    component = fixture.componentInstance;
    culturalOfferService = TestBed.inject(CulturalOfferServiceService);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch offer on init' ,fakeAsync(() => {

    component.ngOnInit();

    expect(component.offerId).toBeDefined();
    expect(component.showAbout).toBeDefined();
    expect(component.showComments).toBeDefined();
    expect(component.showNews).toBeDefined();
    expect(component.showSubscribtion).toBeDefined();

    expect(culturalOfferService.getOneOffer).toHaveBeenCalled();

    expect(component.culturalOffer.id).toBe(1);
    expect(component.culturalOffer.name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffer.about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffer.averageRating).toBe(4);
    expect(component.culturalOffer.categoryName).toBe("Museums");
    expect(component.culturalOffer.location.city).toBe("Penang");
    expect(component.culturalOffer.location.country).toBe("Indonesia");
    expect(component.culturalOffer.location.street).toBe("913 Sycamore Point");
    expect(component.culturalOffer.location.lat).toBe(5.4356367);
    expect(component.culturalOffer.location.lng).toBe(100.3091);
    expect(component.culturalOffer.deleted).toBe(false);
    expect(component.culturalOffer.cover.title).toBe("Thrive");
    expect(component.culturalOffer.cover.id).toBe(1);
    expect(component.culturalOffer.cover.path).toBe("src//main//resources//pictures//museum.jpg");
    expect(component.culturalOffer.comments.length).toBe(0);
    expect(component.culturalOffer.news.length).toBe(0);
    expect(component.culturalOffers.length).toBe(1);

    fixture.detectChanges();
    tick();
    let offerName = fixture.debugElement.query(By.css('label'));
    expect(offerName.nativeElement.innerHTML).toBe("Willms, Sanford and Toy");
    let offerElements = fixture.debugElement.queryAll(By.css('.card-text'));
    expect(offerElements[1].nativeElement.innerHTML).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(offerElements[0].nativeElement.innerHTML).toBe(`Address: ${component.culturalOffer.location.street}, ${component.culturalOffer.location.city}, ${component.culturalOffer.location.country}`);

  }));

  it('should refresh page upon event ', fakeAsync(()=>{

    let event = {
      target : {
        refresh : true
      }
    };

    let culturalOffer : CulturalOffer = {
          "id": 1,
          "name": "Willms, Sanford and Toy",
          "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
          "averageRating": 4.0,
          "categoryName": "Museums",
          "deleted": false,
          "comments": [],
          "news": [],
          "cover": {
              "id": 1,
              "title": "Thrive",
              "path": "src//main//resources//pictures//museum.jpg",
          },
          "location": {
              "city": "Penang",
              "country": "Indonesia",
              "street": "913 Sycamore Point",
              "lng": 100.3091,
              "lat": 5.4356367
          }
    };

    component.culturalOffer = culturalOffer;
    component.refreshPage(event.target.refresh);
    expect(router.navigate).toHaveBeenCalledWith(['/offer/'+ culturalOffer.id]);
  }));

  it('should navigate to homepage after it is deleted', fakeAsync(()=>{

    let event = {
      target : {
        leave : true
      }
    };
    component.leavePage(event.target.leave);
    expect(router.navigate).toHaveBeenCalledWith(['/content']);
  }));

});
