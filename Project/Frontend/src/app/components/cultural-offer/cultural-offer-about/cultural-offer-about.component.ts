import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap/rating/rating-config';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Constants } from 'src/app/app.constants';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOffer } from '../../model/cultural-offer';

@Component({
  selector: 'app-cultural-offer-about',
  templateUrl: './cultural-offer-about.component.html',
  styleUrls: ['./cultural-offer-about.component.scss']
})
export class CulturalOfferAboutComponent implements OnInit {

  culturalOffer : CulturalOffer;
  culturalOffers! : CulturalOffer[];
  showAbout : boolean;
  showNews: boolean;
  showComments: boolean;
  showSubscribtion : boolean;

  offerId : number;

  userRole = localStorage.getItem('role')||'none';

  constructor(private router : Router, private culturalOfferService : CulturalOfferServiceService, private route : ActivatedRoute) {
    this.offerId = +this.route.snapshot.paramMap.get('offerId');
    this.showAbout = false;
    this.showNews = false;
    this.showComments = false;
    this.showSubscribtion = false;

  }

  ngOnInit() {
     this.culturalOfferService.getOneOffer(this.offerId)
    .pipe(
      catchError( err => {
        alert("Cultural offer with that id doesn't exist!");
        this.router.navigate(['content']);
        console.log('Handling error locally and rethrowing it...', err);
        return throwError(err);
      })
    )
    .subscribe(data => {
      this.culturalOffers = [];
      this.culturalOffer = data.body;
      console.log(data.body);
      console.log(this.culturalOffer);
      this.culturalOffers.push(this.culturalOffer);
      this.showAbout=true;
    });
    window.scroll(0,0);
  }

  refreshPage(refresh : boolean){
    if(refresh){
      console.log( "AAAAAAAA");
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/offer/'+this.culturalOffer.id]);
    }
  }

  leavePage(leave : boolean){
    if(leave){
      this.router.navigate(['/content']);
    }
  }

}
