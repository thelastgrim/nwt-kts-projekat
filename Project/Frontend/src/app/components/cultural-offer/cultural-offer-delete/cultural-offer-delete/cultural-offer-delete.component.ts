import { Input, Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CulturalOffer } from 'src/app/components/model/cultural-offer';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';

@Component({
  selector: 'app-cultural-offer-delete',
  templateUrl: './cultural-offer-delete.component.html',
  styleUrls: ['./cultural-offer-delete.component.scss']
})
export class CulturalOfferDeleteComponent implements OnInit {

  @Input() culturalOffer: CulturalOffer;

  @Output() refreshEvent = new EventEmitter<boolean>();

  beingDeleted : Boolean = false;

  constructor(private culturalOfferService : CulturalOfferServiceService) { }

  ngOnInit() {
  }

  deleteCulturalOffer(){
    if(confirm("Are you sure you want delete "+this.culturalOffer.name+"?")) {
      this.beingDeleted = true;
      this.culturalOfferService.delete(this.culturalOffer.id).subscribe(()=>{
        this.beingDeleted = false;
        this.refreshEvent.emit(true);
      });
    }
  }

}
