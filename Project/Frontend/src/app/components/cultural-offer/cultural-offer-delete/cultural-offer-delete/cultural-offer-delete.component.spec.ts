import {ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'

import { of } from 'rxjs';

import { AuthService } from 'src/app/services/auth/auth.service';
import { CulturalOfferDeleteComponent } from './cultural-offer-delete.component';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOffer } from 'src/app/components/model/cultural-offer';

describe('CulturalOfferDeleteComponent', () => {
    let component: CulturalOfferDeleteComponent;
    let fixture: ComponentFixture<CulturalOfferDeleteComponent>;

    ///servisi koje komponenta koristi
    let culturalOfferService: any;


    beforeEach(() => {

        spyOn(window, 'confirm').and.callFake(function () {
            return true;
        });
   
        let culturalOfferServiceMock = {
          delete: jasmine.createSpy('delete')
              .and.returnValue(of()),    
        }

        TestBed.configureTestingModule({
            declarations: [ CulturalOfferDeleteComponent ],
            imports: [FormsModule],
            providers:    [ {provide:CulturalOfferServiceService, useValue: culturalOfferServiceMock }]
         });

         fixture = TestBed.createComponent(CulturalOfferDeleteComponent);
         component = fixture.componentInstance;
         culturalOfferService = TestBed.inject(CulturalOfferServiceService);

    });

    it('should prompt admin before deleted an offer ', fakeAsync(() => {
        let culturalOffer = new CulturalOffer;
        culturalOffer.name = "TEST CU"
        culturalOffer.id = 1

        component.culturalOffer = culturalOffer;
        component.deleteCulturalOffer()
        expect(window.confirm).toHaveBeenCalledWith('Are you sure you want delete TEST CU?');
        
    
    }));

  it('should delete selected cultural offer', fakeAsync(() => {
        let culturalOffer = new CulturalOffer;
        culturalOffer.name = "TEST CU"
        culturalOffer.id = 1

        component.culturalOffer = culturalOffer;

        component.deleteCulturalOffer()
        tick()

        expect(culturalOfferService.delete).toHaveBeenCalled()
   
  }));
});
