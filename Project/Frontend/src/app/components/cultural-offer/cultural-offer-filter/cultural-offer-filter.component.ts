import { Component,OnInit} from '@angular/core';
import { Subject, Observable, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { Category } from '../../model/category';
import { CulturalOffer } from '../../model/cultural-offer';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-cultural-offer-filter',
  templateUrl: './cultural-offer-filter.component.html',
  styleUrls: ['./cultural-offer-filter.component.scss'],
  providers: [NgbTypeaheadConfig]
})
export class CulturalOfferFilterComponent implements OnInit {


  page : number;

  collectionSize : number;
  pageSize : number;

  city! : string;
  category! : string;

  culturalOffers! : CulturalOffer[];
  categoryNames!: String[];
  categoryIds!: Object;
  cities! : String[];

  showCat: boolean;
  showCity: boolean;

  focusOnCatList : boolean;
  focusOnCityList : boolean;

  public model: any;

  constructor(private culturalOfferService : CulturalOfferServiceService, private categoryService : CategoryServiceService) {
    this.showCat = false;
    this.showCity = false;
    this.focusOnCatList = false;
    this.focusOnCityList = false;
    this.categoryNames = [];
    this.pageSize = 6;
    this.page = 1;
    this.collectionSize = 100;
    this.categoryIds = {};
  }


  changePage(){
    if(this.city){
      this.write(this.city);
    }else if(this.category){
      this.write(this.category);
    }else{
      this.culturalOfferService.getAllByPage(this.page - 1).subscribe(data =>{
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }
  }

  ngOnInit() {
    console.log("FILTER NG ON INIT");
      this.culturalOfferService.getAllUniqueCities().subscribe(data => {
        this.cities = data.body;
        console.log(this.cities);
      });

      this.categoryService.getAllCategories().subscribe( data => {
        data.body.forEach(value => {
          this.categoryNames.push(value.name);
          this.categoryIds[value.name] = value.id;
        });
      });

      this.culturalOfferService.getAllByPage(this.page - 1).subscribe(data =>{
        this.culturalOffers = data.body.content;
        this.pageSize = data.body.content.length;
        this.collectionSize = data.body.totalElements;
      });
    }

  checkCity( city: String){
    if(this.city !== city){
      this.page = 1;
    }
  }

  checkCategory(catName : String){
    if(this.category !== catName){
      this.page = 1;
    }
  }

  write(c : string){
    console.log("FILTER WRITE");

    if(this.cities.includes(c)){
      this.checkCity(c);
      this.city = c;
    }else if(this.categoryNames.includes(c)){
      this.checkCategory(c);
      this.category = c;
    }

    if(this.city && this.category){
      console.log("OBAAA");
      let categoryId = this.categoryNames.indexOf(this.category);
      this.culturalOfferService.getPageOfferByCityAndCat(this.city, this.categoryIds[this.category], this.page - 1).subscribe(data =>{
        console.log(data);
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }else if(this.city){
      console.log("GRAD: " + this.city);
      this.culturalOfferService.getPageOffersByCity(this.city, this.page - 1).subscribe(data => {
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }else if(this.category){
      console.log("KAT: " + this.category);
      let categoryId = this.categoryNames.indexOf(this.category);
      this.culturalOfferService.getPageOffersByCategory(this.categoryIds[this.category], this.page - 1).subscribe(data => {
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      })
    }else{
      console.log("Nista");
    }

  }

  remove(toRemove : string){
    console.log("Uso sam");
    this.page = 1;
    if(this.cities.includes(toRemove) && this.category){
      console.log("1 - brisi grad, ostavi kat");
      this.city = '';
      let categoryId = this.categoryNames.indexOf(this.category);

      this.culturalOfferService.getPageOffersByCategory(this.categoryIds[this.category], this.page - 1).subscribe(data => {
        console.log(data);
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }else if(this.categoryNames.includes(toRemove) && this.city){
      console.log("2 - brisi kat, ostavi grad");
      this.category = '';
      this.culturalOfferService.getPageOffersByCity(this.city, this.page - 1).subscribe(data => {
        console.log(data);
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }else if((this.cities.includes(toRemove) && !this.category) || (this.categoryNames.includes(toRemove) && !this.city)){
      this.city = '';
      this.category = '';
      console.log("3 - brisi sve");
      this.culturalOfferService.getAllByPage().subscribe(data =>{
        console.log(data);
        this.culturalOffers = data.body.content;
        this.collectionSize = data.body.totalElements;
        this.pageSize = data.body.content.length;
      });
    }else{
      console.log("Nistaaa");
    }
  }

  removeAll(){
    this.city = '';
    this.category = '';
    console.log("3 - brisi sve");
    this.page = 1;
    this.culturalOfferService.getAllByPage(this.page - 1).subscribe(data =>{
      console.log(data);
      this.culturalOffers = data.body.content;
      this.collectionSize = data.body.totalElements;
      this.pageSize = data.body.content.length;
      });
  }
}
