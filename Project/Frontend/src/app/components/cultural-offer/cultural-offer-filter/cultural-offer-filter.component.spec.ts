/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';

import { CulturalOffer } from '../../model/cultural-offer';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ngbCarouselTransitionOut } from '@ng-bootstrap/ng-bootstrap/carousel/carousel-transition';
import { CulturalOfferFilterComponent } from './cultural-offer-filter.component';
import { Page } from '../../model/page';
import { Category } from '../../model/category';
import { CategoryServiceService } from 'src/app/services/category-service/category-service.service';
import { Content } from '@angular/compiler/src/render3/r3_ast';

describe('CulturalOfferFilterComponent', () => {
  let component: CulturalOfferFilterComponent;
  let fixture: ComponentFixture<CulturalOfferFilterComponent>;

  let culturalOfferService : any;
  let categoryService : any;

  beforeEach(() => {
    let pageOffer : Page<CulturalOffer> = {
      "content": [
        {
            "id": 1,
            "name": "Willms, Sanford and Toy",
            "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
            "averageRating": 4.0,
            "categoryName": "Museums",
            "deleted": false,
            "comments": [],
            "news": [],
            "cover": {
                "id": 1,
                "title": "Thrive",
                "path": "src//main//resources//pictures//museum.jpg",
            },
            "location": {
                "city": "Penang",
                "country": "Indonesia",
                "street": "913 Sycamore Point",
                "lng": 100.3091,
                "lat": 5.4356367
            },
        },
    ],
    "pageable": {
        "sort": {
            "sorted": false,
            "unsorted": true,
        },
        "offset": 0,
        "pageSize": 2,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalElements": 2,
    "last": false,
    "totalPages": 1,
    "size": 2,
    "number": 0,
    "sort": {
        "sorted": false,
        "unsorted": true,
    },
    "numberOfElements": 2,
    "first": true,
    };
    let pageOffers : Page<CulturalOffer> = {
      "content": [
        {
            "id": 1,
            "name": "Willms, Sanford and Toy",
            "about": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.",
            "averageRating": 4.0,
            "categoryName": "Museums",
            "deleted": false,
            "comments": [],
            "news": [],
            "cover": {
                "id": 1,
                "title": "Thrive",
                "path": "src//main//resources//pictures//museum.jpg",
            },
            "location": {
                "city": "Penang",
                "country": "Indonesia",
                "street": "913 Sycamore Point",
                "lng": 100.3091,
                "lat": 5.4356367
            },
        },
        {
            "id": 2,
            "name": "McLaughlin-Nader",
            "about": "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.",
            "averageRating": 3.0,
            "categoryName": "Museums",
            "deleted": false,
            "comments": [],
            "news": [],
            "cover": {
                "id": 1,
                "title": "Thrive",
                "path": "src//main//resources//pictures//museum.jpg",
            },
            "location": {
                "city": "Kristiansand S",
                "country": "Norway",
                "street": "18 Maple Wood Crossing",
                "lng": 10.7533605,
                "lat": 59.9384597
            },
        }
    ],
    "pageable": {
        "sort": {
            "sorted": false,
            "unsorted": true,
        },
        "offset": 0,
        "pageSize": 2,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalElements": 2,
    "last": false,
    "totalPages": 1,
    "size": 2,
    "number": 0,
    "sort": {
        "sorted": false,
        "unsorted": true,
    },
    "numberOfElements": 2,
    "first": true,
    };

    let cities : String[] = ["Kristiansand S", "Penang"];

    let categories : Category[] = [
      {
        "id" : 1,
        "name" : "Museums",
        "deleted" : false,
        "culturalOffers" : []
      }
    ]
    let culturalOfferServiceMock = {
      getPageOfferByCityAndCat : jasmine.createSpy('getPageOfferByCityAndCat').and.returnValue(of({body : pageOffer})),
      getPageOffersByCategory : jasmine.createSpy('getPageOffersByCategory').and.returnValue(of({body : pageOffers})),
      getPageOffersByCity : jasmine.createSpy('getPageOffersByCity').and.returnValue(of({body : pageOffer})),
      getAllByPage : jasmine.createSpy('getAllByPage').and.returnValue(of({body : pageOffers})),
      getAllUniqueCities : jasmine.createSpy('getAllUniqueCities').and.returnValue(of({body : cities})),
    }

    let categoryServiceMock = {
      getAllCategories : jasmine.createSpy('getAllCategories').and.returnValue(of({body : categories})),
    }


    TestBed.configureTestingModule({
       declarations: [ CulturalOfferFilterComponent ],
       imports: [],
       providers:    [ {provide: CulturalOfferServiceService, useValue: culturalOfferServiceMock },
        {provide : CategoryServiceService, useValue: categoryServiceMock},

       ]
    });

    fixture = TestBed.createComponent(CulturalOfferFilterComponent);
    component = fixture.componentInstance;
    culturalOfferService = TestBed.inject(CulturalOfferServiceService);
    categoryService = TestBed.inject(CategoryServiceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should should fetch unique cities, categories, and offers (by page) on init', () => {
    component.ngOnInit();

    expect(component.showCat).toBeDefined();
    expect(component.showCity).toBeDefined();
    expect(component.focusOnCatList).toBeDefined();
    expect(component.focusOnCityList).toBeDefined();
    expect(component.pageSize).toBeDefined();
    expect(component.page).toBeDefined();
    expect(component.collectionSize).toBeDefined();
    expect(component.categoryNames).toBeDefined();
    expect(component.categoryIds).toBeDefined();

    expect(culturalOfferService.getAllUniqueCities).toHaveBeenCalled;
    expect(categoryService.getAllCategories).toHaveBeenCalled;
    expect(culturalOfferService.getAllByPage).toHaveBeenCalledWith(component.page - 1);

    expect(component.categoryNames[0]).toBe("Museums");

    expect(component.cities[0]).toBe("Kristiansand S");
    expect(component.cities[1]).toBe("Penang");

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");

    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");

    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");

    expect(component.culturalOffers[0].categoryName).toBe("Museums");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);
  });

  it('should should fetch page filtered by city', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.write("Penang");
    expect(component.city).toBe("Penang");
    expect(culturalOfferService.getPageOffersByCity).toHaveBeenCalledWith(component.city, component.page-1);

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(1);
    expect(component.page).toBe(1);

  }));

  it('should should fetch page filtered by category', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.write("Museums");
    expect(component.category).toBe("Museums");
    expect(culturalOfferService.getPageOffersByCategory).toHaveBeenCalledWith(component.categoryIds["Museums"], component.page-1);

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");


    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);

  }));

  it('should should fetch page filtered by city and category', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.city = "Penang";
    component.write("Museums");
    expect(component.category).toBe("Museums");
    expect(culturalOfferService.getPageOfferByCityAndCat).toHaveBeenCalledWith(component.city, component.categoryIds["Museums"], component.page-1);

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(1);
    expect(component.page).toBe(1);

  }));

  it('should should fetch nothing because selected string is not in cities or categoryNames', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.city = "Penang";
    component.category = "Museums"
    component.write("Something");
    expect(component.city).toBe("Penang");
    expect(component.category).toBe("Museums");
  }));

  it('should check if city filter changed and set page to one if yes', fakeAsync(() => {

    component.city = "Penang";
    component.page = 5;
    component.checkCity("Kristiansand S");
    expect(component.page).toBe(1);
  }));

  it('should check if category filter changed and set page to one if yes', fakeAsync(() => {

    component.category = "Museums"
    component.page = 5;
    component.checkCategory("Teathers");
    expect(component.page).toBe(1);

  }));

  it('should remove city filter in case category filter is also chosen', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.city = "Penang";
    component.category = "Museums"
    component.page = 5;

    component.remove("Penang");
    expect(component.page).toBe(1);
    expect(culturalOfferService.getPageOffersByCategory).toHaveBeenCalledWith(component.categoryIds["Museums"], component.page-1);
    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);
  }));

  it('should remove category filter in case city filter is also chosen', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.city = "Penang";
    component.category = "Museums"
    component.page = 5;

    component.remove("Museums");
    expect(component.page).toBe(1);
    expect(culturalOfferService.getPageOffersByCity).toHaveBeenCalledWith(component.city, component.page-1);

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(1);
    expect(component.page).toBe(1);

  }));

  it('should remove category filter in case city filter is only chosen', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.city = "Penang";
    component.page = 5;

    component.remove("Penang");
    expect(component.page).toBe(1);

    expect(culturalOfferService.getAllByPage).toHaveBeenCalledWith();

    expect(component.city).toBe('');
    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);

  }));

  it('should remove category filter in case category filter is only chosen', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.category = "Museums"
    component.page = 5;

    component.remove("Museums");

    expect(component.page).toBe(1);

    expect(culturalOfferService.getAllByPage).toHaveBeenCalledWith();

    expect(component.category).toBe('');

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);

  }));

  it('should remove all filters', fakeAsync(() => {

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.city = "Penang";
    component.categoryIds["Museums"] = 1;
    component.category = "Museums"
    component.page = 5;

    component.removeAll();
    expect(component.page).toBe(1);
    expect(component.city).toBe('');
    expect(component.category).toBe('');

    expect(culturalOfferService.getAllByPage).toHaveBeenCalledWith(component.page - 1);

    expect(component.culturalOffers[0].name).toBe("Willms, Sanford and Toy");
    expect(component.culturalOffers[0].about).toBe("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.");
    expect(component.culturalOffers[0].location.city).toBe("Penang");
    expect(component.culturalOffers[0].categoryName).toBe("Museums");

    expect(component.culturalOffers[1].name).toBe("McLaughlin-Nader");
    expect(component.culturalOffers[1].about).toBe("Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    expect(component.culturalOffers[1].location.city).toBe("Kristiansand S");
    expect(component.culturalOffers[1].categoryName).toBe("Museums");

    expect(component.collectionSize).toBe(2);
    expect(component.pageSize).toBe(2);
    expect(component.page).toBe(1);

  }));

  it('should change page if neither filters are active', (()=>{

    component.page = 2;
    component.changePage();
    expect(culturalOfferService.getAllByPage).toHaveBeenCalledWith(component.page - 1);

  }));

  it('should change page if city filter is active', (()=>{

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.city = "Penang"
    component.page = 2;
    component.changePage();
    expect(culturalOfferService.getPageOffersByCity).toHaveBeenCalledWith(component.city, component.page - 1);

  }));

  it('should change page if category filter is active', (()=>{

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.category = "Museums"
    component.page = 2;
    component.changePage();
    expect(culturalOfferService.getPageOffersByCategory).toHaveBeenCalledWith(component.categoryIds["Museums"], component.page - 1);

  }));

  it('should change page if both category and city filters are active', (()=>{

    component.cities = ["Kristiansand S", "Penang"];
    component.categoryNames = ["Museums"];
    component.categoryIds["Museums"] = 1;
    component.category = "Museums"
    component.city = "Penang";
    component.page = 2;
    component.changePage();
    expect(culturalOfferService.getPageOfferByCityAndCat).toHaveBeenCalledWith(component.city,component.categoryIds["Museums"], component.page - 1);

  }));
});
