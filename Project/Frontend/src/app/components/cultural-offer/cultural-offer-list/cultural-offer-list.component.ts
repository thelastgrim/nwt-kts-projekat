import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges} from '@angular/core';
import { Constants } from 'src/app/app.constants';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOffer } from '../../model/cultural-offer';

import { Router } from '@angular/router';


@Component({
  selector: 'app-cultural-offer-list',
  templateUrl: './cultural-offer-list.component.html',
  styleUrls: ['./cultural-offer-list.component.scss']
})
export class CulturalOfferListComponent implements OnChanges {


  cover = null;

  userRole = localStorage.getItem('role')||'none';
  display = false;
  readonly pageSize = new Constants().PAGE_SIZE

  currentPage = 4; // izabrani broj na paginatoru
  collectionSize = 100;

  duzina = 0;

  //broj strana na paginatoru je collectionSize/pageSize

  @Input()
  currentCategory! : number;

  @Input()
  searchByName : string = "";

  culturalOffers: CulturalOffer[] = [];

  constructor(private router: Router,private culturalOfferService : CulturalOfferServiceService) {
    this.cover = "http://placehold.it/700x400"
  }


  ngOnChanges(): void  {
    this.currentPage = 1;
    if(this.userRole =='user'){
      this.showOffersUser();
    }else{ //ako je admin ili neulogovani korisnik
      this.showOffers();
    }
  }

  showOffers(){

    if(typeof(this.searchByName) === 'undefined' || this.searchByName === ''){
     
      if(this.currentCategory != 0){
        this.culturalOfferService.getPageOffersByCategory(this.currentCategory, this.currentPage-1).subscribe((data) =>{
        
          this.culturalOffers = data.body.content;
          this.collectionSize = data.body.totalElements;
          });
      }

    }
    if(this.searchByName != ""){
        this.culturalOfferService.search(this.searchByName, this.currentPage-1).subscribe((data) =>{
            this.culturalOffers = data.body.content;
            this.collectionSize = data.body.totalElements;
            
          },(error : HttpErrorResponse)=>{
            
            alert("Cultural Offer with that name does not exist. Please check the spelling.")
          });
     
    }

  }

  showOffersUser(){
   
    if(typeof(this.searchByName) === 'undefined' || this.searchByName === ''){
    
      
      this.culturalOfferService.getPageOffersByCategoryAndUser(this.currentCategory, this.currentPage-1).subscribe((data) =>{
       
      this.culturalOffers = data.body.offers;
      this.collectionSize = data.body.collectionSize;
      });
    }
    if(this.searchByName != ""){
        this.culturalOfferService.search(this.searchByName, this.currentPage-1).subscribe((data) =>{
          this.culturalOffers = data.body.content;
          this.collectionSize = data.body.totalElements;
          },(error : HttpErrorResponse)=>{ 
            alert("Cultural Offer with that name does not exist. Please check the spelling.")
          });
    }

  }

  createImageFromBlob(picture: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () =>{
      if(reader.result){
        //this.cover = this.duzina!=0?reader.result:"http://placehold.it/700x400"
      }

    }, false);

    if(picture){
      reader.readAsDataURL(picture);
    }
  }


  onPageChange(page:number){
    /*
      'page' je broj iz paginatora 1, 2, 3....
       zato se setuje na page-1, da bi se dobavljanje
       stranice sa servera 0, 1, 2...
    */
   
    if(this.userRole =='user'){
      this.showOffersUser();
    }else{
      this.showOffers(); //prikazivanje svih ponuda pocevsi od nove stranice
    }
  }
  /* NIDJE SE NE KORISTI
  showCommentModal(){
    this.display = !this.display;
  }
  */

  showOffer(offerId : number){
    this.router.navigate(['/offer/'+ offerId]);
  }

  refreshList(emitted : boolean){
    if(emitted){
      this.ngOnChanges();
    }
  }
}
