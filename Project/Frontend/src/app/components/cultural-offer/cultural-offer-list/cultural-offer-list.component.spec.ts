import {ComponentFixture, flush, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { By, DomSanitizer } from '@angular/platform-browser';
import { DebugElement, Sanitizer } from '@angular/core'
import {async, fakeAsync, tick} from '@angular/core/testing';
import {FormsModule} from '@angular/forms'
import { of } from 'rxjs';

import { NewsService } from 'src/app/services/news-service/news.service';
import { NgbPaginationModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { CulturalOfferListComponent } from './cultural-offer-list.component';
import { CulturalOffer } from '../../model/cultural-offer';
import { Page } from '../../model/page';
import { CulturalOfferServiceService } from 'src/app/services/cultural-offer-service/cultural-offer-service.service';
import { CulturalOfferCoverComponent } from '../cultural-offer-cover/cultural-offer-cover.component';
import { HttpClientModule } from '@angular/common/http';
import { OffersForUser } from '../../model/offersForUser';


describe('CulturalOfferListComponent', () => {
    let component: CulturalOfferListComponent;
    let fixture: ComponentFixture<CulturalOfferListComponent>;
    ///servisi koje komponenta koristi
    let culturalOfferService: any;
    let router: any;
    let httpClient : any;;


    beforeEach(() => {

        let page: Page<CulturalOffer> = {
            content : [{
                id: 1,
                name : "TESTCU1",
                about : "ABOUT1",
                averageRating : 1,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 45,
                    lat : 46,
                    country : "SERBIA",
                    city : "NOVI SAD",
                    street :"GOGOLJEVA"
                },
                cover : {
                    id:1,
                    path: "/PIC",
                    title: "PIC1"
                },
                categoryName: "CAT1"
            },{
                id: 2,
                name : "TESTCU2",
                about : "ABOUT2",
                averageRating : 2,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 47,
                    lat : 48,
                    country : "SERBIA",
                    city : "NIS",
                    street :"GOGOLJEVA"
                },
                cover : {
                    id:1,
                    path: "/PIC",
                    title: "PIC1"
                },
                categoryName: "CAT1"
            }],
            pageable:{
                sort: {unsorted:true, sorted:false},
                pageSize: 10,
                pageNumber: 0,
                offset:0,
                unpaged:false,
                paged:true,
            },
            last: false,
            totalPages: 5,
            totalElements: 500,
            first: true,
            sort: {unsorted:true, sorted:false},
            numberOfElements: 2,
            size: 1,
            number: 1,

        }

        let customPage : OffersForUser = {
            offers: [
              {
                id: 1,
                name : "TESTCU1",
                about : "ABOUT1",
                averageRating : 1,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 45,
                    lat : 46,
                    country : "SERBIA",
                    city : "NOVI SAD",
                    street :"GOGOLJEVA"
                },
                cover : {
                    id:1,
                    path: "/PIC",
                    title: "PIC1"
                },
                categoryName: "CAT1"
              },
              {
                id: 2,
                name : "TESTCU2",
                about : "ABOUT2",
                averageRating : 2,
                deleted : false,
                comments: [],
                news : [],
                location : {
                    lng : 47,
                    lat : 48,
                    country : "SERBIA",
                    city : "NIS",
                    street :"GOGOLJEVA"
                },
                cover : {
                    id:1,
                    path: "/PIC",
                    title: "PIC1"
                },
                categoryName: "CAT1"
              }
          ],
          collectionSize : 2
          }

        //const mockErrorResponse = { status: 400, statusText: 'Bad Request' };

        //let alreadyCalled = false
        let culturaOfferServiceMock = {

            getPageOffersByCategory : jasmine.createSpy('getPageOffersByCategory')
                .and.returnValue(of({body :page})),
            search : jasmine.createSpy('search')
                .and.returnValue(of({body :page})),
            getPageOffersByCategoryAndUser : jasmine.createSpy('getPageOffersByCategoryAndUser')
                .and.returnValue(of({body : customPage})),
        }

        let routerMock = {
            navigate: jasmine.createSpy('navigate')
          };

        let httpCommonResponse = {
            isValid: true
        };

        let httpClientMock = {
            get: jasmine.createSpy('get')
            .and.returnValue(of(httpCommonResponse))
        }

        TestBed.configureTestingModule({
            declarations: [ CulturalOfferListComponent],
            imports: [FormsModule, NgbPaginationModule,NgbRatingModule],
            providers:    [
                            {provide: CulturalOfferServiceService, useValue: culturaOfferServiceMock},
                            {provide: Router, useValue: routerMock},
                            {provide: HttpClientModule, useValue: httpClientMock}]
         });

         fixture = TestBed.createComponent(CulturalOfferListComponent);
         component = fixture.componentInstance;
         culturalOfferService = TestBed.inject(CulturalOfferServiceService)
         httpClient = TestBed.inject(HttpClientModule)
         router = TestBed.inject(Router)

    });

    it('should show regular offers', fakeAsync(() => {
        component.currentCategory = 1
        component.userRole = 'user'
        component.showOffers()
        expect(culturalOfferService.getPageOffersByCategory).toHaveBeenCalled()



        expect(component.culturalOffers.length).toEqual(2)
        expect(component.collectionSize).toEqual(500)

        expect(component.culturalOffers[0].id).toEqual(1)
        expect(component.culturalOffers[0].name).toEqual("TESTCU1")
        expect(component.culturalOffers[0].about).toEqual("ABOUT1")
        expect(component.culturalOffers[0].averageRating).toEqual(1)
        expect(component.culturalOffers[0].comments.length).toEqual(0)
        expect(component.culturalOffers[0].news.length).toEqual(0)
        expect(component.culturalOffers[0].location.lng).toEqual(45)
        expect(component.culturalOffers[0].location.lat).toEqual(46)
        expect(component.culturalOffers[0].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[0].location.city).toEqual("NOVI SAD")
        expect(component.culturalOffers[0].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[0].cover.id).toEqual(1)
        expect(component.culturalOffers[0].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[0].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[0].categoryName).toEqual("CAT1")

        expect(component.culturalOffers[1].id).toEqual(2)
        expect(component.culturalOffers[1].name).toEqual("TESTCU2")
        expect(component.culturalOffers[1].about).toEqual("ABOUT2")
        expect(component.culturalOffers[1].averageRating).toEqual(2)
        expect(component.culturalOffers[1].comments.length).toEqual(0)
        expect(component.culturalOffers[1].news.length).toEqual(0)
        expect(component.culturalOffers[1].location.lng).toEqual(47)
        expect(component.culturalOffers[1].location.lat).toEqual(48)
        expect(component.culturalOffers[1].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[1].location.city).toEqual("NIS")
        expect(component.culturalOffers[1].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[1].cover.id).toEqual(1)
        expect(component.culturalOffers[1].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[1].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[1].categoryName).toEqual("CAT1")

        fixture.detectChanges()
        tick()

        let cos = fixture.debugElement.queryAll(By.css('.card-body'))
        expect(cos.length).toEqual(2)
        expect(cos[0].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU1")
        expect(cos[0].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT1")

        expect(cos[1].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU2")
        expect(cos[1].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT2")
    }));

    it('should show regular offers searched by name', fakeAsync(() => {
        component.currentCategory =1;
        component.userRole = 'user'
        component.searchByName = "TESTCU"
        component.showOffers()
        expect(culturalOfferService.search).toHaveBeenCalled()



        expect(component.culturalOffers.length).toEqual(2)
        expect(component.collectionSize).toEqual(500)

        expect(component.culturalOffers[0].id).toEqual(1)
        expect(component.culturalOffers[0].name).toEqual("TESTCU1")
        expect(component.culturalOffers[0].about).toEqual("ABOUT1")
        expect(component.culturalOffers[0].averageRating).toEqual(1)
        expect(component.culturalOffers[0].comments.length).toEqual(0)
        expect(component.culturalOffers[0].news.length).toEqual(0)
        expect(component.culturalOffers[0].location.lng).toEqual(45)
        expect(component.culturalOffers[0].location.lat).toEqual(46)
        expect(component.culturalOffers[0].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[0].location.city).toEqual("NOVI SAD")
        expect(component.culturalOffers[0].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[0].cover.id).toEqual(1)
        expect(component.culturalOffers[0].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[0].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[0].categoryName).toEqual("CAT1")

        expect(component.culturalOffers[1].id).toEqual(2)
        expect(component.culturalOffers[1].name).toEqual("TESTCU2")
        expect(component.culturalOffers[1].about).toEqual("ABOUT2")
        expect(component.culturalOffers[1].averageRating).toEqual(2)
        expect(component.culturalOffers[1].comments.length).toEqual(0)
        expect(component.culturalOffers[1].news.length).toEqual(0)
        expect(component.culturalOffers[1].location.lng).toEqual(47)
        expect(component.culturalOffers[1].location.lat).toEqual(48)
        expect(component.culturalOffers[1].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[1].location.city).toEqual("NIS")
        expect(component.culturalOffers[1].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[1].cover.id).toEqual(1)
        expect(component.culturalOffers[1].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[1].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[1].categoryName).toEqual("CAT1")

        fixture.detectChanges()
        tick()

        let cos = fixture.debugElement.queryAll(By.css('.card-body'))
        expect(cos.length).toEqual(2)
        expect(cos[0].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU1")
        expect(cos[0].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT1")

        expect(cos[1].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU2")
        expect(cos[1].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT2")

    }));

    it('should show subscribed offer for logged user first', fakeAsync(() => {
        component.currentCategory = 1
        component.userRole = 'user'
        component.showOffersUser()
        expect(culturalOfferService.getPageOffersByCategoryAndUser).toHaveBeenCalled()



        expect(component.culturalOffers.length).toEqual(2)
        expect(component.collectionSize).toEqual(2)

        expect(component.culturalOffers[0].id).toEqual(1)
        expect(component.culturalOffers[0].name).toEqual("TESTCU1")
        expect(component.culturalOffers[0].about).toEqual("ABOUT1")
        expect(component.culturalOffers[0].averageRating).toEqual(1)
        expect(component.culturalOffers[0].comments.length).toEqual(0)
        expect(component.culturalOffers[0].news.length).toEqual(0)
        expect(component.culturalOffers[0].location.lng).toEqual(45)
        expect(component.culturalOffers[0].location.lat).toEqual(46)
        expect(component.culturalOffers[0].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[0].location.city).toEqual("NOVI SAD")
        expect(component.culturalOffers[0].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[0].cover.id).toEqual(1)
        expect(component.culturalOffers[0].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[0].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[0].categoryName).toEqual("CAT1")

        expect(component.culturalOffers[1].id).toEqual(2)
        expect(component.culturalOffers[1].name).toEqual("TESTCU2")
        expect(component.culturalOffers[1].about).toEqual("ABOUT2")
        expect(component.culturalOffers[1].averageRating).toEqual(2)
        expect(component.culturalOffers[1].comments.length).toEqual(0)
        expect(component.culturalOffers[1].news.length).toEqual(0)
        expect(component.culturalOffers[1].location.lng).toEqual(47)
        expect(component.culturalOffers[1].location.lat).toEqual(48)
        expect(component.culturalOffers[1].location.country).toEqual("SERBIA")
        expect(component.culturalOffers[1].location.city).toEqual("NIS")
        expect(component.culturalOffers[1].location.street).toEqual("GOGOLJEVA")
        expect(component.culturalOffers[1].cover.id).toEqual(1)
        expect(component.culturalOffers[1].cover.path).toEqual("/PIC")
        expect(component.culturalOffers[1].cover.title).toEqual("PIC1")
        expect(component.culturalOffers[1].categoryName).toEqual("CAT1")

        fixture.detectChanges()
        tick()

        let cos = fixture.debugElement.queryAll(By.css('.card-body'))
        expect(cos.length).toEqual(2)
        expect(cos[0].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU1")
        expect(cos[0].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT1")

        expect(cos[1].query(By.css("#offerDetailsLink")).nativeElement.textContent.trim()).toEqual("TESTCU2")
        expect(cos[1].query(By.css(".card-text")).nativeElement.textContent.trim()).toEqual("ABOUT2")
    }));

    it('should show regular offers on ngChange', fakeAsync(() => {
        component.userRole = null
        component.ngOnChanges()
        expect(culturalOfferService.getPageOffersByCategory).toHaveBeenCalled()

    }));

    it('should show user subscribed offers first on ngChange', fakeAsync(() => {
        component.userRole = 'user'
        component.showOffersUser()
        expect(culturalOfferService.getPageOffersByCategoryAndUser).toHaveBeenCalled()

    }));

    it('should navigate to offer/id', fakeAsync(() => {
        component.showOffer(1);
        expect(router.navigate).toHaveBeenCalledWith(["/offer/1"])

    }));

});
