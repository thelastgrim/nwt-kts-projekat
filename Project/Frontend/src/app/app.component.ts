import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cultural-content-frontend';

  constructor(private router : Router){

  }

  filter(){
    this.router.navigateByUrl('filter');
  }

}
