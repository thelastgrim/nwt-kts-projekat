package com.cultural_content.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Picture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String title;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
	@Column(nullable = false)
	private String path;
	
	@ManyToOne(cascade={CascadeType.PERSIST}, fetch = FetchType.LAZY)
	private Comment comment;
	
	//@OneToOne
	//@MapsId
	@OneToMany(mappedBy = "cover", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<CulturalOffer> culturalOffer = new HashSet<CulturalOffer>();
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private User user;
	
	
	
	//@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//private Comment comment;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Picture(Integer id, String title, String path) {
		super();
		this.id = id;
		this.title = title;
		this.path = path;
	}

	public Picture() {
		super();
	}

	public Picture(String title2, String path2) {
		super();
		this.title = title2;
		this.path = path2;
	}

	@Override
	public String toString() {
		return "Picture [id=" + id + ", title=" + title + ", path=" + path +", comment=" + comment.getId() +"]";
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Set<CulturalOffer> getCulturalOffer() {
		return culturalOffer;
	}

	public void setCulturalOffer(Set<CulturalOffer> culturalOffer) {
		this.culturalOffer = culturalOffer;
	}

	
}
