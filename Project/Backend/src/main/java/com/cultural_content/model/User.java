package com.cultural_content.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import org.springframework.security.core.userdetails.UserDetails;


@Entity
@DiscriminatorValue("USER")
public class User extends Person implements UserDetails{

	@Column(nullable = true)
	private boolean activated;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Comment> comments = new HashSet<Comment>();
	
	@ManyToMany(mappedBy = "users", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<CulturalOffer> culturalOffers  = new HashSet<CulturalOffer>();
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Picture> pictures = new HashSet<Picture>();

	public User() {
		super();
	}
	
	public User(String username, @Email String email, String password, boolean activated) {
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
		this.setActivated(activated);
		
	}
	
	
	
	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<CulturalOffer> getCulturalOffers() {
		return culturalOffers;
	}

	public void setCulturalOffers(Set<CulturalOffer> culturalOffers) {
		this.culturalOffers = culturalOffers;
	}

	public Set<Picture> getPictures() {
		return pictures;
	}
	
	public void setPictures(Set<Picture> pictures) {
		this.pictures = pictures;
	}
	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	@Override
	public String toString() {
		return "activated=" + activated+ " "+ super.toString();
	//	return "User [activated=" + activated + ", comments=" + comments + ", culturalOffers=" + culturalOffers
	//			+ ", pictures=" + pictures + "]";
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
