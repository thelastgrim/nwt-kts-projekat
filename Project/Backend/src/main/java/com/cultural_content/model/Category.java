package com.cultural_content.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "Name can not be empty string")
	@Column(nullable = false)
	private String name;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
	@OneToMany(mappedBy = "category", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<CulturalOffer> cultural_offers = new HashSet<CulturalOffer>();
	
	public Category() {

	}
	
	public Category(Integer id, String name, Set<CulturalOffer> cultural_offers) {
		super();
		this.id = id;
		this.name = name;
		this.cultural_offers = cultural_offers;
	}
	
	public Category(Integer id) {
		super();
		this.id = id;
	}
	

	public Integer getId() {
		return id;
	}

	public Set<CulturalOffer> getCultural_offers() {
		return cultural_offers;
	}

	public void setCultural_offers(Set<CulturalOffer> cultural_offers) {
		this.cultural_offers = cultural_offers;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category findOne(Integer id2) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
}
