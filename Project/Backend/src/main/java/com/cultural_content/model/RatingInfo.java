package com.cultural_content.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;



@Entity
public class RatingInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	private Integer userId;
	
	@NotNull
	private Integer offerId;
	
	@Override
	public String toString() {
		return "RatingInfo [id=" + id + ", userId=" + userId + ", offerId=" + offerId + ", ratingValue=" + ratingValue
				+ "]";
	}

	@NotNull
	private double ratingValue;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public double getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(double ratingValue) {
		this.ratingValue = ratingValue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RatingInfo(@NotBlank Integer userId, @NotBlank Integer offerId, @NotBlank double ratingValue) {
		super();
		this.userId = userId;
		this.offerId = offerId;
		this.ratingValue = ratingValue;
	}

	public RatingInfo() {
		super();
	}

}
