package com.cultural_content.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private Date date;
	
	@Column(nullable = true, length = 1024)
	private String text;
	
	@Column(nullable = false)
	@Min(value = 1)
	@Max(value = 5)
	private double rating;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
	@OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//private Set<Picture> pictures = new HashSet<Picture>();
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private CulturalOffer culturalOffer;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private User user;
	
	@OneToMany(mappedBy = "comment",  cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	private Set<Picture> pictures = new HashSet<Picture>();

	public Comment(Integer id, Date date, String text, double rating, Set<Picture> pictures) {
		super();
		this.id = id;
		this.date = date;
		this.text = text;
		this.rating = rating;
		this.pictures = pictures;
	}
	//TO DO zasto imamo dva
	public Comment(Integer id, Date date, String text, double rating) {
		super();
		this.id = id;
		this.date = date;
		this.text = text;
		this.rating = rating;
	}
	
	public Comment() {
		super();
	}
	
	public Comment(Date date2, String text2, double rating2, Set<Picture> pictures2) {
		super();
		this.date = date2;
		this.text = text2;
		this.rating = rating2;
		this.pictures = pictures2;
	}
	public CulturalOffer getCulturalOffer() {
		return culturalOffer;
	}

	public void setCulturalOffer(CulturalOffer culturalOffer) {
		this.culturalOffer = culturalOffer;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(Set<Picture> pictures) {
		this.pictures = pictures;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	

}
