package com.cultural_content.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
public class CulturalOffer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "Name of cultural offer cannot be empty!")
	@Column(nullable = false)
	private String name;
	
	@NotBlank(message = "About of cultural offer cannot be empty!")
	@Column(nullable = false, length = 1024)
	private String about;
	
	@Min(value = 0,  message = "Average rating cannot be less than 0")
	@Max(value = 5, message = "Average rating cannot be more than 5")
	@Column(nullable = true)
	private double averageRating;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Category category;
	
	@OneToMany(mappedBy = "culturalOffer", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Comment>comments = new HashSet<Comment>();

	/*
	 * EAGER je PROMENJENO U LAZY
	 * jer se sa eager ucitava na frontu mnogo dugo
	 * 
	 */
	@OneToMany(mappedBy = "culturalOffer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<News> news = new HashSet<News>();
	
	@OneToOne(mappedBy = "culturalOffer", cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Location location;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "subscribers_offers",
			joinColumns = @JoinColumn(name ="offer_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
			)
	private Set<User> users = new HashSet<User>();
	
	//@OneToOne(mappedBy = "culturalOffer", cascade = CascadeType.ALL)
	//@PrimaryKeyJoinColumn
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Picture cover;
	
	public CulturalOffer() {
		
	}
	
	public CulturalOffer(Integer id, String name, String about, double averageRating) {
		super();
		this.id = id;
		this.name = name;
		this.about = about;
		this.averageRating = averageRating;
	}
	
	public CulturalOffer(String name, String about, double averageRating) {
		super();
		this.name = name;
		this.about = about;
		this.averageRating = averageRating;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsersAccounts(Set<User> usersAccounts) {
		this.users = usersAccounts;
	}


	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "CulturalOffer [id=" + id + ", name=" + name + ", about=" + about + ", averageRating=" + averageRating
				+ ", deleted=" + deleted + ", category=" + category + ", comments=" + comments + ", news=" + news
				+ ", location=" + location + ", users=" + users + ", pictures=" + cover + "]";
	}

	public Picture getCover() {
		return cover;
	}

	public void setCover(Picture cover) {
		this.cover = cover;
	}
	
	

}
