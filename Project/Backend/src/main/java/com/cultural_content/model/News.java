package com.cultural_content.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

@Entity
public class News {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = true)
	private Date dateAdded;
	
	@NotBlank(message = "Content of the news can not be empty string")
	@Column(nullable = false, length = 1024)
	private String text;
	
	@NotBlank(message = "Title can not be empty string")
	@Column(nullable = false)
	private String title;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
//	@NotBlank(message = "News must be asociated with culturalOffer")
//	  FetchType.EAGER -> ucitace se i sve veze sa objektom odmah
//	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//	@NotBlank(message = "News must be asociated with culturalOffer")
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private CulturalOffer culturalOffer;
	
	public News() {
	
	}
	
	public News(Integer id, Date dateAdded, String text, String title, CulturalOffer culturalOffer) {
		super();
		this.id = id;
		this.dateAdded = dateAdded;
		this.text = text;
		this.title = title;
		this.culturalOffer = culturalOffer;
	}

	public CulturalOffer getCulturalOffer() {
		return culturalOffer;
	}

	public void setCulturalOffer(CulturalOffer culturalOffer) {
		this.culturalOffer = culturalOffer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
