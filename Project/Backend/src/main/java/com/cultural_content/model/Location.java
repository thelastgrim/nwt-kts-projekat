package com.cultural_content.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Max(value = 180)
	@Min(value = -180)
	@Column(nullable = false)
	private double LNG;
	
	@Max(value = 90)
	@Min(value = -90)
	@Column(nullable = false)
	private double LAT;
	
	@Column(columnDefinition = "boolean default false")
	boolean deleted;
	
	@NotBlank(message = "Country cannot be empty!")
	private String country;
	
	@NotBlank(message = "City cannot be empty!")
	private String city;
	
	@NotBlank(message = "Street cannot be empty!")
	private String street;
	
	@OneToOne
	@MapsId
	private CulturalOffer culturalOffer;
	
	
	public Location() {
		super();
	}
	
	public Location(Integer id, double lNG, double lAT, String country, String city, String street) {
		super();
		this.id = id;
		LNG = lNG;
		LAT = lAT;
		this.country = country;
		this.city = city;
		this.street = street;
	}
	
	public Location(double lNG, double lAT, String country, String city, String street) {
		super();
		this.id = null;
		LNG = lNG;
		LAT = lAT;
		this.country = country;
		this.city = city;
		this.street = street;
	}

	public Integer getId() {
		return id;
	}
	
	public CulturalOffer getCulturalOffer() {
		return culturalOffer;
	}

	public void setCulturalOffer(CulturalOffer culturalOffer) {
		this.culturalOffer = culturalOffer;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public double getLNG() {
		return LNG;
	}

	public void setLNG(double lNG) {
		LNG = lNG;
	}

	public double getLAT() {
		return LAT;
	}

	public void setLAT(double lAT) {
		LAT = lAT;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


}
