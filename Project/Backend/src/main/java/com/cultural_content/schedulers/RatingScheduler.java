	package com.cultural_content.schedulers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.RatingInfo;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.RatingInfoRepo;
import com.cultural_content.service.CommentService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.RatingInfoService;

@Service
public class RatingScheduler {
	
	@Autowired
	private CulturalOfferService culturalOfferService;
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	/*
	 * Otkomentarisati @.... i timing promeniti, sada je 10 sekundi (10000)
	 */
	//Scheduled(fixedRate = 10000)
	public void scheduleFixedRateTask() {
	
		List<CulturalOffer> offers = culturalOfferService.findAllByDeleted();
		if(offers.isEmpty()) {
			System.out.println("nema ponuda");
			return;
		}
		
		for (CulturalOffer culturalOffer : offers) {
			
			List<RatingInfo> ratings = ratingInfoService.findByOfferId(culturalOffer.getId());
			
			if (ratings.isEmpty()) {
				//System.out.println("nema ocena za ovu ponudu");
				continue;
			}
			double oldRating = culturalOffer.getAverageRating();
			double newRating = 0;
			
			for (RatingInfo ratingInfo : ratings) {
				newRating+= ratingInfo.getRatingValue();
			}
			
			newRating = newRating/ratings.size();
			
			if(oldRating == newRating) {
				 logger.info("Cultural offer id = "+culturalOffer.getId() +" average rating hasnt changed.("+oldRating+")");
			}else {
				
				CulturalOffer c = culturalOfferService.findById(culturalOffer.getId());
				c.setAverageRating(newRating);
				try {
					CulturalOffer cc = culturalOfferService.update(c, c.getId());
		
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				logger.info("Cultural offer id = "+culturalOffer.getId() +" average rating changed from "+ oldRating+" to "+newRating+".");
			}
			
		}
		
	    logger.info( "Fixed rate task - " + System.currentTimeMillis() / 1000);
	  
	}

}
