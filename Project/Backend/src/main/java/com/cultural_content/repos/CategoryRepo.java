package com.cultural_content.repos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Integer>{
	
	//find one category by id and which is not deleted
	Category findByIdAndDeleted(Integer cateogryId, boolean deleted)throws ResourceNotFoundException;
	
	//find one category by name and which is not deleted
	Category findOneByNameAndDeleted(String name, boolean deleted);
	
	//find all categories which are not deleted
	List<Category> findAllBydeleted(boolean deleted);
	
	//find all categories which are not deleted with paging
	Page<Category> findAllBydeleted(boolean deleted, Pageable pageable);

}
