package com.cultural_content.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.Authority;

@Repository
public interface AuthorityRepo extends JpaRepository<Authority, Integer>{
	
	Authority findByname(String name);
}
