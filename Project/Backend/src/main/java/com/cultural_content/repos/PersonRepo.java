package com.cultural_content.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.Person;

@Repository
public interface PersonRepo extends JpaRepository<Person, Integer>{

	Person findOneByemail(String email);
	
	//Person findOneByusername(String username);
}

