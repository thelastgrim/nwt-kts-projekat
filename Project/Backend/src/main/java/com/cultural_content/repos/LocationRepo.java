package com.cultural_content.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.Location;

@Repository
public interface LocationRepo extends JpaRepository<Location, Integer>{
	
	Location findOneByLATAndLNGAndDeleted(double lAT, double lNG, boolean deleted);
	
	List<Location> findAllBydeletedOrderByCityAsc(boolean deleted);

}
