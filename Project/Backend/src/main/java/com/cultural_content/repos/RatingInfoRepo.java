package com.cultural_content.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.RatingInfo;

@Repository
public interface RatingInfoRepo extends JpaRepository<RatingInfo, Integer>{

	RatingInfo findByUserIdAndOfferId(Integer id, Integer ido);

	List<RatingInfo> findByOfferId(Integer id);

}
