package com.cultural_content.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>{

//	User findOneByemail(String email);
//	
//	User findOneByusername(String username);
	
	//User findOneByActivated(Boolean activated);
	
	User findByEmailAndUsername(String email,String username);
	
	//User findByEmail(String email);
	
	//User findByUsername(String username);
	
	User findByEmailOrUsername(String email,String username);
	

}
