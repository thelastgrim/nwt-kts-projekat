package com.cultural_content.repos;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.News;

@Repository
public interface NewsRepo extends JpaRepository<News, Integer>{

	List<News> findByCulturalOfferIdAndDeleted(Integer id, boolean deleted);
	
	News findOneByidAndDeleted(Integer id, boolean deleted);
	
//	Page<News> findAllBydeleted(boolean deleted, Pageable pageable);

	Page<News> findAllByCulturalOfferIdAndDeleted(Integer culturalOffer, boolean b, Pageable pageable);

//	News findBydateAddedAndtext(Date dateAdded, String text);

}
