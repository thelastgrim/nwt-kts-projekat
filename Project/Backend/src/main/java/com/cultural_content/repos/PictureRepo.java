package com.cultural_content.repos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.Picture;

@Repository
public interface PictureRepo extends JpaRepository<Picture, Integer>{

	//Picture findOneBypath(String path);
	
	//Picture findOneByid(Integer id);
	
	List<Picture> findAllByCommentIdAndDeleted(Integer id, boolean deleted);

	Page<Picture> findAllByCommentIdAndDeleted(Integer id, boolean deleted, Pageable pageable);

	Picture findByIdAndDeleted(Integer id, boolean deleted);
	
	List<Picture> findAllByCulturalOfferIdAndCommentNullAndDeleted(Integer id, boolean deleted);
	
	
}
