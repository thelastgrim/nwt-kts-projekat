package com.cultural_content.repos;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cultural_content.model.CulturalOffer;

@Repository
public interface CulturalOfferRepo extends JpaRepository<CulturalOffer, Integer>{
	
	List<CulturalOffer> findAllByCategory_idAndDeleted(Integer id, boolean deleted);
	
	//List<CulturalOffer> findAllByLocation_cityAndDeleted(String city, boolean deleted);
	
	Page<CulturalOffer> findByLocation_cityAndDeleted(String city, boolean deleted, Pageable pageable);
	
	Page<CulturalOffer> findByCategory_idAndDeleted(Integer id, boolean deleted, Pageable pageable);
	
	Page<CulturalOffer> findAllBydeleted(boolean deleted, Pageable pageable);

	Page<CulturalOffer> findByNameContainingIgnoreCaseAndDeleted(String name, boolean deleted, Pageable pageable);
	
	CulturalOffer findByidAndDeleted(Integer id, boolean deleted);

	CulturalOffer findOneByNameAndDeleted(String name, boolean deleted);

	CulturalOffer findByNameAndIdNotAndDeleted(String name, Integer id, boolean deleted);

	List<CulturalOffer> findAllByDeleted(boolean deleted);
	
	List<CulturalOffer> findByCategory_idAndDeletedAndUsersId(Integer id, boolean deleted, Integer user_id);
	
	Page<CulturalOffer> findByCategory_idAndLocation_cityAndDeleted(Integer id, String city, boolean deleted, Pageable pageable);

}
