package com.cultural_content.repos;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cultural_content.model.Comment;

public interface CommentRepo extends JpaRepository<Comment, Integer>{

	//Comment findOneBytext(String text);
	
	//Comment findOneByid(Integer id);
	
	List<Comment> findByCulturalOfferIdAndDeleted(Integer id, boolean deleted);
	Page<Comment> findByCulturalOfferIdAndDeleted(Integer id, boolean deleted, Pageable pageable);

	Comment findByIdAndDeleted(Integer id, boolean b);
}
