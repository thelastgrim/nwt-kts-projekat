package com.cultural_content.helper;

public class Constants {
	
	public static String UPLOAD_LOCATION = "src//main//resources//pictures//";
	
	public static long MAX_UPLOAD_SIZE = 5*1000000; // 5 MB
	
	public static String EXCEPTION_MESSAGE_1 = "Comment doesnt exist!";
	public static String EXCEPTION_MESSAGE_2 = "Comment with given id is already deleted!";
	public static String EXCEPTION_MESSAGE_3 = "Picture with given id is deleted!";
	public static String EXCEPTION_MESSAGE_4 = "File too large!";
	public static String EXCEPTION_MESSAGE_5 = "Format not allowed!";
	public static String EXCEPTION_MESSAGE_6 = "Rating value must be between 1 and 5!";

}
