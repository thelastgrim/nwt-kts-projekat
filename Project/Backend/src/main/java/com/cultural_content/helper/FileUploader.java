package com.cultural_content.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.web.multipart.MultipartFile;

import static com.cultural_content.helper.Constants.MAX_UPLOAD_SIZE;
import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_4;
import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_5; 


public class FileUploader {
	
	public static String upload(MultipartFile file) throws Exception  {
		int c = 0;
		if (!file.isEmpty()) {
	        byte[] bytes = null;
			try {
				bytes = file.getBytes();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     if (file.getSize()>MAX_UPLOAD_SIZE) {
			throw new Exception(EXCEPTION_MESSAGE_4);
		}
	     
	       
	        
	        File tempFile = new File(Constants.UPLOAD_LOCATION+String.valueOf(c)+file.getOriginalFilename());
	        
	        while(tempFile.exists()) {
	        	c++;
	        	tempFile = new File(Constants.UPLOAD_LOCATION+String.valueOf(c)+file.getOriginalFilename());
	        	
	        }
	        
	        Path path = Paths.get(Constants.UPLOAD_LOCATION+String.valueOf(c)+file.getOriginalFilename());
	       
	        String type = Files.probeContentType(path);
	        
	        if(type == null || !type.split("/")[0].equals("image")) {
	        	throw new Exception(EXCEPTION_MESSAGE_5);
	        }
	        
	        try {
	        	
				Files.write(path, bytes);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

		System.out.println("Upload success");
		return Constants.UPLOAD_LOCATION+String.valueOf(c)+file.getOriginalFilename();
		

	}
}
