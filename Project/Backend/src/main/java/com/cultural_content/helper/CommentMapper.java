package com.cultural_content.helper;

import java.util.HashSet;
import java.util.Set;

import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOPicture;
import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;

public class CommentMapper implements MapperInterface<Comment, DTOComment>{
	
	PictureMapper pm = new PictureMapper();

	@Override
	public Comment toEntity(DTOComment dto) {
		
		Set<Picture> pictures = new HashSet<>();
		
		for (DTOPicture dtoPicture : dto.getPictures()) {
			pictures.add(pm.toEntity(dtoPicture));
		}
		
		
		return new Comment(dto.getDate(), dto.getText(), dto.getRating(), pictures);
	}

	@Override
	public DTOComment toDto(Comment entity) {
		
		Set<DTOPicture> dtosPictures = new HashSet<>();
		
		for (Picture picture : entity.getPictures()) {
			dtosPictures.add(pm.toDto(picture));
		}
		
		return new DTOComment(entity.getId(), entity.getDate(), entity.getText(), entity.getRating(),entity.getUser().getUsername(), dtosPictures);
	}

}
