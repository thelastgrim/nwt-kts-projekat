package com.cultural_content.helper;

import com.cultural_content.dto.DTONews;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.News;

public class NewsMapper implements MapperInterface<News,DTONews> {
	
	@Override
	public News toEntity(DTONews dto) {
		return new News(
				dto.getId(),
				dto.getDateAdded(),
				dto.getText(),
				dto.getTitle(),
				new CulturalOffer(dto.getCulturalOfferId(), null, null, 0)
				);
	}

	@Override
	public DTONews toDto(News entity) {
		return new DTONews(
				entity.getId(), 
				entity.getDateAdded(),
				entity.getText(),
				entity.getTitle(),
				entity.getCulturalOffer().getId()
				);
	}

}
