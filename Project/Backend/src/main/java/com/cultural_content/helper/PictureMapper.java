package com.cultural_content.helper;

import com.cultural_content.dto.DTOPicture;
import com.cultural_content.model.Picture;

public class PictureMapper implements MapperInterface<Picture, DTOPicture>{

	@Override
	public Picture toEntity(DTOPicture dto) {
		// TODO Auto-generated method stub
		return new Picture(dto.getTitle(), dto.getPath());
	}

	@Override
	public DTOPicture toDto(Picture entity) {
		// TODO Auto-generated method stub
		return new DTOPicture(entity.getId(), entity.getTitle(), entity.getPath());
		//entity.getCulturalOffer() INFINITE LOOP
	}

}
