package com.cultural_content.helper;

import java.util.HashSet;
import java.util.Set;

import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOCulturalOffer;
import com.cultural_content.dto.DTONews;
import com.cultural_content.dto.DTOPicture;
import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.News;
import com.cultural_content.model.Picture;

public class CulturalOfferMapper implements MapperInterface<CulturalOffer, DTOCulturalOffer> {
	
	LocationMapper locationMapper;
	NewsMapper newsMapper;
	CommentMapper commentMapper;
	PictureMapper pictureMapper;

	@Override
	public CulturalOffer toEntity(DTOCulturalOffer dto) {
		locationMapper = new LocationMapper();
		return new CulturalOffer(dto.getName(), dto.getAbout(), dto.getAverageRating());
	}

	@SuppressWarnings("unused")
	@Override
	public DTOCulturalOffer toDto(CulturalOffer entity) {
		
		locationMapper = new LocationMapper();
		newsMapper = new NewsMapper();
		commentMapper = new CommentMapper();
		pictureMapper = new PictureMapper();
		
		Set<DTONews> newsDTO = new HashSet<DTONews>();
		
		for(News news : entity.getNews()) {
			newsDTO.add(newsMapper.toDto(news));
		}
		
		Set<DTOComment> commentDTO = new HashSet<DTOComment>();
		
		for(Comment comment : entity.getComments()) {
			commentDTO.add(commentMapper.toDto(comment));
		}
		
		
		String categoryName;
		if(entity.getCategory().getName() == null) {
			categoryName = null;
		}else {
			categoryName = entity.getCategory().getName();
		}
		
		return new DTOCulturalOffer(entity.getId(), 
									entity.getName(),
									entity.getAbout(), 
									entity.getAverageRating(),
									entity.getCategory().getName(), 
									entity.isDeleted(),
									commentDTO, 
									newsDTO, 
									locationMapper.toDto(entity.getLocation()),
									pictureMapper.toDto(entity.getCover()));
	}

}
