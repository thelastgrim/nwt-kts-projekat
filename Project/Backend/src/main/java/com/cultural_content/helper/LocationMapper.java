package com.cultural_content.helper;

import com.cultural_content.dto.DTOLocation;
import com.cultural_content.model.Location;

public class LocationMapper implements MapperInterface<Location, DTOLocation> {
	
	
	@Override
	public Location toEntity(DTOLocation dto) {

		return new Location(dto.getLNG(), dto.getLAT(), dto.getCountry() ,dto.getCity(), dto.getStreet());
	}

	@Override
	public DTOLocation toDto(Location entity) {
		
		return new DTOLocation(entity.getLNG(), entity.getLAT(), entity.getCity(), entity.getCountry(), entity.getStreet());
	}

}
