package com.cultural_content.helper;

import com.cultural_content.dto.DTOUser;
import com.cultural_content.model.Person;
import com.cultural_content.model.User;

public class UserMapper implements MapperInterface<User, DTOUser>{

	@Override
	public User toEntity(DTOUser dto) {
		return new User(dto.getUsername(), dto.getEmail(), dto.getPassword(), false);
	}

	@Override
	public DTOUser toDto(User entity) {
		return new DTOUser(entity.getId(), entity.getEmail(),entity.getPassword(),entity.getUsername(), entity.isActivated());
	}

}
