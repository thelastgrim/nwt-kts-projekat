package com.cultural_content.dto;

import java.util.ArrayList;
import java.util.List;

public class DTOUsersOffers {
	
	private List<DTOCulturalOffer> offers = new ArrayList<DTOCulturalOffer>();
	private int collectionSize;
	
	public List<DTOCulturalOffer> getOffers() {
		return offers;
	}
	public void setOffers(List<DTOCulturalOffer> offers) {
		this.offers = offers;
	}
	public int getCollectionSize() {
		return collectionSize;
	}
	public void setCollectionSize(int collectionSize) {
		this.collectionSize = collectionSize;
	}
	public DTOUsersOffers(List<DTOCulturalOffer> offers, int collectionSize) {
		super();
		this.offers = offers;
		this.collectionSize = collectionSize;
	}
	


}
