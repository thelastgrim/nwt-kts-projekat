package com.cultural_content.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.springframework.web.multipart.MultipartFile;

public class DTOCulturalOfferAdd {
	@NotBlank(message = "Name of cultural offer cannot be empty")
	private String name;
	
	@NotBlank(message = "About cannot be empty")
	private String about;
	
	private int id;

	private String lng;
	private String lat;
	private String country;
	private String city;
	private String street;
	
	private MultipartFile picture;
	
	private String category;
	
	public DTOCulturalOfferAdd() {
		super();
	}
	
	public DTOCulturalOfferAdd(@NotBlank(message = "Name of cultural offer cannot be empty") String name,
			@NotBlank(message = "About cannot be empty") String about, String lng, String lat, String country,
			String city, String street, MultipartFile picture, String category) {
		super();
		this.name = name;
		this.about = about;
		this.lng = lng;
		this.lat = lat;
		this.country = country;
		this.city = city;
		this.street = street;
		this.picture = picture;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public MultipartFile getPicture() {
		return picture;
	}

	public void setPicture(MultipartFile picture) {
		this.picture = picture;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
