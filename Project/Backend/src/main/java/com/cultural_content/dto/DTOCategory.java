package com.cultural_content.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotBlank;

public class DTOCategory {
	
	
	private Integer id;
	
	@NotBlank(message = "Name can not be empty")
	private String name;
	
	private boolean deleted;
	
	private Set<DTOCulturalOffer> cultural_offers = new HashSet<DTOCulturalOffer>();

	public DTOCategory() {
		super();
	}

	public DTOCategory(Integer id, String name, boolean deleted, Set<DTOCulturalOffer> cultural_offers) {
		super();
		this.id = id;
		this.name = name;
		this.cultural_offers = cultural_offers;
		this.deleted = deleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<DTOCulturalOffer> getCultural_offers() {
		return cultural_offers;
	}

	public void setCultural_offers(Set<DTOCulturalOffer> cultural_offers) {
		this.cultural_offers = cultural_offers;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
	
	
}
