package com.cultural_content.dto;

public class DTOLoginResponse {
	
	private DTOUserTokenState dtoUserTokenState;
	private int[] subbs;
	
	
	public DTOLoginResponse(DTOUserTokenState dtoUserTokenState, int[] subbs) {
		super();
		this.dtoUserTokenState = dtoUserTokenState;
		this.subbs = subbs;
	}


	public DTOLoginResponse() {
		super();
	}


	public DTOUserTokenState getDtoUserTokenState() {
		return dtoUserTokenState;
	}


	public void setDtoUserTokenState(DTOUserTokenState dtoUserTokenState) {
		this.dtoUserTokenState = dtoUserTokenState;
	}


	public int[] getSubbs() {
		return subbs;
	}


	public void setSubbs(int[] subbs) {
		this.subbs = subbs;
	}
 
}
