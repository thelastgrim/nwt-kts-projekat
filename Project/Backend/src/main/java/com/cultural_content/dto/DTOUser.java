package com.cultural_content.dto;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class DTOUser {
	//private Integer id;
	
	@NotBlank(message = "Email cannot be empty.")
    @Email(message = "Email format is not valid.")
    private String email;

    @NotBlank(message = "Password cannot be empty.")
    private String password;
    
    @NotBlank(message = "Username cannot be empty.")
    private String username;

//	private boolean status;
	
	private Set<Integer> commentsId;
	
//	private Integer culturalOffersId;
	private Set<Integer> culturalOffersId;
	
	private boolean activated;
	
	//ako bude trebalo
	//private Integer pictureId;
	

	public DTOUser() {
		
	}
	
	public DTOUser(boolean status, Set<Integer> commentsId, Set<Integer> culturalOffersId) {
		super();
//		this.status = status;
		this.commentsId = commentsId;
		this.culturalOffersId = culturalOffersId;
	}

	public DTOUser(Integer id, @NotBlank(message = "Email cannot be empty.") @Email(message = "Email format is not valid.") String email,
            @NotBlank(message = "Password cannot be empty.") String password, @NotBlank(message = "Username cannot be empty.") String username, boolean activated) {
		
		this.email = email;
		this.username = username;
		this.password = password;
		this.commentsId = null;
		this.culturalOffersId = null;
		this.activated = activated;
		
	}
	
/*	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
*/
	

	/*public Integer getCulturalOffersId() {
		return culturalOffersId;
	}*/

	/*public void setCulturalOffersId(Integer culturalOffersId) {
		this.culturalOffersId = culturalOffersId;
	}
*/
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/*public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}*/

	public Set<Integer> getCommentsId() {
		return commentsId;
	}

	public void setCommentsId(Set<Integer> commentsId) {
		this.commentsId = commentsId;
	}

	public Set<Integer> getCulturalOffersId() {
		return culturalOffersId;
	}

	public void setCulturalOffersId(Set<Integer> culturalOffersId) {
		this.culturalOffersId = culturalOffersId;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	
}

