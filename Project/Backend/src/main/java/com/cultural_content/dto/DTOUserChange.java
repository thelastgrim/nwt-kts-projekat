package com.cultural_content.dto;

import javax.validation.constraints.NotBlank;

public class DTOUserChange {
	@NotBlank(message = "Password cannot be empty.")
    private String newPassword;
	
	@NotBlank(message = "Password cannot be empty.")
    private String oldPassword;

	public DTOUserChange() {
		super();
		
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	
	
	

}
