package com.cultural_content.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.cultural_content.model.Picture;

public class DTOCulturalOffer {
	
	private Integer id;
	
	@NotBlank(message = "Name of cultural offer cannot be empty")
	private String name;
	
	@NotBlank(message = "About cannot be empty")
	private String about;
	
	@Min(value = 0,  message = "Average rating cannot be less than 0")
	@Max(value = 5, message = "Average rating cannot be more than 5")
	private double averageRating;
	
	private String categoryName;
	
	private boolean deleted;
	
	private Set<DTOComment>comments = new HashSet<DTOComment>();
	
	private Set<DTONews> news = new HashSet<DTONews>();
	
	private DTOPicture cover;
	
	private DTOLocation location;
	
	public DTOCulturalOffer() {
		super();
	}

	public DTOCulturalOffer(Integer id, String name, String about, double averageRating, String categoryName, boolean deleted,
			Set<DTOComment> comments, Set<DTONews> news, DTOLocation location, DTOPicture cover) {
		super();
		this.id = id;
		this.name = name;
		this.about = about;
		this.averageRating = averageRating;
		this.categoryName = categoryName;
		this.deleted = deleted;
		this.comments = comments;
		this.news = news;
		this.location = location;
		this.cover = cover;
	}
	
	public DTOCulturalOffer(Integer Id, String name, String about, double averageRating, DTOLocation location) {
		super();
		this.id = Id;
		this.name = name;
		this.about = about;
		this.averageRating = averageRating;
		this.location = location;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public String getCategory() {
		return categoryName;
	}

	public void setCategory(String categoryName) {
		this.categoryName = categoryName;
	}

	public Set<DTOComment> getComments() {
		return comments;
	}

	public void setComments(Set<DTOComment> comments) {
		this.comments = comments;
	}

	public Set<DTONews> getNews() {
		return news;
	}

	public void setNews(Set<DTONews> news) {
		this.news = news;
	}

	public DTOLocation getLocation() {
		return location;
	}

	public void setLocation(DTOLocation location) {
		this.location = location;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	@Override
	public String toString() {
		return "DTOCulturalOffer [id=" + id + ", name=" + name + ", about=" + about + ", averageRating=" + averageRating
				+ ", categoryName=" + cover + ", location=" + location + "]";
	}

	public DTOPicture getCover() {
		return cover;
	}

	public void setCover(DTOPicture cover) {
		this.cover = cover;
	}

	

}
