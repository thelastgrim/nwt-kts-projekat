package com.cultural_content.dto;

public class DTOPictureBase65 {
	
	private String picture;

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public DTOPictureBase65(String picture) {
		super();
		this.picture = picture;
	}

	public DTOPictureBase65() {
		super();
	}
	
	

}
