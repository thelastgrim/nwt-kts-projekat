package com.cultural_content.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.cultural_content.model.CulturalOffer;

public class DTOLocation {
	
	private Integer id;
	
	@Max(value = 180)
	@Min(value = -180)
	private double LNG;
	
	@Max(value = 90)
	@Min(value = -90)
	private double LAT;
	
	@NotBlank(message = "Location cannot be empty")
	private String city;
	
	@NotBlank(message = "Location cannot be empty")
	private String country;
	
	@NotBlank(message = "Location cannot be empty")
	private String street;
	
	public DTOLocation() {
		super();
	}
	
	public DTOLocation(double lNG, double lAT, String city, String country, String street) {
		super();
		this.LNG = lNG;
		this.LAT = lAT;
		this.city = city;
		this.country = country;
		this.street = street;
	}


	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getLNG() {
		return LNG;
	}

	public void setLNG(double lNG) {
		LNG = lNG;
	}

	public double getLAT() {
		return LAT;
	}

	public void setLAT(double lAT) {
		LAT = lAT;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	
}
