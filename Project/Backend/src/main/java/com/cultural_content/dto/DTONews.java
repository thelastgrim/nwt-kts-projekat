package com.cultural_content.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

public class DTONews {
	
	private Integer id;

	private Date dateAdded;
	
	@NotBlank(message = "Text can not be empty")
	private String text;

	@NotBlank(message = "Title can not be empty")
	private String title;
	
	private Integer culturalOfferId;

	public DTONews() {
		
	}
	
	public DTONews(Integer id, Date dateAdded,
			@NotBlank(message = "Text can not be empty") String text,
			@NotBlank(message = "Title can not be empty") String title, Integer culturalOfferId ) {
		super();
		this.id = id;
		this.dateAdded = dateAdded;
		this.text = text;
		this.title = title;
		this.culturalOfferId = culturalOfferId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCulturalOfferId() {
		return culturalOfferId;
	}

	public void setCulturalOfferId(Integer culturalOfferId) {
		this.culturalOfferId = culturalOfferId;
	}
}
