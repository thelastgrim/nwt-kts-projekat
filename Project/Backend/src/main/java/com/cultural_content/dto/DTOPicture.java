package com.cultural_content.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.User;
import com.sun.istack.NotNull;

import lombok.ToString;

public class DTOPicture {
	
	@Positive
	private Integer id;
	
	@NotBlank
	private String title;
	
	
	private String path;

	@NotNull
	private CulturalOffer culturalOffer;

	@NotNull
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public CulturalOffer getCulturalOffer() {
		return culturalOffer;
	}

	public void setCulturalOffer(CulturalOffer culturalOffer) {
		this.culturalOffer = culturalOffer;
	}

	public User getUserAccount() {
		return user;
	}

	public void setUserAccount(User user) {
		this.user = user;
	}

	public DTOPicture(Integer id, String title, String path, CulturalOffer culturalOffer) {
		super();
		this.id = id;
		this.title = title;
		this.path = path;
		this.culturalOffer = culturalOffer;
	}

	public DTOPicture() {
		super();
	}

	public DTOPicture(Integer id2, String title2, String path2) {
		this.id = id2;
		this.title = title2;
		this.path = path2;
	}

}
