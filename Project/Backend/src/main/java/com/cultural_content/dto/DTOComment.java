package com.cultural_content.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.sun.istack.NotNull;

public class DTOComment {

	@Positive
	private Integer id;
	
	@NotNull
	private Date date;
	
	public DTOComment() {
		super();
	}
	
	private String text;
	
	private String username;
	
	public DTOComment(Integer id, Date date, String text, double rating, String username, Set<DTOPicture> pictures) {
		super();
		this.id = id;
		this.date = date;
		this.text = text;
		this.rating = rating;
		this.pictures = pictures;
		this.username = username;
	}
	@DecimalMax(value = "5.0", message = "Value cant be higher than 5.0")
	@DecimalMin(value = "1.0", message = "Value cant be lower than 1.0")
	private double rating;
	
	
	private Set<DTOPicture> pictures = new HashSet<DTOPicture>(); 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public Set<DTOPicture> getPictures() {
		return pictures;
	}

	public void setPictures(Set<DTOPicture> pictures) {
		this.pictures = pictures;
	}

	@Override
	public String toString() {
		return "DTOComment [id=" + id + ", date=" + date + ", text=" + text + ", rating=" + rating + ", pictures="
				+ pictures + "]";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
