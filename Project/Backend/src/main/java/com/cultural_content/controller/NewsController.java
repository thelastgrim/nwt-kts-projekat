package com.cultural_content.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTONews;
import com.cultural_content.helper.NewsMapper;
import com.cultural_content.model.News;
import com.cultural_content.service.NewsService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;


@RestController
@RequestMapping(value =  "/categories/{categoryId}/culturalOffers/{culturalOfferId}/news", produces = MediaType.APPLICATION_JSON_VALUE)
public class NewsController {

	 @Autowired
	 private NewsService newsService;
	 
	 private NewsMapper newsMapper;
	 
	 //Create one news
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	 @RequestMapping(method = RequestMethod.POST)
	 public ResponseEntity<DTONews> createNews(@Valid @RequestBody DTONews newsDTO, @PathVariable("culturalOfferId") Integer id){
		News news;
		news = newsService.create(newsMapper.toEntity(newsDTO));
		System.out.println(news.getTitle());
		
		try {
			newsService.notifySubscribers(id, newsMapper.toEntity(newsDTO));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(newsMapper.toDto(news), HttpStatus.CREATED);
	 }
	 
	 //Read all news
	 @RequestMapping(method = RequestMethod.GET)
	 public ResponseEntity<List<DTONews>> getAllNews(@PathVariable("categoryId") Integer categoryId, @PathVariable("culturalOfferId") Integer culturalOfferId ){
		 List<News> news ;
		 try {
			news = newsService.findAll(categoryId, culturalOfferId);
			}catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
			}
		return new ResponseEntity<>(toDTONewsList(news), HttpStatus.OK); 
	 }
	 
	 //Read all news with paging
	 @RequestMapping(value = "/by-page", method = RequestMethod.GET)
	 public ResponseEntity<Page<DTONews>> getAllNews(@PathVariable("culturalOfferId") Integer culturalOfferId, Pageable pageable ){
		 Page<News> page = newsService.findAll(culturalOfferId,pageable);

		 if (page == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		 
		List<DTONews> newsDTOlist = toDTONewsList(page.toList());
		Page<DTONews> pageNewsDTO = new PageImpl<>(newsDTOlist, page.getPageable(), page.getTotalElements());
		 
		return new ResponseEntity<>(pageNewsDTO, HttpStatus.OK); 
	 }
	 
	 //Read one news
	 @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	 public ResponseEntity<DTONews> getOneNews(@PathVariable("id") Integer newsId ){
		News news = newsService.findOne(newsId);
		   if(news != null) {
					return new ResponseEntity<>(newsMapper.toDto(news), HttpStatus.OK); 
			    }else { 
			    	return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
			    }
	 }
	 
	 //Update one news
	 @PreAuthorize("hasRole('ROLE_ADMIN')") 
	 @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<DTONews> updateNews(@RequestBody DTONews newsDTO, @PathVariable Integer id){
		 News news;
		 try {
			 news = newsService.update(newsMapper.toEntity(newsDTO), id);
		 } catch (Exception e) {
			 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		 }
		 
		 try {
				newsService.notifySubscribers(id, newsMapper.toEntity(newsDTO));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 return new ResponseEntity<>(newsMapper.toDto(news), HttpStatus.OK);
	 }
	 
	 //Delete one news
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	 public ResponseEntity<Void> deleteNews(@PathVariable Integer id){
		try {
			newsService.delete(id);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	public NewsController() {
		newsMapper = new NewsMapper();
	}

	private List<DTONews> toDTONewsList(List<News> news) {
		List<DTONews> newsDTO = new ArrayList<>();
		
		for(News oneNews: news) {
			newsDTO.add(newsMapper.toDto(oneNews));
		}
		return newsDTO;
	}
	 
}
