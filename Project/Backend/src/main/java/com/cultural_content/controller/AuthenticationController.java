package com.cultural_content.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOUser;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.helper.UserMapper;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Person;
import com.cultural_content.model.User;
import com.cultural_content.security.EncryptDecrypt;
import com.cultural_content.security.TokenUtils;
import com.cultural_content.service.EmailService;
import com.cultural_content.service.UserService;

@RestController
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenUtils tokenUtils;
	 
	private UserMapper userMapper;
	
	@Autowired
    private AuthenticationManager authenticationManager;
	
	@Autowired
    private EmailService emailService;
	    
	public AuthenticationController() {
		userMapper = new UserMapper();
	}
	
	@PostMapping("/log-in")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody DTOUserLogin authenticationRequest,
                                                                    HttpServletResponse response) {
		
		String role = "admin";
		Authentication authentication = null;
		try {
			authentication = authenticationManager
	                .authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
	                        authenticationRequest.getPassword()));
		}catch (BadCredentialsException e) {
			Object o = "Sorry, user doesn't exist :(";
			return new ResponseEntity<>(o,HttpStatus.BAD_REQUEST);
		}
        

		Person user = (Person) authentication.getPrincipal();
     //   User user = (User) authentication.getPrincipal();
		boolean flag = true; // flag koji nam govori da li je dosao ispravan korisnik, odnosno ukoliko je dosao Admin(Person) ili aktivan korisnik(onaj koji je aktivirao nalog preko linka) onda je true, u suprotnom je false
		
		User u = null;
		
		
		int[] subbs = null;
		ArrayList<Integer> ids = new ArrayList<Integer>();
		if(user instanceof User) { //ako je dosao User a ne Admin(Person)
			u = (User) user;
			for (CulturalOffer cc : u.getCulturalOffers()) {
				ids.add(cc.getId());
			}
			subbs = ids.stream().mapToInt(i -> i).toArray();
			role = "user";
			if(!u.isActivated()) { //i ukoliko taj User nije aktivirao nalog saljemo mu bad request
				flag = false;	   //tj.flag ide na false
			}
		}
		
        if(flag) {
        	  // Ubaci korisnika u trenutni security kontekst
            SecurityContextHolder.getContext().setAuthentication(authentication);
        // Kreiraj token za tog korisnika
        	


            // Kreiraj token za tog korisnika
            
            String jwt = tokenUtils.generateToken(user.getEmail(), user.getUsername(), role); // prijavljujemo se na sistem sa email adresom
            int expiresIn = tokenUtils.getExpiredIn();
            
 
            
    		System.out.println("USER ="+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    		
            // Vrati token kao odgovor na uspesnu autentifikaciju
		            return ResponseEntity.ok(new DTOLoginResponse(new DTOUserTokenState(jwt, expiresIn), subbs));
        }else {
        	Object o = "Sorry, user doesn't exist :(";
        	return new ResponseEntity<>(o,HttpStatus.BAD_REQUEST);
        }
        
      
    }
	
	@PostMapping("/sign-up")
    public ResponseEntity<?> addUser(@Valid @RequestBody DTOUser userRequest) throws Exception {
		System.err.println(" - - - "+userRequest.getEmail() + " " + userRequest.getUsername()+ " "+ userRequest.getPassword());

        User existUser = this.userService.findByEmailOrUsername(userRequest.getEmail(), userRequest.getUsername());
        
        if (existUser != null) {
            throw new Exception("Username or email already exists");
        }

        try {
        	User u = userMapper.toEntity(userRequest);
            existUser = userService.create(u);
            String toEncrypt = existUser.getEmail()+existUser.getId();
            System.err.println("To encrypt: " + toEncrypt);
            EncryptDecrypt ed;
    	    String encrypted = null;
    	    ed = new EncryptDecrypt();
			encrypted = ed.encrypt(toEncrypt).replace("/", "culturalContent");
			System.err.println("encrypted: " + encrypted);
			SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(u.getEmail());
            mailMessage.setSubject("Complete Registration!");
         //   mailMessage.setFrom("some@hotmail.com");
            mailMessage.setText("To confirm your account, please click here : "
                    +"http://localhost:8080/auth/confirm-account/"+encrypted);
            
          //posalji mejl
			new Thread(new Runnable() {
			    public void run() {
			    	 emailService.sendEmail(mailMessage);
			    }
			}).start();
           
    	    
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
        
        
        return new ResponseEntity<>(userMapper.toDto(existUser), HttpStatus.CREATED);
    }
	
	@GetMapping(value = "/confirm-account/{confirmationToken}", produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
    public String confirmUserAccount(@PathVariable("confirmationToken")String confirmationToken)
    {
     //   ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

		EncryptDecrypt ed;
		try {
			ed = new EncryptDecrypt();
			String data = ed.decrypt(confirmationToken.replace("culturalContent","/"));
			System.err.println(data);
			String id = data.split(".com")[1];
			User u = userService.findById(Integer.parseInt(id));//.get()
			System.err.println(u);
			if(u.isActivated()) {
				return "<html><body style=\"font-size: xx-large;\">\r\n" + 
						"<div style=\"margin-left: 20%; margin-top: 7%; color: #ff9999;\">	\r\n" + 
						"	Account already activated!\r\n" + 
						"</div>\r\n" + 
						"<div style=\"margin-left: 50%;\">\r\n" + 
						
						"</div>\r\n" + 
						"</body>\r\n" + 
						"</html>";
			}
			
			u.setActivated(true);
			userService.saveUser(u);
			
		} catch (NoSuchElementException e) {
			return "Account already activated!";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			return "There's been problem with account activation.";
		}
		
       
		//return "Account successfuly activated. You can now log in.";
	/*	return "<html>\n" + "<header><title>Welcome</title></header>\n" +
        "<body>\n" + "Hello world\n" + "</body>\n" + "</html>";*/
		return "<html><body style=\"font-size: xx-large;\">\r\n" + 
				"<div style=\"margin-left: 12%; margin-top: 8%; color:#71da71;\">\r\n" + 
				"	\r\n" + 
				"	Account successfuly activated. You can now log in!\r\n" + 
				"	\r\n" + 
				"</div>\r\n" + 
				"<div style=\"margin-left: 50%; margin-top: 6%;\">\r\n" + 
				
				"</div>\r\n" + 
				"</body>\r\n" + 
				"</html>";
		
    }

	

}
