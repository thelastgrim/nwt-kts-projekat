package com.cultural_content.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.persistence.Convert;
import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOPicture;
import com.cultural_content.dto.DTOPictureBase65;
import com.cultural_content.dto.DTOPictureUpload;
import com.cultural_content.helper.FileUploader;
import com.cultural_content.helper.PictureMapper;
import com.cultural_content.model.Picture;
import com.cultural_content.model.User;
import com.cultural_content.service.CommentService;
import com.cultural_content.service.PictureService;
import com.cultural_content.service.UserService;

@RestController
@RequestMapping(value = "/pictures", produces = MediaType.APPLICATION_JSON_VALUE)
public class PictureController {
	
	
	@Autowired
	private PictureService pictureService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CommentService commentService;
	
	private PictureMapper pictureMapper = new PictureMapper();
	
	
	@RequestMapping(value = "/{idp}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTOPictureBase65> getPicture(@PathVariable Integer idp){
	
		Picture picture = pictureService.getById(idp);
		
		DTOPictureBase65 dtoPictureBase65 = new DTOPictureBase65();
		
		if(picture == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}else {
			/*
			ByteArrayResource inputStream = null;
			try {
				inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(
				        picture.getPath()
				)));
			} catch (IOException e) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}*/
			byte[] fileContent = null;
			try {
				fileContent = Files.readAllBytes(new File(picture.getPath()).toPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String s = Base64.getEncoder().encodeToString(fileContent);
			dtoPictureBase65.setPicture(s);
			
			return new ResponseEntity<>(dtoPictureBase65	, HttpStatus.OK);
			
		}
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/culturalOffers/{id}/comments/{idc}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<DTOPicture> createPictureForComment(@ModelAttribute DTOPictureUpload dtoPictureUpload, @PathVariable Integer id, @PathVariable Integer idc) throws Exception{
		System.out.println("usooo");
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(dtoPictureUpload.getTitle() == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
		}
		if(dtoPictureUpload.getFile() == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
		
		}
		
		/*
		 * ne mogu da izmenstim u service jel mi treba user iz kontexta
		 * a kako da ga dobijem u integracionom testu servisa???
		 * 
		 */
		String serverPath = null;
		try {
			serverPath = FileUploader.upload(dtoPictureUpload.getFile());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Picture p = new Picture();
		
		//p.setCulturalOffer(culturalOfferRepo.findById(id).get());
		
		p.setComment(commentService.findById(idc));
		p.setTitle(dtoPictureUpload.getTitle());
		p.setPath(serverPath);
		p.setUser(userService.findById(user.getId()));
		
		Picture pp = pictureService.createAndUploadForComment(p);
		
		
		return new ResponseEntity<>(pictureMapper.toDto(pp), HttpStatus.CREATED);
	}

	/*
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<DTOPicture>> getAllPictures(@PathVariable Integer id, @PathVariable Integer idc){
		
		List<Picture> pictures = pictureService.getAllPicturesByCommentId(idc);
		if(pictures.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDTOPictureList(pictures), HttpStatus.OK);
	}
	
	*/
	
	/*
	@RequestMapping(value = "/by-page", method = RequestMethod.GET)
	public ResponseEntity<Page<DTOPicture>> getAllPicturesByPage(@PathVariable Integer idc, Pageable pageable){
		
		Page<Picture> page = pictureService.findAll(pageable, idc);
		if (page.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		List<DTOPicture> picturesDTOS = toDTOPictureList(page.toList());
		Page<DTOPicture> pagePicturesDTOS = new PageImpl<>(picturesDTOS, page.getPageable(), page.getTotalElements());
		
		return new ResponseEntity<>(pagePicturesDTOS, HttpStatus.OK);
		
	}
	*/


	
	/*
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<DTOPicture> createPicture(@ModelAttribute DTOPictureUpload dtoPictureUpload, @PathVariable Integer idc) throws Exception{
		System.out.println();
		if(dtoPictureUpload.getTitle() == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
		}
		
		Picture p = pictureService.createAndUpload(dtoPictureUpload, idc);
		
		return new ResponseEntity<>(pictureMapper.toDto(p), HttpStatus.CREATED);
	}
	*/

	
	/*
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{idp}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTOPicture> updatePicture(@Valid @RequestBody DTOPicture dtoPicture, @PathVariable Integer idp) throws Exception{
		
		Picture p = pictureService.getById(idp);
		if(p == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(p.getUser().getId()!= user.getId()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		
		p = pictureService.update(pictureMapper.toEntity(dtoPicture), idp);
		
		return new ResponseEntity<>(pictureMapper.toDto(p), HttpStatus.OK);
		
	}
	*/
	
	/*
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletePicture(@PathVariable Integer id) throws Exception{
		
		System.out.println("usao u delete");
		Picture p = pictureService.getById(id);
		
		if(p == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(p.getUser().getId()!= user.getId()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		
		pictureService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	*/
	private List<DTOPicture> toDTOPictureList(List<Picture> pictures){
		
		List<DTOPicture> picturesDTOS = new ArrayList<>();
		
		for (Picture picture : pictures) {
			picturesDTOS.add(pictureMapper.toDto(picture));
		}
		
		return picturesDTOS;
		
	}

}
