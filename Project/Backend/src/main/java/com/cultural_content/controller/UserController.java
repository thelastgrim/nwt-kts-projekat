package com.cultural_content.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOUser;
import com.cultural_content.dto.DTOUserChange;
import com.cultural_content.helper.UserMapper;
import com.cultural_content.model.User;
import com.cultural_content.service.UserService;

@RestController
@RequestMapping(value =  "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	
	 
	 @Autowired
	 private UserService userService;
	 

	 private UserMapper userMapper;
	 
	 public UserController() {
	        userMapper = new UserMapper();
	    }
	  @CrossOrigin(origins = "http://localhost:4200")
	  @PreAuthorize("hasRole('ROLE_USER')")
	  @PutMapping( consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<DTOUser> updateUser(@Valid @RequestBody DTOUserChange userChangeDTO) throws Exception{
	      User u = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();  
		  System.err.println("          ---- ID:  "+ u.getId());
		  	User existUser = this.userService.findById(u.getId());
	        //kada bude implementiran Context brisemo id i uzimamo korisnika iz Context-a
	        if(existUser == null) {
	        	throw new Exception("User with given id doesn't exist");
	        }	        
	        try {
	        	existUser = userService.update(existUser, userChangeDTO.getOldPassword(),userChangeDTO.getNewPassword() );
	        	
	        } catch (Exception e) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        //ako nam je servise vratio null to znaci da je stara lozinka neispravna
	        if(existUser == null) {
        		throw new Exception("Wrong password");
        	}
	        return new ResponseEntity<>(userMapper.toDto(existUser), HttpStatus.OK);
	    }

/*	  @PreAuthorize("hasRole('ROLE_ADMIN')")	  
	  @GetMapping()
	  public ResponseEntity<List<DTOUser>> getAllUsers() {
	        List<User> users = userService.findAll();
	        return new ResponseEntity<>(toDTOUserList(users), HttpStatus.OK);
	    }*/
	  

	   @PreAuthorize("hasRole('ROLE_USER')")	  
	   @GetMapping(value="/{id}")
	    public ResponseEntity<DTOUser> getUser(@PathVariable Integer id){
	        User user = userService.findById(id);
	        if(user == null){
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }

	        return new ResponseEntity<>(userMapper.toDto(user), HttpStatus.OK);
	    }
	  
	  private List<DTOUser> toDTOUserList(List<User> users){
	        List<DTOUser> userDTOS = new ArrayList<>();
	        for (User user: users) {
	            userDTOS.add(userMapper.toDto(user));
	        }
	        return userDTOS;
	    }
	 
}
