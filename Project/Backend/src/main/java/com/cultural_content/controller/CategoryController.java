package com.cultural_content.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOCategory;
import com.cultural_content.helper.CategoryMapper;
import com.cultural_content.model.Category;
import com.cultural_content.service.CategoryService;


@RestController
@RequestMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	private CategoryMapper categoryMapper;
	
	//POST one category  - CREATE
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping
	public ResponseEntity<DTOCategory> createCategory(@Valid @RequestBody DTOCategory categoryName){
		Category category;
		try {
			category = categoryService.create(categoryMapper.toEntity(categoryName));
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(categoryMapper.toDto(category), HttpStatus.CREATED);
	}

	 //GET all categories - READ all
	 @GetMapping
	 public ResponseEntity<List<DTOCategory>> getCategories(){
		List<Category> categories = categoryService.findAll();
		return new ResponseEntity<>(toCategoryDTOList(categories), HttpStatus.OK);
	 }

//	  //GET one category   - READ one
	  @GetMapping(value = "/{id}")
	  public ResponseEntity<DTOCategory> getOne(@PathVariable("id") Integer categoryId ){
			Category category;
			category = categoryService.findOne(categoryId);	
		   if(category != null) {
				return new ResponseEntity<>(categoryMapper.toDto(category), HttpStatus.OK); 
		    }else { 
		    	return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		    }
		 }
	
	  //PUT one category   - UPDATE
	  @PreAuthorize("hasRole('ROLE_ADMIN')")
	  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<DTOCategory> updateCategory(@RequestBody DTOCategory categoryDTO, @PathVariable Integer id){
		  Category category;
			 try {
				 category = categoryService.update(categoryMapper.toEntity(categoryDTO), id);
			 } catch (Exception e) {
				 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			 }
			 return new ResponseEntity<>(categoryMapper.toDto(category), HttpStatus.OK);
		 }
	
	 //DELETE category    - DELETE

	 @CrossOrigin(origins = "http://localhost:4200")
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	 @DeleteMapping(value="/{id}")
	 public ResponseEntity<Void> deleteCategory(@PathVariable Integer id){
		try {
			categoryService.delete(id);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);	
		}
			System.out.println("Obrisano");
			return new ResponseEntity<>(HttpStatus.OK);
		}
	   
	public CategoryController() {
		categoryMapper = new CategoryMapper();
	}
	
	private List<DTOCategory> toCategoryDTOList(List<Category> categories) {
		System.out.println("4");
		List<DTOCategory> categoryDTOS = new ArrayList<>();
		System.out.println("5");
		for(Category category : categories) {
			categoryDTOS.add(categoryMapper.toDto(category));
			System.out.println("6");
		}
		
		return categoryDTOS;
	}

}