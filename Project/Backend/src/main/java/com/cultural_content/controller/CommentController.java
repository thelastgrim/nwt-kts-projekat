package com.cultural_content.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOComment;
import com.cultural_content.helper.CommentMapper;
import com.cultural_content.model.Comment;
import com.cultural_content.model.RatingInfo;
import com.cultural_content.model.User;
import com.cultural_content.service.CommentService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.RatingInfoService;
import com.cultural_content.service.UserService;

@RestController
@RequestMapping(value =  "culturalOffers/{id}/comments", produces = MediaType.APPLICATION_JSON_VALUE)		
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CulturalOfferService culturalOfferService;
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	
	private CommentMapper commentMapper = new CommentMapper();
	

	@RequestMapping(value = "/by-page", method = RequestMethod.GET)
	public ResponseEntity<Page<DTOComment>> getAllCommentsByPage(@PathVariable Integer id, Pageable pageable){
		//System.out.println(pageable);
		Page<Comment> page = commentService.findAll(pageable, id);
		
		if (page==null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	
		List<DTOComment> commentsDTOS = toDTOCommentList(page.toList());
		Page<DTOComment> pageCommentsDTOS = new PageImpl<>(commentsDTOS, page.getPageable(), page.getTotalElements());
		
		
		return new ResponseEntity<>(pageCommentsDTOS, HttpStatus.OK);
		
	}
	

	@RequestMapping(method = RequestMethod.GET)
		public ResponseEntity<List<DTOComment>> getAllComments(@PathVariable Integer id){
			
			List<Comment> comments = commentService.findAllByCultularOfferId(id);
			if (comments == null) {
				//System.out.println("ITS EMPTYYYYYYYYYYYYYYYY");
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}else {
				return new ResponseEntity<>(toDTOCommentList(comments), HttpStatus.OK);
			}
			
		}
	

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<DTOComment> getComment(@PathVariable Integer id){
		
		Comment c = commentService.findById(id);
		
		if (c == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>(commentMapper.toDto(c), HttpStatus.OK);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTOComment> createComment(@Valid @RequestBody DTOComment dtoComment, @PathVariable Integer id) throws Exception{
		Comment comment;
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		comment = commentMapper.toEntity(dtoComment);
		
		
		comment.setCulturalOffer(culturalOfferService.findById(id));
		comment.setUser(userService.findById(user.getId()));
		
		comment = commentService.create(comment);
		
		return new ResponseEntity<>(commentMapper.toDto(comment), HttpStatus.CREATED);
		
		
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTOComment> updateComment(@Valid @RequestBody DTOComment dtoComment, @PathVariable Integer id) throws Exception{
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Comment c = commentService.findById(id);
		if (c == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
			
			if(user.getId()!= c.getUser().getId()) {
				return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			}
		
		c = commentService.update(commentMapper.toEntity(dtoComment), id);
		
		
		
		return new ResponseEntity<>(commentMapper.toDto(c), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteComment(@PathVariable("id") Integer id) throws Exception{
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Comment c = commentService.findById(id);
		
		if (c == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
		if(user.getId()!= c.getUser().getId()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		
		
		commentService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	private List<DTOComment> toDTOCommentList(List<Comment> comments){
		
		List<DTOComment> commentsDTOS = new ArrayList<>();
		
		for (Comment comment : comments) {
			commentsDTOS.add(commentMapper.toDto(comment));
		}
		
		return commentsDTOS;
	}
	
	/*
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value="/rate", method = RequestMethod.POST)
	 public ResponseEntity<DTORating> rateCulturalOffer(@Valid @RequestBody DTORating dtoRating){
		System.out.println(dtoRating.getRating());
		
		Comment
		 
		return new ResponseEntity<>(null, HttpStatus.OK); 
	 }
	 */
}
