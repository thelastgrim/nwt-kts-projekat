package com.cultural_content.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cultural_content.dto.DTOCulturalOffer;
import com.cultural_content.dto.DTOCulturalOfferAdd;
import com.cultural_content.dto.DTOPictureUpload;
import com.cultural_content.dto.DTOUsersOffers;
import com.cultural_content.helper.CulturalOfferMapper;
import com.cultural_content.helper.LocationMapper;
import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;
import com.cultural_content.model.News;
import com.cultural_content.model.Picture;
import com.cultural_content.model.User;
import com.cultural_content.service.CategoryService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.LocationService;
import com.cultural_content.service.PictureService;
import net.minidev.json.JSONObject;


@RestController
@RequestMapping(value =  "/offers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CulturalOfferController {
	
	@Autowired
	private CulturalOfferService culturalOfferService;
	
	@Autowired 
	private LocationService locationService;
	
	@Autowired 
	private CategoryService categoryService;
	
	private CulturalOfferMapper culturalOfferMapper;

	private LocationMapper locationMapper;
	

	@Autowired
	private PictureService pictureService;
	
	public CulturalOfferController() {
		
		culturalOfferMapper = new CulturalOfferMapper();
		locationMapper = new LocationMapper();
	}

	private List<DTOCulturalOffer> toCulturalOfferDTOList(List<CulturalOffer> culturalOffers){
        List<DTOCulturalOffer> culturalOfferDTOS = new ArrayList<>();
        for (CulturalOffer culturalOffer: culturalOffers) {
        	System.out.print(culturalOffer.getLocation()+"\n");
        	culturalOfferDTOS.add(culturalOfferMapper.toDto(culturalOffer));
        }
        return culturalOfferDTOS;
    }
	
	@RequestMapping(value="cities", method = RequestMethod.GET)
	 public ResponseEntity<List<String>> getAllUniqueCities(){
		List<String> cities = new ArrayList<String>();
		cities = locationService.findAllUniqueCities();
		return new ResponseEntity<>(cities, HttpStatus.OK);
	 }
	
	@RequestMapping(value="all-by-page", method = RequestMethod.GET)
	 public ResponseEntity<Page<DTOCulturalOffer>> getAllCulturalOffers(Pageable pageable){
		 Page<CulturalOffer> page = culturalOfferService.findAll(pageable);
		 List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(page.toList());
		 Page<DTOCulturalOffer> pageCulturalOfferDTO = new PageImpl<>(CulturalOffersDTOlist, page.getPageable(), page.getTotalElements());
		 
		return new ResponseEntity<>(pageCulturalOfferDTO, HttpStatus.OK); 
	 }
	
	@RequestMapping(value="page-by-city/{cityName}", method = RequestMethod.GET)
	 public ResponseEntity<Page<DTOCulturalOffer>> getAllCulturalOffersByCity(@PathVariable String cityName, Pageable pageable){
		 Page<CulturalOffer> page = culturalOfferService.findAllByCity(cityName, pageable);
		 List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(page.toList());
		 Page<DTOCulturalOffer> pageCulturalOfferDTO = new PageImpl<>(CulturalOffersDTOlist, page.getPageable(), page.getTotalElements());
		 
		return new ResponseEntity<>(pageCulturalOfferDTO, HttpStatus.OK); 
	 }
	
	@RequestMapping(value="page-by-category/{categoryId}", method = RequestMethod.GET)
	 public ResponseEntity<Page<DTOCulturalOffer>> getAllCulturalOffersByCategory(@PathVariable Integer categoryId, Pageable pageable){
		if(categoryId != null) {
			System.err.println("Usli smo u page-by-category ");
			System.out.println("cat id: " + categoryId + "----" + pageable.toString());
			 Page<CulturalOffer> page = culturalOfferService.findAllByCategory(categoryId, pageable);
			 System.out.println(page.getNumberOfElements());
			 
			 List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(page.toList());
			 Page<DTOCulturalOffer> pageCulturalOfferDTO = new PageImpl<>(CulturalOffersDTOlist, page.getPageable(), page.getTotalElements());
			
			 
			return new ResponseEntity<>(pageCulturalOfferDTO, HttpStatus.OK); 
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	
	 }
	
	@RequestMapping(value="by-page/categories/{categoryId}/location/{cityName}", method = RequestMethod.GET)
	 public ResponseEntity<Page<DTOCulturalOffer>> getAllCulturalOffersByCategoryAndLocation(@PathVariable Integer categoryId, @PathVariable String cityName, Pageable pageable){
		 Page<CulturalOffer> page = culturalOfferService.findByCategoryAndLocation(categoryId, cityName, pageable);
		 List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(page.toList());
		 Page<DTOCulturalOffer> pageCulturalOfferDTO = new PageImpl<>(CulturalOffersDTOlist, page.getPageable(), page.getTotalElements());
		 
		return new ResponseEntity<>(pageCulturalOfferDTO, HttpStatus.OK); 
	 }
	
	@RequestMapping(value="/search", method = RequestMethod.GET)
	ResponseEntity<Page<DTOCulturalOffer>> getCulturalOffersByName(@RequestParam(value = "name") String name, Pageable pageable) throws Exception{
		Page<CulturalOffer> page = culturalOfferService.findByName(name, pageable);
		if (page.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); 
		}
		List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(page.toList());
		Page<DTOCulturalOffer> pageCulturalOfferDTO = new PageImpl<>(CulturalOffersDTOlist, page.getPageable(), page.getTotalElements());
		System.out.println("Ispis pretrage " + pageCulturalOfferDTO.getNumberOfElements() + " " + page.getTotalElements());
		return new ResponseEntity<>(pageCulturalOfferDTO, HttpStatus.OK); 
	}
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<DTOCulturalOffer>> getAllCulturalOffers(){
		List<CulturalOffer> culturalOffers = new ArrayList<CulturalOffer>();
		culturalOffers = culturalOfferService.findAllByDeleted();
		return new ResponseEntity<>(toCulturalOfferDTOList(culturalOffers), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{offerId}", method = RequestMethod.GET)
	ResponseEntity<DTOCulturalOffer> getOneCulturalOffer(@PathVariable Integer offerId){
		CulturalOffer culturalOffer;
		try {
			culturalOffer = culturalOfferService.findOne(offerId);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(culturalOfferMapper.toDto(culturalOffer), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/categories/{categoryId}", method = RequestMethod.GET)
	ResponseEntity<List<DTOCulturalOffer>> getAllCulturalOffersByCategory(@PathVariable Integer categoryId){
		List<CulturalOffer> culturalOffers = new ArrayList<CulturalOffer>();
		culturalOffers = culturalOfferService.findAllByCategory(categoryId);
		return new ResponseEntity<>(toCulturalOfferDTOList(culturalOffers), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<DTOCulturalOfferAdd> createCulturalOffer(@ModelAttribute DTOCulturalOfferAdd dtoCulturalOfferAdd) throws Exception{
		
		CulturalOffer culturalOffer = new CulturalOffer();
		DTOCulturalOfferAdd dto = new DTOCulturalOfferAdd();
		Location created= null;
		try {
			System.out.println("IPAK KREIRA");
			double lng =Double.parseDouble(dtoCulturalOfferAdd.getLng());  
			double lat =Double.parseDouble(dtoCulturalOfferAdd.getLat());  
			Location location = new Location(lng, lat, dtoCulturalOfferAdd.getCity(), dtoCulturalOfferAdd.getCountry(), dtoCulturalOfferAdd.getStreet());
		
			culturalOffer.setName(dtoCulturalOfferAdd.getName());
			culturalOffer.setAbout(dtoCulturalOfferAdd.getAbout());
			culturalOffer.setCategory(categoryService.findByName(dtoCulturalOfferAdd.getCategory()));
			culturalOffer.setLocation(location);
			
			created  = locationService.create(location, culturalOffer);

			CulturalOffer createCO = culturalOfferService.findById(created.getId());
			DTOPictureUpload pu = new DTOPictureUpload();
			pu.setFile(dtoCulturalOfferAdd.getPicture());
			pu.setTitle(culturalOffer.getName());
			Picture p = pictureService.createAndUploadforCulturalOffer(pu, created.getId());
			createCO.setCover(p);
			culturalOfferService.update(createCO, createCO.getId());
			
			dto.setName(culturalOffer.getName());
			dto.setCategory(culturalOffer.getCategory().getId().toString());
			dto.setAbout(dtoCulturalOfferAdd.getAbout());
			dto.setLat(dtoCulturalOfferAdd.getLat());
			dto.setLng(dtoCulturalOfferAdd.getLng());
			dto.setCountry(dtoCulturalOfferAdd.getCountry());
			dto.setStreet(dtoCulturalOfferAdd.getStreet());
			
			dto.setId(culturalOffer.getId());
			
		}catch(Exception e) {
			System.out.print("\n" + e + "\n");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<DTOCulturalOfferAdd>(dto,HttpStatus.CREATED);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/{offerId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTOCulturalOffer> updateCulturalOffer(@Valid @RequestBody DTOCulturalOffer culturalOfferDTO, @PathVariable Integer offerId){
		
		CulturalOffer culturalOffer;
		try {
			culturalOffer = culturalOfferService.update(culturalOfferMapper.toEntity(culturalOfferDTO), offerId);
		}catch(Exception e) {
			System.out.print("\n" + e + "\n");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(culturalOfferMapper.toDto(culturalOffer), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/{offerId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteCulturalOffer(@PathVariable Integer offerId){
		try {
			culturalOfferService.delete(offerId);
			
		}catch(Exception e) {
			System.out.print("\n" + e + "\n");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{offerId}/subscribe", method = RequestMethod.POST)
	public ResponseEntity<Void> subscribeToCulturalOffer(@PathVariable Integer offerId) throws Exception{
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		culturalOfferService.subscribe(offerId, user.getId());
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/{offerId}/unsubscribe", method = RequestMethod.POST)
	public ResponseEntity<Void> unsubscribeFromCulturalOffer(@PathVariable Integer offerId) throws Exception{
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		culturalOfferService.unsubscribe(offerId, user.getId());
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	
	@RequestMapping(value="page-by-category-and-user/{categoryId}", method = RequestMethod.GET)
	 public ResponseEntity<DTOUsersOffers> getAllCulturalOffersByCategoryAndUser(@PathVariable Integer categoryId, Pageable pageable){
		User user = new User();
		try {
			user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		
		List<CulturalOffer> userCatsList = culturalOfferService.findAllByCategoryAndUserId(categoryId, user.getId());
		List<CulturalOffer> allCatsList =culturalOfferService.findAllByCategory(categoryId);
		
		List<CulturalOffer> differences = allCatsList.stream()
	            .filter(element -> !userCatsList.contains(element))
	            .collect(Collectors.toList());

		List<CulturalOffer> all = new ArrayList<CulturalOffer>();
		for(CulturalOffer culturalOffer : userCatsList) {
			//da se komentari i news ne vracaju na front
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
			all.add(culturalOffer);
		}

		for(CulturalOffer culturalOffer : differences) {
			//da se komentari i news ne vracaju na front
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
			all.add(culturalOffer);
		}		
		
		List<DTOCulturalOffer> CulturalOffersDTOlist = toCulturalOfferDTOList(all);
		
		PagedListHolder<DTOCulturalOffer> page = new PagedListHolder<DTOCulturalOffer>(CulturalOffersDTOlist);
		page.setPageSize(pageable.getPageSize());
		page.setPage(pageable.getPageNumber());
		/*
		System.err.println(" - - - - - - - getAllCulturalOffersByCategoryAndUser");
		
		for(DTOCulturalOffer d: page.getPageList() ) {
			System.err.println(d.toString());
			
		}
		System.err.println(" - - - - - - -");
		 */
		/*Saljem json a ne ceo PagedListHolder objekat, zato sto se u page pored trazenih ponuda
		i drugih korisnih podatak nalazi i source, odnosno sve ponude iz baze 
		koje nema smisla slati na front*/
		/*
		JSONObject json = new JSONObject();
		json.put("data", page.getPageList());
		json.put("collectionSize", page.getNrOfElements());
		
		System.out.println(json);
		*/
		return new ResponseEntity<>(new DTOUsersOffers(page.getPageList(), page.getNrOfElements()), HttpStatus.OK); 
	 }
	
	
}


