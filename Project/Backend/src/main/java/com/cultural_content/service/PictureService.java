package com.cultural_content.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.cultural_content.dto.DTOPictureUpload;
import com.cultural_content.helper.FileUploader;
import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;
import com.cultural_content.model.User;
import com.cultural_content.repos.CommentRepo;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.PictureRepo;
import com.cultural_content.repos.UserRepo;

import static com.cultural_content.helper.Constants.*;


@Service
public class PictureService {
	
	@Autowired
	private PictureRepo pictureRepo;
	
	@Autowired
	private CulturalOfferRepo culturalOfferRepo;
	
	@Autowired
	private CommentRepo commentRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	//public Picture findByPath(String path) {
		//return this.pictureRepo.findOneBypath(path);
	//}
	
	public List<Picture> getAllPicturesByCommentId(Integer id) {
		return pictureRepo.findAllByCommentIdAndDeleted(id, false);
	}
	
	
	public Picture getById(Integer id) {
		
		return pictureRepo.findByIdAndDeleted(id, false);
	}
	
	public Picture createAndUploadForComment(Picture p) {
		
		return pictureRepo.save(p);
	}
	
	public Picture createAndUploadforCulturalOffer (DTOPictureUpload dtoPictureUpload, Integer idc) {
		//User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		String serverPath = null;
		try {
			serverPath = FileUploader.upload(dtoPictureUpload.getFile());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Picture p = new Picture();
		
		p.getCulturalOffer().add(culturalOfferRepo.findById(idc).get());
		
		//p.setComment(commentService.findById(idc));
		p.setTitle(dtoPictureUpload.getTitle());
		p.setPath(serverPath);
		//p.setUser(UserService.findById(user.getId()));
		
		return pictureRepo.save(p);
	}
	
	public Picture update(Picture picture, Integer id) throws Exception {
		
		Picture p = pictureRepo.findByIdAndDeleted(id, false);
		
		if(p==null) {
			throw new Exception(EXCEPTION_MESSAGE_3);
		}
		p.setTitle(picture.getTitle());
		
		return pictureRepo.save(p);
	}

	public void delete(Integer id) throws Exception {
		
		Picture picture = pictureRepo.findByIdAndDeleted(id, false);
		if(picture == null) {
			throw new Exception(EXCEPTION_MESSAGE_3);
		}
		picture.setDeleted(true);
		pictureRepo.save(picture);
		
	}
	
	public Picture getAnyById(Integer id) {
		return pictureRepo.findById(id).get();
	}

	public Page<Picture> findAll(Pageable pageable, Integer id) {
		
		return pictureRepo.findAllByCommentIdAndDeleted(id, false, pageable);
	}


	public List<Picture> getAllPicturesByCulturalOfferId(Integer dbCulturalOfferId) {
		return pictureRepo.findAllByCulturalOfferIdAndCommentNullAndDeleted(dbCulturalOfferId, false);
	}

	
}
