	package com.cultural_content.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.cultural_content.model.Category;
import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.News;
import com.cultural_content.model.Picture;
import com.cultural_content.repos.CategoryRepo;
import com.cultural_content.repos.CommentRepo;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.NewsRepo;
import com.cultural_content.repos.PictureRepo;

@Service
public class CategoryService {
	

    @Autowired
    private CategoryRepo categoryRepo;
    
    @Autowired
    private CulturalOfferRepo culturalOfferRepo;
    
	@Autowired 
	NewsRepo newsRepository;
	
	@Autowired 
	CommentRepo commentRepository;
	
	@Autowired 
	PictureRepo pictureRepository;
    
    public Category findByName(String name) {

        return categoryRepo.findOneByNameAndDeleted(name, false);
    }
    
	public Category findOne(Integer cateogryId) throws ResourceNotFoundException {
		System.out.println("tu smo");
		return categoryRepo.findByIdAndDeleted(cateogryId, false);
	}
	
	public List<Category> findAll() {
		return categoryRepo.findAllBydeleted(false);
	}

	public Page<Category> findAll(Pageable pageable) {
		return categoryRepo.findAllBydeleted(false, pageable);
	}

	public Category create(Category category) throws Exception {
		if(categoryRepo.findOneByNameAndDeleted(category.getName(), false) != null) {
			throw new Exception("Category with given name already exists!");
		}
		return categoryRepo.save(category);
	}

	public Category update(Category entity, Integer id) throws Exception {
		Category existingCategory = categoryRepo.findByIdAndDeleted(id, false);

		if(existingCategory == null) {
			throw new Exception("Category with given id doesn not exist!");
		}else if(existingCategory.isDeleted()) {
			throw new Exception("Category with given id is deleted, it cannot be updated!");
		}
		
		String newName = entity.getName();
		Category checkNameEntity = findByName(entity.getName());

		if(checkNameEntity != null) {
			throw new Exception("Category with given name already exists!");
		}else {
			existingCategory.setName(newName);
			return  categoryRepo.save(existingCategory);
		}
	}

	public void delete(Integer id) throws Exception {
		Category existingCategory = categoryRepo.findByIdAndDeleted(id, false);
		if(existingCategory == null) {
			throw new Exception("Category with given id does not exist!");
		}
			
//		if(existingCategory.getCultural_offers().size() > 0) {
//
//			System.out.println(existingCategory.getCultural_offers().size());
//			throw new Exception("Category can not be deleted because it has offers!");
//		}
		
		if(existingCategory.isDeleted()) {
			throw new Exception("Category with given id is already deleted!");
		}
		else {
			existingCategory.setDeleted(true);
			
			for(CulturalOffer culturalOffer : existingCategory.getCultural_offers()) {
				culturalOffer.setDeleted(true);
				culturalOffer.getLocation().setDeleted(true);
				for(News news : culturalOffer.getNews()) {
					news.setDeleted(true);
					newsRepository.save(news);
				}
				for(Comment comment : culturalOffer.getComments()) {
					comment.setDeleted(true);
					commentRepository.save(comment);
					for(Picture picture : comment.getPictures()) {
						picture.setDeleted(true);
						pictureRepository.save(picture);
					}
				}
				culturalOfferRepo.save(culturalOffer);
			}
			
			existingCategory = categoryRepo.save(existingCategory);
		}
	
	}
}
