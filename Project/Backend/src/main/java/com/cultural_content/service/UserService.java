package com.cultural_content.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cultural_content.model.Authority;
import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;

@Service
public class UserService {
	
	@Autowired
	private UserRepo userRepo;	

    @Autowired
    private AuthorityService authService;	

    @Autowired
    private PasswordEncoder passwordEncoder;
	
	
	 public User create(User entity) throws Exception {
	        if(userRepo.findByEmailAndUsername(entity.getEmail(), entity.getUsername()) != null){
	            throw new Exception("User with given email address or username already exists");
	        }
	        
	        User u = new User();
	        u.setUsername(entity.getUsername());
	        u.setPassword(passwordEncoder.encode(entity.getPassword()));
	    //    u.setPassword(entity.getPassword());
	        u.setEmail(entity.getEmail());
	        Set<Authority> auth = authService.findByName("ROLE_USER");
	        u.setAuthorities(auth);
	        u.setActivated(false); //activated je false sve dok ne poseti link

	        u = this.userRepo.save(u);
	        System.err.println("USER:" +u);
	        return u;
	    }
	
	    public User update(User user, String oldPass, String newPass) throws Exception {
	        if(passwordEncoder.matches(oldPass, user.getPassword())) {
	        	user.setPassword(passwordEncoder.encode(newPass));
	 	        return userRepo.save(user);
	        }else {
	        	return null;
	        }
	       
	    }
	 
	//findByNotActivated
/*	public User findByActivated(Boolean activated) {
		return this.userRepo.findOneByActivated(activated);
	}*/
	
/*	public User findByEmailAndUsername(String email,String username) {
		return this.userRepo.findByEmailAndUsername(email, username);
	}*/
	
	public User findByEmailOrUsername(String email,String username) {
		return this.userRepo.findByEmailOrUsername(email, username);
	}
	
	public User findById(Integer id) {
		return this.userRepo.findById(id).orElse(null);
	}
	
/*	 public List<User> findAll() {
	        return userRepo.findAll();
	    }*/
	
	 public void saveUser(User u) {
		 userRepo.save(u);
	 }
	 
	 public void deleteUser(User u) {
		 userRepo.delete(u);
	 }
	 
	 

}
