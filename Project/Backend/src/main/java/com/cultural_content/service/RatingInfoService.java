package com.cultural_content.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cultural_content.model.RatingInfo;
import com.cultural_content.repos.RatingInfoRepo;

@Service
public class RatingInfoService {
	
	@Autowired
	private RatingInfoRepo ratingInfoRepo;
	
	public RatingInfo findByUserIdAndOfferId(Integer id, Integer ido) {
		
		return ratingInfoRepo.findByUserIdAndOfferId(id, ido);
	}

	public RatingInfo create(RatingInfo ratingInfo) {
		
		return ratingInfoRepo.save(ratingInfo);
		
	}

	public RatingInfo update(RatingInfo _ratingInfo) {
		
		RatingInfo ratingInfo = ratingInfoRepo.findById(_ratingInfo.getId()).get();;
		
		ratingInfo.setRatingValue(_ratingInfo.getRatingValue());
		return ratingInfoRepo.save(ratingInfo);
		
	}

	public List<RatingInfo> findByOfferId(Integer id) {
		
		return ratingInfoRepo.findByOfferId(id);
	}

	public void delete(RatingInfo ratingInfo) {
		ratingInfoRepo.delete(ratingInfo);
		
	}

}
