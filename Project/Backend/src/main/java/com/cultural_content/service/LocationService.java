package com.cultural_content.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.LocationRepo;

@Service
public class LocationService {
	
	@Autowired
	private LocationRepo locationRepo;
	
	@Autowired
	CulturalOfferRepo culturalOfferRepo;
	
	public Location findByLatitudeAndLongitude(double lat, double lng) {
		return locationRepo.findOneByLATAndLNGAndDeleted(lat, lng, false);
	
	}
	
	public Location create(Location location, CulturalOffer culturalOffer) throws Exception {
		
		if(locationRepo.findOneByLATAndLNGAndDeleted(location.getLAT(), location.getLNG(), false)!=null || culturalOfferRepo.findOneByNameAndDeleted(culturalOffer.getName(), false)!=null) {
			throw new Exception("Cultural Offer on this location already exists!");
		}
		location.setCulturalOffer(culturalOffer);
		return locationRepo.save(location);
	}
	
	public List<String> findAllUniqueCities() {
		
		List<Location> locations = locationRepo.findAllBydeletedOrderByCityAsc(false);
		List<String> cities = new ArrayList<String>();
		for(Location loc : locations) {
			if(!cities.contains(loc.getCity())) {
				cities.add(loc.getCity());
			}
		}
		return cities;
	}

}
