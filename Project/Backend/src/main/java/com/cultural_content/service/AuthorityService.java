package com.cultural_content.service;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cultural_content.model.Authority;
import com.cultural_content.repos.AuthorityRepo;

@Service
public class AuthorityService {
	
	@Autowired
	private AuthorityRepo authorityRepo;
	
	//prodiskutovati o tome da li vracati listu ili samo jedan Authority
	/*  public List<Authority> findById(int id) {
	        Authority auth = this.authorityRepo.getOne(id);
	        List<Authority> auths = new ArrayList<>();
	        auths.add(auth);
	        return auths;
	    }*/
	  
	  public Set<Authority> findByName(String name) {
	        Authority auth = this.authorityRepo.findByname(name);
	        Set<Authority> auths = new HashSet<Authority>();
	        auths.add(auth);
	        return auths;
	    }
	

}
