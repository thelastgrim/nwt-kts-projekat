package com.cultural_content.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.News;
import com.cultural_content.model.User;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.NewsRepo;

@Service
public class NewsService {
	
	@Autowired
	private NewsRepo newsRepo;
	
	@Autowired
	private CulturalOfferRepo culturalOfferRepo;
	
	@Autowired
	private EmailService emailService;
	
	public News findOne(Integer newsId) throws ResourceNotFoundException {
		return newsRepo.findOneByidAndDeleted(newsId, false);
		
	}

//	public News findByDateAddedAndText(Date dateAdded, String text) {
//		//return this.newsRepo.findBydateAddedAndtext(dateAdded, text);
//		return null;
//	}

	public List<News> findAll(Integer category ,Integer culturalOffer) throws Exception {
		
		List<News> existingAllNews = newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer, false);
		List<News> existingNews = new ArrayList<>();
		
		for(News news : existingAllNews) {
			if(news.getCulturalOffer().getCategory().getId() == category) {
				if(existingAllNews.isEmpty()) {
					System.out.println("existing"+ existingNews.get(0).getTitle());
					throw new Exception("News does not exist!");
				}else {
					existingNews.add(news);
					System.out.println("existing"+ existingNews.get(0).getTitle());
				}
			}
		}
		if(existingNews.isEmpty()) {
			throw new Exception("News does not exist!");
		}else {

			return existingNews;
		}
		
		//ArrayList<News> to_Ret = new ArrayList<>(culturalOfferRepo.getOne(culturalOffer).getNews());
		
	
	}

	public Page<News> findAll(Integer culturalOffer, Pageable pageable) {		
		Page<News> pages = newsRepo.findAllByCulturalOfferIdAndDeleted(culturalOffer,false, pageable);
		
		if(pages.isEmpty()) {
			return null;
		}else {
			return pages;
		}
	}

	public News create(News entity){
		return newsRepo.save(entity);
	}

	public News update(News entity, Integer id) throws Exception {
		News existingNews = newsRepo.findById(id).orElse(null);
		if(existingNews == null) {
			throw new Exception("News with given id does not exist!");
		}else if(existingNews.isDeleted()) {
			throw new Exception("News with given id is deleted and cannot be updated!");
		}
		
		Date date = new Date(); // This object contains the current date value
		
		existingNews.setTitle(entity.getTitle());
		existingNews.setText(entity.getText());
		existingNews.setDateAdded(date);
		
		return newsRepo.save(existingNews);
	}

	public void delete(Integer id) throws Exception {
		
		News existingNews = newsRepo.findById(id).orElse(null);
		if(existingNews == null) {
			throw new Exception("News with given id does not exist!");
		}else if(existingNews.isDeleted()){
			throw new Exception("News with given id is already deleted!");
		}
		
		existingNews.setDeleted(true);
		newsRepo.save(existingNews);
	}

	public void notifySubscribers(Integer id, News news) throws Exception {
		
		CulturalOffer culturalOffer;
		try {
			culturalOffer = culturalOfferRepo.findByidAndDeleted(id, false);
		}catch(Exception e) {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		for (User user : culturalOffer.getUsers()) {
			mailMessage.setTo(user.getEmail());
			mailMessage.setSubject("Recent news about "+culturalOffer.getName() +" - "+news.getTitle());
			mailMessage.setText(news.getText());
			new Thread(new Runnable() {
			    public void run() {
			    	 emailService.sendEmail(mailMessage);
			    }
			}).start();
		}
		
		
	}

}
