package com.cultural_content.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;
import com.cultural_content.model.RatingInfo;
import com.cultural_content.repos.CommentRepo;
import com.cultural_content.repos.PictureRepo;
import com.cultural_content.repos.RatingInfoRepo;

import static com.cultural_content.helper.Constants.*;

@Service
public class CommentService {
	
	@Autowired
	private CommentRepo commentRepo;
	
	@Autowired
	private PictureRepo pictureRepo;
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	@Autowired
	private RatingInfoRepo ratingInfoRepo;
	
	//public Comment findByText(String text) {
	//	return this.commentRepo.findOneBytext(text);
	//}
	
	public List<Comment> findAllByCultularOfferId(Integer id){
		List<Comment> comments = commentRepo.findByCulturalOfferIdAndDeleted(id, false);
		
		if(comments.isEmpty()) {
			return null;
		}
		
		for (Comment comment : comments) {
			comment.getPictures().removeIf(p -> p.isDeleted());
			
		}
		return comments;
	}
	
	public Comment findById(Integer id) {
		
		try {
			return commentRepo.findByIdAndDeleted(id, false);
		} catch (NoSuchElementException e) {
			return null;
		}
		
	}
	
	public Comment create(Comment comment) throws Exception {
	
		if(comment.getRating()<1.0 || comment.getRating()> 5.0) {
			throw new Exception(EXCEPTION_MESSAGE_6);
		}
		//rating
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(comment.getUser().getId(), comment.getCulturalOffer().getId());
				
		if(ratingInfo == null) { 
			/*
			 * ako je prvi rejting od usera
			 * kreirace se novi rejting
			 */
			ratingInfo = new RatingInfo(comment.getUser().getId(), comment.getCulturalOffer().getId(), comment.getRating());
			ratingInfoService.create(ratingInfo);
			
		}else {
			/*
			 * ako je user vec jednom rejtovao
			 * a sada ostavlja drugi komentar sa novim rejtingom
			 * taj rejting se racuna samo
			 */
			ratingInfo.setRatingValue(comment.getRating());
			ratingInfoService.update(ratingInfo);
		}
				
		return commentRepo.save(comment); 
	}
	
	public Comment update(Comment comment, Integer id) throws Exception {
		
		if(comment==null) {
			throw new Exception(EXCEPTION_MESSAGE_1);
		}
		
		if(comment.getRating()<1.0 || comment.getRating()> 5.0) {
			throw new Exception(EXCEPTION_MESSAGE_6);
		}
		
		Comment c = commentRepo.findByIdAndDeleted(id, false);
		
		
		c.setText(comment.getText());
		c.setRating(comment.getRating());
		
		c.setPictures(comment.getPictures());
		c.setDate(comment.getDate());
	
		
		//for (Picture p: c.getPictures()) {
			//System.out.println("slika: "+ p.toString());
		//}
		
		return commentRepo.save(c);
	}
	
	public void delete (Integer id) throws Exception {
		
		Comment comment = commentRepo.findByIdAndDeleted(id, false);
		if (comment == null) {
			throw new Exception(EXCEPTION_MESSAGE_1);
		}
		if(comment.isDeleted()) {
			throw new Exception(EXCEPTION_MESSAGE_2);
		}
		comment.setDeleted(true);
		for(Picture picture : comment.getPictures()) {
			picture.setDeleted(true);
			pictureRepo.save(picture);
		}
		commentRepo.save(comment);
	}
	
	public Comment getAnyById(Integer id) {
		//deleted or not
		return commentRepo.findById(id).get();
	}

	public Page<Comment> findAll(Pageable pageable, Integer id) {
		Page<Comment> pages = commentRepo.findByCulturalOfferIdAndDeleted(id, false, pageable);
		if(pages.isEmpty()) {
			return null;
		}
		for (Comment comment : pages) {
			comment.getPictures().removeIf(p -> p.isDeleted());
		}
		return pages;
	}
}
