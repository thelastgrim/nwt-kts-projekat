package com.cultural_content.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;
import com.cultural_content.model.News;
import com.cultural_content.model.Picture;
import com.cultural_content.model.User;
import com.cultural_content.repos.CommentRepo;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.LocationRepo;
import com.cultural_content.repos.NewsRepo;
import com.cultural_content.repos.PictureRepo;

import javassist.tools.reflect.Reflection;

@Service
public class CulturalOfferService {
	
	@Autowired
	private CulturalOfferRepo culturalOfferRepo;
	
	@Autowired 
	CategoryService categoryService;
	
	@Autowired 
	NewsRepo newsRepository;
	
	@Autowired 
	LocationRepo locationRepository;
	
	@Autowired 
	CommentRepo commentRepository;
	
	@Autowired 
	PictureRepo pictureRepository;
	
	@Autowired 
	NewsRepo newsRepo;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	UserService userService;
	
	public CulturalOffer findById(Integer id) {
		return this.culturalOfferRepo.findByidAndDeleted(id, false);
	}
	
//	public List<CulturalOffer> findByCity(String city){
//		return this.culturalOfferRepo.findAllByLocation_cityAndDeleted(city, false);
//	}
//	
	
	public CulturalOffer findOne(Integer Id) throws Exception {
		CulturalOffer culturalOffer = culturalOfferRepo.findByidAndDeleted(Id, false);
		if(culturalOffer != null) {
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			
			return culturalOffer;
		}else {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
	}
	
	public Page<CulturalOffer> findByName(String name, Pageable pageable) throws Exception {
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByNameContainingIgnoreCaseAndDeleted(name, false, pageable);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>((newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false))));
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			culturalOffer.getComments().removeIf(p -> p.isDeleted());
		}
		return culturalOffers;
	}
	
	public List<CulturalOffer> findAllByCategory(Integer Id){
		List<CulturalOffer> culturalOffers = culturalOfferRepo.findAllByCategory_idAndDeleted(Id, false);
		for(CulturalOffer culturalOffer : culturalOffers) {
			
			culturalOffer.setNews(new HashSet<News>((newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false))));
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			//culturalOffer.getNews().removeIf(p -> p.isDeleted());
			culturalOffer.getComments().removeIf(p -> p.isDeleted());
		}
		return culturalOffers;
	}
	
	public CulturalOffer create(CulturalOffer culturalOffer, Location location, Integer categoryId) throws Exception {

		if((culturalOfferRepo.findOneByNameAndDeleted(culturalOffer.getName(), false) != null)) {
			throw new Exception("Cultural offer with that name already exists!");
		}else if(locationRepository.findOneByLATAndLNGAndDeleted(location.getLAT(), location.getLNG(), false) != null) {
			throw new Exception("Cultural Offer on this location already exists!");
		}

		System.out.println("0*");
		CulturalOffer newOffer = new CulturalOffer();


		System.out.println("2*");
		newOffer.setLocation(location);
		

		System.out.println("3*");
		newOffer.setCategory(categoryService.findOne(categoryId));


		System.out.println("4*");
		newOffer = culturalOfferRepo.save(culturalOffer);

		System.out.println("Cultural offer created!");
		return newOffer;
	}
	
	
	public CulturalOffer update(CulturalOffer culturalOffer, Integer Id) throws Exception {
		
		CulturalOffer existingCulturalOffer = culturalOfferRepo.findByidAndDeleted(Id, false);
		if(existingCulturalOffer == null) {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
		
		if(culturalOfferRepo.findByNameAndIdNotAndDeleted(culturalOffer.getName(), Id, false) != null) {
			throw new Exception("Cultural offer with given name already exists");
		}
		
		notifySubscribers(existingCulturalOffer, culturalOffer);
		
		existingCulturalOffer.setAbout(culturalOffer.getAbout());
		existingCulturalOffer.setName(culturalOffer.getName());
		if(culturalOffer.getCover() != null) {
			existingCulturalOffer.setCover(culturalOffer.getCover());
		}
		if(new Exception().getStackTrace()[1].getClassName().equals("com.cultural_content.schedulers.RatingScheduler")) {
			existingCulturalOffer.setAverageRating(culturalOffer.getAverageRating());
			System.err.println("saved");
		}
		//System.out.println("pozvan od: "+ new Exception().getStackTrace()[1].getClassName());
		//System.out.println("pozvan od: "+ new Exception().getStackTrace()[0].getClassName());
		//System.out.println("pozvan od: "+ new Exception().getStackTrace()[2].getClassName());
		
		return culturalOfferRepo.save(existingCulturalOffer);
	}
	
	public void delete(Integer Id) throws Exception {
		
		CulturalOffer existingCulturalOffer = culturalOfferRepo.findByidAndDeleted(Id, false);
		if(existingCulturalOffer == null) {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
		
		existingCulturalOffer.setDeleted(true);
		if(existingCulturalOffer.getLocation() != null)
			existingCulturalOffer.getLocation().setDeleted(true);

		for(News news : newsRepo.findByCulturalOfferIdAndDeleted(existingCulturalOffer.getId(), false)) {
			
			news.setDeleted(true);
			newsRepository.save(news);
		}
		for(Comment comment : existingCulturalOffer.getComments()) {
			comment.setDeleted(true);
			commentRepository.save(comment);
			for(Picture picture : comment.getPictures()) {
				picture.setDeleted(true);
				pictureRepository.save(picture);
			}
		}
		culturalOfferRepo.save(existingCulturalOffer);
	}
	
	public Page<CulturalOffer> findAllByCity(String city, Pageable pageable){
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByLocation_cityAndDeleted(city, false, pageable);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
		}
		return culturalOffers;
	}
	
	//metoda se koristi samo za dobavljanje na homepageu, gde ne trabaju da imaju news i komentari 
	public Page<CulturalOffer> findAllByCategory(Integer id, Pageable pageable){
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByCategory_idAndDeleted(id, false, pageable);
		
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>());
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			//culturalOffer.getComments().removeIf(p -> p.isDeleted());
			culturalOffer.setComments(new HashSet<Comment>());
		}
		
		return culturalOffers;
	}
	
	public Page<CulturalOffer> findAll(Pageable pageable){
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findAllBydeleted(false, pageable);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
		}
		return culturalOffers;
	}

	public List<CulturalOffer> findAllByDeleted() {
		List<CulturalOffer> culturalOffers = culturalOfferRepo.findAllByDeleted(false);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>((newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false))));
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			//culturalOffer.getNews().removeIf(p -> p.isDeleted());
			culturalOffer.getComments().removeIf(p -> p.isDeleted());
		}
		return culturalOffers;		
	}

	public void subscribe(Integer offerId, Integer user_id) throws Exception {
		
		User user = userService.findById(user_id);
		CulturalOffer culturalOffer;
		
		try {
			culturalOffer = culturalOfferRepo.findByidAndDeleted(offerId, false);
		}catch(Exception e) {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
		for (User u : culturalOffer.getUsers()) {
			if (u.getId() == user.getId()) {
				throw new Exception("Already subscribed");
			}
		}
		
		culturalOffer.getUsers().add(user);
		culturalOfferRepo.save(culturalOffer);
		
		
	}

	public void unsubscribe(Integer offerId, Integer user_id) throws Exception {
		
		User user = userService.findById(user_id);
		CulturalOffer culturalOffer;
		
		try {
			culturalOffer = culturalOfferRepo.findByidAndDeleted(offerId, false);
		}catch(Exception e) {
			throw new Exception("Cultural offer with given id doesn't exist");
		}
		boolean find = false;
		for (User u : culturalOffer.getUsers()) {
			if (u.getId() == user.getId()) {
				culturalOffer.getUsers().remove(u);
				culturalOfferRepo.save(culturalOffer);
				find = true;
				break;
			}
		}
		
		if (!find) {
			throw new Exception("You are not subscribed.");
		}
		
	}
	
	
	public List<CulturalOffer> findAllByCategoryAndUserId(Integer id, Integer user_id){
		List<CulturalOffer> culturalOffers = culturalOfferRepo.findByCategory_idAndDeletedAndUsersId(id, false, user_id);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>((newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false))));
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			culturalOffer.getComments().removeIf(p -> p.isDeleted());
		}
		return culturalOffers;
	}
	
	public Page<CulturalOffer> findByCategoryAndLocation(Integer categoryId, String city, Pageable pageable){
		
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByCategory_idAndLocation_cityAndDeleted(categoryId, city, false, pageable);
		for(CulturalOffer culturalOffer : culturalOffers) {
			culturalOffer.setNews(new HashSet<News>());
			culturalOffer.setComments(new HashSet<Comment>());
		}
		return culturalOffers;
		
	}
	
	public void notifySubscribers(CulturalOffer existingOffer, CulturalOffer newOffer) {
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		for (User user : existingOffer.getUsers()) {
			mailMessage.setTo(user.getEmail());
			mailMessage.setSubject("Updates about "+ existingOffer.getName());
			if(existingOffer.getName().compareTo(newOffer.getName()) != 0 && existingOffer.getAbout().compareTo(newOffer.getAbout()) != 0) {
				mailMessage.setText("New name of the offer: " + newOffer.getName() + "\n" + "New about: " + newOffer.getAbout());
			}else if(existingOffer.getName().compareTo(newOffer.getName()) != 0) {
				mailMessage.setText("New name of the offer: " + newOffer.getName() + "\n");
			}else if(existingOffer.getAbout().compareTo(newOffer.getAbout()) != 0) {
				mailMessage.setText("New about: " + newOffer.getAbout());
			}else {
				mailMessage.setText("");
			}
			
			if(mailMessage.getText().length() != 0) {
				new Thread(new Runnable() {
				    public void run() {
				    	 emailService.sendEmail(mailMessage);
				    }
				}).start();
			}
			
		}
	}

	public CulturalOffer findOneByName(String name) throws Exception {
		CulturalOffer culturalOffer = culturalOfferRepo.findOneByNameAndDeleted(name, false);
		if(culturalOffer != null) {
			culturalOffer.setNews(new HashSet<News>((newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false))));
			//culturalOffer.setNews(Set.copyOf(newsRepo.findByCulturalOfferIdAndDeleted(culturalOffer.getId(), false)));
			//culturalOffer.getNews().removeIf(p -> p.isDeleted());
			culturalOffer.getComments().removeIf(p -> p.isDeleted());
			return culturalOffer;
		}else {
			throw new Exception("Cultural offer with given name doesn't exist");
		}
	}
	
}
