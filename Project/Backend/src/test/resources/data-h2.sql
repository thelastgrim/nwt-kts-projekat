-- password is 'admin' (bcrypt encoded)
INSERT INTO all_users ( username, email, password, type) VALUES ('admin', 'admin@gmail.com', '$2y$12$qdRW3knqrzXQsTUwM0Rc8eGPHUnme9YSFCe5I4Uz67JSlNnWG8fcm', 'ADMIN');
-- password is 'user' (bcrypt encoded)
INSERT INTO all_users ( username, email, password, activated, type) VALUES ( 'test', 'test@gmail.com','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq', 'true', 'USER');
INSERT INTO all_users ( username, email, password, activated, type) VALUES ('test2', 'test2@gmail.com','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq', 'true', 'USER');

INSERT INTO authority (name) VALUES ('ROLE_ADMIN');
INSERT INTO authority (name) VALUES ('ROLE_USER');

insert into user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN
insert into user_authority (user_id, authority_id) values (2, 2); -- user has ROLE_USER
insert into user_authority (user_id, authority_id) values (3, 2); -- user has ROLE_USER




--SLIKE
insert into picture(path, title) values ('src//main//resources//pictures//0Belica 2.jpg', 'Pigeon in hand');

--KATEGORIJE 5
insert into category (name) VALUES ('Museums');
insert into category (name) VALUES ('Teathers');
insert into category (name) VALUES ('Concerts');
insert into category (name) VALUES ('Festival');

insert into category (name) VALUES ('Gallery');

--PONUDE 18 ih ima trenutno
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Museum1', 1, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Museum2', 1, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Museum3', 1, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Museum4', 1, 1);

insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Teathers1', 2, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Teathers2', 2, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Teathers3', 2, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Teathers4', 2, 1);

insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Concerts1', 3, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Concerts2', 3, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 5.0, 'Concerts3', 3, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 4.0, 'Concerts4', 3, 1);

insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 3.0, 'Festival1', 4, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 2.0, 'Festival2', 4, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 3.0, 'Festival3', 4, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 2.0, 'Festival4', 4, 1);

insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 1.0, 'Gallery1', 5, 1);
insert into cultural_offer (about, average_rating, name, category_id, cover_id) VALUES ( 'test', 0.0, 'Gallery2', 5, 1);
--insert into cultural_offer (about, average_rating, name, category_id) VALUES ( 'Founded in 1861', 5.0, 'Srpsko narodno pozoriste', 2);

--LOKACIJE--
insert into location (lat, lng, city, country, street, cultural_offer_id) values (51, 19.84, 'Test1', 'Test1', 'Test', 1);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (52, 19.84, 'Test2', 'Test2', 'Test', 2);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (53, 19.84, 'Test3', 'Test3', 'Test', 3);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (54, 19.84, 'Test4', 'Test4', 'Test', 4);

insert into location (lat, lng, city, country, street, cultural_offer_id) values (55, 19.84, 'Test5', 'Test5', 'Test', 5);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (56, 19.84, 'Test6', 'Test6', 'Test', 6);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (57, 19.84, 'Test7', 'Test7', 'Test', 7);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (58, 19.84, 'Test8', 'Test8', 'Test', 8);

insert into location (lat, lng, city, country, street, cultural_offer_id) values (59, 19.84, 'Test', 'Test9', 'Test', 9);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (60, 19.84, 'Test', 'Test10', 'Test', 10);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (70, 19.84, 'Test', 'Test11', 'Test', 11);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (80, 19.84, 'Test', 'Test12', 'Test', 12);

insert into location (lat, lng, city, country, street, cultural_offer_id) values (90, 19.84, 'Test', 'Test13', 'Test', 13);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (10, 19.84, 'Test', 'Test14', 'Test', 14);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (20, 19.84, 'Test', 'Test15', 'Test', 15);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (30, 19.84, 'Test', 'Test16', 'Test', 16);


insert into location (lat, lng, city, country, street, cultural_offer_id) values (12, 19.84, 'Test', 'Test17', 'Test', 17);
insert into location (lat, lng, city, country, street, cultural_offer_id) values (13, 19.84, 'Test', 'Test18', 'Test', 18);




--News 20
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-01','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov1.0',1);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-07-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov1.1',1);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-08-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov1.2',1);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-02','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov2',2);

insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-03','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov3',3);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-04','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov4',4);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-05','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov5',5);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov6',6);

insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-07','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov7',7);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-08','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov8',8);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-09','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov9',9);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-10','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov10',10);

insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-11','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov11',11);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-12','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov12',12);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-01-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov13',13);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-02-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov14',14);

insert into news(date_added, text, title, cultural_offer_id) values ('2020-03-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov15',15);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-04-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov16',16);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-05-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov17',17);
insert into news(date_added, text, title, cultural_offer_id) values ('2020-06-06','Culture unites people of a single society together through shared beliefs, traditions, and expectations.','naslov18',18);


--KOMENTARI
insert into comment (date, rating, text, cultural_offer_id, user_id) VALUES ('2020-06-06', 5.0, 'This places was great.', 1, 2);
insert into comment (date, rating, text, cultural_offer_id, user_id) VALUES ('2020-05-05', 1.0, 'Shitty museum', 1, 3);


--SLIKE
insert into picture(path, title, comment_id, user_id) values ('src//main//resources//pictures//0Belica 2.jpg', 'Pigeon in hand 2', 1, 2);

--OCENE
insert into rating_info(offer_id, rating_value, user_id) values (1, 3.0, 2);

--SUBSCRIBE
insert into subscribers_offers(offer_id, user_id) values (1, 2);
insert into subscribers_offers(offer_id, user_id) values (2, 2);
insert into subscribers_offers(offer_id, user_id) values (3, 2);