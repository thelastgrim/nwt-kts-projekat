package com.cultural_content.constants;

public class UserConstants {

	public static final Integer DB_USER_ID = 2; //1 je admin valjda
	public static final Integer DB_SECOND_USER_ID = 3; //1 je admin valjda
	public static final String DB_USER_EMAIL = "test@gmail.com";
	public static final String DB_USER_USERNAME = "test";
	public static final String DB_USER_PASSWORD = "user";
	public static final String NEW_USER_USERNAME = "newUser";
	public static final String NEW_USER_PASSWORD = "newPassword";
	public static final String NEW_USER_EMAIL= "newEmail@gmail.com";
	public static final Integer FAKE_USER_ID = 258; 
	public static final String USER_ALREADY_EXIST_EXCEPTION_MSG = "User with given email address or username already exists";
	public static final String FAKE_OLD_PASSWORD= "user2";
	public static final String DB_WRONG_USER_PASSWORD = "wrong";
	public static final Integer NEW_USER_ID_UNIT = 951; 

	



	
	
}
