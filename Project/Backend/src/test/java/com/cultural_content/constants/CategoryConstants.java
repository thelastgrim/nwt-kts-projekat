package com.cultural_content.constants;

public class CategoryConstants {

	public static final Integer DB_CATEGORY_ID = 1;
	public static final Integer DB_CATEGORY_SIZE = 5;
	
	public static final String DB_CATEGORY_NAME = "Museums";
	public static final String DB_CATEGORY_NAME_ALREADY_EXIST = "Teathers";

	public static final String DB_CATEGORY_NAME_NOT_EXIST = "notExist";
	
	
	public static final String DB_NEW_CATEGORY = "NewCategory";
	public static final String DB_NEW_CATEGORY_NAME = "NewCategoryName";
	
	public static final int DB_NEW_CATEGORY_ID = 6;

	public static final int DB_CATEGORY_ID_NOT_EXIT = 7;
	
	public static final String DB_CATEGORY_UPDATED_TEXT = "UpdatedCategory";
	
	public static final boolean DB_CATEGORY_DELETED = false;

	public static final Integer DB_CATEGORY_ID_FOR_DELETE = 2;
	
	public static final Integer DB_CATEGORY_PAGE_SIZE = 5;
	
}
