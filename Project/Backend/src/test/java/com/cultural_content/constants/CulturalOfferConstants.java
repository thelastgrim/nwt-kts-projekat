package com.cultural_content.constants;

public class CulturalOfferConstants {
	
	public static final Integer DB_CULTURAL_OFFER_ID = 1;
	
	
	public static final Integer DB_CULTURAL_OFFER_SIZE = 18;
	public static final Integer DB_CULTURAL_OFFER_SIZE_BY_CATEGORY = 4;
	public static final Integer DB_CULTURAL_OFFER_SIZE_BY_CITY = 10;
	public static final Integer DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_USER = 3;
	public static final Integer DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_CITY = 0;
	
	public static final String DB_CULTURAL_OFFER_NAME = "Museum1";
	public static final String DB_CULTURAL_OFFER_NAME_2 = "Festival1";
	
	public static final boolean DB_CULTURAL_OFFER_DELETED = false;
	
	public static final String NEW_CULTURAL_OFFER_NAME = "Museum5";
	public static final String NEW_CULTURAL_OFFER_ABOUT = "Museum is founded.";
	public static final double NEW_CULTURAL_OFFER_AVG_RATING = 5;
	
	public static final String UPDATED_CULTURAL_OFFER_NAME = "NewMuseum";
	
	public static final String CULTURAL_OFFER_NAME_FOR_DELETE = "DeleteMuseumNow";
	
	public static final Integer NOT_DB_CULTURAL_OFFER_ID = 100;
	public static final String NOT_DB_CULTURAL_OFFER_NAME = "-1";
	public static final Integer NOT_DB_CATEGORY_ID = 0;
	
	public static final Integer DB_CULTURAL_OFFER_ID_SUBSCRIBE = 4;
	
	public static final String ID_OF_NEW_OFFER = "19";
	
	
}
