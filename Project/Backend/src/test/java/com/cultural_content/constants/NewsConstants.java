package com.cultural_content.constants;

public class NewsConstants {

	public static final Integer DB_NEWS_ID = 1;
	public static final String DB_NEW_TITLE_ID_1 = "naslov1.0";

	public static final String DB_NEW_TEXT_ID_1 = "Culture unites people of a single society together through shared beliefs, traditions, and expectations.";

	public static final Integer DB_NEWS_SIZE = 20;
	public static final boolean DB_NEWS_DELETED = false;
	public static final Integer DB_NEWS_UPDATE_ID = 2;
	
	//for search by cultural offer
	public static final Integer DB_CULTURAL_OFFER_ID = 1;

	public static final Integer DB_CATEGORY_ID = 1;
	public static final Integer DB_NEWS_IN_CULTURAL_OFFER = 3;//only for cultural offer id = 1
	
	//for creating new news
	public static final String DB_NEW_NEWS_TITLE = "newNewsTitle";
	public static final String DB_NEW_NEWS_TEXT = "newNewsText";

	
	//Find all news - fail
	public static final Integer DB_CULTURAL_OFFER_DOES_NOT_EXIST = -1;

	//Find one news- fail
	public static final int DB_NEW_DOES_NOT_EXIST_ID = 21;

}
