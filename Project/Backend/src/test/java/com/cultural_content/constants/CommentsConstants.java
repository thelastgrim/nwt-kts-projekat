package com.cultural_content.constants;

public class CommentsConstants {

	public static final Integer DB_COMMENTS_ID = 1;
	public static final Integer DB_COMMENTS_SIZE = 2;
	public static final boolean DB_COMMENTS_DELETED = false;
	public static final String DB_COMMENT_TEXT = "TEST COMMENT";
	public static final String DB_COMMENT_UPDATED_TEXT = "TEST COMMENT UPDATED";
}
