package com.cultural_content.constants;

public class PictureConstants {
	
	public static final Integer DB_PICTURE_ID = 1;
	public static final Integer DB_PICTURE_SIZE = 1;
	public static final boolean DB_PICTURE_DELETED = false;
	
	public static final String DB_PICTURE_TITLE = "TEST PICTURE";
	public static final String DB_PICTURE_TITLE_UPDATED = "TEST PICTURE UPDATED";
	
	public static String DB_PICTURE_UPLOAD_LOCATION = "src//main//resources//pictures//pic.jpg";
	public static String DB_PICTURE_TO_UPLOAD = "src//test//resources//testpic.jpg";

}
