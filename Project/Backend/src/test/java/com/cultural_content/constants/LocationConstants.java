package com.cultural_content.constants;

public class LocationConstants {
	
	public static final double DB_LOCATION_LAT = 51;
	public static final double DB_LOCATION_LNG = 19.84;

	public static final String NEW_LOCATION_LAT_CREATE_FAIL = "51";
	public static final String NEW_LOCATION_LNG_CREATE_FAIL = "19.84";
	

	public static final  String NEW_LOCATION_LAT_CREATE = "90";
	public static final  String NEW_LOCATION_LNG_CREATE = "180";
	
	public static final String DB_LOCATION_NAME = "Test";
	
	public static final boolean DB_LOCATION_DELETED = false;
	
	public static final Integer DB_LOCATION_SIZE = 18;
	
	public static final Integer DB_LOCATION_UNIQUE_CITIES_SIZE = 9;
	

	public static final double NEW_LOCATION_LAT = 90;
	public static final double NEW_LOCATION_LNG = 180;
	
	public static final String NEW_LOCATION = "Lokacija1";
	public static final String NEW_LOCATION_CITY = "Test";
	public static final String NEW_LOCATION_STREET = "Street";
	public static final String NEW_LOCATION_COUNTRY = "Country";
	
	public static final double NEW_LOCATION_LAT_FOR_DELETE = -90;
	public static final double NEW_LOCATION_LNG_FOR_DELETE = -180;
}
