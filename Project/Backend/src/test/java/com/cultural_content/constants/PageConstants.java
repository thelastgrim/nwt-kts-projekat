package com.cultural_content.constants;

public class PageConstants {
	
	public static final int PAGE_INDEX = 0;
	public static final int PAGE_SIZE = 10;

}
