package com.cultural_content.constants;

public class RatingInfoConstants {
	
	public static final Integer DB_RATING_INFO_ID = 1;
	public static final Integer DB_RATING_INFO_SIZE = 1;
	//public static final boolean DB_COMMENTS_DELETED = false;
	public static final double DB_RATING_INFO_VALUE = 2.0;
	public static final double DB_RATING_INFO_VALUE_UPDATED = 2.5;
	
	public static final int DB_RATINGS_FOR_CULTURAL_OFFER = 2; //No. of ratings for cultural offer with id = 1
	//1 in base and 1 created

}
