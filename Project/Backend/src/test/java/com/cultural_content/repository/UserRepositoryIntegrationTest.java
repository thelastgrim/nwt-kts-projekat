package com.cultural_content.repository;

import static com.cultural_content.constants.UserConstants.DB_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.DB_USER_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class UserRepositoryIntegrationTest {

	@Autowired
	private UserRepo userRepo;
	
	@Test
	public void testfindByEmailOrUsername() {
		User user = userRepo.findByEmailOrUsername(DB_USER_EMAIL, DB_USER_USERNAME);
		assertEquals(DB_USER_EMAIL, user.getEmail());
		assertEquals(DB_USER_USERNAME, user.getUsername());
	}
	
	@Test
	public void testfindByEmailAndUsername() {
		User user = userRepo.findByEmailOrUsername(DB_USER_EMAIL, DB_USER_USERNAME);
		assertEquals(DB_USER_EMAIL, user.getEmail());
		assertEquals(DB_USER_USERNAME, user.getUsername());
	}
}
