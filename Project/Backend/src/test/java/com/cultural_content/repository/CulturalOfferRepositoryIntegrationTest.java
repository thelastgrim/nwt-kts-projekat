package com.cultural_content.repository;

import static com.cultural_content.constants.CategoryConstants.DB_CATEGORY_ID;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_DELETED;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_NAME_2;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE_BY_CATEGORY;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_USER;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_NAME;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.repos.CulturalOfferRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CulturalOfferRepositoryIntegrationTest {

	@Autowired
	CulturalOfferRepo culturalOfferRepo;
	
	@Test
	public void testfindAllByCategory_idAndDeleted() {
		List<CulturalOffer> culturalOffers = new ArrayList<CulturalOffer>();
		culturalOffers = culturalOfferRepo.findAllByCategory_idAndDeleted(DB_CATEGORY_ID, DB_CULTURAL_OFFER_DELETED);
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, culturalOffers.size());
	}
	
	@Test
	public void testfindAllByLocation_cityAndDeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByLocation_cityAndDeleted(DB_LOCATION_NAME, DB_CULTURAL_OFFER_DELETED, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	
	@Test
	public void testfindByCategory_idAndDeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByCategory_idAndDeleted(DB_CATEGORY_ID, DB_CULTURAL_OFFER_DELETED, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	@Test
	public void testFindByNameContainingIgnoreCaseAndDeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByNameContainingIgnoreCaseAndDeleted(DB_CULTURAL_OFFER_NAME, DB_CULTURAL_OFFER_DELETED, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	
	@Test
	public void testfindAllBydeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findAllBydeleted(DB_CULTURAL_OFFER_DELETED, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	
	@Test
	public void testfindByidAndDeleted() {
		CulturalOffer culturalOffer = culturalOfferRepo.findByidAndDeleted(DB_CULTURAL_OFFER_ID, DB_CULTURAL_OFFER_DELETED);
		assertEquals(DB_CULTURAL_OFFER_ID, culturalOffer.getId());
	}
	
	@Test
	public void testfindByNameAndIdNotAndDeleted() {
		CulturalOffer culturalOffer = culturalOfferRepo.findByNameAndIdNotAndDeleted(DB_CULTURAL_OFFER_NAME_2, DB_CULTURAL_OFFER_ID, DB_CULTURAL_OFFER_DELETED);
		assertEquals(DB_CULTURAL_OFFER_NAME_2, culturalOffer.getName());
		assertNotSame(DB_CULTURAL_OFFER_ID, culturalOffer.getId());
	}
	
	@Test
	public void testfindOneByNameAndDeleted() {
		CulturalOffer culturalOffer = culturalOfferRepo.findOneByNameAndDeleted(DB_CULTURAL_OFFER_NAME, DB_CULTURAL_OFFER_DELETED);
		assertEquals(DB_CULTURAL_OFFER_NAME, culturalOffer.getName());
	}
	
	@Test
	public void testfindAllByDeleted() {
		List<CulturalOffer> culturalOffers = new ArrayList<CulturalOffer>();
		culturalOffers = culturalOfferRepo.findAllByDeleted(DB_CULTURAL_OFFER_DELETED);
		assertEquals(DB_CULTURAL_OFFER_SIZE, culturalOffers.size());
	}
	
	@Test
	public void findByCategory_idAndDeletedAndUsersId() {
		List<CulturalOffer> culturalOffers = new ArrayList<CulturalOffer>();
		culturalOffers = culturalOfferRepo.findByCategory_idAndDeletedAndUsersId(DB_CATEGORY_ID, DB_CULTURAL_OFFER_DELETED, DB_USER_ID);
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_USER, culturalOffers.size());
	}
	
	@Test
	public void testFindByCategory_idAndLocation_cityAndDeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferRepo.findByCategory_idAndLocation_cityAndDeleted(DB_CATEGORY_ID, DB_LOCATION_NAME, DB_CULTURAL_OFFER_DELETED, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	
	
}
