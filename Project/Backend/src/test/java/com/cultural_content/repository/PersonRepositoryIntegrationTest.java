package com.cultural_content.repository;

import static com.cultural_content.constants.PersonConstant.DB_PERSON_EMAIL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Person;
import com.cultural_content.repos.PersonRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class PersonRepositoryIntegrationTest {
	
	@Autowired
	private PersonRepo personRepo;
	
	@Test
	public void testfindOneByemail() {
		Person person = personRepo.findOneByemail(DB_PERSON_EMAIL);
		assertEquals(DB_PERSON_EMAIL, person.getEmail());
	}

}
