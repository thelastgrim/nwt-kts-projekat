package com.cultural_content.repository;

import static com.cultural_content.constants.NewsConstants.*;

import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.News;
import com.cultural_content.repos.NewsRepo;;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class NewsRepositoryIntegrationTest {
	
	@Autowired
	private NewsRepo newsRepo;
	
	@Test
	public void testFindByCulturalOfferIdAndDeleted() {
		List<News> news = newsRepo.findByCulturalOfferIdAndDeleted(DB_CULTURAL_OFFER_ID, DB_NEWS_DELETED);
		assertEquals(DB_NEWS_IN_CULTURAL_OFFER, news.size());
	}
	@Test
	public void testFindOneByIdAndDeleted() {
		News news = newsRepo.findOneByidAndDeleted(DB_NEWS_ID, DB_NEWS_DELETED);
		assertEquals(DB_NEWS_ID, news.getId());
		
	}
	
	@Test
	public void testFindAllByDeleted() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<News> news = newsRepo.findAllByCulturalOfferIdAndDeleted(DB_CULTURAL_OFFER_ID,DB_NEWS_DELETED, pageable);
		assertEquals(PAGE_SIZE, news.getSize());
		
		
	}
	

}
