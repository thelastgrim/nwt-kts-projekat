package com.cultural_content.repository;

import static com.cultural_content.constants.LocationConstants.DB_LOCATION_DELETED;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_LAT;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_LNG;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Location;
import com.cultural_content.repos.LocationRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class LocationRepositoryIntegrationTest {
	
	@Autowired
	LocationRepo locationRepository;
	
	@Test
	public void testFindOneByLATAndLNG() {
		
		Location location = locationRepository.findOneByLATAndLNGAndDeleted(DB_LOCATION_LAT, DB_LOCATION_LNG, DB_LOCATION_DELETED);
		assertEquals(DB_LOCATION_LAT,location.getLAT());
		assertEquals(DB_LOCATION_LNG,location.getLNG());
	}
	
	@Test
	public void testFindAllBydeleted() {
		
		List<Location> locations = locationRepository.findAllBydeletedOrderByCityAsc(DB_LOCATION_DELETED);
		assertEquals(DB_LOCATION_SIZE, locations.size());
		
	}
}
