package com.cultural_content.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static com.cultural_content.constants.CategoryConstants.*;

import static com.cultural_content.constants.PageConstants.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Category;
import com.cultural_content.repos.CategoryRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CategoryRepositoryIntegrationTest {
	
	@Autowired
	private CategoryRepo categoryRepo;
	
	@Test
	public void testfindByIdAndDeleted() {
		Category category = categoryRepo.findByIdAndDeleted(DB_CATEGORY_ID, DB_CATEGORY_DELETED);
		assertEquals(DB_CATEGORY_ID, category.getId());
	}
	
	@Test
	public void testfindOneByNameAndDeleted() {
		Category category = categoryRepo.findOneByNameAndDeleted(DB_CATEGORY_NAME, DB_CATEGORY_DELETED);
		assertEquals(DB_CATEGORY_NAME, category.getName());
	}
	
	@Test
	public void testfindAllBydeleted() {
		//find all categories which are not deleted
		List<Category> categories = categoryRepo.findAllBydeleted(DB_CATEGORY_DELETED);
		assertEquals(DB_CATEGORY_SIZE, categories.size());
	}
	
	
	@Test
	public void testfindAllBydeletedPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Category> categories = categoryRepo.findAllBydeleted(DB_CATEGORY_DELETED, pageable);
		assertEquals(PAGE_SIZE, categories.getSize());
	}
	

}
