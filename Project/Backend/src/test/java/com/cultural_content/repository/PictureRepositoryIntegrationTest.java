package com.cultural_content.repository;

import static com.cultural_content.constants.CommentsConstants.DB_COMMENTS_ID;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static com.cultural_content.constants.PictureConstants.DB_PICTURE_DELETED;
import static com.cultural_content.constants.PictureConstants.DB_PICTURE_ID;
import static com.cultural_content.constants.PictureConstants.DB_PICTURE_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Picture;
import com.cultural_content.repos.PictureRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class PictureRepositoryIntegrationTest {

	@Autowired
	private PictureRepo pictureRepo;
	
	@Test
	public void testFindByIdDeleted() {
	
		Picture picture = pictureRepo.findByIdAndDeleted(DB_PICTURE_ID, DB_PICTURE_DELETED);
		
		assertEquals(DB_PICTURE_ID, picture.getId());
	}
	
	@Test
	public void testFindAllByCommentIdAndDeleted() {
		
		List<Picture> pictures = pictureRepo.findAllByCommentIdAndDeleted(DB_COMMENTS_ID, DB_PICTURE_DELETED);
		
		assertEquals(DB_PICTURE_SIZE, pictures.size());
	}
	
	@Test
	public void testFindAllByCommentIdAndDeletedPageable() {
		
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		
		Page<Picture> pictures = pictureRepo.findAllByCommentIdAndDeleted(DB_COMMENTS_ID, DB_PICTURE_DELETED, pageable);
		
		assertEquals(PAGE_SIZE, pictures.getSize());
		
	}
}

