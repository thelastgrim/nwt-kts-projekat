package com.cultural_content.repository;

import static com.cultural_content.constants.AuthorityConstant.DB_AUTHORITY_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Authority;
import com.cultural_content.repos.AuthorityRepo;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class AuthorityRepositoryIntegrationTest {
	
	@Autowired
	private AuthorityRepo authorityRepo;
	
	@Test
	public void testfindByname() {
		Authority auth = authorityRepo.findByname(DB_AUTHORITY_NAME);
		assertEquals(DB_AUTHORITY_NAME, auth.getName());
	}
	

}
