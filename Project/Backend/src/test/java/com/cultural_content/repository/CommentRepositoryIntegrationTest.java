package com.cultural_content.repository;

import static com.cultural_content.constants.CommentsConstants.DB_COMMENTS_DELETED;
import static com.cultural_content.constants.CommentsConstants.DB_COMMENTS_ID;
import static com.cultural_content.constants.CommentsConstants.DB_COMMENTS_SIZE;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Comment;
import com.cultural_content.repos.CommentRepo;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CommentRepositoryIntegrationTest {

	@Autowired
	CommentRepo commentRepo;
	
	@Test
	public void testFindByIdAndDeleted() {
		Comment comment = commentRepo.findByIdAndDeleted(DB_COMMENTS_ID, DB_COMMENTS_DELETED);
		
		assertEquals(DB_COMMENTS_ID, comment.getId());
		
	}
	
	@Test
	public void testFindByCulturalOfferIdAndDeleted() {
		
		List<Comment> comments = commentRepo.findByCulturalOfferIdAndDeleted(DB_CULTURAL_OFFER_ID, DB_COMMENTS_DELETED);
		
		assertEquals(DB_COMMENTS_SIZE, comments.size());
	}
	
	@Test
	public void testFindByCulturalOfferIdAndDeletedPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
	
		Page<Comment> comments = commentRepo.findByCulturalOfferIdAndDeleted(DB_CULTURAL_OFFER_ID,
																			DB_COMMENTS_DELETED,
																			pageable);
		
		
		
		assertEquals(PAGE_SIZE, comments.getSize());
		
		
	}
	
	
}
