package com.cultural_content.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.RatingInfo;
import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;
import com.cultural_content.schedulers.RatingScheduler;

import static com.cultural_content.constants.UserConstants.*;
import static com.cultural_content.constants.CulturalOfferConstants.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class SchedulerIntegrationTest {
	
	@Autowired
	private RatingScheduler ratingScheduler;
	
	@Autowired
	private UserService userService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private CulturalOfferService culturalOfferService;
	@Autowired
	private RatingInfoService ratingInfoService;
	
	@Test
	public void testRatingSchedulerVoteInBase() {
		/*vote in base is 3.0
		 * cultural offer has 5.0 rating (default rating)
		 */
		ratingScheduler.scheduleFixedRateTask();
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		
		assertEquals(3.0, culturalOffer.getAverageRating());
	
		
		
	}
	
	@Test
	public void testRatingSchedulerSameUserVotesAgain() throws Exception {
		/*vote in base is 3.0
		 * cultural offer has 5.0 rating (default rating)
		 * vote in base changes to 1.0;
		 */
		Comment comment = new Comment();
		comment.setDate(new Date());
		comment.setId(999);
		comment.setCulturalOffer(culturalOfferService.findById(1));
		comment.setRating(1.0);
		
		comment.setUser(userService.findById(2));
		
		RatingInfo ratingInfoBefore = ratingInfoService.findByUserIdAndOfferId(2, 1);
		int oldSize = ratingInfoService.findByOfferId(DB_CULTURAL_OFFER_ID).size();
		
		Comment commentToDelete = commentService.create(comment);
		ratingScheduler.scheduleFixedRateTask();
		
		RatingInfo newRatingInfo = ratingInfoService.findByUserIdAndOfferId(2, 1);
		
		assertEquals(ratingInfoBefore.getId(), newRatingInfo.getId());
		assertEquals(ratingInfoBefore.getUserId(), newRatingInfo.getUserId());
		assertEquals(ratingInfoBefore.getOfferId(), newRatingInfo.getOfferId());
		assertNotEquals(ratingInfoBefore.getRatingValue(), newRatingInfo.getRatingValue());
		
		assertEquals(DB_CULTURAL_OFFER_ID, newRatingInfo.getOfferId());
		assertEquals(DB_USER_ID, newRatingInfo.getUserId());
		assertEquals(1.0, newRatingInfo.getRatingValue());
		assertEquals(oldSize, ratingInfoService.findByOfferId(DB_CULTURAL_OFFER_ID).size());
		
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		
		assertEquals(1.0, culturalOffer.getAverageRating());
		
		commentService.delete(commentToDelete.getId());
	}
	
	@Test
	public void testRatingSchedulerFirstVoteNewUser() throws Exception {
		/*
		 * cultural offer has 3.0 rating (from previous test)
		 * new user rates it 4.5
		 */
		Comment comment = new Comment();
		comment.setDate(new Date());
		comment.setId(1000);
		comment.setCulturalOffer(culturalOfferService.findById(1));
		comment.setRating(4.5);
		
		comment.setUser(userService.findById(3));
		
		RatingInfo ratingInfoBefore = ratingInfoService.findByUserIdAndOfferId(3, 1);
		int oldSize = ratingInfoService.findByOfferId(DB_CULTURAL_OFFER_ID).size();
		
		Comment commentToDelete = commentService.create(comment);
		ratingScheduler.scheduleFixedRateTask();
		
		RatingInfo newRatingInfo = ratingInfoService.findByUserIdAndOfferId(3, 1);
		
		assertNull(ratingInfoBefore);
		
		assertEquals(DB_CULTURAL_OFFER_ID, newRatingInfo.getOfferId());
		assertEquals(3, newRatingInfo.getUserId());
		assertEquals(4.5, newRatingInfo.getRatingValue());
		assertEquals(oldSize+1, ratingInfoService.findByOfferId(DB_CULTURAL_OFFER_ID).size());
		
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		
		assertEquals(3.75, culturalOffer.getAverageRating());

		//rollback
		ratingInfoService.delete(newRatingInfo);
		commentService.delete(commentToDelete.getId());
	}
}
