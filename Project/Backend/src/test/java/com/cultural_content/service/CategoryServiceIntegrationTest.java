package com.cultural_content.service;

import static com.cultural_content.constants.CommentsConstants.*;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.CategoryConstants.*;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cultural_content.model.Category;
import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CategoryServiceIntegrationTest {

	@Autowired
	private CategoryService categoryService;
	
	@Test
	public void findByName() {
		Category category = categoryService.findByName(DB_CATEGORY_NAME);
		assertEquals(DB_CATEGORY_NAME, category.getName());
	}
	
	@Test
	public void findByName_not_exist() {
		Category category = categoryService.findByName(DB_CATEGORY_NAME_NOT_EXIST);
		assertEquals(null, category);
	}
	
	@Test
	public void findOne() {
		Category category = categoryService.findOne(DB_CATEGORY_ID);
		assertEquals(DB_CATEGORY_ID, category.getId());
	}
	
	@Test
	public void findOne_not_exist() {
		Category category = categoryService.findOne(DB_CATEGORY_ID_NOT_EXIT);
		assertEquals(null,category);
	}
	
	@Test
	public void findAll() {
		List<Category> categories = categoryService.findAll();
		assertEquals(DB_CATEGORY_SIZE, categories.size());
	}
	
	@Test
	public void testFindAllPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Category> categories = categoryService.findAll(pageable);
		assertEquals(DB_CATEGORY_PAGE_SIZE, categories.getNumberOfElements());
	}

	@Test
	public void testCreate() throws Exception {
		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		
		Category category = new Category();
		category.setName(DB_NEW_CATEGORY_NAME);
		category.setDeleted(false);

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		Category created = categoryService.create(category);
		
		
		assertEquals(DB_NEW_CATEGORY_NAME, created.getName());
		assertEquals(DB_CATEGORY_SIZE+1,categoryService.findAll().size());
	
		int id = categoryService.findByName(DB_NEW_CATEGORY_NAME).getId();
		categoryService.delete(id);

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		
	}
	
	@Test
	public void testCreate_Fail() throws Exception {
		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		
		Category category = new Category();
		category.setName(DB_CATEGORY_NAME_ALREADY_EXIST);
		category.setDeleted(false);

		Exception exception = assertThrows(Exception.class, () -> {
			categoryService.create(category);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "name already exists";
		
		assertTrue(actualMessage.contains(exprectedMessage));
		
	}
	
	@Test
	public void testUpdate() throws Exception {
		Category category = categoryService.findByName(DB_CATEGORY_NAME);
		category.setName(DB_CATEGORY_UPDATED_TEXT);
		Category updated = categoryService.update(category, category.getId());
		assertEquals(DB_CATEGORY_UPDATED_TEXT, updated.getName());
		
	}
	
	@Test
	public void testUpdate_fail() throws Exception {
		Category category = categoryService.findByName(DB_CATEGORY_NAME);
		category.setName(DB_CATEGORY_NAME_ALREADY_EXIST);
		
		Exception exception = assertThrows(Exception.class, () -> {
			categoryService.update(category, category.getId());
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "name already exists";
		
		assertTrue(actualMessage.contains(exprectedMessage));
	}
	
	
	@Test
	@Transactional(readOnly = true)
	public void testDelete() throws Exception {	
		
		Category category = new Category();
		category.setName(DB_NEW_CATEGORY_NAME);
		
		Category created = categoryService.create(category);
		
		categoryService.delete(created.getId());
		
		assertEquals(true,category.isDeleted());
//		assertEquals(null, categoryService.findOne(created.getId()));
		
	}
	
	@Test
	public void testDelete_fail() throws Exception {	

		Exception exception = assertThrows(Exception.class, () -> {
			categoryService.delete(DB_CATEGORY_ID_NOT_EXIT);
	    });
		
		String actualMessage = exception.getMessage();
		String expectedMessage = "Category with given id does not exist!";
		
		assertTrue(actualMessage.contains(expectedMessage));
	}
}
