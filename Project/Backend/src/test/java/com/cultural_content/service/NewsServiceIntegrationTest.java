package com.cultural_content.service;

import static com.cultural_content.constants.CategoryConstants.DB_CATEGORY_ID_NOT_EXIT;
import static com.cultural_content.constants.NewsConstants.*;

import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cultural_content.model.Category;
import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.News;
import com.cultural_content.model.Picture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class NewsServiceIntegrationTest {


	@Autowired
	private NewsService newsService;
	
	@Autowired 
	private CulturalOfferService culturalOfferService;
	
	@Test
	public void findOne() throws Exception {
		News news = newsService.findOne(DB_NEWS_ID);
		assertEquals(DB_NEWS_ID, news.getId());
	}
	
	@Test
	public void findOne_fail() throws Exception {
		News news = newsService.findOne(DB_NEW_DOES_NOT_EXIST_ID);
		assertEquals(null, news);
	}
	
	@Test
	public void findAll() throws Exception {
		List<News> news = newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID);
		assertEquals(DB_NEWS_IN_CULTURAL_OFFER, news.size());
	}
	
	@Test
	public void findAll_fail() throws Exception {
		Exception exception = assertThrows(Exception.class, () -> {
			 newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_DOES_NOT_EXIST);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "News does not exist";
		
		assertTrue(actualMessage.contains(exprectedMessage));

	}
	
	
	@Test
	public void testFindAllPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<News> news = newsService.findAll(DB_CULTURAL_OFFER_ID,pageable);
		assertEquals(DB_NEWS_IN_CULTURAL_OFFER, news.getNumberOfElements());
	}
	
	@Test
	public void testCreate() throws Exception {
		News news = new News();
		news.setTitle(DB_NEW_NEWS_TITLE);
		news.setText(DB_NEW_NEWS_TEXT);
		news.setDeleted(false);
		
		News created = newsService.create(news);
		assertEquals(DB_NEW_NEWS_TITLE, created.getTitle());
		
		newsService.delete(news.getId());
	}
	
	@Test
	public void testUpdate() throws Exception {
		News news = newsService.findOne(DB_NEWS_UPDATE_ID);
		news.setTitle(DB_NEW_NEWS_TITLE);
		News updated = newsService.update(news, news.getId());
		assertEquals(DB_NEW_NEWS_TITLE, updated.getTitle());
		
	}
	
	@Test
	public void testUpdate_fail() throws Exception {
		
		News news = newsService.findOne(DB_NEW_DOES_NOT_EXIST_ID);
	   
		assertEquals(null, news);
		
	}
	
	@Test
	@Transactional(readOnly = true)
	public void testDelete() throws Exception {	

		News newNews = new News();
		newNews.setTitle(DB_NEW_NEWS_TITLE);
		newNews.setText(DB_NEW_NEWS_TEXT);
		newNews.setDeleted(false);
		newsService.create(newNews);
		newsService.delete(newNews.getId());
		System.out.println("asdfasdfasd" + newNews.isDeleted());
		assertTrue(newNews.isDeleted());

	}
	
	@Test
	public void testDelete_fail() throws Exception {	
		Exception exception = assertThrows(Exception.class, () -> {
			newsService.delete(DB_NEW_DOES_NOT_EXIST_ID);
	    });
		
		
		String actualMessage = exception.getMessage();
		String expectedMessage = "News with given id is already deleted!";
		String expectedMessage2 = "News with given id does not exist!";
		
		assertTrue(actualMessage.contains(expectedMessage)||actualMessage.contains(expectedMessage2));
	}
}
