package com.cultural_content.service;


import static com.cultural_content.constants.UserConstants.DB_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static com.cultural_content.constants.UserConstants.DB_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.DB_USER_USERNAME;
import static com.cultural_content.constants.UserConstants.DB_WRONG_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.FAKE_USER_ID;
import static com.cultural_content.constants.UserConstants.NEW_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.NEW_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.NEW_USER_USERNAME;
import static com.cultural_content.constants.UserConstants.USER_ALREADY_EXIST_EXCEPTION_MSG;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.User;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class UserServiceIntegrationTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	
	@Test
	public void testCreate() throws Exception {
		User user = new User(NEW_USER_USERNAME, NEW_USER_EMAIL, NEW_USER_PASSWORD, true);
		User u = userService.create(user);
		assertNotNull(u);
		assertEquals(NEW_USER_USERNAME, u.getUsername());
		assertEquals(NEW_USER_EMAIL, u.getEmail());
		userService.deleteUser(user);

	}
	
	@Test
	public void testCreateUsernameOrEmailAlreadyExist() {
		
		User user = new User(DB_USER_USERNAME, DB_USER_EMAIL, NEW_USER_PASSWORD, true);
		 
		Exception exception = assertThrows(Exception.class, () -> {
		   userService.create(user);
	    });
		
	    assertEquals(USER_ALREADY_EXIST_EXCEPTION_MSG, exception.getMessage());


	}
	
	@Test
	public void testUpdate() throws Exception  {
		//User user = new User(DB_USER_USERNAME, DB_USER_EMAIL, DB_USER_PASSWORD, true);
		User user = userService.findById(DB_USER_ID);
		User updatedUser = userService.update(user, DB_USER_PASSWORD, NEW_USER_PASSWORD);
		//assertEquals(passwordEncoder.encode(NEW_USER_PASSWORD),updatedUser.getPassword());
		assertTrue(passwordEncoder.matches(NEW_USER_PASSWORD, updatedUser.getPassword()));
		user.setPassword(DB_USER_PASSWORD); //vracamo staro stanje u bazi
//		
	/*	User user = new User(NEW_USER_USERNAME, NEW_USER_EMAIL, NEW_USER_PASSWORD, true);
		User updatedUser = userService.update(user, NEW_USER_PASSWORD, "nova");
		assertTrue(passwordEncoder.matches("nova", updatedUser.getPassword()));*/
	//	assertEquals(passwordEncoder.encode("nova"),updatedUser.getPassword());
		//user.setPassword(DB_USER_PASSWORD); //vracamo staro stanje u bazi
		
	}
	
	@Test
	public void testUpdateWrongPassword() throws Exception  {
		//User user = new User(DB_USER_USERNAME, DB_USER_EMAIL, DB_USER_PASSWORD, true);
		User user = userService.findById(DB_USER_ID);
		User updatedUser = userService.update(user, DB_WRONG_USER_PASSWORD, NEW_USER_PASSWORD);
		assertNull(updatedUser);
		
	}
	
	
	@Test
	public void testfindByEmailOrUsername() {
		User user = userService.findByEmailOrUsername(DB_USER_EMAIL, DB_USER_USERNAME);
		assertEquals(DB_USER_EMAIL, user.getEmail());
		assertEquals(DB_USER_USERNAME, user.getUsername());
	}
	
	@Test
	public void testfindById() {
		User user = userService.findById(DB_USER_ID);
		assertEquals(DB_USER_ID, user.getId());
	}
	
	@Test
	public void testfindByIdNotFound() {
		User user = userService.findById(FAKE_USER_ID);
		assertNull(user);
	}
	
	

}
