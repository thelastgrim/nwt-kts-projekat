package com.cultural_content.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;
import com.cultural_content.repos.CommentRepo;

import static com.cultural_content.constants.CommentsConstants.*;
import static com.cultural_content.constants.PictureConstants.*;
import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_1;
import static com.cultural_content.constants.PageConstants.*;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static com.cultural_content.helper.Constants.*;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class PictureServiceIntegrationTest {
	
	@Autowired
	private PictureService pictureService;
	
	@Autowired
	private CommentRepo commentRepo;
	
	@Test
	public void testGetAllPicturesByCommentId() {
		List<Picture> pictures = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID);
		assertEquals(DB_PICTURE_SIZE, pictures.size());
	
	}
	
	@Test
	public void testGetAllPicturesByNonExistingCommentId() {
		List<Picture> pictures = pictureService.getAllPicturesByCommentId(456123);
		assertTrue(pictures.isEmpty());
	
	}
	
	@Test
	public void testFindAllPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Picture> pictures = pictureService.findAll(pageable, DB_COMMENTS_ID);
		assertEquals(DB_PICTURE_SIZE, pictures.getNumberOfElements());
	}
	
	@Test
	public void testFindAllPageableNonExistingId() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Picture> pictures = pictureService.findAll(pageable, 456123);
		assertTrue(pictures.isEmpty());
	}
	
	@Test
	public void testGetById() {
		Picture picture = pictureService.getById(DB_PICTURE_ID);
		assertEquals(DB_PICTURE_ID, picture.getId());
	}
	
	@Test
	public void testGetByNonExistingId() {
		Picture picture = pictureService.getById(456123);
		assertEquals(null, picture);
	}
	
	@Test
	public void testCreate() {
		Picture picture = new Picture();
		picture.setTitle(DB_PICTURE_TITLE);
		picture.setPath(DB_PICTURE_UPLOAD_LOCATION);
		Picture created = pictureService.createAndUploadForComment(picture);
		assertEquals(DB_PICTURE_TITLE, created.getTitle());
	}
	
	@Test
	public void testUpdate() throws Exception {
		Picture picture = pictureService.getById(DB_PICTURE_ID);
		picture.setTitle(DB_PICTURE_TITLE_UPDATED);
		Picture updated = pictureService.update(picture, picture.getId());
		assertEquals(DB_PICTURE_TITLE_UPDATED, updated.getTitle());
	}
	
	@Test
	public void testUpdatePictureExceptionWhenItDoesntExist() {
		Picture picture = pictureService.getById(DB_PICTURE_ID);
		picture.setTitle(DB_PICTURE_TITLE_UPDATED);
		
	    Exception exception = assertThrows(Exception.class, () -> {
	    	pictureService.update(picture, 456123);
	    });


	    assertEquals(EXCEPTION_MESSAGE_3, exception.getMessage());
	}
	
	@Test
	public void testDelete() throws Exception {
		pictureService.delete(2);
		
		
		
		assertEquals(null, pictureService.getById(2));
		
		
		Picture picture = new Picture();
		picture.setId(2);
		picture.setPath(DB_PICTURE_UPLOAD_LOCATION);
		picture.setComment(commentRepo.getOne(DB_COMMENTS_ID));
		pictureService.createAndUploadForComment(picture);
	}
	
	@Test
	public void testLogicDelete() throws Exception {
		pictureService.delete(2);
		
		assertEquals(true, pictureService.getAnyById(2).isDeleted());
		
		
		Picture picture = new Picture();
		picture.setId(2);
		picture.setPath(DB_PICTURE_UPLOAD_LOCATION);
		picture.setComment(commentRepo.getOne(DB_COMMENTS_ID));
		pictureService.createAndUploadForComment(picture);
	}
	
	@Test
	public void testDeletePictureExceptionWhenItDoesntExist() {
		assertEquals(null, pictureService.getById(456123));
		
	    Exception exception = assertThrows(Exception.class, () -> {
	    	pictureService.delete(456123);
	    });


	    assertEquals(EXCEPTION_MESSAGE_3, exception.getMessage());
	}
	
	

}
