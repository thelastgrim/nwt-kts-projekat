package com.cultural_content.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.RatingInfo;
import com.cultural_content.model.User;
import com.cultural_content.repos.CommentRepo;
import com.cultural_content.repos.CulturalOfferRepo;
import com.cultural_content.repos.RatingInfoRepo;
import com.cultural_content.repos.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CommentUnitTest {
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	@Autowired
	
	@MockBean
	private CommentRepo commentRepo;
	
	@MockBean
	private UserRepo userRepo;
	
	@MockBean
	private RatingInfoRepo ratingInfoRepo;
	
	@MockBean
	private CulturalOfferRepo culturalOfferRepo;
	
	@Before
	public void setup() {
		
		
		
		RatingInfo ratingInfo = new RatingInfo(1, 1, 2.5);
		RatingInfo ratingInfoUpdate = new RatingInfo(1, 1, 2.5);
		
		given(ratingInfoRepo.save(ratingInfo)).willReturn(ratingInfoUpdate);
		
		User user = new User();
		user.setActivated(true);
		user.setId(1);

		List<User> users = new ArrayList<>();
		users.add(user);
		
		CulturalOffer culturalOffer = new CulturalOffer();
		culturalOffer.setId(1);
		culturalOffer.setAverageRating(5.0);
		
		given(userRepo.findAll()).willReturn(users);
		given(userRepo.getOne(1)).willReturn(user);
		given(culturalOfferRepo.getOne(1)).willReturn(culturalOffer);
		given(ratingInfoRepo.getOne(1)).willReturn(ratingInfo);

	}
	
	@Test
	public void testCreateRatingInfo() throws Exception {
		Comment comment = new Comment();
		comment.setId(1);
		comment.setDate(new Date());
		comment.setCulturalOffer(culturalOfferRepo.getOne(1));
		comment.setRating(2.5);
		comment.setUser(userRepo.getOne(1));
		
		Comment created = commentService.create(comment);
		RatingInfo ratingInfo = new RatingInfo(1, 1, 2.5);
		System.out.println(ratingInfo.toString());
		
		//verify(ratingInfoRepo, times(1)).save(ratingInfo);
		assertTrue(true);
	}

}
