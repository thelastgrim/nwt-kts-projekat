package com.cultural_content.service;

import static com.cultural_content.constants.CategoryConstants.DB_CATEGORY_ID;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_ABOUT;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_AVG_RATING;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_LAT;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_LNG;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_UNIQUE_CITIES_SIZE;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_CITY;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_COUNTRY;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_LAT;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_LNG;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_STREET;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class LocationServiceIntegrationTest {
	
	@Autowired
	LocationService locationService;
	
	@Test
	public void testfindByLatitudeAndLongitude() {
		
		Location location = locationService.findByLatitudeAndLongitude(DB_LOCATION_LAT, DB_LOCATION_LNG);
		assertEquals(DB_LOCATION_LAT,location.getLAT());
		assertEquals(DB_LOCATION_LNG,location.getLNG());
	}
	
	@Test
	public void testCreate() throws Exception {
		CulturalOffer culturalOffer = new CulturalOffer();
		culturalOffer.setName(NEW_CULTURAL_OFFER_NAME);
		culturalOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		culturalOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		culturalOffer.setDeleted(false);
		
		Location newLocation = new Location();
		newLocation.setCity(NEW_LOCATION_CITY);
		newLocation.setCountry(NEW_LOCATION_COUNTRY);
		newLocation.setDeleted(false);
		newLocation.setLAT(NEW_LOCATION_LAT);
		newLocation.setLNG(NEW_LOCATION_LNG);
		newLocation.setStreet(NEW_LOCATION_STREET);
		
		Location created = locationService.create(newLocation, culturalOffer);
		assertEquals(NEW_LOCATION_CITY, created.getCity());
		assertNotNull(created.getCulturalOffer());
	}
	
	@Test
	public void testCreate_locationExists() throws Exception {
		CulturalOffer culturalOffer = new CulturalOffer();
		culturalOffer.setName(NEW_CULTURAL_OFFER_NAME);
		culturalOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		culturalOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		culturalOffer.setDeleted(false);
		
		Location newLocation = new Location();
		newLocation.setCity(NEW_LOCATION_CITY);
		newLocation.setCountry(NEW_LOCATION_COUNTRY);
		newLocation.setDeleted(false);
		newLocation.setLAT(DB_LOCATION_LAT);
		newLocation.setLNG(DB_LOCATION_LNG);
		newLocation.setStreet(NEW_LOCATION_STREET);
		
		Exception exception = assertThrows(Exception.class, () -> {
			locationService.create(newLocation, culturalOffer);
	    });
		
		String actualMessage = exception.getMessage();
		String expectedMessage = "already exists";
		
		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testFindAllUniqueCities() {
		
		List<String> uniqueCities = locationService.findAllUniqueCities();
		assertEquals(DB_LOCATION_UNIQUE_CITIES_SIZE, uniqueCities.size());
	}
}
