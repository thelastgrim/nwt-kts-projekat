package com.cultural_content.service;


import static com.cultural_content.constants.UserConstants.DB_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.DB_USER_USERNAME;
import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static com.cultural_content.constants.UserConstants.NEW_USER_USERNAME;
import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_3;
import static com.cultural_content.constants.UserConstants.NEW_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.NEW_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.FAKE_USER_ID;
import static com.cultural_content.constants.UserConstants.USER_ALREADY_EXIST_EXCEPTION_MSG;
import static com.cultural_content.constants.UserConstants.DB_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.DB_WRONG_USER_PASSWORD;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Authority;
import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;
import static com.cultural_content.constants.AuthorityConstant.DB_AUTHORITY_NAME;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class AuthorityServiceIntegrationTest {
	
	@Autowired
	private AuthorityService authService;
	
	@Test
	public void testfindByName() {
		Set<Authority> auths = authService.findByName(DB_AUTHORITY_NAME);
		assertEquals(1, auths.size());
	}
	

}
