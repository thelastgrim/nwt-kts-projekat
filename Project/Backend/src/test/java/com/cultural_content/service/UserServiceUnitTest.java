package com.cultural_content.service;
import static com.cultural_content.constants.UserConstants.NEW_USER_ID_UNIT;
import static com.cultural_content.constants.UserConstants.NEW_USER_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class UserServiceUnitTest {
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepo userRepo;
	
	 @Before
	    public void setup() {
	 
		 User u = new User();
		 u.setUsername(NEW_USER_USERNAME);
		 u.setId(NEW_USER_ID_UNIT);
		 User saved = new User();
		 saved.setUsername(NEW_USER_USERNAME);
		 saved.setId(NEW_USER_ID_UNIT);
	     given(userRepo.findById(NEW_USER_ID_UNIT)).willReturn(java.util.Optional.of(u));
	     given(userRepo.save(u)).willReturn(saved);

	 }

	
	  @Test
	    public void testFindById() {
	        User found = userService.findById(NEW_USER_ID_UNIT);
	        verify(userRepo, times(1)).findById(NEW_USER_ID_UNIT);
	        assertEquals(NEW_USER_ID_UNIT, found.getId());
	    }
	  	 
}
