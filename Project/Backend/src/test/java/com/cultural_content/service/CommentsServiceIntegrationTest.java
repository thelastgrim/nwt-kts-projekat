package com.cultural_content.service;

import static com.cultural_content.constants.CommentsConstants.*;

import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static com.cultural_content.constants.UserConstants.*;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.Comment;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Picture;
import com.cultural_content.model.User;

import static com.cultural_content.helper.Constants.*;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CommentsServiceIntegrationTest {

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private UserService userService;
	@Autowired
	private CulturalOfferService culturalOfferService;
	
	@Test
	public void testFindAllByCultularOfferId() {
		List<Comment> comments = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID);
		assertEquals(DB_COMMENTS_SIZE, comments.size());
	}
	
	@Test
	public void testFindAllByCultularOfferIdNotExistingId() {
		List<Comment> comments = commentService.findAllByCultularOfferId(789456123);
		assertEquals(null, comments);
	}
	@Test
	public void testFindAllPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Comment> comments = commentService.findAll(pageable, DB_CULTURAL_OFFER_ID);
		assertEquals(DB_COMMENTS_SIZE, comments.getNumberOfElements());
		
	}
	
	@Test
	public void testFindAllPageableNotExistingId() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<Comment> comments = commentService.findAll(pageable,789456123);
		assertEquals(null, comments);
		
	}
	@Test
	public void testFindById() {
		Comment comment = commentService.findById(DB_COMMENTS_ID);
		assertEquals(DB_COMMENTS_ID, comment.getId());
	}
	
	@Test
	public void testFindByIdNotExistingId() {
		Comment comment = commentService.findById(789456123);
		assertEquals(null, comment);
	}
	
	@Test
	public void testCreate() throws Exception {
		Comment comment = new Comment();
		comment.setId(55);
		comment.setDate(new Date());
		comment.setText(DB_COMMENT_TEXT);
		comment.setRating(3.0);
		comment.setDeleted(false);

		comment.setUser(userService.findById(DB_USER_ID));
		comment.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		
		
		
		Comment created = commentService.create(comment);
		
		
		
		assertEquals(DB_COMMENT_TEXT, created.getText());
		
	}
	
	@Test
	public void testCreateWithoutRating() {
		Comment comment = new Comment();
		comment.setId(55);
		comment.setDate(new Date());
		comment.setText(DB_COMMENT_TEXT);
		comment.setDeleted(false);

		comment.setUser(userService.findById(DB_USER_ID));
		comment.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		
		Exception exception = assertThrows(Exception.class, () ->{
			Comment created = commentService.create(comment);
		});
		
		
		assertEquals(EXCEPTION_MESSAGE_6, exception.getMessage());
		
	}
	
	@Test
	public void testUpdate() throws Exception {
		Comment comment = commentService.findById(DB_COMMENTS_ID);
		comment.setText(DB_COMMENT_UPDATED_TEXT);
		Comment updated = commentService.update(comment, comment.getId());
		assertEquals(DB_COMMENT_UPDATED_TEXT, updated.getText());
		
	}
	
	@Test
	public void testUpdateCommentExceptionWhenItDoesntExist() {
		Comment comment = commentService.findById(456123);
		
		
	    Exception exception = assertThrows(Exception.class, () -> {
	    	commentService.update(comment, 456123);
	    });


	    assertEquals(EXCEPTION_MESSAGE_1, exception.getMessage());
	}
	
	@Test
	public void testUpdateCommentWithoutRating() {
		Comment comment = commentService.findById(DB_COMMENTS_ID);
		comment.setRating(0.0);
		
	    Exception exception = assertThrows(Exception.class, () -> {
	    	commentService.update(comment, DB_COMMENTS_ID);
	    });


	    assertEquals(EXCEPTION_MESSAGE_6, exception.getMessage());
	}
	
	@Test
	public void testDelete() throws Exception {	
		commentService.delete(DB_COMMENTS_ID);
		assertEquals(null, commentService.findById(DB_COMMENTS_ID));
		
		
		
		Comment comment = new Comment();
		comment.setId(DB_COMMENTS_ID);
		comment.setDate(new Date());
		comment.setUser(userService.findById(DB_USER_ID));
		comment.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		comment.setRating(3.0);
		commentService.create(comment);
			
	}
	
	@Test
	public void testLogicDelete() throws Exception {	
		commentService.delete(DB_COMMENTS_ID);
		System.out.println(commentService.getAnyById(DB_COMMENTS_ID));
		assertEquals(true, commentService.getAnyById(DB_COMMENTS_ID).isDeleted());
		
		
		
		Comment comment = new Comment();
		comment.setId(DB_COMMENTS_ID);
		comment.setDate(new Date());
		comment.setUser(userService.findById(DB_USER_ID));
		comment.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		comment.setRating(3.0);
		commentService.create(comment);
			
	}
	
	
	
	@Test
	public void testDeleteCommentExceptionWhenItDoesntExist() throws Exception {
		assertEquals(null, commentService.findById(456123));
		
	    Exception exception = assertThrows(Exception.class, () -> {
	    	commentService.delete(456123);
	    });


	    assertEquals(EXCEPTION_MESSAGE_1, exception.getMessage());
	    
	}
	
	
}
