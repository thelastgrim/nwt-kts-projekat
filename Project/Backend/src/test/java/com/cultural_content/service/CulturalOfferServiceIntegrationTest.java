package com.cultural_content.service;

import static com.cultural_content.constants.CategoryConstants.DB_CATEGORY_ID;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_NAME_2;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE_BY_CATEGORY;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_CITY;
import static com.cultural_content.constants.CulturalOfferConstants.DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_USER;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_ABOUT;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_AVG_RATING;
import static com.cultural_content.constants.CulturalOfferConstants.NEW_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.CulturalOfferConstants.NOT_DB_CATEGORY_ID;
import static com.cultural_content.constants.CulturalOfferConstants.NOT_DB_CULTURAL_OFFER_ID;
import static com.cultural_content.constants.CulturalOfferConstants.NOT_DB_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.CulturalOfferConstants.UPDATED_CULTURAL_OFFER_NAME;
import static com.cultural_content.constants.LocationConstants.DB_LOCATION_NAME;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_CITY;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_COUNTRY;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_LAT;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_LNG;
import static com.cultural_content.constants.LocationConstants.NEW_LOCATION_STREET;
import static com.cultural_content.constants.PageConstants.PAGE_INDEX;
import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static com.cultural_content.constants.UserConstants.DB_SECOND_USER_ID;
import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;
import com.cultural_content.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CulturalOfferServiceIntegrationTest {
	
	@Autowired
	CulturalOfferService culturalOfferService;
	
	@Autowired
	LocationService locationService;
	
	@Transactional(readOnly=true)
	@Test
	public void testCreate() throws Exception {
		
		CulturalOffer newOffer = new CulturalOffer();
		newOffer.setName(NEW_CULTURAL_OFFER_NAME);
		newOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		newOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		newOffer.setDeleted(false);
		
		Location location = new Location();
		location.setCity(NEW_LOCATION_CITY);
		location.setCountry(NEW_LOCATION_COUNTRY);
		location.setDeleted(false);
		location.setLAT(NEW_LOCATION_LAT);
		location.setLNG(NEW_LOCATION_LNG);
		location.setStreet(NEW_LOCATION_STREET);
		
		CulturalOffer createdOffer = 
				culturalOfferService.create(
				newOffer, 
				location, 
				DB_CATEGORY_ID);
		
		assertEquals(NEW_CULTURAL_OFFER_NAME, createdOffer.getName());

		culturalOfferService.delete(createdOffer.getId());
		assertTrue(createdOffer.isDeleted());
	}
	
	@Test
	public void testCreate_NameAlreadyExists() throws Exception {
		
		CulturalOffer newOffer = new CulturalOffer();
		newOffer.setName(DB_CULTURAL_OFFER_NAME_2);
		newOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		newOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		newOffer.setDeleted(false);
		
		Location location = new Location();
		location.setCity(NEW_LOCATION_CITY);
		location.setCountry(NEW_LOCATION_COUNTRY);
		location.setDeleted(false);
		location.setLAT(NEW_LOCATION_LAT);
		location.setLNG(NEW_LOCATION_LNG);
		location.setStreet(NEW_LOCATION_STREET);
		
		Exception exception = assertThrows(Exception.class, () -> {
			culturalOfferService.create(newOffer, location, DB_CATEGORY_ID);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "name already exists";
		
		assertTrue(actualMessage.contains(exprectedMessage));
		
	}
	
	@Test
	public void testUpdate() throws Exception{
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		culturalOffer.setName(UPDATED_CULTURAL_OFFER_NAME);
		CulturalOffer updated = culturalOfferService.update(culturalOffer, DB_CULTURAL_OFFER_ID);
		assertEquals(UPDATED_CULTURAL_OFFER_NAME, updated.getName());
		
		//Vrati na staro stanje
		updated.setName(DB_CULTURAL_OFFER_NAME);
		culturalOfferService.update(updated, DB_CULTURAL_OFFER_ID);
	}
	
	@Test
	public void testUpdate_NameAlreadyExists() throws Exception {
		
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		culturalOffer.setName(DB_CULTURAL_OFFER_NAME_2);
		Exception exception = assertThrows(Exception.class, () -> {
			culturalOfferService.update(culturalOffer, DB_CULTURAL_OFFER_ID);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "name already exists";
		
		assertTrue(actualMessage.contains(exprectedMessage));
		
	}
	
	@Transactional(readOnly=true)
	@Test
	public void testDelete() throws Exception {
		
		CulturalOffer newOffer = new CulturalOffer();
		newOffer.setName(NEW_CULTURAL_OFFER_NAME);
		newOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		newOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		newOffer.setDeleted(false);
		
		Location location = new Location();
		location.setCity(NEW_LOCATION_CITY);
		location.setCountry(NEW_LOCATION_COUNTRY);
		location.setDeleted(false);
		location.setLAT(NEW_LOCATION_LAT);
		location.setLNG(NEW_LOCATION_LNG);
		location.setStreet(NEW_LOCATION_STREET);
		
		CulturalOffer offerForDelete = 
				culturalOfferService.create(
				newOffer, 
				location, 
				DB_CATEGORY_ID);
		
		culturalOfferService.delete(offerForDelete.getId());
		assertTrue(offerForDelete.isDeleted());
		
	}
	
	@Test
	public void testDelete_OfferIdDoesntExist() throws Exception {
		
		Exception exception = assertThrows(Exception.class, () -> {
			culturalOfferService.delete(NOT_DB_CULTURAL_OFFER_ID);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "id doesn't exist";
		
		assertTrue(actualMessage.contains(exprectedMessage));
		
	}
	
	@Test
	public void testFindById() {
		
		CulturalOffer culturalOffer = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		System.out.println(culturalOffer.getName());
		assertEquals(DB_CULTURAL_OFFER_ID, culturalOffer.getId());
	}
	
	@Test 
	public void testFindById_idDoesntExist() {
		CulturalOffer culturalOffer = culturalOfferService.findById(NOT_DB_CULTURAL_OFFER_ID);
		assertEquals(null, culturalOffer);
	}
	
	@Test
	public void testFindOne() throws Exception {
		
		CulturalOffer culturalOffer = culturalOfferService.findOne(DB_CULTURAL_OFFER_ID);
		assertEquals(DB_CULTURAL_OFFER_ID, culturalOffer.getId());
	}
	
	@Test 
	public void testFindOne_idDoesntExist() {
		Exception exception = assertThrows(Exception.class, () -> {
			culturalOfferService.findOne(NOT_DB_CULTURAL_OFFER_ID);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "id doesn't exist";
		
		assertTrue(actualMessage.contains(exprectedMessage));
	}
	
	@Test
	public void testFindOneByName() throws Exception {
		CulturalOffer culturalOffer = culturalOfferService.findOneByName(DB_CULTURAL_OFFER_NAME);
		assertEquals(DB_CULTURAL_OFFER_NAME, culturalOffer.getName());
	}
	
	@Test
	public void testFindOneByName_nameDoesntExist() throws Exception {

		Exception exception = assertThrows(Exception.class, () -> {
			culturalOfferService.findOneByName(NOT_DB_CULTURAL_OFFER_NAME);
	    });
		
		String actualMessage = exception.getMessage();
		String exprectedMessage = "name doesn't exist";
		
		assertTrue(actualMessage.contains(exprectedMessage));
	}
	
	
	@Test
	public void testFindAllByCategory() {
		
		List<CulturalOffer> culturalOffers = culturalOfferService.findAllByCategory(DB_CATEGORY_ID);
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, culturalOffers.size());
	}
	
	@Test
	public void testFindAllByCategory_NoOffersInCategory() {
		
		List<CulturalOffer> culturalOffers = culturalOfferService.findAllByCategory(NOT_DB_CATEGORY_ID);
		assertEquals(true, culturalOffers.isEmpty());
	}
	
	@Test
	public void testFindAllByDeleted() {
		
		List<CulturalOffer> culturalOffers = culturalOfferService.findAllByDeleted();
		assertEquals(DB_CULTURAL_OFFER_SIZE, culturalOffers.size());
	}
	
	@Test
	public void testFindAllByCategoryAndUserId() {
		
		List<CulturalOffer> culturalOffers = culturalOfferService.findAllByCategoryAndUserId(DB_CATEGORY_ID, DB_USER_ID);
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_USER, culturalOffers.size());
	}
	
	@Test
	public void testfindAllByCity() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferService.findAllByCity(DB_LOCATION_NAME, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getNumberOfElements());
	}
	
	@Test
	public void testFindByName() throws Exception {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferService.findByName(DB_CULTURAL_OFFER_NAME, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
	}
	
	@Test
	public void testFindAllByCategoryPageable() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferService.findAllByCategory(DB_CATEGORY_ID, pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getSize());
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, culturalOffers.getNumberOfElements());
	}
	
	@Test
	public void testFindAll() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferService.findAll(pageable);
		assertEquals(PAGE_SIZE, culturalOffers.getNumberOfElements());
		
	}
	
	@Test
	public void testSubscribeAndUnsubscribe() throws Exception {
		
		CulturalOffer co = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		int sizeBeforeSubscribe = co.getUsers().size();
		
		culturalOfferService.subscribe(DB_CULTURAL_OFFER_ID, DB_SECOND_USER_ID);
		
		co = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		boolean check = false;
		for(User user : co.getUsers()) {
			if(user.getId() == DB_SECOND_USER_ID) {
				check = true;
				break;
			}
		}
		assertTrue(check);
		assertEquals(sizeBeforeSubscribe + 1, co.getUsers().size());
		
		culturalOfferService.unsubscribe(DB_CULTURAL_OFFER_ID, DB_SECOND_USER_ID);
		
		co = culturalOfferService.findById(DB_CULTURAL_OFFER_ID);
		for(User user : co.getUsers()) {
			if(user.getId() == DB_SECOND_USER_ID) {
				check = false;
				break;
			}
		}
		assertTrue(check);
		assertEquals(sizeBeforeSubscribe, co.getUsers().size());
	}
	
	@Test
	public void testFindByCategoryAndLocation() {
		Pageable pageable = PageRequest.of(PAGE_INDEX, PAGE_SIZE);
		Page<CulturalOffer> culturalOffers = culturalOfferService.findByCategoryAndLocation(DB_CATEGORY_ID, DB_LOCATION_NAME, pageable);
		assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_CITY, culturalOffers.getNumberOfElements());
		
	}

}
