package com.cultural_content.service;

import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_3;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.cultural_content.helper.FileUploader;

import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_4;
import static com.cultural_content.helper.Constants.EXCEPTION_MESSAGE_5;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class FileUploaderIntegrationTest {
	private final String NEW_FILE_NAME = "testpic.jpg";
	private final String NEW_FILE_NAME_IN_DATABASE = "0testpic.jpg";
	
	
	
	private final String FILE_NAME_ALREADY_IN_DATABASE = "Belica 2.jpg";
	private final String FILE_NAME_WHEN_ADDED_IN_DATABASE = "1Belica 2.jpg";
	
	@Test
	public void testUpload() throws Exception {
		
		
		MultipartFile multipartFile = new MockMultipartFile("testpic.jpg", NEW_FILE_NAME, "jpg", new FileInputStream(new File("src/test/resources/testpic.jpg")));
		
		
		String path = FileUploader.upload(multipartFile);
		
		File file = new File(path);
		
		assertTrue(file.exists());
		assertEquals(NEW_FILE_NAME_IN_DATABASE, file.getName());
		assertEquals(multipartFile.getSize(), file.length());
		
		file.delete();
	}
	
	@Test
	public void testUploadNameAlreadyExists() throws Exception {
		
		
		MultipartFile multipartFile = new MockMultipartFile("testpic.jpg", FILE_NAME_ALREADY_IN_DATABASE, "jpg", new FileInputStream(new File("src/test/resources/testpic.jpg")));
		
		
		String path = FileUploader.upload(multipartFile);
		
		File file = new File(path);
		
		assertTrue(file.exists());
		assertEquals(FILE_NAME_WHEN_ADDED_IN_DATABASE, file.getName());
		assertEquals(multipartFile.getSize(), file.length());
		
		file.delete();
		
	}
	@Test
	public void testUploadMaxFileSizeExceeded() throws Exception{
		
		MultipartFile multipartFile = new MockMultipartFile("testpic.jpg", "randomName", "jpg", new FileInputStream(new File("src/test/resources/Enrique_Simonet_-_Marina_veneciana_6MB.jpg")));
		
		Exception exception = assertThrows(Exception.class, () -> {
			String path = FileUploader.upload(multipartFile);
	    });


	    assertEquals(EXCEPTION_MESSAGE_4, exception.getMessage());
	}
	
	@Test
	public void testUploadWrongFileFormat() throws Exception{
		
		MultipartFile multipartFile = new MockMultipartFile("testpic.jpg", "randomName", "jpg", new FileInputStream(new File("src/test/resources/test.properties")));
		
		Exception exception = assertThrows(Exception.class, () ->{
			String path = FileUploader.upload(multipartFile);
		});
		
	    assertEquals(EXCEPTION_MESSAGE_5, exception.getMessage());
	}
	

}
