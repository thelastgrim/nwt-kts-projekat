package com.cultural_content.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.model.RatingInfo;


import static com.cultural_content.constants.RatingInfoConstants.*;
import static com.cultural_content.constants.UserConstants.*;
import static com.cultural_content.constants.CulturalOfferConstants.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class RatingInfoServiceIntegrationTest {
	
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	@Test
	public void testFindByUserIdAndOfferId() {
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(DB_USER_ID, DB_CULTURAL_OFFER_ID);
		
		assertEquals(DB_RATING_INFO_ID, ratingInfo.getId());
	}
	
	@Test
	public void testFindByUserIdAndNonExistingOfferId() {
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(DB_USER_ID, 456123);
		
		assertEquals(null, ratingInfo);
	}
	
	@Test
	public void testFindByNonExistingUserIdAndOfferId() {
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(456123, DB_CULTURAL_OFFER_ID);
		
		assertEquals(null, ratingInfo);
	}
	
	@Test
	public void testFindByNonExistingUserIdAndNonExistingOfferId() {
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(456123, 456123);
		
		assertEquals(null, ratingInfo);
	}
	
	@Test
	public void testFindByOfferId() {
		List<RatingInfo> ratingInfos = ratingInfoService.findByOfferId(DB_CULTURAL_OFFER_ID);
		assertEquals(DB_RATINGS_FOR_CULTURAL_OFFER, ratingInfos.size());
	}
	
	@Test
	public void testFindByNonExistingOfferId() {
		List<RatingInfo> ratingInfos = ratingInfoService.findByOfferId(456123);
		assertTrue(ratingInfos.isEmpty());
	}
	
	@Test
	public void testCreate() {
		RatingInfo ratingInfo = new RatingInfo(DB_SECOND_USER_ID, DB_CULTURAL_OFFER_ID, DB_RATING_INFO_VALUE);
		RatingInfo created = ratingInfoService.create(ratingInfo);
		assertEquals(DB_RATING_INFO_VALUE, created.getRatingValue());
	}
	
	@Test
	public void testUpdate() {
		RatingInfo ratingInfo = ratingInfoService.findByUserIdAndOfferId(DB_USER_ID, DB_CULTURAL_OFFER_ID);
		
		ratingInfo.setRatingValue(DB_RATING_INFO_VALUE_UPDATED);
		RatingInfo updated = ratingInfoService.update(ratingInfo);
		assertEquals(DB_RATING_INFO_VALUE_UPDATED, updated.getRatingValue());
		
	}
	
	
	
	
	

	
}
