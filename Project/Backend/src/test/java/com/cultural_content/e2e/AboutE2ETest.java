package com.cultural_content.e2e;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.AboutPage;
import com.cultural_content.pages.ContentPage;
import com.cultural_content.pages.LoginPage;

public class AboutE2ETest {
	
	private WebDriver driver;
	
	private AboutPage aboutPage;
	
    private LoginPage loginPage;

    private ContentPage contentPage;
    
    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        aboutPage = PageFactory.initElements(driver, AboutPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        contentPage = PageFactory.initElements(driver, ContentPage.class);
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    @Test
    public void ShowCulturalOfferDetails() throws InterruptedException{
    	
    	logIn("admin@gmail.com", "admin");
    	
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(1).click();
    	
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/2", driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedAboutLink();
    	
    	aboutPage.ensureIsDisplayedOfferName();
    	assertEquals(aboutPage.getOfferName().getText(), "McLaughlin-Nader");
    	aboutPage.ensureIsDisplayedOfferAbout();
    	assertEquals(aboutPage.getOfferAbout().getText(), "Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.");
    	
    	
    	
    	
    }
    
    @Test
    public void ShowCulturalOfferDetails_ShowNewsAdmin() throws InterruptedException{
    	
    	logIn("admin@gmail.com", "admin");
    	
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(1).click();
    	
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/2", driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedNewsNavLink();
    	
    	aboutPage.getNewsNavLink().click();
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedTitleNews();
    	assertTrue(driver.findElement(By.className("container")).isDisplayed());
    	assertTrue(aboutPage.getAddNews().isDisplayed());
    	justWait();
    	List<WebElement> news = driver.findElements(By.id("newsTitle"));
    	assertEquals(news.size(), 10);
    	assertEquals(news.get(0).getText(), "Cheetah");
    	
    	aboutPage.getAddNews().click();
    	
    	driver.switchTo().activeElement();
    	
    	aboutPage.getNewsTitleArea().sendKeys("Izlozba 29.1.2021.");
    	aboutPage.getNewsTextArea().sendKeys("Obavestavamo vas da se odrzava nova izlozba");
        justWait();

        aboutPage.getAddBtn().click();
        justWait();
    
        assertEquals("News has been successfully added!", driver.switchTo().alert().getText());
        
        driver.switchTo().alert().accept();
        
        justWait();
        justWait();
        
        news = driver.findElements(By.id("newsTitle"));
        assertEquals(news.get(0).getText(), "Izlozba 29.1.2021.");
	
    }
    
  
    @Test
    public void ShowCulturalOfferDetails_UpdateOffer() throws InterruptedException{
    	
    	logIn("admin@gmail.com", "admin");
    	
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(offers.size() - 2).click();
    	
    	//id ponude
    	int id = offers.size() - 1;
    	
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/" + id, driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedOpenUpdateBtn();
    	
    	aboutPage.getOpenUpdateBtn().click();
    	
    	driver.switchTo().activeElement();
   	 
    	aboutPage.ensureIsDisplayedUpdateForm();
    	aboutPage.ensureIsDisplayedUpdateDescription();
	   	aboutPage.ensureIsDisplayedUpdateName();
	   	aboutPage.ensureIsDisplayedUpdateBtn();
	   	aboutPage.ensureIsDisplayedCloseUpdateBtn();
	   	aboutPage.ensureIsDisplayedCancelUpdateBtn();
	   	 
	   	aboutPage.getUpdateName().clear();
	   	aboutPage.getUpdateName().sendKeys("New New Name");
	   	 
	   	aboutPage.getUpdateDescriptionText().clear();
	   	aboutPage.getUpdateDescriptionText().sendKeys("New New description");
   	 
	   	aboutPage.getUpdateBtn().click();
   	 
        assertEquals(aboutPage.getOfferName().getText() , "New New Name");
        assertEquals(aboutPage.getOfferAbout().getText() , "New New description");

    }
    
    @Test
    public void ShowCulturalOfferDetails_UpdateFail() throws InterruptedException{
    	
    	logIn("admin@gmail.com", "admin");
    	
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(offers.size() - 2).click();
    	
    	//id ponude
    	int id = offers.size() - 1;
    	
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/" + id, driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedOpenUpdateBtn();
    	
    	aboutPage.getOpenUpdateBtn().click();
    	
    	driver.switchTo().activeElement();
   	 
    	aboutPage.ensureIsDisplayedUpdateForm();
    	aboutPage.ensureIsDisplayedUpdateDescription();
	   	aboutPage.ensureIsDisplayedUpdateName();
	   	aboutPage.ensureIsDisplayedUpdateBtn();
	   	aboutPage.ensureIsDisplayedCloseUpdateBtn();
	   	aboutPage.ensureIsDisplayedCancelUpdateBtn();
	   	 
	   	aboutPage.getUpdateName().clear();
	   	aboutPage.getUpdateName().sendKeys("Willms, Sanford and Toy");
	   	 
   	 
	   	aboutPage.getUpdateBtn().click();
	   	justWait();
   	 
	   	assertEquals("Cultural offer with that name already exists!",
				driver.switchTo().alert().getText());

    }
    
    
    
    @Test
    public void ShowCulturalOfferDetails_ShowCommentsUser() throws InterruptedException{
    	
    	logIn("test@gmail.com", "user");
    	
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(1).click();
    	
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/2", driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedAboutLink();
    	aboutPage.ensureIsDisplayedNewsNavLink();
    	aboutPage.ensureIsDisplayedCommentsLink();
    	aboutPage.ensureIsDisplayedSubscriptionLink();
    	
    	aboutPage.getCommentsLink().click();
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedTitleComments();
    	assertTrue(driver.findElement(By.className("container")).isDisplayed());
    	assertTrue(aboutPage.getLeaveNewComment().isDisplayed());
    	justWait();
    	
    	List<WebElement> comments = driver.findElements(By.id("commentText"));
    	
    	justWait();
    	
    	assertEquals(comments.size(), 10);
    	assertEquals(comments.get(0).getText(), "In sagittis dui vel nisl.");
    	 
   	 	aboutPage.getLeaveNewComment().click();
   	 
   	 	driver.switchTo().activeElement();
         
        aboutPage.getCommentText().sendKeys("Ovo je novi komentar od strane korisnika");
        aboutPage.getSaveComment().click();
        
        assertEquals("Comment created.", driver.switchTo().alert().getText());
         
        driver.switchTo().alert().accept();
       
        justWait();
        justWait();
        justWait();
        justWait();
        
        comments = driver.findElements(By.id("commentText"));
        justWait();
        System.out.println();
    	
    	
    }
    
    @Test
    public void ShowCulturalOfferDetails_ShowSubscriptionUser() throws InterruptedException{
    	
    	logIn("test@gmail.com", "user");
    	
    	justWait();
    	justWait();
    	
    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	
    	List<WebElement> offers = driver.findElements(By.id("offerDetailsLink"));
    	
    	offers.get(1).click();
    	
    	justWait();
    	justWait();
    	
    	assertEquals("http://localhost:4200/offer/2", driver.getCurrentUrl());
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedAboutLink();
    	aboutPage.ensureIsDisplayedNewsNavLink();
    	aboutPage.ensureIsDisplayedCommentsLink();
    	aboutPage.ensureIsDisplayedSubscriptionLink();
    	
    	aboutPage.getSubscriptionLink().click();
    	
    	justWait();
    	
    	aboutPage.ensureIsDisplayedSubscribeText();
    	assertEquals(aboutPage.getSubscribeText().getText(), "Subscribe to our newsletter and stay updated on the latest news on McLaughlin-Nader!\nYou can unsubscribe at any time.");
    	assertTrue(aboutPage.getSubUnsub().isDisplayed());
    	
    	assertEquals(aboutPage.getSubUnsub().getText(), "UNSUBSCRIBE");
    }
    
    private void logIn(String username, String password) throws InterruptedException {
    	driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys(username);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLoginBtn().click();
    }
    
    private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }

    }
}
