package com.cultural_content.e2e;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.LoginPage;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LoginE2ETest {
	
	private WebDriver driver;
	
	private LoginPage loginPage;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
	}

	@After
    public void tearDown() {
        driver.quit();
    }
	
	@Test
	public void LoginTestSuccess() throws InterruptedException {
		
		driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys("test@gmail.com");
		loginPage.getPassword().sendKeys("user");
		loginPage.getLoginBtn().click();
		
		loginPage.ensureIsNotVisibleLoginBtn();
		
		assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
		
	}
	
	@Test
	public void LoginTestWrongCredentials() throws InterruptedException {
		
		driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys("Atest@gmail.com");
		loginPage.getPassword().sendKeys("Auser");
		loginPage.getLoginBtn().click();
		
		loginPage.ensureAlertIsDisplayed();
		
	    Alert alt = driver.switchTo().alert();
		alt.accept();
		
		assertEquals("http://localhost:4200/log-in", driver.getCurrentUrl());
	
		
	}
	
	@Test
	public void LogoutTestSuccess() throws InterruptedException {
		
		driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys("test@gmail.com");
		loginPage.getPassword().sendKeys("user");
		loginPage.getLoginBtn().click();
		
		justWait();
		
		loginPage.ensureIsDisplayedDropdown();
		loginPage.getDropdownMenu().click();
		loginPage.getLogoutBtn().click();
		
		driver.switchTo().alert().accept();
		
		assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
		assertNotNull(loginPage.getLoginBtn());
		
		
	}
	
	@Test
	public void LoginTestRegisterLink() throws InterruptedException {
		
		driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.ensureIsDisplayedRegisterLink();
		
		loginPage.getRegisterLink().click();
		
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());
		
	}
	
	
	
	private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
