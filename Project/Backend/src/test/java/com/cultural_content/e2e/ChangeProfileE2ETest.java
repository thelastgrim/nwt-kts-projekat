package com.cultural_content.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.ChangeProfilePage;
import com.cultural_content.pages.LoginPage;

public class ChangeProfileE2ETest {
	private WebDriver driver;

	private ChangeProfilePage changeProfilePage;
	
    private LoginPage loginPage;

	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

		driver = new ChromeDriver();

        driver.manage().window().maximize();
        changeProfilePage = PageFactory.initElements(driver, ChangeProfilePage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);

	}
	
	@After
    public void tearDown() {
        driver.quit();
    }
	
	
	@Test
	public void changeProfileTestSuccess() throws InterruptedException {
		
		
		logIn("userUser@gmail.com", "user");
		justWait();
		
		changeProfilePage.ensureIsDisplayedDropdown();
		changeProfilePage.getDropdownMenu().click();
		changeProfilePage.getProfileBtn().click();
		
		changeProfilePage.getOldPassword().sendKeys("user");
		changeProfilePage.getNewPassword().sendKeys("novanova2");
		changeProfilePage.getRePassword().sendKeys("novanova2");
		changeProfilePage.getChangePass().click();
		
		changeProfilePage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("Password updated!", alertText);
		assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
		
		
	}
	
	@Test
	public void changeProfilePasswordsDoesntMatchTest() throws InterruptedException {
		
		
		logIn("test@gmail.com", "user");
		justWait();
		
		changeProfilePage.ensureIsDisplayedDropdown();
		changeProfilePage.getDropdownMenu().click();
		changeProfilePage.getProfileBtn().click();
		
		changeProfilePage.getOldPassword().sendKeys("user");
		changeProfilePage.getNewPassword().sendKeys("novanova1");
		changeProfilePage.getRePassword().sendKeys("novanova2");
		
		changeProfilePage.getShow().forEach(elem -> elem.click());
		justWait();
		changeProfilePage.getChangePass().click();
		
		changeProfilePage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("The passwords does not match! Repeat password again!", alertText);
		assertEquals("http://localhost:4200/changeProfile", driver.getCurrentUrl());
		
		
	}
	
	@Test
	public void changeProfileWrongPasswordTest() throws InterruptedException {
		
		
		logIn("test@gmail.com", "user");
		justWait();
		
		changeProfilePage.ensureIsDisplayedDropdown();
		changeProfilePage.getDropdownMenu().click();
		changeProfilePage.getProfileBtn().click();
		
		changeProfilePage.getOldPassword().sendKeys("pogresnaLozinka");
		changeProfilePage.getNewPassword().sendKeys("novanova2");
		changeProfilePage.getRePassword().sendKeys("novanova2");
		
		changeProfilePage.getShow().forEach(elem -> elem.click());
		justWait();
		changeProfilePage.getChangePass().click();
		
		changeProfilePage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("The old password you have entered is incorrect!", alertText);
		assertEquals("http://localhost:4200/changeProfile", driver.getCurrentUrl());
		
		
	}
	
	@Test
	public void changeProfileEmptyInput() throws InterruptedException {
		
		
		logIn("test@gmail.com", "user");
		justWait();
		
		changeProfilePage.ensureIsDisplayedDropdown();
		changeProfilePage.getDropdownMenu().click();
		changeProfilePage.getProfileBtn().click();
		
		changeProfilePage.getOldPassword().click();
		changeProfilePage.getNewPassword().sendKeys("nova");
		changeProfilePage.getRePassword().sendKeys("nova");
		changeProfilePage.ensureErrorIsVisible();

		changeProfilePage.getChangePass().click();
		justWait();
		
		changeProfilePage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
		alt.accept();

		assertEquals("http://localhost:4200/changeProfile", driver.getCurrentUrl());
		
		
	}
	
	  private void logIn(String username, String password) throws InterruptedException {
	    	driver.get("http://localhost:4200/log-in");
			
			justWait();
			
			loginPage.getEmail().sendKeys(username);
			loginPage.getPassword().sendKeys(password);
			loginPage.getLoginBtn().click();
	    }
	  
	  private void justWait() throws InterruptedException {
	        synchronized (driver)
	        {
	            driver.wait(1000);
	        }
	    }
	

}
