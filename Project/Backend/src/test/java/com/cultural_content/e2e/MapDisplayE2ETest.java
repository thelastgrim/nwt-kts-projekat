package com.cultural_content.e2e;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.MapDisplayPage;

public class MapDisplayE2ETest {
	
	private WebDriver driver;
	
	private MapDisplayPage mapDisplayPage;
	

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        mapDisplayPage = PageFactory.initElements(driver, MapDisplayPage.class);
	}

	@After
    public void tearDown() {
        driver.quit();
    }
	
	@Test
	public void FilterByCitySucces() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCityButton();
		mapDisplayPage.ensureIsDisplayedPaginator();
		mapDisplayPage.ensureIsDisplayedCityFilterText();
		mapDisplayPage.getCityFilterText().click();
		justWait();
		List<WebElement> cities = driver.findElements(By.id("cityFilterBtn"));
    	assertTrue(cities.size() > 0);
    
    	mapDisplayPage.getCityFilterText().sendKeys("Penang");
    	List<WebElement> city = driver.findElements(By.id("cityFilterBtn"));
    	assertEquals(1, city.size());
    	mapDisplayPage.getCityFilterBtn().click();
    	
    	List<WebElement> pages = driver.findElements(By.className("page-link"));
    	assertEquals(pages.get(2).getText().charAt(0), '2');
    	//znaci da osim <<<<, <<, >> i >>>>, ima i brojeva stranica
    	assertTrue(pages.size() > 4);
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCityButton();
    	mapDisplayPage.getRemoveCityBtn().click();
 
    	String value = mapDisplayPage.getCityFilterText().getText();
    	assertEquals("", value );		
	}
	
	@Test
	public void FilterByCity_NoCityOffered() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCityButton();
		mapDisplayPage.ensureIsDisplayedPaginator();
		mapDisplayPage.ensureIsDisplayedCityFilterText();
		mapDisplayPage.getCityFilterText().click();
		justWait();
		List<WebElement> cities = driver.findElements(By.id("cityFilterBtn"));
    	assertTrue(cities.size() > 0);
    	
    	mapDisplayPage.getCityFilterText().sendKeys("Some city");
    	List<WebElement> city = driver.findElements(By.id("cityFilterBtn"));
    	assertEquals(0, city.size());
		
	}
	
	@Test
	public void FilterByCat_NoCategoryOffered() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCatButton();
		mapDisplayPage.ensureIsDisplayedPaginator();
		mapDisplayPage.ensureIsDisplayedCatFilterText();
		mapDisplayPage.getCatFilterText().click();
		justWait();
		List<WebElement> categories = driver.findElements(By.id("catFilterBtn"));
		assertTrue(categories.size() > 0);
    	
    	mapDisplayPage.getCatFilterText().sendKeys("Some category");
    	List<WebElement> cat = driver.findElements(By.id("catFilterBtn"));
    	assertEquals(0, cat.size());
    	

	}
	
	@Test
	public void FilterByCatSucces() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCatButton();
		mapDisplayPage.ensureIsDisplayedPaginator();
		mapDisplayPage.ensureIsDisplayedCatFilterText();
		mapDisplayPage.getCatFilterText().click();
		justWait();
		List<WebElement> categories = driver.findElements(By.id("catFilterBtn"));
		assertTrue(categories.size() > 0);
    	
    	mapDisplayPage.getCatFilterText().sendKeys("Concerts");
    	List<WebElement> cat = driver.findElements(By.id("catFilterBtn"));
    	assertEquals(1, cat.size());
    	mapDisplayPage.getCatFilterBtn().click();
    
    	
    	List<WebElement> pages = driver.findElements(By.className("page-item"));
    	
    	
    	assertEquals(pages.get(1).getText().split("")[0], "1");
    	assertTrue(pages.size()==7); 
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCatButton();
    	mapDisplayPage.getRemoveCategoryBtn().click();

    	String value = mapDisplayPage.getCatFilterText().getText();
    	assertEquals("", value );
		
	}
	
	@Test
	public void FilterByCityAndCategorySucces() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCityButton();
		mapDisplayPage.ensureIsDisplayedCityFilterText();
		mapDisplayPage.ensureIsNotDisplayedRemoveCatButton();
		mapDisplayPage.ensureIsDisplayedCatFilterText();
		mapDisplayPage.ensureIsDisplayedPaginator();
		
		mapDisplayPage.getCityFilterText().click();
    	mapDisplayPage.getCityFilterText().sendKeys("Flórina");
    	List<WebElement> city = driver.findElements(By.id("cityFilterBtn"));
    	assertEquals(1, city.size());
    	mapDisplayPage.getCityFilterBtn().click();
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCityButton();
   
		
		mapDisplayPage.getCatFilterText().click();
    	mapDisplayPage.getCatFilterText().sendKeys("Museums");
    	List<WebElement> cat = driver.findElements(By.id("catFilterBtn"));
    	assertEquals(1, cat.size());
    	mapDisplayPage.getCatFilterBtn().click();
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCatButton();
    	
    	List<WebElement> pages = driver.findElements(By.className("page-link"));
    	

    	assertEquals(pages.get(1).getText().charAt(0), '1');
    	assertTrue(pages.size() == 3);
    	
    	mapDisplayPage.ensureIsDisplayedClearAllButton();
    	mapDisplayPage.getClearAllBtn().click();
    	
    	String categoryValue = mapDisplayPage.getCatFilterText().getText();
    	assertEquals("", categoryValue );
    	
    	String cityValue = mapDisplayPage.getCityFilterText().getText();
    	assertEquals("", cityValue );
	
	}
	
	@Test
	public void FilterByCityAndCategory_NoOffersUnderFilters() throws InterruptedException {
		
		driver.get("http://localhost:4200/map");
		justWait();
		
		mapDisplayPage.ensureIsDisplayedMap();
		mapDisplayPage.ensureIsNotDisplayedRemoveCityButton();
		mapDisplayPage.ensureIsDisplayedCityFilterText();
		mapDisplayPage.ensureIsNotDisplayedRemoveCatButton();
		mapDisplayPage.ensureIsDisplayedCatFilterText();
		mapDisplayPage.ensureIsDisplayedPaginator();
		
		mapDisplayPage.getCityFilterText().click();
    	mapDisplayPage.getCityFilterText().sendKeys("Flórina");
    	List<WebElement> city = driver.findElements(By.id("cityFilterBtn"));
    	assertEquals(1, city.size());
    	mapDisplayPage.getCityFilterBtn().click();
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCityButton();
   
		
		mapDisplayPage.getCatFilterText().click();
    	mapDisplayPage.getCatFilterText().sendKeys("Teathers");
    	List<WebElement> cat = driver.findElements(By.id("catFilterBtn"));
    	assertEquals(1, cat.size());
    	mapDisplayPage.getCatFilterBtn().click();
    	
    	mapDisplayPage.ensureIsDisplayedRemoveCatButton();
    	
    	List<WebElement> pages = driver.findElements(By.className("page-link"));
    	assertFalse(pages.get(1).getText().charAt(0) == '1');
    	assertFalse(pages.size() > 4);

	}
	
	private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }
	
	

}
