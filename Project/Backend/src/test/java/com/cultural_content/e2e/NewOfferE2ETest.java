package com.cultural_content.e2e;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Window;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.AboutPage;
import com.cultural_content.pages.ContentPage;
import com.cultural_content.pages.LoginPage;
import com.cultural_content.pages.NewCategory;
import com.cultural_content.pages.NewOffer;

import ch.qos.logback.classic.pattern.EnsureExceptionHandling;




public class NewOfferE2ETest {
	
	private WebDriver driver;

    private LoginPage loginPage;

    private ContentPage contentPage;
    
    private NewOffer newOfferForm;
    
    private AboutPage aboutPage;
   
    
    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        contentPage = PageFactory.initElements(driver, ContentPage.class);

        newOfferForm = PageFactory.initElements(driver, NewOffer.class);
        aboutPage = PageFactory.initElements(driver, AboutPage.class);
        
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    
    @Test
    public void addOfferInputIncompletePicture() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();    	
    	newOfferForm.setNewName("NewOffer");

		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.ensureIsDisplayedAlertPic();
	   	justWait();
        
		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");
	   	
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
    
    @Test
    public void addOfferSuccess() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("NewOfferNameAgain");

		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 59");
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpicforAddOfferSucces.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("Opis");
		
		newOfferForm.ensureIsDisplayedSaveBtn();
	   	justWait();

	   	newOfferForm.getSaveBtn().click();
	
	   	aboutPage.ensureIsDisplayedOfferName();
    }
    @Test
    public void addOfferFail() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("Gallery1");

		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Vojvode Bojovica 2");
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpicforAddOfferSucces.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("Opis");
		
		newOfferForm.ensureIsDisplayedSaveBtn();
	   	justWait();
	   	newOfferForm.getSaveBtn().click();
	   	justWait();
	   	contentPage.ensureAlertIsDisplayed();
	   	justWait();
	   	assertEquals("Cultural offer with that name, or on that location already exists. Try again!",
				driver.switchTo().alert().getText());
	   	}

    @Test
    public void addOfferInputIncompleteName() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("o");
       	newOfferForm.getDriver().findElement(By.id("name")).sendKeys(Keys.BACK_SPACE);
     
		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

	  	newOfferForm.ensureIsDisplayedAlertName();
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
    
    @Test
    public void addOfferInputIncompleteCountry() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("NewOffer");
     
		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("s");
       	newOfferForm.getDriver().findElement(By.id("country")).sendKeys(Keys.BACK_SPACE);
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

	  	newOfferForm.ensureIsDisplayedAlertCountry();
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
    
    @Test
    public void addOfferInputIncompleteCity() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("NewOffer");
     
		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("n");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys(Keys.BACK_SPACE);
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

	  	newOfferForm.ensureIsDisplayedAlertCity();
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
    
    @Test
    public void addOfferInputIncompleteStreet() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("NewOffer");
     
		newOfferForm.getDriver().findElement(By.id("category")).click();
		justWait();
		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("B");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys(Keys.BACK_SPACE);
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

	  	newOfferForm.ensureIsDisplayedAlertStreet();
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
    
    @Test
    public void addOfferInputIncompleteDescription() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();
        
    	newOfferForm.setNewName("o");
     
		newOfferForm.getDriver().findElement(By.id("category")).click();

		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys(Keys.BACK_SPACE);

	  	newOfferForm.ensureIsDisplayedAlertDescription();
	   	assertFalse(newOfferForm.getDriver().findElement(By.id("save-btn")).isEnabled());
    }
  
    @Test
    public void addOfferCancel() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();
    	contentPage.ensureIsDisplayAddOfferButton();
    	contentPage.getAddOfferBtn().click();
    	newOfferForm.ensureIsDisplayedNewOfferForm();

    	newOfferForm.setNewName("o");

		newOfferForm.getDriver().findElement(By.id("category")).click();

		newOfferForm.getDriver().findElement(By.xpath("//div[2]/div[2]/div/div/div/button")).click();
		
		newOfferForm.getDriver().findElement(By.id("country")).sendKeys("Srbija");
		newOfferForm.getDriver().findElement(By.id("city")).sendKeys("Novi Sad");
		newOfferForm.getDriver().findElement(By.id("street")).sendKeys("Bulevar Cara Lazara 60");
		
		newOfferForm.getDriver().findElement(By.id("pic")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\testpic.jpg" );

		newOfferForm.getDriver().findElement(By.id("form103")).sendKeys("o");

		newOfferForm.getCancelBtn().click();
		justWait();
		assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
		
    }
  
    private void logIn(String username, String password) throws InterruptedException {
    	driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys(username);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLoginBtn().click();
    }
    
    private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
