package com.cultural_content.e2e;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.ContentPage;
import com.cultural_content.pages.LoginPage;




public class ContentE2ETest {
	
	private WebDriver driver;

    private LoginPage loginPage;

    private ContentPage contentPage;
    
    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        contentPage = PageFactory.initElements(driver, ContentPage.class);
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    @Test
    public void DeleteCulturalOfferHomepageSuccess() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	
    	contentPage.ensureIsDisplayedDeleteOfferIcon();
    	
    	List<WebElement> deletes = driver.findElements(By.id("deleteOfferIcon"));
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	
    	String lastOfferName = offers.get(offers.size()-1).getText();
    	
    	deletes.get(deletes.size()-1).click();
    	
    	driver.switchTo().alert().accept();
    	
    	justWait();
    	
    	offers = driver.findElements(By.className("card-title"));
    	
    	String newLastOfferName = offers.get(offers.size()-1).getText();
    	
    	assertNotEquals(lastOfferName, newLastOfferName);
    	
    }
    
    @Test
    public void DeleteCulturalOfferDetailsSuccess() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	
    	justWait();

    	contentPage.ensureIsDisplayedOfferDetailsLink();
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	
    	justWait();
    	offers.get(offers.size()-2).click();
    	//contentPage.ensureIsDisplayedNewsNavLink();
    	//contentPage.ensureIsDisplayedDeleteOfferIcon();
    	
    	//mora dase skroluje da bi komponenta bila u viewu da bi mogla da se klikne
    	driver.switchTo().activeElement().sendKeys(Keys.HOME);
    	justWait();

    	//justWait();
    	contentPage.getDeleteOffer().click();
    	
    	driver.switchTo().alert().accept();
    	
    	contentPage.ensureIsDisplayedSearchField();
    	
    	assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
    	
    }
    
    @Test
    public void BrowseUnauthenticatedSeeDetailsTest() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();

 
    	
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	
    	offers.get(0).click();
    	
    	justWait();

    	//ne moze /offer/1 jer se ne znamsta ce biti obrisano sta nece
    	assertNotEquals("http://localhost:4200/content", driver.getCurrentUrl());
    		
    }
    
    
    @Test
    public void BrowseUnauthenticatedNextPageTest() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();
    	contentPage.ensureIsDisplayedPaginator();
    	
    	contentPage.getPaginator().click();
    	
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	List<WebElement> pages = driver.findElements(By.className("page-link"));
    	
    	String offer = offers.get(0).getText();
    	
    	
    	pages.get(2).click();
    	
    	justWait();
    	
    	List<WebElement> offersAfter = driver.findElements(By.className("card-title"));
    	
    	String offerAfter = offersAfter.get(0).getText();
    	
    	assertNotEquals(offer, offerAfter);
    		
    }
    
    @Test
    public void BrowseUnauthenticatedSearchTestElementsFound() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();
    	contentPage.ensureIsDisplayedSearchField();
    	
    	contentPage.getSearchField().sendKeys("barton");
    	
    	contentPage.getSearchBtn().click();
    	
    	justWait();
  
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	
    	assertEquals(6, offers.size());
    		
    }
    
    @Test
    public void BrowseUnauthenticatedSearchElementsTestNotFound() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();
    	contentPage.ensureIsDisplayedSearchField();
    	
    	contentPage.getSearchField().sendKeys("-1111");
    	
    	contentPage.getSearchBtn().click();
    	justWait();
    	assertEquals("Cultural Offer with that name does not exist. Please check the spelling.",
    				driver.switchTo().alert().getText());
    		
    }
    
    @Test
    public void BrowseUnauthenticatedResetSearchTest() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();
    	contentPage.ensureIsDisplayedSearchField();
    	
    	contentPage.getSearchField().sendKeys("barton");
  
    	contentPage.getSearchBtn().click();
    	
    	justWait();
    	
    	contentPage.getResetBtn().click();
    	justWait();
    	
    	List<WebElement> categories = driver.findElements(By.name("categoryName"));
    	
    	assertEquals("MUSEUMS", categories.get(0).getText());
  
    		
    }
    
    @Test
    public void BrowseUnauthenticatedChangeCategoryTest() throws InterruptedException {
    	
    	driver.get("http://localhost:4200/content");
    	justWait();
    	contentPage.ensureIsDisplayedLoginBtn();
 
    	List<WebElement> categories = driver.findElements(By.name("categoryName"));
    	
    	categories.get(1).click();
    	
    	justWait();
    	
    	List<WebElement> offers = driver.findElements(By.className("card-title"));
    	
    
    	assertEquals("Teathers1",  offers.get(0).getText());
  
    		
    }
    
    
    @Test
    public void LeaveCommentTestSuccess() throws InterruptedException {

    	logIn("test@gmail.com", "user");
    	


        justWait();
    	
        contentPage.ensureIsDisplayedUsername();
        contentPage.ensureIsDisplayedLeaveComment();
        
        contentPage.getLeaveComment().click();
        
        //contentPage.ensureIsDisplayedLeaveCommentForm();
        driver.switchTo().activeElement();
         
        //contentPage.ensureIsDisplayedCommentTextArea();
        
        contentPage.getCommentText().sendKeys("Ovo je novi komentar od strane korisnika");
        justWait();
        contentPage.getPictureInput().sendKeys(System.getProperty("user.dir")+"\\src\\test\\resources\\testpic.jpg");
        contentPage.getSaveComment().click();
        
        assertEquals("Comment created.", driver.switchTo().alert().getText());
        
    }
    
    @Test
    public void LeaveCommentTestFail() throws InterruptedException {

    	logIn("test@gmail.com", "user");
    	
        justWait();
    	
        contentPage.ensureIsDisplayedUsername();
        contentPage.ensureIsDisplayedLeaveComment();
        
        contentPage.getLeaveComment().click();
        
        //contentPage.ensureIsDisplayedLeaveCommentForm();
        driver.switchTo().activeElement();
         
        //contentPage.ensureIsDisplayedCommentTextArea();
        
        justWait();
        contentPage.getPictureInput().sendKeys(System.getProperty("user.dir")+"\\src\\test\\resources\\Enrique_Simonet_-_Marina_veneciana_6MB.jpg");
        contentPage.getSaveComment().click();
        
        assertEquals("File too large.", driver.switchTo().alert().getText());
    }
    
    @Test
    public void SubscribeTest() throws InterruptedException {

    	logIn("test@gmail.com", "user");
    	
        justWait();
    	
        contentPage.ensureIsDisplayedUsername();
        contentPage.ensureIsDisplayedSubUnsub();
        
        List<WebElement> buttons = driver.findElements(By.id("subUnsubBtn"));
        
        buttons.get(4).click();
        
        justWait();
       
        assertEquals("UNSUBSCRIBE", buttons.get(4).getText());
        
        
        
        
        
           
        //contentPage.getSubUnsub().click(); //reverte

    }
    
    @Test
    public void UnsubscribeTest() throws InterruptedException {

    	logIn("test@gmail.com", "user");
    	
        justWait();
    	
        contentPage.ensureIsDisplayedUsername();
        contentPage.ensureIsDisplayedSubUnsub();
        
        
        contentPage.getSubUnsub().click();
        justWait();
     
        assertEquals("SUBSCRIBE", contentPage.getSubUnsub().getText());
     
        
        
        
       
           
    }
    
    @Test
    public void addNewsTestSuccess() throws InterruptedException {

    	logIn("admin@gmail.com", "admin");
        justWait();
    	
        contentPage.ensureIsDisplayedAddNewsBtn();     
        contentPage.getAddNewsBtn().click();
        
        driver.switchTo().activeElement();
            
        contentPage.getNewsTitleArea().sendKeys("Izlozba 25.1.2021.");
        contentPage.getNewsTextArea().sendKeys("Obavestavamo vas da se odrzava nova izlozba");
        justWait();

        contentPage.getAddBtn().click();
        justWait();
        justWait();
        justWait();
        assertEquals("News has been successfully added!", driver.switchTo().alert().getText());
        
    }
    
    @Test
    public void addNewsEmptyInputTest() throws InterruptedException {

    	logIn("admin@gmail.com", "admin");
        justWait();
    	
        contentPage.ensureIsDisplayedAddNewsBtn();     
        contentPage.getAddNewsBtn().click();
        
        driver.switchTo().activeElement();
            
        contentPage.getNewsTitleArea().click();
        contentPage.getNewsTextArea().sendKeys("Obavestavamo vas da se odrzava nova izlozba");
        justWait();

        contentPage.getAddBtn().click();
        contentPage.ensureErrorIsVisible();
        
        assertNotNull(contentPage.getAddBtn());
        
    }
    
    @Test
    public void addNewsCancelTest() throws InterruptedException {

    	logIn("admin@gmail.com", "admin");
        justWait();
    	
        contentPage.ensureIsDisplayedAddNewsBtn();     
        contentPage.getAddNewsBtn().click();
        
        driver.switchTo().activeElement();
            
        contentPage.getNewsTitleArea().sendKeys("Izlozba 25.1.2021.");
        contentPage.getNewsTextArea().sendKeys("Obavestavamo vas da se odrzava nova izlozba");
        justWait();

        contentPage.getCloseBtn().click();
        justWait();
        contentPage.ensureIsNotVisibleAddBtn();
        assertNotNull(contentPage.getAddNewsBtn());
    }
    
    @Test
    public void updateOfferSuccess() throws InterruptedException {
    	 logIn("admin@gmail.com", "admin");
    	 
    	 contentPage.ensureIsDisplayedOpenUpdateBtn();
    	 
    	 List<WebElement> updates = driver.findElements(By.id("openUpdateBtn"));
     	 List<WebElement> offersNames = driver.findElements(By.className("card-title"));
     	 List<WebElement> offersDescription = driver.findElements(By.className("card-text"));
     	
    	 updates.get(updates.size() - 2).click();
    	 
    	 driver.switchTo().activeElement();
    	 
    	 contentPage.ensureIsDisplayedUpdateForm();
    	 contentPage.ensureIsDisplayedUpdateDescription();
    	 contentPage.ensureIsDisplayedUpdateName();
    	 contentPage.ensureIsDisplayedUpdateBtn();
    	 contentPage.ensureIsDisplayedCloseUpdateBtn();
    	 contentPage.ensureIsDisplayedCancelUpdateBtn();
    	 
    	 contentPage.getUpdateName().clear();
    	 contentPage.getUpdateName().sendKeys("New Name");
    	 
    	 contentPage.getUpdateDescriptionText().clear();
    	 contentPage.getUpdateDescriptionText().sendKeys("New description");
    	 
    	 contentPage.getUpdateBtn().click();
    	 
         assertEquals(offersNames.get(offersNames.size() - 2).getText() , "New Name");
         assertEquals(offersDescription.get(offersDescription.size() - 2).getText() , "New description");
         
    	 
    }
    
    @Test
    public void updateOfferSuccess_NameAlreadyExists() throws InterruptedException {
    	 logIn("admin@gmail.com", "admin");
    	 
    	 contentPage.ensureIsDisplayedOpenUpdateBtn();
    	 
    	 List<WebElement> updates = driver.findElements(By.id("openUpdateBtn"));
     	
    	 updates.get(updates.size() - 2).click();
    	 
    	 driver.switchTo().activeElement();
    	 
    	 contentPage.ensureIsDisplayedUpdateForm();
    	 contentPage.ensureIsDisplayedUpdateDescription();
    	 contentPage.ensureIsDisplayedUpdateName();
    	 contentPage.ensureIsDisplayedUpdateBtn();
    	 contentPage.ensureIsDisplayedCloseUpdateBtn();
    	 contentPage.ensureIsDisplayedCancelUpdateBtn();
    	 
    	 contentPage.getUpdateName().clear();
    	 contentPage.getUpdateName().sendKeys("Willms, Sanford and Toy");
    	 
    	 
    	 contentPage.getUpdateBtn().click();
    	 justWait();
    	 
    	 assertEquals("Cultural offer with that name already exists!",
 				driver.switchTo().alert().getText());
    	 
    	 
    	 
    }
    
    @Test
    public void updateOfferSuccess_NameEmpty() throws InterruptedException {
    	 logIn("admin@gmail.com", "admin");
    	 
    	 contentPage.ensureIsDisplayedOpenUpdateBtn();
    	 
    	 List<WebElement> updates = driver.findElements(By.id("openUpdateBtn"));
     	
    	 updates.get(updates.size() - 2).click();
    	 
    	 driver.switchTo().activeElement();
    	 
    	 contentPage.ensureIsDisplayedUpdateForm();
    	 contentPage.ensureIsDisplayedUpdateDescription();
    	 contentPage.ensureIsDisplayedUpdateName();
    	 contentPage.ensureIsDisplayedUpdateBtn();
    	 contentPage.ensureIsDisplayedCloseUpdateBtn();
    	 contentPage.ensureIsDisplayedCancelUpdateBtn();
    	 
    	 contentPage.getUpdateName().sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE)));
    	 
    	 justWait();
 	   	
    	 contentPage.ensureIsDisplayedAlertUpdateName();
    	 assertFalse(contentPage.getUpdateBtn().isEnabled());
 	 
    }
    
    @Test
    public void updateOfferSuccess_DescriptionEmpty() throws InterruptedException {
    	 logIn("admin@gmail.com", "admin");
    	 
    	 contentPage.ensureIsDisplayedOpenUpdateBtn();
    	 
    	 List<WebElement> updates = driver.findElements(By.id("openUpdateBtn"));
     	
    	 updates.get(updates.size() - 2).click();
    	 
    	 driver.switchTo().activeElement();
    	 
    	 contentPage.ensureIsDisplayedUpdateForm();
    	 contentPage.ensureIsDisplayedUpdateDescription();
    	 contentPage.ensureIsDisplayedUpdateName();
    	 contentPage.ensureIsDisplayedUpdateBtn();
    	 contentPage.ensureIsDisplayedCloseUpdateBtn();
    	 contentPage.ensureIsDisplayedCancelUpdateBtn();
    	 
    	 contentPage.getUpdateDescriptionText().sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE)));
    	 
    	 justWait();
 	   	
    	 contentPage.ensureIsDisplayedAlertUpdateDescription();
    	 assertFalse(contentPage.getUpdateBtn().isEnabled());
 	 
    }
    
    
    
    private void logIn(String username, String password) throws InterruptedException {
    	driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys(username);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLoginBtn().click();
    }
    
    private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
