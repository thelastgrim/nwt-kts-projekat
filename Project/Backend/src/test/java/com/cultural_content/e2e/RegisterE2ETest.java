package com.cultural_content.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.RegisterPage;

public class RegisterE2ETest {
	
	private WebDriver driver;

	private RegisterPage registerPage;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
//		DesiredCapabilities dc = new DesiredCapabilities();
//		dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
//        driver = new ChromeDriver(dc);
		driver = new ChromeDriver();

        driver.manage().window().maximize();
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
	}
	
	@After
    public void tearDown() {
        driver.quit();
    }
	
	@Test
	public void RegisterTestSuccess() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		
		registerPage.getUsername().sendKeys("nesto sto nije u bazi");
		registerPage.getEmail().sendKeys("ana.jordanovic@hotmail.com");
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka123");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureAlertIsDisplayed();		
	    Alert alt = driver.switchTo().alert();
		alt.accept();
		justWait();
		registerPage.ensureIsNotVisibleRegisterBtn();
		assertEquals("http://localhost:4200/content", driver.getCurrentUrl());
		
	}
	
	@Test
	public void RegisterTestUsernameAlreadyExist() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		registerPage.ensureIsDisplayedUsername();
		registerPage.getUsername().sendKeys("test"); //username koji postoji u bazi
		registerPage.getEmail().sendKeys("some.adddress@gmail.com");
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka123");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureAlertIsDisplayed();		
	    Alert alt = driver.switchTo().alert();
		alt.accept();
		
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());
		
		
	}
	
	@Test
	public void RegisterTestEmailAlreadyExist() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		
		registerPage.getUsername().sendKeys("nesto sto nije u bazi2");
		registerPage.getEmail().sendKeys("test@gmail.com"); //mail koji je u bazi
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka123");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("Username or email already exists. Try again", alertText);
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());
				
	}
	
	@Test
	public void RegisterInavlidEmailFormatTest() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		
		registerPage.getUsername().sendKeys("nesto sto nije u bazi3");
		registerPage.getEmail().sendKeys("test"); //nevalidan mail
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka123");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("Wrong credentials :(", alertText);
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());
				
	}
	
	@Test
	public void RegisterPasswordsDoesntMatchTest() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		
		registerPage.getUsername().sendKeys("nesto sto nije u bazi4");
		registerPage.getEmail().sendKeys("jordanovicaleksandra@gmail.com");
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureAlertIsDisplayed();	
		justWait();
	    Alert alt = driver.switchTo().alert();
	    String alertText = alt.getText();
		alt.accept();

		assertEquals("The passwords does not match! Please try again!", alertText);
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());				
	}
	
	@Test
	public void RegisterEmptyInputTest() throws InterruptedException {
		
		driver.get("http://localhost:4200/register");
		
		justWait();
		
		registerPage.getUsername().sendKeys("nesto sto nije u bazi5");
		registerPage.getEmail().sendKeys("");
		registerPage.getPassword().sendKeys("lozinka123");
		registerPage.getRePassword().sendKeys("lozinka123");	
		registerPage.getRegisterBtn().click();
		
		registerPage.ensureErrorIsVisible();		
		
		assertEquals("http://localhost:4200/register", driver.getCurrentUrl());				
	}
	
	private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
