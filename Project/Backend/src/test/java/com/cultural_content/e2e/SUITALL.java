package com.cultural_content.e2e;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.test.context.TestPropertySource;

@RunWith(Suite.class)
@SuiteClasses({AboutE2ETest.class,
			   LoginE2ETest.class,
			   MapDisplayE2ETest.class,
			   ChangeProfileE2ETest.class,
			   NewCategoryE2ETest.class,
			   ContentE2ETest.class,
			   NewOfferE2ETest.class,
			   RegisterE2ETest.class,
			   ContentE2ETest.class,
			   ContentDeleteCategoryE2ETest2.class})
@TestPropertySource("classpath:test.properties")
public class SUITALL {

}
