package com.cultural_content.e2e;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.ContentPage;
import com.cultural_content.pages.LoginPage;




public class ContentDeleteCategoryE2ETest2 {
	
	private WebDriver driver;

    private LoginPage loginPage;

    private ContentPage contentPage;
    
    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        contentPage = PageFactory.initElements(driver, ContentPage.class);
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    @Test
    public void DeleteCategoryOfferHomepageSuccess() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	
    	contentPage.ensureIsDisplayedDeleteOfferIcon();
    	justWait();
    	
    	List<WebElement> deleteOffer = driver.findElements(By.id("deleteCategoryIcon"));
    	List<WebElement> categories = contentPage.getDriver().findElements(By.className("deleteCategoryClass"));
    	if(deleteOffer.size()!=0) {
    		justWait();
        	
        	
        	String lastCategoryName = categories.get(categories.size()-1).getText();
        	justWait();
        	categories.get(categories.size()-1).click();
        	
        	driver.switchTo().alert().accept();
        	
        	justWait();
        	contentPage.ensureIsDeletingNotDone();
        	
        	List<WebElement> categoriesAfter = contentPage.getDriver().findElements(By.className("deleteCategoryClass"));
        	justWait();
        	assertNotEquals(categories.size(), categoriesAfter.size());
    	}else {

        	List<WebElement> categoriesAfter = contentPage.getDriver().findElements(By.className("deleteCategoryClass"));
    		assertEquals(categories.size(), categoriesAfter.size());
    	}
    }
    
    @Test
    public void DeleteCategoryOfferHomepageCancel() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	
    	contentPage.ensureIsDisplayedDeleteOfferIcon();
    	justWait();
    	List<WebElement> deleteOffer = driver.findElements(By.id("deleteCategoryIcon"));
    	List<WebElement> categories = contentPage.getDriver().findElements(By.className("deleteCategoryClass"));
    	justWait();
    	
    	
    	String lastCategoryName = categories.get(categories.size()-1).getText();
    	justWait();
    	categories.get(categories.size()-1).click();
    	
    	driver.switchTo().alert().dismiss();
    	
    	justWait();
    	
    	List<WebElement> categoriesAfter = contentPage.getDriver().findElements(By.className("deleteCategoryClass"));
    	justWait();
    	assertEquals(categories.size(), categoriesAfter.size());
    	
    }
   
    private void logIn(String username, String password) throws InterruptedException {
    	driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys(username);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLoginBtn().click();
    }
    
    private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
