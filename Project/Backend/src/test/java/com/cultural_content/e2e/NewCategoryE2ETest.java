package com.cultural_content.e2e;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.cultural_content.pages.ContentPage;
import com.cultural_content.pages.LoginPage;
import com.cultural_content.pages.NewCategory;

import ch.qos.logback.classic.pattern.EnsureExceptionHandling;




public class NewCategoryE2ETest {
	
	private WebDriver driver;

    private LoginPage loginPage;

    private ContentPage contentPage;
    
    private NewCategory addCategoryForm;
    
    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        contentPage = PageFactory.initElements(driver, ContentPage.class);

    	addCategoryForm = PageFactory.initElements(driver, NewCategory.class);

        
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    @Test
    public void addCategorySuccess() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();

    	contentPage.ensureIsDisplayAddCategoryButton();
    	justWait();
    	List<WebElement> categoriesBefore = contentPage.getDriver().findElements(By.name("categoryName"));
    	int sizeBefore = categoriesBefore.size();
    	contentPage.getAddCategoryBtn().click();
    	addCategoryForm.ensureIsDisplayedNewCategoryForm();
    	justWait();
        
    	addCategoryForm.setNewCategoryNameInput("NewCategory");
        
    	addCategoryForm.getSaveBtn().click();
    	justWait();
        
    	contentPage.ensureIsDisplayedCategories();
    	justWait();
    	
    	List<WebElement> categoriesAfter = contentPage.getDriver().findElements(By.name("categoryName"));
    	int sizeAfter = categoriesAfter.size();
    	assertEquals(sizeBefore + 1, sizeAfter);

    	assertEquals("NewCategory".toUpperCase(), categoriesAfter.get(sizeAfter-1).getText());
    }
    
    @Test
    public void addCategoryExistName() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();

    	contentPage.ensureIsDisplayAddCategoryButton();
    	contentPage.getAddCategoryBtn().click();
    	addCategoryForm.ensureIsDisplayedNewCategoryForm();
    	justWait();
        
    	addCategoryForm.setNewCategoryNameInput("Museums");
    	justWait();
        addCategoryForm.ensureIsDisplayedAlertExistName();
        assertFalse(addCategoryForm.getSaveBtn().isEnabled());
        
    }
    
    @Test
    public void addCategoryRequiredName() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();

    	contentPage.ensureIsDisplayAddCategoryButton();
    	contentPage.getAddCategoryBtn().click();
    	addCategoryForm.ensureIsDisplayedNewCategoryForm();
    	justWait();
    	  
    	addCategoryForm.setNewCategoryNameInput("s");
    	justWait();

    	addCategoryForm.getDriver().findElement(By.id("newName")).sendKeys(Keys.BACK_SPACE);
     	justWait();
        addCategoryForm.ensureIsDisplayedAlertRequierdName();
        assertFalse(addCategoryForm.getSaveBtn().isEnabled());
    }
    
    @Test
    public void addCategoryMinlengthName() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();

    	contentPage.ensureIsDisplayAddCategoryButton();
    	contentPage.getAddCategoryBtn().click();
    	addCategoryForm.ensureIsDisplayedNewCategoryForm();
    	justWait();
        
    	addCategoryForm.setNewCategoryNameInput("s");
    	justWait();
        addCategoryForm.ensureIsDisplayedAlertMinlengthName();
        assertFalse(addCategoryForm.getSaveBtn().isEnabled());
        
    }
    
    @Test
    public void addCategoryCancel() throws InterruptedException {
    	logIn("admin@gmail.com", "admin");
    	justWait();

    	contentPage.ensureIsDisplayAddCategoryButton();
    	List<WebElement> categoriesBefore = contentPage.getDriver().findElements(By.className("col"));
    	int sizeBefore = categoriesBefore.size();
    	contentPage.getAddCategoryBtn().click();
    	addCategoryForm.ensureIsDisplayedNewCategoryForm();
    	justWait();
        
    	addCategoryForm.setNewCategoryNameInput("NewCategory22");
    	justWait();
        addCategoryForm.ensureIsDisplayedCancelBtn();
        
    	addCategoryForm.getCancelBtnt().click();
    	justWait();
        
    	contentPage.ensureIsDisplayedCategories();
    	justWait();
    	
    	List<WebElement> categoriesAfter = contentPage.getDriver().findElements(By.className("col"));
    	int sizeAfter = categoriesAfter.size();
    	assertEquals(sizeBefore, sizeAfter);
        
    }
    
   
    
    private void logIn(String username, String password) throws InterruptedException {
    	driver.get("http://localhost:4200/log-in");
		
		justWait();
		
		loginPage.getEmail().sendKeys(username);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLoginBtn().click();
    }
    
    private void justWait() throws InterruptedException {
        synchronized (driver)
        {
            driver.wait(1000);
        }
    }

}
