package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContentPage {
	
	private WebDriver driver;
	
	@FindBy(id = "usernameBtn")
	private WebElement username;
	
	@FindBy(id = "commentBtn")
	private WebElement leaveComment;
	
	@FindBy(id = "subUnsubBtn")
	private WebElement subUnsub;
	
	@FindBy(name = "search-entry")
	private WebElement searchField;
	
	@FindBy(id = "searchBtn")
	private WebElement searchBtn;
	
	@FindBy(id = "resetBtn")
	private WebElement resetBtn;
	
	@FindBy(id = "deleteOfferIcon")
	private WebElement deleteOffer;
	
	 //Add category
	@FindBy(id = "add-category")
	private WebElement addCategoryBtn;
	
	@FindBy(id = "add-offer")
	private WebElement addOfferBtn;
	
	@FindBy(id = "categories")
	private WebElement categories;
	
	@FindBy(id = "addNewsBtn")
	private WebElement addNewsBtn;
	
	
	///elementi u CommentFormi
	@FindBy(id = "commentTextArea")
	private WebElement commentText;
	
	@FindBy(id = "saveCommentBtn")
	private WebElement saveComment;
	
	@FindBy(id = "leaveCommentForm")
	private WebElement leaveCommentForm;

	@FindBy(id = "file")
	private WebElement pictureInput;
	
	@FindBy(id = "paginator")
	private WebElement paginator;
	
	//elementi u addNewsFormi
	@FindBy(id = "newsTitleArea")
	private WebElement newsTitleArea;
	
	@FindBy(id = "newsTextArea")
	private WebElement newsTextArea;
	
	@FindBy(id = "addBtn")
	private WebElement addBtn;
	
	@FindBy(id = "closeBtn")
	private WebElement closeBtn;
	
	//update
	@FindBy(id = "openUpdateBtn")
	private WebElement openUpdateBtn;
	
	//update forma
	@FindBy(id = "closeUpdateBtn")
	private WebElement closeUpdateBtn;
	
	@FindBy(id = "updateName")
	private WebElement updateName;
	
	@FindBy(id = "alertUpdateName")
	private WebElement alertUpdateName;
	
	@FindBy(id = "updateDescriptionText")
	private WebElement updateDescriptionText;
	
	@FindBy(id = "alertUpdateDescription")
	private WebElement alertUpdateDescription;
	
	@FindBy(id = "cancelUpdateBtn")
	private WebElement cancelUpdateBtn;
	
	@FindBy(id = "updateBtn")
	private WebElement updateBtn;
	
	@FindBy(id = "updateForm")
	private WebElement updateForm;

	@FindBy(id = "offerDetailsLink")
	private WebElement offerDetailsLink;
	
	public WebElement getOfferDetailsLink() {
		return offerDetailsLink;
	}

	public ContentPage() {
		
	}

	public WebDriver getDriver() {
		return driver;
	}

	public ContentPage(WebDriver driver) {
		this.driver = driver;
	}
	

	public void ensureIsDisplayAddCategoryButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("add-category")));
	}
	
	public void ensureIsDisplayAddOfferButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("add-offer")));
	}
	
	public void ensureIsDisplayedNewsNavLink() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("NewsNavLink")));
    }
	
	public void ensureIsDisplayedDeleteOfferIcon() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteOfferIcon")));
    }
	
	public void ensureIsDisplayedSearchField() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.name("search-entry")));
    }
	
	public void ensureIsDisplayedPaginator() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("paginator")));
    }
	
	public void ensureIsDisplayedLoginBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("logInHeaderBtn")));
    }
	
	public void ensureIsDisplayedUsername() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("usernameBtn")));
	}
	
	public void ensureIsDisplayedSubUnsub() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("subUnsubBtn")));
	}
	
	public void ensureIsDisplayedLeaveComment() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("commentBtn")));
	}
	
	public void ensureIsDisplayedLeaveCommentForm() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("leaveCommentForm")));
	}
	public void ensureIsDisplayedSaveComment() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("saveCommentBtn")));
	}
	public void ensureIsDisplayedCommentTextArea() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("commentTextArea")));
	}

	public void ensureIsDisplayedCategories() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("categories")));
	}
	
	public void ensureIsDisplayedAddNewsBtn() {
		(new WebDriverWait(driver, 50)).until(ExpectedConditions.elementToBeClickable(By.id("addNewsBtn")));
	}
	
	 public void ensureIsNotVisibleAddBtn() {
	        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("addBtn")));
	 }
	
	public void ensureErrorIsVisible() {
        (new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOfElementLocated(By.id("error")));
    }
	
	public void ensureIsDeletingNotDone() {
        (new WebDriverWait(driver, 1000)).until(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner-border")));
    }
	
	public WebElement getUsername() {
		return username;
	}

	public WebElement getLeaveComment() {
		return leaveComment;
	}

	public WebElement getCommentText() {
		return commentText;
	}

	public WebElement getSaveComment() {
		return saveComment;
	}

	public WebElement getLeaveCommentForm() {
		return leaveCommentForm;
	}

	public WebElement getPictureInput() {
		return pictureInput;
	}

	public WebElement getSubUnsub() {
		return subUnsub;
	}

	public WebElement getPaginator() {
		return paginator;
	}

	public WebElement getSearchField() {
		return searchField;
	}

	public WebElement getSearchBtn() {
		return searchBtn;
	}

	public WebElement getResetBtn() {
		return resetBtn;
	}

	public WebElement getDeleteOffer() {
		return deleteOffer;
	}
	
	public WebElement getAddCategoryBtn() {
		return addCategoryBtn;
	}
	
	public WebElement getAddOfferBtn() {
		return addOfferBtn;
	}

	public WebElement getCategories() {
		return categories;
	}

	public WebElement getAddNewsBtn() {
		return addNewsBtn;
	}

	public WebElement getNewsTitleArea() {
		return newsTitleArea;
	}

	public WebElement getNewsTextArea() {
		return newsTextArea;
	}

	public WebElement getAddBtn() {
		return addBtn;
	}


	public WebElement getOpenUpdateBtn() {
		return openUpdateBtn;
	}
	
	public WebElement getCloseUpdateBtn() {
		return closeUpdateBtn;
	}

	public WebElement getUpdateName() {
		return updateName;
	}

	public WebElement getAlertUpdateName() {
		return alertUpdateName;
	}

	public WebElement getUpdateDescriptionText() {
		return updateDescriptionText;
	}

	public WebElement getAlertUpdateDescription() {
		return alertUpdateDescription;
	}

	public WebElement getCancelUpdateBtn() {
		return cancelUpdateBtn;
	}

	public WebElement getUpdateBtn() {
		return updateBtn;
	}
	
	public WebElement getUpdateForm() {
		return updateForm;
	}
	
	public void ensureIsDisplayedOpenUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("openUpdateBtn")));
    }
	
	public void ensureIsDisplayedCloseUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("closeUpdateBtn")));
    }
	
	public void ensureIsDisplayedUpdateName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateName")));
    }
	
	public void ensureIsDisplayedAlertUpdateName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("alertUpdateName")));
    }
	
	public void ensureIsDisplayedUpdateDescription() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateDescriptionText")));
    }
	
	public void ensureIsDisplayedAlertUpdateDescription() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("alertUpdateDescription")));
    }
	
	public void ensureIsDisplayedCancelUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("cancelUpdateBtn")));
    }
	
	public void ensureIsDisplayedUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("updateBtn")));
    }
	
	public void ensureIsNotDisplayedUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("updateBtn")));
    }
	
	public void ensureIsDisplayedUpdateForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateForm")));
    }
	
	public void ensureAlertIsDisplayed() {
    	(new WebDriverWait(driver, 50)).until(ExpectedConditions.alertIsPresent());
    }
	
	public void ensureIsNotDisplayedUpdateForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("updateForm")));
    }
	
	public void ensureIsDisplayedOfferDetailsLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("offerDetailsLink")));
    }

	public WebElement getCloseBtn() {
		return closeBtn;
	}

}
