package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MapDisplayPage {
	
	private WebDriver driver;
	
	//Elementi u Cultural-offer-filter
	@FindBy(id = "cityFilterText")
	private WebElement cityFilterText;
	
	@FindBy(id = "cityFilterBtn")
	private WebElement cityFilterBtn;
	
	@FindBy(id = "removeCityBtn")
	private WebElement removeCityBtn;
	
	@FindBy(id = "catFilterText")
	private WebElement catFilterText;
	
	@FindBy(id = "catFilterBtn")
	private WebElement catFilterBtn;
	
	@FindBy(id = "removeCategoryBtn")
	private WebElement removeCategoryBtn;
	
	@FindBy(id ="clearAllBtn")
	private WebElement clearAllBtn;
	
	@FindBy(id = "paginator")
	private WebElement paginator;

	//elementi u Map
	@FindBy(id = "map")
	private WebElement map;
	
	public WebElement getCityFilterText() {
		return cityFilterText;
	}

	public WebElement getCityFilterBtn() {
		return cityFilterBtn;
	}

	public WebElement getRemoveCityBtn() {
		return removeCityBtn;
	}

	public WebElement getCatFilterText() {
		return catFilterText;
	}

	public WebElement getCatFilterBtn() {
		return catFilterBtn;
	}

	public WebElement getRemoveCategoryBtn() {
		return removeCategoryBtn;
	}

	public WebElement getClearAllBtn() {
		return clearAllBtn;
	}

	public WebElement getPaginator() {
		return paginator;
	}

	public WebElement getMap() {
		return map;
	}
	
	public MapDisplayPage() {}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public MapDisplayPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void ensureIsDisplayedCityFilterText() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("cityFilterText")));
	}
	
	public void ensureIsDisplayedCatFilterText() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("catFilterText")));
	}
	
	public void ensureIsDisplayedCityFilterButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("cityFilterBtn")));
	}
	
	public void ensureIsDisplayedCatFilterButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("catFilterBtn")));
	}
	
	public void ensureIsDisplayedRemoveCityButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("removeCityBtn")));
	}
	
	public void ensureIsDisplayedRemoveCatButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("removeCategoryBtn")));
	}
	
	public void ensureIsDisplayedClearAllButton() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("clearAllBtn")));
	}
	
	public void ensureIsDisplayedPaginator() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("paginator")));
	}
	
	public void ensureIsDisplayedMap() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("map")));
	}
	
	public void ensureIsNotDisplayedRemoveCityButton() {
		(new WebDriverWait(driver, 5)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("removeCityBtn")));
	}
	
	public void ensureIsNotDisplayedRemoveCatButton() {
		(new WebDriverWait(driver, 5)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("removeCategoryBtn")));
	}
	
	public void ensureIsNotDisplayedClearAllButton() {
		(new WebDriverWait(driver, 5)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("clearAllBtn")));
	}
	
	public void ensureIsNotDisplayedClearAll() {
		(new WebDriverWait(driver, 5)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("clearAllDiv")));
	}
	
	
	
	
	
	
	

}
