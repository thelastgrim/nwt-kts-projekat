package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
	private WebDriver driver;
	
	@FindBy(id = "email")
	private WebElement email;
	
	@FindBy(id = "password")
	private WebElement password;
	
	@FindBy(id = "loginBtn")
	private WebElement loginBtn;
	
	@FindBy(id = "registerLink")
	private WebElement registerLink;
	
	@FindBy(id = "dropdown")
	private WebElement dropdownMenu;
	
	@FindBy(id = "logoutBtn")
	private WebElement logoutBtn;
	
	public LoginPage() {
    }

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    
    public void ensureIsDisplayedDropdown() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("dropdown")));
    }

    public void ensureIsDisplayedEmail() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("email")));
    }
    public void ensureIsNotVisibleLoginBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("loginBtn")));
    }
    
    public void ensureIsDisplayedRegisterLink() {
    	(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("registerLink")));
    }

    public void ensureIsNotVisibleEmail() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("email")));
    }
    
    public void ensureAlertIsDisplayed() {
    	(new WebDriverWait(driver, 10)).until(ExpectedConditions.alertIsPresent());
    }

    public WebElement getEmail() {
        return email;
    }
    
    public WebElement getRegisterLink() {
    	return registerLink;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginBtn() {
        return loginBtn;
    }

	public WebElement getDropdownMenu() {
		return dropdownMenu;
	}

	public WebElement getLogoutBtn() {
		return logoutBtn;
	}


}
