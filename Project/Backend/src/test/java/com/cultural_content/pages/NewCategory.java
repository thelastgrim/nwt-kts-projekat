package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewCategory {
	
	private WebDriver driver;
	
	@FindBy(id = "newName")
	private WebElement newCategoryName;
	
	@FindBy(id = "cancel-btn")
	private WebElement cancelBtn;
	
	@FindBy(id = "save-btn")
	private WebElement saveBtn;
	
	@FindBy(name = "exist-name-invalid")
	private WebElement existNameInvalid;
	
	@FindBy(id = "required-name-invalid")
	private WebElement requiredNameInvalid;
	
	@FindBy(id = "minlenght-name-invalid")
	private WebElement minlengthNameInvalid;
	
	
	public NewCategory() {
		
	}

	public WebDriver getDriver() {
		return driver;
	}

	public NewCategory(WebDriver driver) {
		this.driver = driver;
	}
	
	public void ensureIsDisplayedNewCategoryForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("newName")));
    }
	
	public void ensureIsDisplayedCancelBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("cancel-btn")));
    }
	
	public void ensureIsDisplayedSaveBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.name("save-btn")));
    }
	
//	public void ensureIsNotDisplayedSaveBtn() {
//        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.name("save-btn")));
//    }
	
	public void ensureIsDisplayedAlertExistName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("exist-name-invalid")));
    }
	
	public void ensureIsDisplayedAlertRequierdName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("required-name-invalid")));
    }
	
	public void ensureIsDisplayedAlertMinlengthName() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id("minlenght-name-invalid")));
	}
	
	public WebElement getNewCategoryName() {
		return newCategoryName;
	}
	
	public void setNewCategoryNameInput(String value) {
		WebElement el = getNewCategoryName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getCancelBtnt() {
		return cancelBtn;
	}

	public WebElement getSaveBtn() {
		return saveBtn;
	}

}
