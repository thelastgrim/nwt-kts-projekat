package com.cultural_content.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeProfilePage {
	private WebDriver driver;

	@FindBy(id = "username")
	private WebElement username;
	
	@FindBy(id = "oldPassword")
	private WebElement oldPassword;
	
	@FindBy(id = "newPassword")
	private WebElement newPassword;
	
	@FindBy(id = "rePassword")
	private WebElement rePassword;
	
	@FindBy(id = "dropdown")
	private WebElement dropdownMenu;
	
	@FindBy(id = "profileBtn")
	private WebElement profileBtn;
	
	@FindBy(id = "changePass")
	private WebElement changePass;
	
	@FindBy(className = "form-check-input")
	private List<WebElement> show;
	
	public ChangeProfilePage() {
		
	}
	
	public ChangeProfilePage(WebDriver driver) {
		this.driver = driver;
	}
	
	 public void ensureIsDisplayedDropdown() {
	        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("dropdown")));
	    }
	 
	 public void ensureAlertIsDisplayed() {
	    	(new WebDriverWait(driver, 50)).until(ExpectedConditions.alertIsPresent());
	    }

	 public void ensureErrorIsVisible() {
	        (new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOfElementLocated(By.id("oldPassError")));
	    }
	 
	public WebDriver getDriver() {
		return driver;
	}

	public WebElement getUsername() {
		return username;
	}

	public WebElement getOldPassword() {
		return oldPassword;
	}

	public WebElement getNewPassword() {
		return newPassword;
	}

	public WebElement getRePassword() {
		return rePassword;
	}

	public WebElement getDropdownMenu() {
		return dropdownMenu;
	}

	public WebElement getProfileBtn() {
		return profileBtn;
	}

	public WebElement getChangePass() {
		return changePass;
	}

	public List<WebElement> getShow() {
		return show;
	}
	
	

}
