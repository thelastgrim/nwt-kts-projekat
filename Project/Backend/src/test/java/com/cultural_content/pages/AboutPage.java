package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AboutPage {
	
	private WebDriver driver;
	
	//aboutPage
	@FindBy(id="aboutLink")
	WebElement aboutLink;
	
	@FindBy(id="NewsNavLink")
	WebElement NewsNavLink;
	
	@FindBy(id="commentsLink")
	WebElement commentsLink;
	
	@FindBy(id="subscriptionLink")
	WebElement subscriptionLink;
	
	@FindBy(id="openUpdateBtn")
	WebElement openUpdateBtn;
	
	@FindBy(id="deleteBtn")
	WebElement deleteBtn;
	
	@FindBy(id = "deleteOfferIcon")
	private WebElement deleteOffer;
	
	@FindBy(id="offerName")
	WebElement offerName;
	
	@FindBy(id="offerCover")
	WebElement offerCover;
	
	@FindBy(id="offerAddress")
	WebElement offerAddress;
	
	@FindBy(id="rating")
	WebElement rating;
	
	@FindBy(id="offerAbout")
	WebElement offerAbout;
	
	@FindBy(id="seeNews")
	WebElement seeNews;
	
	@FindBy(id="seeComents")
	WebElement seeComents;
	
	@FindBy(id="subscribeText")
	WebElement subscribeText;
	
	@FindBy(id="subscribe")
	WebElement subscribe;
	
	//comment elements

	@FindBy(id="titleComments")
	WebElement titleComments;
	
	@FindBy(id="commentRating")
	WebElement commentRating;
	
	@FindBy(id="commentText")
	WebElement commentTextInList;
	
	@FindBy(id="user")
	WebElement user;
	
	@FindBy(id="paginator")
	WebElement paginatorComment;
	
	@FindBy(id="leaveComment")
	WebElement leaveComment;
	
	//news elements
	@FindBy(id="titleNews")
	WebElement titleNews;
	
	@FindBy(id="addNews")
	WebElement addNews;
	
	@FindBy(id="newsTitle")
	WebElement newsTitle;
	
	@FindBy(id="dateAdded")
	WebElement dateAdded;
	
	@FindBy(id="paginatorNews")
	WebElement paginatorNews;
	
	@FindBy(id="newsText")
	WebElement newsText; 
	
	//comment form
	
	@FindBy(id = "commentBtn")
	private WebElement leaveNewComment;
	
	///elementi u CommentFormi
	@FindBy(id = "commentTextArea")
	private WebElement commentText;
	
	@FindBy(id = "saveCommentBtn")
	private WebElement saveComment;
	
	@FindBy(id = "leaveCommentForm")
	private WebElement leaveCommentForm;

	@FindBy(id = "file")
	private WebElement pictureInput;
	
	@FindBy(id = "paginator")
	private WebElement paginator;
		
		
	
	//news add
	@FindBy(id = "addNewsBtn")
	private WebElement addNewsBtn;
	
	//elementi u addNewsFormi
	@FindBy(id = "newsTitleArea")
	private WebElement newsTitleArea;
	
	@FindBy(id = "newsTextArea")
	private WebElement newsTextArea;
	
	@FindBy(id = "addBtn")
	private WebElement addBtn;
	
	@FindBy(id = "closeBtn")
	private WebElement closeBtn;

	//update
	@FindBy(id = "closeUpdateBtn")
	private WebElement closeUpdateBtn;
	
	@FindBy(id = "updateName")
	private WebElement updateName;
	
	@FindBy(id = "alertUpdateName")
	private WebElement alertUpdateName;
	
	@FindBy(id = "updateDescriptionText")
	private WebElement updateDescriptionText;
	
	@FindBy(id = "alertUpdateDescription")
	private WebElement alertUpdateDescription;
	
	@FindBy(id = "cancelUpdateBtn")
	private WebElement cancelUpdateBtn;
	
	@FindBy(id = "updateBtn")
	private WebElement updateBtn;
	
	@FindBy(id = "updateForm")
	private WebElement updateForm;
	
	@FindBy(id = "subUnsubBtn")
	private WebElement subUnsub;

	
	public WebElement getSubUnsub() {
		return subUnsub;
	}

	public AboutPage() {
		
	}
	
	public AboutPage(WebDriver driver) {
		this.driver = driver;
	}
	

	public WebElement getOfferName() {
		return offerName;
	}

	public WebElement getLeaveNewComment() {
		return leaveNewComment;
	}

	public WebElement getAddNewsBtn() {
		return addNewsBtn;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebElement getCloseUpdateBtn() {
		return closeUpdateBtn;
	}

	public WebElement getUpdateName() {
		return updateName;
	}

	public WebElement getAlertUpdateName() {
		return alertUpdateName;
	}

	public WebElement getUpdateDescriptionText() {
		return updateDescriptionText;
	}

	public WebElement getAlertUpdateDescription() {
		return alertUpdateDescription;
	}

	public WebElement getCancelUpdateBtn() {
		return cancelUpdateBtn;
	}

	public WebElement getUpdateBtn() {
		return updateBtn;
	}

	public WebElement getUpdateForm() {
		return updateForm;
	}

	public WebElement getAboutLink() {
		return aboutLink;
	}

	public WebElement getNewsNavLink() {
		return NewsNavLink;
	}

	public WebElement getCommentsLink() {
		return commentsLink;
	}

	public WebElement getSubscriptionLink() {
		return subscriptionLink;
	}

	public WebElement getOpenUpdateBtn() {
		return openUpdateBtn;
	}

	public WebElement getDeleteBtn() {
		return deleteBtn;
	}

	public WebElement getOfferCover() {
		return offerCover;
	}

	public WebElement getOfferAddress() {
		return offerAddress;
	}

	public WebElement getRating() {
		return rating;
	}

	public WebElement getOfferAbout() {
		return offerAbout;
	}

	public WebElement getSeeNews() {
		return seeNews;
	}

	public WebElement getSeeComents() {
		return seeComents;
	}

	public WebElement getSubscribeText() {
		return subscribeText;
	}

	public WebElement getSubscribe() {
		return subscribe;
	}

	public WebElement getTitleComments() {
		return titleComments;
	}

	public WebElement getCommentRating() {
		return commentRating;
	}

	public WebElement getCommentTextInList() {
		return commentTextInList;
	}

	public WebElement getUser() {
		return user;
	}

	public WebElement getPaginatorComment() {
		return paginatorComment;
	}

	public WebElement getLeaveComment() {
		return leaveComment;
	}

	public WebElement getTitleNews() {
		return titleNews;
	}

	public WebElement getAddNews() {
		return addNews;
	}

	public WebElement getNewsTitle() {
		return newsTitle;
	}

	public WebElement getDateAdded() {
		return dateAdded;
	}

	public WebElement getPaginatorNews() {
		return paginatorNews;
	}

	public WebElement getNewsText() {
		return newsText;
	}
	
	
	public WebElement getDeleteOffer() {
		return deleteOffer;
	}

	public void ensureIsDisplayedOpenUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("openUpdateBtn")));
    }
	
	public void ensureIsDisplayedCloseUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("closeUpdateBtn")));
    }
	
	public void ensureIsDisplayedUpdateName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateName")));
    }
	
	public void ensureIsDisplayedAlertUpdateName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("alertUpdateName")));
    }
	
	public void ensureIsDisplayedUpdateDescription() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateDescriptionText")));
    }
	
	public void ensureIsDisplayedAlertUpdateDescription() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("alertUpdateDescription")));
    }
	
	public void ensureIsDisplayedCancelUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("cancelUpdateBtn")));
    }
	
	public void ensureIsDisplayedUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("updateBtn")));
    }
	
	public void ensureIsNotDisplayedUpdateBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("updateBtn")));
    }
	
	public void ensureIsDisplayedUpdateForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("updateForm")));
    }
	
	public void ensureIsNotDisplayedUpdateForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("updateForm")));
    }
	
	public void ensureIsNotDisplayedAddNewsBtn() {
		(new WebDriverWait(driver, 50)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("addNewsBtn")));
	}
	
	public void ensureIsDisplayedAddNewsBtn() {
		(new WebDriverWait(driver, 50)).until(ExpectedConditions.elementToBeClickable(By.id("addNewsBtn")));
	}
	
	public void ensureIsDisplayedAboutLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("aboutLink")));
    }
	
	public void ensureIsDisplayedNewsNavLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("NewsNavLink")));
    }
	
	public void ensureIsDisplayedCommentsLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("commentsLink")));
    }
	
	public void ensureIsDisplayedSubscriptionLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("subscriptionLink")));
    }
	
	public void ensureIsNotDisplayedSubscriptionLink() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("subscriptionLink")));
    }
	
	public void ensureIsDisplayedDeleteOfferIcon() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteOfferIcon")));
    }
	
	public void ensureIsDisplayedOfferName() {
        (new WebDriverWait(driver, 5000)).until(ExpectedConditions.visibilityOfElementLocated(By.id("offerName")));
    }
	
	public void ensureIsDisplayedRating() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("rating")));
    }
	
	public void ensureIsDisplayedOfferAbout() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("offerAbout")));
    }
	
	public void ensureIsDisplayedOfferAddress() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("offerAddress")));
    }
	
	public void ensureIsNotDisplayedOfferName() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("offerName")));
    }
	
	public void ensureIsNotDisplayedRating() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("rating")));
    }
	
	public void ensureIsNotDisplayedOfferAbout() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("offerAbout")));
    }
	
	public void ensureIsNotDisplayedOfferAddress() {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("offerAddress")));
    }
	
	public void ensureIsDisplayedNewsText() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("newsText")));
	}
	
	public void ensureIsDisplayedDateAdded() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("dateAdded")));
	}
	
	public void ensureIsDisplayedNewsTitle() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("newsTitle")));
	}
	
	public void ensureIsDisplayedTitleNews() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("titleNews")));
	}
	

	public void ensureIsDisplayedCommentRating() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("commentRating")));
	}
	
	public void ensureIsDisplayedUser() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("user")));
	}
	
	public void ensureIsDisplayedCommentTextInList() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("commentText")));
	}
	
	public void ensureIsNotDisplayedLeaveComment() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("commentBtn")));
	}
	
	public void ensureIsDisplayedLeaveComment() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("commentBtn")));
	}
	
	public void ensureIsDisplayedTitleComments() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("titleComments")));
	}
	
	public void ensureIsDisplayedSubscribeText() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.id("subscribeText")));
	}

	public WebElement getCommentText() {
		return commentText;
	}

	public WebElement getSaveComment() {
		return saveComment;
	}

	public WebElement getLeaveCommentForm() {
		return leaveCommentForm;
	}

	public WebElement getPictureInput() {
		return pictureInput;
	}

	public WebElement getPaginator() {
		return paginator;
	}

	public WebElement getNewsTitleArea() {
		return newsTitleArea;
	}

	public WebElement getNewsTextArea() {
		return newsTextArea;
	}

	public WebElement getAddBtn() {
		return addBtn;
	}

	public WebElement getCloseBtn() {
		return closeBtn;
	}
	
	
	
	
	
	
	
	

}
