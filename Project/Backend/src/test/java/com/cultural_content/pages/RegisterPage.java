package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {
	private WebDriver driver;

	@FindBy(id = "username")
	private WebElement username;
	
	@FindBy(id = "email")
	private WebElement email;
	
	@FindBy(id = "password")
	private WebElement password;
	
	@FindBy(id = "rePassword")
	private WebElement rePassword;
	
	@FindBy(id = "registerBtn")
	private WebElement registerBtn;
	
	@FindBy(id = "error")
	private WebElement error;
	
	public RegisterPage() {
		
	}
	
	public RegisterPage(WebDriver driver) {
		this.driver = driver;
	}
	
	 public void ensureIsDisplayedUsername() {
	        (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("username")));
	 }
	 
	 public void ensureIsNotVisibleRegisterBtn() {
	        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.id("registerBtn")));
	 }
	 
	 public void ensureAlertIsDisplayed() {
	    	(new WebDriverWait(driver, 50)).until(ExpectedConditions.alertIsPresent());
	    }
	 
	 public void ensureErrorIsVisible() {
	        (new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOfElementLocated(By.id("error")));
	    }

	public WebDriver getDriver() {
		return driver;
	}

	public WebElement getUsername() {
		return username;
	}

	public WebElement getEmail() {
		return email;
	}

	public WebElement getPassword() {
		return password;
	}

	public WebElement getRePassword() {
		return rePassword;
	}

	public WebElement getRegisterBtn() {
		return registerBtn;
	}

	public WebElement getError() {
		return error;
	}
	 
	 
	 
	 
	 
	 

}
