package com.cultural_content.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewOffer {
	
	private WebDriver driver;
	
	@FindBy(id = "name")
	private WebElement name;
	
	@FindBy(id = "category")
	private WebElement category;
	
	@FindBy(id = "country")
	private WebElement country;
	
	@FindBy(id = "city")
	private WebElement city;
	
	@FindBy(id = "street")
	private WebElement street;
	
	@FindBy(id = "pic")
	private WebElement pic;
	
	@FindBy(id = "form103")
	private WebElement description;
	
	@FindBy(id = "cancel-btn")
	private WebElement cancelBtn;
	
	@FindBy(id = "save-btn")
	private WebElement saveBtn;
	
	
	@FindBy(name = "alert alert-info")
	private WebElement pictureAlert;
	
	@FindBy(className = "alert alert-danger")
	private WebElement alert;
	
	@FindBy(id = "alert-name")
	private WebElement alertName;
	
	@FindBy(id = "alert-country")
	private WebElement alertCountry;
	
	@FindBy(id = "alert-city")
	private WebElement alertCity;
	
	@FindBy(id = "alert-street")
	private WebElement alertStreet;
	
	@FindBy(id = "alert-description")
	private WebElement alertDescription;
	
	@FindBy(id = "alert-pic")
	private WebElement alertPic;
	
	public NewOffer() {
		
	}

	public WebDriver getDriver() {
		return driver;
	}

	public NewOffer(WebDriver driver) {
		this.driver = driver;
	}
	
	public void ensureIsDisplayedNewOfferForm() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("modal-basic-title")));
    }
	
	public void ensureIsDisplayedCancelBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("cancel-btn")));
    }
	
	public void ensureIsDisplayedSaveBtn() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("save-btn")));
    }
	
	
	public void ensureIsDisplayedAlert() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("alert alert-danger")));
    }
	
	public void ensureIsDisplayedAlertName() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-name")));
    }
	
	public void ensureIsDisplayedAlertCountry() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-country")));
    }
	
	public void ensureIsDisplayedAlertCity() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-city")));
    }
	
	public void ensureIsDisplayedAlertStreet() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-street")));
    }
	
	public void ensureIsDisplayedAlertDescription() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-description")));
    }
	
	public void ensureIsDisplayedAlertPic() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("alert-pic")));
    }
	
	public WebElement getNewOfferName() {
		return name;
	}
	
	public void setNewName(String value) {
		WebElement el = getNewOfferName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getAlert() {
		return alert;
	}


	public WebElement getCancelBtnt() {
		return cancelBtn;
	}

	public WebElement getSaveBtn() {
		return saveBtn;
	}

	public WebElement getName() {
		return name;
	}

	public WebElement getCategory() {
		return category;
	}

	public WebElement getCountry() {
		return country;
	}

	public WebElement getCity() {
		return city;
	}

	public WebElement getStreet() {
		return street;
	}

	public WebElement getPic() {
		return pic;
	}

	public WebElement getDescription() {
		return description;
	}

	public WebElement getCancelBtn() {
		return cancelBtn;
	}

	public WebElement getPictureAlert() {
		return pictureAlert;
	}

	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	
	
	public void setCategory(WebElement category) {
		//
	}

	public void setCountry(WebElement country) {
		this.country = country;
	}

	public void setCity(WebElement city) {
		this.city = city;
	}

	public void setStreet(WebElement street) {
		this.street = street;
	}

	public void setPic(WebElement pic) {
		this.pic = pic;
	}

	public void setDescription(WebElement description) {
		this.description = description;
	}

	public void setCancelBtn(WebElement cancelBtn) {
		this.cancelBtn = cancelBtn;
	}

	public void setSaveBtn(WebElement saveBtn) {
		this.saveBtn = saveBtn;
	}

	public void setPictureAlert(WebElement pictureAlert) {
		this.pictureAlert = pictureAlert;
	}

	
	


}
