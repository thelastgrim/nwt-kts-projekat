package com.cultural_content;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.test.context.TestPropertySource;

import com.cultural_content.controller.CategoryControllerIntegrationTest;
import com.cultural_content.controller.CommentControllerIntegrationTest;
import com.cultural_content.controller.CulturalOfferControllerIntegrationTest;
import com.cultural_content.controller.NewsControllerIntegrationTest;
import com.cultural_content.controller.PictureControllerIntegrationTest;
import com.cultural_content.controller.UserControllerIntegrationTest;
import com.cultural_content.repository.AuthorityRepositoryIntegrationTest;
import com.cultural_content.repository.CategoryRepositoryIntegrationTest;
import com.cultural_content.repository.CommentRepositoryIntegrationTest;
import com.cultural_content.repository.CulturalOfferRepositoryIntegrationTest;
import com.cultural_content.repository.LocationRepositoryIntegrationTest;
import com.cultural_content.repository.NewsRepositoryIntegrationTest;
import com.cultural_content.repository.PersonRepositoryIntegrationTest;
import com.cultural_content.repository.PictureRepositoryIntegrationTest;
import com.cultural_content.repository.UserRepositoryIntegrationTest;
import com.cultural_content.service.CategoryServiceIntegrationTest;
import com.cultural_content.service.CommentUnitTest;
import com.cultural_content.service.CommentsServiceIntegrationTest;
import com.cultural_content.service.CulturalOfferServiceIntegrationTest;
import com.cultural_content.service.FileUploaderIntegrationTest;
import com.cultural_content.service.LocationServiceIntegrationTest;
import com.cultural_content.service.NewsServiceIntegrationTest;
import com.cultural_content.service.PictureServiceIntegrationTest;
import com.cultural_content.service.RatingInfoServiceIntegrationTest;
import com.cultural_content.service.SchedulerIntegrationTest;
import com.cultural_content.service.UserServiceIntegrationTest;
import com.cultural_content.service.UserServiceUnitTest;




@RunWith(Suite.class)
@SuiteClasses({PictureControllerIntegrationTest.class, 		 PictureServiceIntegrationTest.class,
	           PictureRepositoryIntegrationTest.class,
			   SchedulerIntegrationTest.class, 		    	 CommentControllerIntegrationTest.class,
			   CategoryControllerIntegrationTest.class,		
			   NewsControllerIntegrationTest.class,
			   UserControllerIntegrationTest.class,
			   AuthorityRepositoryIntegrationTest.class, 	 CategoryRepositoryIntegrationTest.class,
			   CommentRepositoryIntegrationTest.class, 		 CulturalOfferRepositoryIntegrationTest.class,
			   LocationRepositoryIntegrationTest.class, 	 NewsRepositoryIntegrationTest.class,
			   PersonRepositoryIntegrationTest.class, 		
			   UserRepositoryIntegrationTest.class,			 CategoryServiceIntegrationTest.class,
			   CommentsServiceIntegrationTest.class,		 FileUploaderIntegrationTest.class,
			   CommentUnitTest.class,						 LocationServiceIntegrationTest.class,
			   NewsServiceIntegrationTest.class, 			 
			   RatingInfoServiceIntegrationTest.class,  	 
			   UserServiceIntegrationTest.class,			 UserServiceUnitTest.class, CulturalOfferControllerIntegrationTest.class, CulturalOfferServiceIntegrationTest.class})
@TestPropertySource("classpath:test.properties")
public class SuiteAll {

}
