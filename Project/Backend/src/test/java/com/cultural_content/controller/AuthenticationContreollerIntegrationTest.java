package com.cultural_content.controller;
import static com.cultural_content.constants.UserConstants.NEW_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.NEW_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.NEW_USER_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.dto.DTOUser;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.model.User;
import com.cultural_content.security.EncryptDecrypt;
import com.cultural_content.service.UserService;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class AuthenticationContreollerIntegrationTest {
	
	 @Autowired
	 UserService userService;

	 @Autowired
	 private TestRestTemplate restTemplate;
	 private String accessToken;
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOUserTokenState> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOUserTokenState.class);
	        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
	        
	      
	    }

	 @Test
	 @Rollback(true)
	 public void testAddUser() {
		 DTOUser newUser = new DTOUser();
		 newUser.setPassword(NEW_USER_PASSWORD);
		 newUser.setEmail(NEW_USER_EMAIL);
		 newUser.setUsername(NEW_USER_USERNAME);
		
		 
		 HttpHeaders headers = new HttpHeaders();

		 HttpEntity<Object> httpEntity = new HttpEntity<Object>(newUser,headers);

		 ResponseEntity<DTOUser> responseEntity = 
		 restTemplate.exchange("/auth/sign-up", HttpMethod.POST, httpEntity, DTOUser.class);

		 assertEquals(HttpStatus.CREATED,  responseEntity.getStatusCode());
 
	 }
	 

	 @Test
	 @Rollback(true)
	 public void testAddUserBadRequest() {
		 DTOUser newUser = new DTOUser();
		 newUser.setPassword(NEW_USER_PASSWORD);
		 newUser.setEmail("mail");
		 newUser.setUsername(NEW_USER_USERNAME);
		 
		 
		 HttpHeaders headers = new HttpHeaders();

		 HttpEntity<Object> httpEntity = new HttpEntity<Object>(newUser,headers);

		 ResponseEntity<DTOUser> responseEntity = 
		 restTemplate.exchange("/auth/sign-up", HttpMethod.POST, httpEntity, DTOUser.class);

		 assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
 
	 }
	 
	 @Test
	 public void testconfirmUserAccount() throws Exception {
		 User u = new User();
		 u.setUsername("username");
		 u.setEmail("some.address@gmail.com");
		 u.setPassword("password");
		 u.setActivated(false);
		 //u.setId(NEW_USER_ID);
		 userService.saveUser(u);
		 System.err.println("  . . . . "+ u.getId());
         String toEncrypt = u.getEmail()+u.getId();
         System.err.println("To encrypt: " + toEncrypt);
         EncryptDecrypt ed;
 	     String encrypted = null;
 	     ed = new EncryptDecrypt();
 	     encrypted = ed.encrypt(toEncrypt).replace("/", "culturalContent");	 
 	     HttpHeaders headers = new HttpHeaders();
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<String> responseEntity = 
				 restTemplate.exchange("/auth/confirm-account/"+encrypted, HttpMethod.GET, httpEntity, String.class);
		
	//	 System.err.println("    AUTH TEST: "+ responseEntity);
		 assertEquals("<html><body style=\"font-size: xx-large;\">\r\n" + 
					"<div style=\"margin-left: 12%; margin-top: 8%; color:#71da71;\">\r\n" + 
					"	\r\n" + 
					"	Account successfuly activated. You can now log in!\r\n" + 
					"	\r\n" + 
					"</div>\r\n" + 
					"<div style=\"margin-left: 50%; margin-top: 6%;\">\r\n" + 
					
					"</div>\r\n" + 
					"</body>\r\n" + 
					"</html>",  responseEntity.getBody());
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 userService.deleteUser(u);
 
	 }
	 
	 @Test
	 public void testconfirmUserAccountAlreadyActivated() throws Exception {
		 User u = new User();
		 u.setUsername("username");
		 u.setEmail("some.address@gmail.com");
		 u.setPassword("password");
		 u.setActivated(true);
		 //u.setId(NEW_USER_ID);
		 userService.saveUser(u);
		 System.err.println("  . . . . "+ u.getId());
         String toEncrypt = u.getEmail()+u.getId();
         System.err.println("To encrypt: " + toEncrypt);
         EncryptDecrypt ed;
 	     String encrypted = null;
 	     ed = new EncryptDecrypt();
 	     encrypted = ed.encrypt(toEncrypt).replace("/", "culturalContent");	 
 	     HttpHeaders headers = new HttpHeaders();
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<String> responseEntity = 
				 restTemplate.exchange("/auth/confirm-account/"+encrypted, HttpMethod.GET, httpEntity, String.class);
		
	//	 System.err.println("    AUTH TEST: "+ responseEntity);
		 assertEquals("<html><body style=\"font-size: xx-large;\">\r\n" + 
					"<div style=\"margin-left: 20%; margin-top: 7%; color: #ff9999;\">	\r\n" + 
					"	Account already activated!\r\n" + 
					"</div>\r\n" + 
					"<div style=\"margin-left: 50%;\">\r\n" + 
					
					"</div>\r\n" + 
					"</body>\r\n" + 
					"</html>",  responseEntity.getBody());
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 userService.deleteUser(u);
 
	 }
	 
		
	 @Test
	 public void testconfirmUserAccountNegative() throws Exception {
		 User u = new User();
		 u.setUsername("username");
		 u.setEmail("some.address@gmail.com");
		 u.setPassword("password");
		 u.setActivated(false);
		 
	//	 userService.saveUser(u); ne cuvamo korisnika da bismo izazvali negativni scenario,
	//	 odnosno pokusavamo da aktiviramo nalog nepostojeceg korisnika
		 System.err.println("  . . . . "+ u.getId());
         String toEncrypt = u.getEmail()+u.getId();
         System.err.println("To encrypt: " + toEncrypt);
         EncryptDecrypt ed;
 	     String encrypted = null;
 	     ed = new EncryptDecrypt();
 	     encrypted = ed.encrypt(toEncrypt).replace("/", "culturalContent");	 
 	     HttpHeaders headers = new HttpHeaders();
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<String> responseEntity = 
				 restTemplate.exchange("/auth/confirm-account/"+encrypted, HttpMethod.GET, httpEntity, String.class);
		
		 System.err.println("    AUTH TEST: "+ responseEntity);
		 assertEquals("There's been problem with account activation.",  responseEntity.getBody());
	//	 userService.deleteUser(u);
 
	 }
	
}
