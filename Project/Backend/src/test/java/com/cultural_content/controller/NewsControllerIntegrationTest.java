package com.cultural_content.controller;

import static com.cultural_content.constants.NewsConstants.*;

import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cultural_content.dto.DTOCategory;
import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOCulturalOffer;
import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTONews;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.model.Category;
import com.cultural_content.model.Comment;
import com.cultural_content.model.News;
import com.cultural_content.service.CategoryService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.NewsService;
import com.cultural_content.service.UserService;
import com.support.RestResponsePage;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class NewsControllerIntegrationTest {
	
	 @Autowired
	 private TestRestTemplate restTemplate;
	 
	 @Autowired
	 private NewsService newsService;
	 
	 @Autowired
	 private CulturalOfferService culturalOfferService;
	 
	 @Autowired
	 private UserService userService;
	 
	 private String accessToken;
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOLoginResponse.class);
	      
	        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
	        
	    
	    }
	   
	 @Test
	 public void testCreateNews_Success() throws Exception {

		 int size = newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID).size(); // broj slogova pre ubacivanja novog
		 assertEquals(size, DB_NEWS_IN_CULTURAL_OFFER);
		 login("admin@gmail.com", "admin");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    DTONews newNews = new DTONews(null, null,DB_NEW_NEWS_TEXT, DB_NEW_NEWS_TITLE, DB_CULTURAL_OFFER_ID);
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(newNews,headers);
		 
	    ResponseEntity<DTONews> responseEntity =
                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" , HttpMethod.POST, request, DTONews.class);
     
        //Provjera odgovora servisa
	   DTONews news = responseEntity.getBody();
       assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
       assertNotNull(news);
       assertEquals(DB_NEW_NEWS_TITLE, news.getTitle());

       assertEquals(DB_NEW_NEWS_TEXT, news.getText());

       assertEquals(DB_CULTURAL_OFFER_ID, news.getCulturalOfferId());
       
       List<News> listNews = newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID);
       assertEquals(DB_NEWS_IN_CULTURAL_OFFER + 1, listNews.size());
       assertEquals(DB_NEW_NEWS_TITLE, listNews.get(listNews.size()-1).getTitle());

       //deleting added news
       newsService.delete(news.getId());

		assertEquals(DB_NEWS_IN_CULTURAL_OFFER, newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID).size());
	 }
	 
	 
	 @Test
	 public void testGetAllNews_Success() throws Exception {
		 
		 int size = newsService.findAll(DB_CULTURAL_OFFER_ID, DB_CATEGORY_ID).size(); // broj slogova pre ubacivanja novog
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	     // kreiramo objekat koji saljemo u sklopu zahteva
	     // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(headers);
		 
	    ResponseEntity<DTONews[]> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news", HttpMethod.GET, request, DTONews[].class);
     
       //Provjera odgovora servisa
	    DTONews[] newsList = responseEntity.getBody();
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(newsList);
       
       List<News> categories = newsService.findAll(DB_CULTURAL_OFFER_ID, DB_CATEGORY_ID);
       //DB_CATEGORY_SIZE is number of all categories
       assertEquals(DB_NEWS_IN_CULTURAL_OFFER, categories.size());
	 }
	 
	 //FAIL - cultural offer with id does not exist
	 @Test
	 public void testGetAllNews_Fail() throws Exception {
		 
//		 int size = newsService.findAll(DB_CULTURAL_OFFER_DOES_NOT_EXIST).size(); // broj slogova pre ubacivanja novog
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	     // kreiramo objekat koji saljemo u sklopu zahteva
	     // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(headers);
		 
	    ResponseEntity<DTONews[]> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_DOES_NOT_EXIST + "/news", HttpMethod.GET, request, DTONews[].class);
     
       //Provjera odgovora servisa
	 
	    //DTONews[] newsList = responseEntity.getBody();
       assertEquals(4, culturalOfferService.findAllByCategory(DB_CATEGORY_ID).size());

     assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

	 }
	 
	 
	    @Test
	    public void testGetAllNewsPageable() {
	    	 login("test@gmail.com", "user");
			 HttpHeaders headers = new HttpHeaders();
			 headers.add("Authorization", accessToken);

		     HttpEntity<RestResponsePage<DTONews>> request = new HttpEntity<RestResponsePage<DTONews>>(headers);
		     ParameterizedTypeReference<RestResponsePage<DTONews>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTONews>>() {};
		        
			 
		     ResponseEntity<RestResponsePage<DTONews>> responseEntity =
	                restTemplate.exchange("/categories/" + DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/by-page?page=0&size=10",
	                	 HttpMethod.GET, request, typeRef);

		    Page<DTONews> pageNewsDTO = responseEntity.getBody();

	        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	        assertEquals(DB_NEWS_IN_CULTURAL_OFFER, pageNewsDTO.getNumberOfElements());
	    }

	 
	 
	 
	 
	 @Test
	 public void testGetOneNews_Success() throws Exception {
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	     // kreiramo objekat koji saljemo u sklopu zahteva
	     // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(headers);
		 
	    ResponseEntity<DTONews> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + DB_NEWS_ID, HttpMethod.GET, request, DTONews.class);
     
       //Provjera odgovora servisa
	    DTONews news = responseEntity.getBody();
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(news);
       assertEquals(DB_NEW_TITLE_ID_1, news.getTitle());
	 }
	 
	 //FAIL - undefined - wrong news id
	 @Test
	 public void testGetOneNews_Fail() throws Exception {
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	     // kreiramo objekat koji saljemo u sklopu zahteva
	     // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(headers);
		 
	    ResponseEntity<DTONews> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + DB_NEW_DOES_NOT_EXIST_ID, HttpMethod.GET, request, DTONews.class);
     
       //Provjera odgovora servisa
	    DTONews news = responseEntity.getBody();
       assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	 }
	 
	 
	 	 
	 @Test
	 public void testUpdateNews_Success() throws Exception {
		 
		 int size = newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID).size(); // broj slogova pre ubacivanja novog
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);


	    DTONews newNews = new DTONews(null, null, DB_NEW_TEXT_ID_1, DB_NEW_NEWS_TITLE, DB_CULTURAL_OFFER_ID);
		HttpEntity<DTONews> request = new HttpEntity<DTONews>(newNews,headers);
		 
	    ResponseEntity<DTONews> responseEntity =
	                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + DB_NEWS_ID,
	                		HttpMethod.PUT, request, DTONews.class);
     
       //Check response
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(responseEntity.getBody());
       assertEquals(DB_NEWS_ID,responseEntity.getBody().getId());
       assertEquals(DB_NEW_NEWS_TITLE, responseEntity.getBody().getTitle());
       
       //Check is data changed
       News updatedNewsCheck = newsService.findOne(DB_NEWS_ID);
       assertEquals(DB_NEWS_ID, updatedNewsCheck.getId());
       assertEquals(DB_NEW_NEWS_TITLE, updatedNewsCheck.getTitle());
       
       //UNDO changes
       updatedNewsCheck.setTitle(DB_NEW_TITLE_ID_1);
       newsService.update(updatedNewsCheck, updatedNewsCheck.getId());

	 }
	 
	 //Fail news does not exist
	 @Test
	 public void testUpdateNews_Fail() throws Exception {

		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);


	    DTONews newNews = new DTONews(null, null, DB_NEW_TEXT_ID_1, DB_NEW_NEWS_TITLE, DB_CULTURAL_OFFER_ID);
		HttpEntity<DTONews> request = new HttpEntity<DTONews>(newNews,headers);
		 
	    ResponseEntity<DTONews> responseEntity =
	                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + DB_NEW_DOES_NOT_EXIST_ID,
	                		HttpMethod.PUT, request, DTONews.class);
     
      //Check response
      assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

	 }
	 	 

	 //SUCCESS DELETE
	 @Test
	 public void testDeleteNews_Success() throws Exception {
		 
	    int size = newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID).size(); // broj slogova pre ubacivanja novog
		 assertEquals(size, DB_NEWS_IN_CULTURAL_OFFER);
		 login("admin@gmail.com", "admin");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    //Create new news
	    DTONews newNews = new DTONews(DB_NEW_DOES_NOT_EXIST_ID, null, DB_NEW_NEWS_TEXT, DB_NEW_NEWS_TITLE, DB_CULTURAL_OFFER_ID);
	    
	    HttpEntity<DTONews> request = new HttpEntity<DTONews>(newNews,headers);
		 
	    ResponseEntity<DTONews> newNewsRequest =
               restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" , HttpMethod.POST, request, DTONews.class);
		 
	    //id off new added
		 News newsCheck = newsService.findOne(DB_NEW_DOES_NOT_EXIST_ID);
		 
	    ResponseEntity<DTONews> responseEntity =
                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + newsCheck.getId(), HttpMethod.DELETE, request, DTONews.class);
      
       //Check response
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());;

		assertEquals(DB_NEWS_IN_CULTURAL_OFFER,newsService.findAll(DB_CATEGORY_ID, DB_CULTURAL_OFFER_ID).size());
	 }
	 
	 //FAIL - try to delete category which does not exists
	 @Test
	 public void testDeleteCategory_Fail() throws Exception {
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
		 
		 HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(headers);
		     
	     ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID + "/culturalOffers/" + DB_CULTURAL_OFFER_ID + "/news/" + DB_NEW_DOES_NOT_EXIST_ID, HttpMethod.DELETE, request, DTOCategory.class);
      
	    
       //Check response
       assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	 }
	 
}