package com.cultural_content.controller;

import static com.cultural_content.constants.CategoryConstants.*;

import static com.cultural_content.constants.UserConstants.DB_USER_ID;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cultural_content.dto.DTOCategory;
import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.model.Category;
import com.cultural_content.model.Comment;
import com.cultural_content.service.CategoryService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CategoryControllerIntegrationTest {
	
	 @Autowired
	 private TestRestTemplate restTemplate;
	 
	 @Autowired
	 private CategoryService categoryService;
	 
//	 @Autowired
//	 private CulturalOfferService culturalOfferService;
//	 
	 @Autowired
	 private UserService userService;
	 
	 private String accessToken;
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOLoginResponse.class);
	      
	        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
	        
	    
	    }
	   
	 @Test
	 public void testCreateCategory_Success() throws Exception {

		 assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		 int size = categoryService.findAll().size(); // broj slogova pre ubacivanja novog
		 
		 login("admin@gmail.com", "admin");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    DTOCategory newCategory = new DTOCategory(null, DB_NEW_CATEGORY, false, null);
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(newCategory,headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories", HttpMethod.POST, request, DTOCategory.class);
     
        //Provjera odgovora servisa
       DTOCategory category = responseEntity.getBody();
       assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
       assertNotNull(category);
       assertEquals(DB_NEW_CATEGORY, category.getName());
       
       List<Category> categories = categoryService.findAll();
       assertEquals(size + 1, categories.size());
       assertEquals(DB_NEW_CATEGORY,categories.get(categories.size()-1).getName());
		assertEquals(DB_CATEGORY_SIZE + 1,categoryService.findAll().size());
       //deleting added category
       categoryService.delete(category.getId());

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
	 }
	 
	 
	 //FAIL TEST - CREATIN CATEGORY WITH ALREADY EXISTING NAME
	 @Test
	 public void testCreateCategory_Fail() throws Exception {

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		 int size = categoryService.findAll().size();
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);

	    //category with DB_CATEGORY_NAME already exists
	    DTOCategory newCategory = new DTOCategory(null, DB_CATEGORY_NAME, false, null);
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(newCategory,headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories", HttpMethod.POST, request, DTOCategory.class);
     
       //Provjera odgovora servisa
       assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
       
       List<Category> categories = categoryService.findAll();
       assertEquals(size, categories.size());
       assertNotNull(categoryService.findByName(DB_CATEGORY_NAME));

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
	 }
	 
	 @Test
	 public void testGetAllCategories_Success() throws Exception {
		 
		 int size = categoryService.findAll().size(); // broj slogova pre ubacivanja novog
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	     // kreiramo objekat koji saljemo u sklopu zahteva
	     // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(headers);
		 
	    ResponseEntity<DTOCategory[]> responseEntity =
                restTemplate.exchange("/categories", HttpMethod.GET, request, DTOCategory[].class);
     
       //Provjera odgovora servisa
       DTOCategory[] category = responseEntity.getBody();
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(category);
       
       List<Category> categories = categoryService.findAll();
       //DB_CATEGORY_SIZE is number of all categories
       assertEquals(DB_CATEGORY_SIZE, categories.size());
	 }

	 @Test
	 public void testGetOneCategory_Success() throws Exception {
		 
		 int size = categoryService.findAll().size(); // broj slogova pre ubacivanja novog
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/"+ DB_CATEGORY_ID, HttpMethod.GET, request, DTOCategory.class);
     
        //Provjera odgovora servisa
       DTOCategory category = responseEntity.getBody();
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(category);
       assertEquals(DB_CATEGORY_NAME, category.getName());
	 }
	 
	 //FAIL - undefined - wrong cateogry id
	 @Test
	 public void testGetOneCategory_Fail() throws Exception {
		 
		 int size = categoryService.findAll().size(); // broj slogova pre ubacivanja novog
		 
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/7", HttpMethod.GET, request, DTOCategory.class);
     
        //Provjera odgovora servisa
       DTOCategory category = responseEntity.getBody();
       
       assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	 }
	 
	 @Test
	 public void testUpdateCategory_Success() throws Exception {
		 
		 int size = categoryService.findAll().size();
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);

	    DTOCategory updatedCategory = new DTOCategory(DB_CATEGORY_ID, DB_NEW_CATEGORY, false, null);
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(updatedCategory,headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID, HttpMethod.PUT, request, DTOCategory.class);
     
       //Check response
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertNotNull(responseEntity.getBody());
       assertEquals(DB_CATEGORY_ID,responseEntity.getBody().getId());
       assertEquals(DB_NEW_CATEGORY, responseEntity.getBody().getName());
       
       //Check is data changed
       Category updatedCategoryCheck = categoryService.findOne(DB_CATEGORY_ID);
       assertEquals(DB_CATEGORY_ID, updatedCategoryCheck.getId());
       assertEquals(DB_NEW_CATEGORY, updatedCategoryCheck.getName());
       
       //UNDO changes
       updatedCategoryCheck.setName(DB_CATEGORY_NAME);
       categoryService.update(updatedCategoryCheck, updatedCategoryCheck.getId());

	 }
	 
	 //FAIL - try to change name to already existing name in data base
	 @Test
	 public void testUpdateCategory_Fail() throws Exception {
		 
		 int size = categoryService.findAll().size();
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);

	    DTOCategory updatedCategory = new DTOCategory(DB_CATEGORY_ID, DB_CATEGORY_NAME_ALREADY_EXIST, false, null);
	    
	    HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(updatedCategory,headers);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/" + DB_CATEGORY_ID, HttpMethod.PUT, request, DTOCategory.class);
      
       //Check response
       assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

       //Check is data changed
       Category updatedCategoryCheck = categoryService.findOne(DB_CATEGORY_ID);
       assertEquals(DB_CATEGORY_ID, updatedCategoryCheck.getId());
       assertNotEquals(DB_CATEGORY_NAME_ALREADY_EXIST, updatedCategoryCheck.getName());
	 }
	 
	 
	 //SUCCESS DELETE
	 @Test
	 public void testDeleteCategory_Success() throws Exception {

			assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
		login("admin@gmail.com", "admin");
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Authorization", accessToken);
	    
	    //create new category
    	 DTOCategory newCategory = new DTOCategory(null, DB_NEW_CATEGORY, false, null);
		    
		 HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(newCategory,headers);
			 
		 ResponseEntity<DTOCategory> createCategory =
	             restTemplate.exchange("/categories", HttpMethod.POST, request, DTOCategory.class);
		    
		 int size = categoryService.findAll().size();
		 
		 Category newCategoryCheck = categoryService.findByName(DB_NEW_CATEGORY);
		 
	    ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/" + newCategoryCheck.getId(), HttpMethod.DELETE, request, DTOCategory.class);
      
       //Check response
       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
       assertEquals(size-1,categoryService.findAll().size());

		assertEquals(DB_CATEGORY_SIZE,categoryService.findAll().size());
	 }
	 
	 //FAIL - try to delete category which does not exists
	 @Test
	 public void testDeleteCategory_Fail() throws Exception {
		 
		 login("admin@gmail.com", "admin");
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
		    
		 int size = categoryService.findAll().size();
		 
		 HttpEntity<DTOCategory> request = new HttpEntity<DTOCategory>(headers);
		     
	     ResponseEntity<DTOCategory> responseEntity =
                restTemplate.exchange("/categories/" + size + 1, HttpMethod.DELETE, request, DTOCategory.class);
      
	    
       //Check response
       assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
	 }
	 
}