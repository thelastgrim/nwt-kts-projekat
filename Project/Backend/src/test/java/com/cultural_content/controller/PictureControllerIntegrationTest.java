package com.cultural_content.controller;

import static com.cultural_content.constants.CommentsConstants.DB_COMMENTS_ID;
import static com.cultural_content.constants.PictureConstants.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.File;
import java.nio.file.Files;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOPicture;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.model.Picture;
import com.cultural_content.service.PictureService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class PictureControllerIntegrationTest {
	 @Autowired
	 private TestRestTemplate restTemplate;
	 
	 @Autowired
	 private PictureService pictureService;
	 

	 
	 private String accessToken;
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOLoginResponse.class);
	      
	        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
	        
	    
	    }
	   
	
	 @Test
	 public void testGetPicture() {
		
		 
		ResponseEntity<Resource> responskeEntity = 
					restTemplate.getForEntity("/pictures/"+DB_PICTURE_ID, Resource.class);
			
		Resource picture = responskeEntity.getBody();
		assertEquals(HttpStatus.OK, responskeEntity.getStatusCode());
		assertNotNull(picture);
		
	 }
	 
	 
	 
	 @Test
	 public void testGetPictureNonExistingId() {
		 
		ResponseEntity<Resource> responskeEntity = 
				restTemplate.getForEntity("/pictures/-1", Resource.class);
		
		Resource picture = responskeEntity.getBody();
		assertEquals(HttpStatus.NOT_FOUND, responskeEntity.getStatusCode());
		assertNull(picture);
		
	 }
	 
	 @Transactional
	 @Commit
	 @Test
	 public void testCreatePictureForComment() throws Exception{
		
		 File file = new File(DB_PICTURE_TO_UPLOAD);

		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		 headers.add("Authorization", accessToken);
		 System.out.println(accessToken);
		 MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
         ContentDisposition contentDisposition = ContentDisposition
                 .builder("form-data")
                 .name("file")
                 .filename(file.getName())
                 .build();
         
         fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
         HttpEntity<Object> httpEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), fileMap);
         MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
         body.add("file", httpEntity);
         body.add("title", DB_PICTURE_TITLE);
         HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		 
         
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/pictures/culturalOffers/1/comments/1", HttpMethod.POST, requestEntity, DTOPicture.class);
		 
		 DTOPicture added = responseEntity.getBody();
		 
		 //odgovor servera
		 assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		 assertNotNull(added);
		 
		 //provera baze
		 List<Picture> pictures = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID);
		 assertEquals(size+1, pictures.size());
		 Picture picture = pictureService.getById(added.getId());
		 assertEquals(added.getId(), picture.getId());
		 assertEquals(added.getTitle(), picture.getTitle());
		 
		 //resetovanje
		 pictureService.delete(added.getId());
		 File f = new File(added.getPath());
		 f.delete();
		  
	 }
	 
	 
	 @Transactional
	 @Commit
	 @Test
	 public void testCreatePictureNoPictureName() throws Exception{
		
		 File file = new File(DB_PICTURE_TO_UPLOAD);

		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		 headers.add("Authorization", accessToken);
		 System.out.println(accessToken);
		 MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
         ContentDisposition contentDisposition = ContentDisposition
                 .builder("form-data")
                 .name("file")
                 .filename(file.getName())
                 .build();
         
         fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
         HttpEntity<Object> httpEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), fileMap);
         MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
         body.add("file", httpEntity);
         //body.add("title", DB_PICTURE_TITLE);
         HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		 
         
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/pictures/culturalOffers/1/comments/1", HttpMethod.POST, requestEntity, DTOPicture.class);
		 
		 DTOPicture added = responseEntity.getBody();
		 
		 
		 assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());
		 assertNull(added);
		 
		 List<Picture> pictures = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID);
		 assertEquals(size, pictures.size());
		  
	 }
	 
	 @Transactional
	 @Commit
	 @Test
	 public void testCreatePictureNoPicture() throws Exception{
		
		 File file = new File(DB_PICTURE_TO_UPLOAD);

		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		 headers.add("Authorization", accessToken);
		 System.out.println(accessToken);
		 MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
         ContentDisposition contentDisposition = ContentDisposition
                 .builder("form-data")
                 .name("file")
                 .filename(file.getName())
                 .build();
         
         fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
    
         MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
         //body.add("file", httpEntity);
         body.add("title", DB_PICTURE_TITLE);
         HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		 
         
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/pictures/culturalOffers/1/comments/1", HttpMethod.POST, requestEntity, DTOPicture.class);
		 
		 DTOPicture added = responseEntity.getBody();
		 
		 
		 assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());
		 assertNull(added);
		 
		 List<Picture> pictures = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID);
		 assertEquals(size, pictures.size());
		  
	 }
	 
	 /*
	 @Test
	 @Transactional
	 public void testGetAllPicturesNonExistingComment() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOPicture[]> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/789456123/pictures", HttpMethod.GET, httpEntity,DTOPicture[].class);
		 
		DTOPicture[] pictures = responseEntity.getBody();
		
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertNull(pictures);
		
	 }
	*/
	 
	 /*
	 @Test
	 @Transactional
	 public void testGetAllPictures() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOPicture[]> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1/pictures", HttpMethod.GET, httpEntity,DTOPicture[].class);
		 
		DTOPicture[] pictures = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(DB_PICTURE_SIZE,pictures.length);
		assertEquals(DB_PICTURE_ID, pictures[0].getId());
		
	 }
	 */
	 
	
	 
	 /*
	 @Test
	 public void testGetAllPicturesPageable() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<RestResponsePage<DTOPicture>> httpEntity = new HttpEntity<RestResponsePage<DTOPicture>>(headers);
		 ParameterizedTypeReference<RestResponsePage<DTOPicture>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOPicture>>() {
		};
		 
		 ResponseEntity<RestResponsePage<DTOPicture>> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1/pictures/by-page?page=0&size=10", HttpMethod.GET, httpEntity,typeRef);
		 
		Page<DTOPicture> pictures = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(DB_PICTURE_SIZE, pictures.getNumberOfElements());
		 assertEquals(DB_PICTURE_ID, pictures.getContent().get(0).getId());
	 }
	 */
	 
	 /*
	 @Test
	 public void testGetAllPicturesPageableByNonExistingId() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<RestResponsePage<DTOPicture>> httpEntity = new HttpEntity<RestResponsePage<DTOPicture>>(headers);
		 ParameterizedTypeReference<RestResponsePage<DTOPicture>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOPicture>>() {
		};
		 
		 ResponseEntity<RestResponsePage<DTOPicture>> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/456/pictures/by-page?page=0&size=10", HttpMethod.GET, httpEntity,typeRef);
		 
		Page<DTOPicture> pictures = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertEquals(null, pictures);
	 }
	 */
	 /*
	 
	 @Transactional
	 @Commit
	 @Test
	 public void testCreatePicture() throws Exception{
		
		 File file = new File(DB_PICTURE_TO_UPLOAD);

		 
		 
		 MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
         ContentDisposition contentDisposition = ContentDisposition
                 .builder("form-data")
                 .name("file")
                 .filename(file.getName())
                 .build();
         
         fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
         HttpEntity<Object> httpEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), fileMap);
         MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
         body.add("file", httpEntity);
         body.add("title", DB_PICTURE_TITLE);
         HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body);
		 
         
		 //int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
         int size = pictureService.getAllPicturesByCulturalOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/pictures", HttpMethod.POST, requestEntity, DTOPicture.class);
		 
		 DTOPicture added = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		 assertNotNull(added);
		 assertEquals(DB_PICTURE_TITLE, added.getTitle());
		 
		 List<Picture> pictures = pictureService.getAllPicturesByCulturalOfferId(DB_CULTURAL_OFFER_ID);
		 assertEquals(size+1, pictures.size());
		 assertEquals(DB_PICTURE_TITLE, pictures.get(pictures.size()-1).getTitle());
		 assertTrue(new File(added.getPath()).exists());
		 
		 pictureService.delete(added.getId());
		 File f = new File(added.getPath());
		 f.delete();
	 }
	 */
	 
	 

	 /*
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdatePicture() throws Exception{
		 DTOPicture dtoPicture = new DTOPicture();
		 dtoPicture.setTitle(DB_PICTURE_TITLE_UPDATED);
		 //dtoPicture.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoPicture, headers);
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1/pictures/1", HttpMethod.PUT, httpEntity, DTOPicture.class);
		 
		 DTOPicture updated = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertNotNull(updated);
		 assertEquals(DB_PICTURE_TITLE_UPDATED, updated.getTitle());
		 assertEquals(DB_PICTURE_ID, updated.getId());
		 
		 Picture dbUpdated = pictureService.getById(DB_PICTURE_ID);
		 assertEquals(DB_PICTURE_ID, dbUpdated.getId());
		 assertEquals(DB_PICTURE_TITLE_UPDATED, dbUpdated.getTitle());
		 
	 }
	 */
	 
	 /*
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdatePictureNonExistingId() throws Exception{
		 
		
		 
		 DTOPicture dtoPicture = new DTOPicture();
		 dtoPicture.setTitle(DB_PICTURE_TITLE_UPDATED);
		
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoPicture, headers);
		 
		 
		 ResponseEntity<DTOPicture> responseEntity = 
					 restTemplate.exchange("/pictures/456123", HttpMethod.PUT, httpEntity, DTOPicture.class);
		 
		 
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertNull(responseEntity.getBody()); 
	 }
	 */
	 
	 /*
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdatePictureDifferentUser() throws Exception{
		 Picture inBase = pictureService.getById(DB_PICTURE_ID);
		 
		 DTOPicture dtoPicture = new DTOPicture();
		 dtoPicture.setTitle(DB_PICTURE_TITLE_UPDATED);
		 
		 login("test2@gmail.com", "user"); //different user tries to update pictures
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoPicture, headers);
		 
		 ResponseEntity<DTOPicture> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1/pictures/1", HttpMethod.PUT, httpEntity, DTOPicture.class);
		 
		 DTOPicture updated = responseEntity.getBody();
		 assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
		 assertNull(updated);
		 
		 
		 Picture dbUpdated = pictureService.getById(DB_PICTURE_ID);
		 assertEquals(DB_PICTURE_ID, dbUpdated.getId());
		 assertEquals(inBase.getTitle(), dbUpdated.getTitle());
		 
	 }
	 */
	 /*
	 @Test
	 //@Transactional
	 //@Commit
	 public void testDeletePicture() throws Exception{
	
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/pictures/1", HttpMethod.DELETE, httpEntity, Void.class);
		 
		 //odgovor servera
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(null, responseEntity.getBody());
		 
		 //provera baze
		 int postSize = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 assertEquals(size-1, postSize);
		 
		 Picture pp = new Picture();
		 pp.setId(DB_PICTURE_ID);
		 pp.setComment(commentService.findById(DB_COMMENTS_ID));;
		 pp.setTitle(DB_PICTURE_TITLE);
		 pp.setPath(DB_PICTURE_UPLOAD_LOCATION);
		 pp.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID)); 
		 
	 }
	 
	 */
	 
	 /*
	 @Test
	 @Transactional
	 @Commit
	 public void testDeletePictureDifferentUser() throws Exception{
		 Picture inBase = pictureService.getById(DB_PICTURE_ID);
		 
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1/pictures/1", HttpMethod.DELETE, httpEntity, Void.class);
		 //odgovor servera
		 assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
		 assertNull(responseEntity.getBody());
		 
		 //provera baze
		 int postSize = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 assertEquals(size, postSize);
		 
		 Picture toDelete = pictureService.getById(DB_PICTURE_ID);
		 
		 assertFalse(toDelete.isDeleted());
		 assertEquals(inBase.getTitle(), toDelete.getTitle());
		 
		 
	 }
	 */
	 /*
	 @Test
	 @Transactional
	 @Commit
	 public void testDeletePictureNonExistingId() throws Exception{
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/pictures/-1", HttpMethod.DELETE, httpEntity, Void.class);
		 //odgovor servera
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertNull(responseEntity.getBody());
		 
		 //provera baze
		 int postSize = pictureService.getAllPicturesByCommentId(DB_COMMENTS_ID).size();
		 assertEquals(size, postSize);
		 
		 
	 }
	 */

}
