package com.cultural_content.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;

import javax.mail.Header;

import org.junit.jupiter.api.AfterAll;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOPicture;
import com.cultural_content.dto.DTOPictureUpload;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.service.CommentService;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.PictureService;
import com.cultural_content.service.UserService;
import com.support.RestResponsePage;
import com.cultural_content.model.Comment;
import com.cultural_content.model.Picture;

import static com.cultural_content.constants.CommentsConstants.*;
import static com.cultural_content.constants.CulturalOfferConstants.*;
import static com.cultural_content.constants.PageConstants.*;
import static com.cultural_content.constants.UserConstants.*;
import static com.cultural_content.constants.PictureConstants.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CommentControllerIntegrationTest {
	
	 @Autowired
	 private TestRestTemplate restTemplate;
	 
	 @Autowired
	 private CommentService commentService;
	 
	 @Autowired
	 private CulturalOfferService culturalOfferService;
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private PictureService pictureService;
	 
	 private String accessToken;
	 
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOLoginResponse.class);
	      
	        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
	        
	    
	    }
	   
	 @Test
	 public void testGetComment() {
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
	    
		 
		ResponseEntity<DTOComment> responskeEntity = 
				restTemplate.exchange("/culturalOffers/1/comments/1", HttpMethod.GET,httpEntity,DTOComment.class);
		
		DTOComment dtoComment = responskeEntity.getBody();
		assertEquals(HttpStatus.OK, responskeEntity.getStatusCode());
		assertNotNull(dtoComment);
		assertEquals(DB_COMMENTS_ID, dtoComment.getId());
	 }
	 
	 @Test
	 public void testGetCommentNotFoundById() {
		 login("test@gmail.com", "user");
	        // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
	     HttpHeaders headers = new HttpHeaders();
	     headers.add("Authorization", accessToken);
	        // kreiramo objekat koji saljemo u sklopu zahteva
	        // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
	    HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
	    
		 
		ResponseEntity<DTOComment> responskeEntity = 
				restTemplate.exchange("/culturalOffers/1/comments/15647", HttpMethod.GET,httpEntity,DTOComment.class);
		
		DTOComment dtoComment = responskeEntity.getBody();
		assertEquals(HttpStatus.NOT_FOUND, responskeEntity.getStatusCode());
		assertEquals(null, dtoComment);
	 }

	 @Test
	 @Transactional
	 public void testGetAllComments() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOComment[]> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments", HttpMethod.GET, httpEntity,DTOComment[].class);
		 
		DTOComment[] comments = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(DB_COMMENTS_SIZE,comments.length);
		assertEquals(DB_COMMENTS_ID, comments[0].getId());
		
	 }
	 
	 @Test
	 @Transactional
	 public void testGetAllCommentsNotFoundByCulturalOfferId() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOComment[]> responseEntity = 
				 restTemplate.exchange("/culturalOffers/555/comments", HttpMethod.GET, httpEntity,DTOComment[].class);
		 
		//DTOComment[] comments = responseEntity.getBody();
		
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals(null, responseEntity.getBody());
		
	 }
	 
	 @Test
	 public void testGetAllCommentsPageable() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<RestResponsePage<DTOComment>> httpEntity = new HttpEntity<RestResponsePage<DTOComment>>(headers);
		 ParameterizedTypeReference<RestResponsePage<DTOComment>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOComment>>() {
		};
		 
		 ResponseEntity<RestResponsePage<DTOComment>> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/by-page?page=0&size=10", HttpMethod.GET, httpEntity, typeRef);
		 
		Page<DTOComment> comments = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(DB_COMMENTS_SIZE, comments.getNumberOfElements());
		 assertEquals(DB_COMMENTS_ID, comments.getContent().get(0).getId());
	 }
	 
	 @Test
	 public void testGetAllCommentsPageableByNonExistingId() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<RestResponsePage<DTOComment>> httpEntity = new HttpEntity<RestResponsePage<DTOComment>>(headers);
		 ParameterizedTypeReference<RestResponsePage<DTOComment>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOComment>>() {
		};
		 
		 ResponseEntity<RestResponsePage<DTOComment>> responseEntity = 
				 restTemplate.exchange("/culturalOffers/566/comments/by-page?page=0&size=10", HttpMethod.GET, httpEntity, typeRef);
		 
		Page<DTOComment> comments = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertEquals(null, comments);
		
	 }

	 
	 
	 @Test
	 @Transactional
	 @Commit
	 public void testCreateComment() throws Exception{
		 
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_TEXT);
		 dtoComment.setRating(3.0);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 int size = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments", HttpMethod.POST, httpEntity, DTOComment.class);
		 
		 DTOComment added = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		 assertNotNull(added);
		 assertEquals(DB_COMMENT_TEXT, added.getText());
		 
		 List<Comment> comments = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID);
		 assertEquals(size+1, comments.size());
		 assertEquals(DB_COMMENT_TEXT, comments.get(comments.size()-1).getText());
		 
		 commentService.delete(added.getId());	  
	 }
	 
	 @Test
	 @Transactional
	 @Commit
	 public void testCreateCommentWithoutRating() throws Exception{
		 
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_TEXT);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 int size = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments", HttpMethod.POST, httpEntity, DTOComment.class);
		 
		 DTOComment added = responseEntity.getBody();
		 
		 assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		 assertNotNull(added);
		 assertNull(added.getId());
		 assertNull(added.getText());
		 assertNull(added.getDate());
		 
		 List<Comment> comments = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID);
		 assertEquals(size, comments.size());
		 
	 }
	 
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdateComment() throws Exception{
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_UPDATED_TEXT);
		 dtoComment.setRating(3.0);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1", HttpMethod.PUT, httpEntity, DTOComment.class);
		 
		 DTOComment updated = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertNotNull(updated);
		 assertEquals(DB_COMMENT_UPDATED_TEXT, updated.getText());
		 assertEquals(DB_COMMENTS_ID, updated.getId());
		 
		 Comment dbUpdated = commentService.findById(DB_COMMENTS_ID);
		 assertEquals(DB_COMMENTS_ID, dbUpdated.getId());
		 assertEquals(DB_COMMENT_UPDATED_TEXT, dbUpdated.getText());
		 
	 }
	 
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdateCommentWithoutRating() throws Exception{
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_UPDATED_TEXT);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 Comment inBase = commentService.findById(DB_COMMENTS_ID);		 
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1", HttpMethod.PUT, httpEntity, DTOComment.class);
		 
		 DTOComment updated = responseEntity.getBody();
		 assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		 assertNotNull(updated);
		 assertNull(updated.getText());
		 assertNull(updated.getId());
		 assertNull(updated.getDate());
		 
		 Comment dbUpdated = commentService.findById(DB_COMMENTS_ID);
		 assertEquals(inBase.getId(), dbUpdated.getId());
		 assertEquals(inBase.getText(), dbUpdated.getText());
		 
	 }
	 
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdateCommentForDifferentUser() throws Exception{
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_UPDATED_TEXT);
		 dtoComment.setRating(3.0);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/2", HttpMethod.PUT, httpEntity, DTOComment.class);
		 
		 DTOComment updated = responseEntity.getBody();
		 assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
		 assertEquals(null, updated);
		 
	 }
	 
	 @Test
	 @Transactional
	 @Rollback(true)
	 public void testUpdateCommentIdNotFound() throws Exception{
		 DTOComment dtoComment = new DTOComment();
		 dtoComment.setDate(new Date());
		 dtoComment.setText(DB_COMMENT_UPDATED_TEXT);
		 dtoComment.setRating(3.0);
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(dtoComment, headers);
		 
		 ResponseEntity<DTOComment> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/189465187", HttpMethod.PUT, httpEntity, DTOComment.class);
		 
		 DTOComment updated = responseEntity.getBody();
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertEquals(null, updated);
		 
	 }
	 
	 @Test
	 @Transactional
	 @Commit
	 public void testDeleteComment() throws Exception{
	
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/1", HttpMethod.DELETE, httpEntity, Void.class);
		 //odgovor servera
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(null, responseEntity.getBody());
		 
		 //provera baze
		 int postSize = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 assertEquals(size-1, postSize);
		 
		 Comment cc = new Comment();
		 cc.setId(DB_COMMENTS_ID);
		 cc.setDate(new Date());
		 cc.setUser(userService.findById(DB_USER_ID));
		 cc.setCulturalOffer(culturalOfferService.findById(DB_CULTURAL_OFFER_ID));
		 cc.setRating(3.0);

		 commentService.create(cc);
		 
		 Picture pp = new Picture();
		 DTOPictureUpload dtoPictureUpload = new DTOPictureUpload();
		 //dtoPictureUpload.set
		 pp.setId(DB_PICTURE_ID);
		 pp.setComment(commentService.findById(DB_COMMENTS_ID));
		 pp.setTitle(DB_PICTURE_TITLE);
		 pp.setPath(DB_PICTURE_UPLOAD_LOCATION);
		 pp.setDeleted(false);
		 
		 //pictureService.createAndUploadForComment(dtoPictureUpload, id, idc)(pp);
		  
	 }
	 
	 @Test
	 @Transactional
	 @Commit
	 public void testDeleteCommentForDifferentUser() throws Exception{
	
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/2", HttpMethod.DELETE, httpEntity, Void.class);
		 //odgovor servera
		 assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
		 assertEquals(null, responseEntity.getBody());
		 
		 //provera baze
		 int postSize = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 assertEquals(size, postSize);
		 
	 }
	 
	 @Test
	 @Transactional
	 @Commit
	 public void testDeleteCommentIdNotFound() throws Exception{
	
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 int size = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 
		 
		 ResponseEntity<Void> responseEntity = 
				 restTemplate.exchange("/culturalOffers/1/comments/4564846", HttpMethod.DELETE, httpEntity, Void.class);
		 //odgovor servera
		 assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		 assertEquals(null, responseEntity.getBody());
		 
		 //provera baze
		 int postSize = commentService.findAllByCultularOfferId(DB_CULTURAL_OFFER_ID).size();
		 assertEquals(size, postSize);
		 
	 }
}
