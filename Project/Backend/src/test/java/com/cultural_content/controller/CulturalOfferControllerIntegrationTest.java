package com.cultural_content.controller;

import static com.cultural_content.constants.CategoryConstants.DB_CATEGORY_ID;
import static com.cultural_content.constants.CulturalOfferConstants.*;
import static com.cultural_content.constants.LocationConstants.*;

import static com.cultural_content.constants.PageConstants.PAGE_SIZE;
import static com.cultural_content.constants.PictureConstants.DB_PICTURE_TO_UPLOAD;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cultural_content.dto.DTOComment;
import com.cultural_content.dto.DTOCulturalOffer;
import com.cultural_content.dto.DTOCulturalOfferAdd;
import com.cultural_content.dto.DTOLocation;
import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.dto.DTOUsersOffers;
import com.cultural_content.model.CulturalOffer;
import com.cultural_content.model.Location;
import com.cultural_content.service.CulturalOfferService;
import com.cultural_content.service.LocationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.support.RestResponsePage;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CulturalOfferControllerIntegrationTest {
	
	@Autowired 
	private CulturalOfferController offerController;
	
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private CulturalOfferService culturalOfferService;
	
	@Autowired
	private LocationService locationService;
	
    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
                new DTOUserLogin(username,password), DTOLoginResponse.class);
      
        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
        
    
    }
    
    @Transactional
	@Test
    @WithMockUser(username = "admin@gmail.com", password =  "admin", roles = "ADMIN")
    public void testCreateCulturalOffer_Success() throws Exception {

      int before = culturalOfferService.findAllByDeleted().size();
      
      File file = ResourceUtils.getFile(DB_PICTURE_TO_UPLOAD);
      int size = (int) file.length();
      byte[] bytes = new byte[size];
      try {
          BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
          buf.read(bytes, 0, bytes.length);
          buf.close();
      } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }

      MockMultipartFile file9 = new MockMultipartFile("picture", "ONoImage.jpg", "image/png",bytes);

      MockMvc mockMvc = MockMvcBuilders.standaloneSetup(offerController).build();
      mockMvc.perform(((MockMultipartHttpServletRequestBuilder) MockMvcRequestBuilders.fileUpload("/offers")
    		  .param("name", NEW_CULTURAL_OFFER_NAME)
    		  .param("about", NEW_CULTURAL_OFFER_ABOUT)	  
      		  .param("lng", NEW_LOCATION_LNG_CREATE)
      		  .param("lat", NEW_LOCATION_LAT_CREATE)		
      		  .param("country", NEW_LOCATION_CITY)
      		  .param("City", NEW_LOCATION_COUNTRY)
      		  .param("street", NEW_LOCATION_STREET))
    		  .file(file9)
    		  .param("category", "Museums"))
                  .andDo(MockMvcResultHandlers.print())
                  .andExpect(MockMvcResultMatchers.status().isCreated())
                  .andReturn();
      
	  assertEquals(before + 1, culturalOfferService.findAllByDeleted().size());
      culturalOfferService.delete(culturalOfferService.findOneByName(NEW_CULTURAL_OFFER_NAME).getId());
	  assertEquals(before, culturalOfferService.findAllByDeleted().size());
    }

    @Transactional
	@Test
    @WithMockUser(username = "admin@gmail.com", password =  "admin", roles = "ADMIN")
    public void testCreateCulturalOffer_NameAlreadyExists() throws Exception {
    	
        File file = ResourceUtils.getFile(DB_PICTURE_TO_UPLOAD);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MockMultipartFile file9 = new MockMultipartFile("picture", "ONoImage.jpg", "image/png",bytes);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(offerController).build();

        mockMvc.perform(((MockMultipartHttpServletRequestBuilder) MockMvcRequestBuilders.fileUpload("/offers")
    	 		  .param("name", DB_CULTURAL_OFFER_NAME)
         		  .param("about", NEW_CULTURAL_OFFER_ABOUT)	  
  	      		  .param("lng", NEW_LOCATION_LNG_CREATE)
   	      		  .param("lat", NEW_LOCATION_LAT_CREATE)		
   	      		  .param("country", NEW_LOCATION_CITY)
   	      		  .param("City", NEW_LOCATION_COUNTRY)
    	      	  .param("street", NEW_LOCATION_STREET))
    	      	  .file(file9)
    	   		  .param("category", "Museums"))
    	                   .andDo(MockMvcResultHandlers.print())
    	                   .andExpect(MockMvcResultMatchers.status().isBadRequest())
    	                   .andReturn();    
      }
    
        @Transactional
    	@Test
        @WithMockUser(username = "admin@gmail.com", password =  "admin", roles = "ADMIN")
        public void testCreateCulturalOffer_LocationAlreadyExists() throws Exception {

            File file = ResourceUtils.getFile(DB_PICTURE_TO_UPLOAD);
            int size = (int) file.length();
            byte[] bytes = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                buf.read(bytes, 0, bytes.length);
                buf.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            MockMultipartFile file9 = new MockMultipartFile("picture", "ONoImage.jpg", "image/png",bytes);

            MockMvc mockMvc = MockMvcBuilders.standaloneSetup(offerController).build();
            mockMvc.perform(((MockMultipartHttpServletRequestBuilder) MockMvcRequestBuilders.fileUpload("/offers")
          		  .param("name", NEW_CULTURAL_OFFER_NAME)
          		  .param("about", NEW_CULTURAL_OFFER_ABOUT)	  
          		  .param("lng", NEW_LOCATION_LNG_CREATE_FAIL)
          		  .param("lat", NEW_LOCATION_LAT_CREATE_FAIL)	
          		  .param("country", NEW_LOCATION_CITY)
          		  .param("City", NEW_LOCATION_COUNTRY)
          		  .param("street", NEW_LOCATION_STREET))
          		  .file(file9)
          		  .param("category", "Museums"))
                        .andDo(MockMvcResultHandlers.print())
                        .andExpect(MockMvcResultMatchers.status().isBadRequest())
                        .andReturn(); 
       
    }
  
    
    /*
    @Test
    public void testUpdateCulturalOffer() throws Exception {
    	
	  for(CulturalOffer co : culturalOfferService.findAllByDeleted()) {
    	  System.out.println("PONUDE PRIJE " + co.getName());
      }
	  login("admin@gmail.com", "admin");
      // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
      HttpHeaders headers = new HttpHeaders();
      headers.add("Authorization", accessToken);
      // kreiramo objekat koji saljemo u sklopu zahteva
      // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
      
      DTOCulturalOffer culturalOfferDTO = new DTOCulturalOffer(null, UPDATED_CULTURAL_OFFER_NAME, NEW_CULTURAL_OFFER_ABOUT, 5, "", false, null, null, null,null);
      HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(culturalOfferDTO, headers);
	 
      ResponseEntity<DTOCulturalOffer> responseEntity =
           restTemplate.exchange("/offers/" + DB_CULTURAL_OFFER_ID, HttpMethod.PUT, request, DTOCulturalOffer.class);

      //Provjera odgovora
      DTOCulturalOffer culturalOffer = responseEntity.getBody();
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
      assertNotNull(culturalOffer);
      assertEquals(UPDATED_CULTURAL_OFFER_NAME, culturalOffer.getName());
      assertEquals(NEW_CULTURAL_OFFER_ABOUT, culturalOffer.getAbout());
      
      for(CulturalOffer co : culturalOfferService.findAllByDeleted()) {
    	  System.out.println("PONUDE KASNIJEE " + co.getName());
      }
      
    //UNDO changes
      CulturalOffer updatedCulturalOffer = culturalOfferService.findOne(DB_CULTURAL_OFFER_ID);
      updatedCulturalOffer.setName(DB_CULTURAL_OFFER_NAME);
      culturalOfferService.update(updatedCulturalOffer, updatedCulturalOffer.getId());
      

      for(CulturalOffer co : culturalOfferService.findAllByDeleted()) {
    	  System.out.println("PONUDE KASNIJEE " + co.getName());
      }

    }
    */
    
    /*
    @Test
    public void testUpdateCulturalOffer_NameAlreadyExist() throws Exception {
    	
	  for(CulturalOffer co : culturalOfferService.findAllByDeleted()) {
    	  System.out.println("PONUDE PRIJE " + co.getName());
      }
	  login("admin@gmail.com", "admin");
      // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
      HttpHeaders headers = new HttpHeaders();
      headers.add("Authorization", accessToken);
      // kreiramo objekat koji saljemo u sklopu zahteva
      // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu
      
      DTOCulturalOffer culturalOfferDTO = new DTOCulturalOffer(null, DB_CULTURAL_OFFER_NAME_2, NEW_CULTURAL_OFFER_ABOUT, 5, "", false, null, null, null,null);
      HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(culturalOfferDTO, headers);
	 
      ResponseEntity<DTOCulturalOffer> responseEntity =
           restTemplate.exchange("/offers/" + DB_CULTURAL_OFFER_ID, HttpMethod.PUT, request, DTOCulturalOffer.class);

      //Provjera odgovora
      assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
      
      for(CulturalOffer co : culturalOfferService.findAllByDeleted()) {
    	  System.out.println("PONUDE KASNIJEE " + co.getName());
      }

    }
    */
    
	@Test
    public void testDeleteCulturalOffer() throws Exception {
    	
	  login("admin@gmail.com", "admin");
      // postavimo JWT token u zaglavlje zahteva da bi bilo dozvoljeno da pozovemo funkcionalnost
      HttpHeaders headers = new HttpHeaders();
      headers.add("Authorization", accessToken);
      // kreiramo objekat koji saljemo u sklopu zahteva
      // objekat nema telo, vec samo zaglavlje, jer je rec o GET zahtevu

		CulturalOffer newOffer = new CulturalOffer();
		newOffer.setName(CULTURAL_OFFER_NAME_FOR_DELETE);
		newOffer.setAbout(NEW_CULTURAL_OFFER_ABOUT);
		newOffer.setAverageRating(NEW_CULTURAL_OFFER_AVG_RATING);
		newOffer.setDeleted(false);
		
		Location location = new Location();
		location.setCity(NEW_LOCATION_CITY);
		location.setCountry(NEW_LOCATION_COUNTRY);
		location.setDeleted(false);
		location.setLAT(NEW_LOCATION_LAT);
		location.setLNG(NEW_LOCATION_LNG);
		location.setStreet(NEW_LOCATION_STREET);
		
	
		culturalOfferService.create(
				newOffer, 
				location, 
				DB_CATEGORY_ID);
				
      DTOLocation locationDTO = new DTOLocation(NEW_LOCATION_LNG_FOR_DELETE, NEW_LOCATION_LAT_FOR_DELETE, NEW_LOCATION_CITY, NEW_LOCATION_COUNTRY, NEW_LOCATION_STREET);
      DTOCulturalOffer culturalOfferDTO = new DTOCulturalOffer(null, CULTURAL_OFFER_NAME_FOR_DELETE, NEW_CULTURAL_OFFER_ABOUT, NEW_CULTURAL_OFFER_AVG_RATING, "Museum", false, null, null, locationDTO,null);
      HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(culturalOfferDTO, headers);

  	  int sizeBeforeDelete = culturalOfferService.findAllByDeleted().size(); 
      int id = culturalOfferService.findOneByName(CULTURAL_OFFER_NAME_FOR_DELETE).getId();

	  System.out.println(id);
      ResponseEntity<DTOCulturalOffer> responseEntity =
              restTemplate.exchange("/offers/" + id , HttpMethod.DELETE, request, DTOCulturalOffer.class);
   
      assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
      assertEquals(sizeBeforeDelete - 1, culturalOfferService.findAllByDeleted().size());
    
    }
    
    
    @Test
    public void testDeleteCulturalOffer_IdDoesntExist() throws Exception {
    	
	  login("admin@gmail.com", "admin");
      HttpHeaders headers = new HttpHeaders();
      headers.add("Authorization", accessToken);
      
      HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
	
      ResponseEntity<DTOCulturalOffer> responseEntity =
              restTemplate.exchange("/offers/" + NOT_DB_CULTURAL_OFFER_ID, HttpMethod.DELETE, request, DTOCulturalOffer.class);
      
      assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

    }
    
    @Test
    public void testSubscribeToCulturalOffer() throws Exception {
    	
    	  login("test2@gmail.com", "user");
          HttpHeaders headers = new HttpHeaders();
          headers.add("Authorization", accessToken);
          
          HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
          
          ResponseEntity<DTOCulturalOffer> responseEntity =
                  restTemplate.exchange("/offers/" + DB_CULTURAL_OFFER_ID_SUBSCRIBE + "/subscribe", HttpMethod.POST, request, DTOCulturalOffer.class);
          
          assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
          
               
    }
    
    @Test
    public void testUnsubscribeToCulturalOffer() throws Exception {
    	
    	  login("test2@gmail.com", "user");
          HttpHeaders headers = new HttpHeaders();
          headers.add("Authorization", accessToken);
          
          
          HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
          
          ResponseEntity<DTOCulturalOffer> responseEntity =
                  restTemplate.exchange("/offers/" + DB_CULTURAL_OFFER_ID_SUBSCRIBE + "/unsubscribe", HttpMethod.POST, request, DTOCulturalOffer.class);
          
          assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
          
          
    }
    
    @Test
    public void testgetAllUniqueCities() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ParameterizedTypeReference<List<String>> typeRef = new ParameterizedTypeReference<List<String>>() {};
        
        ResponseEntity<List<String>> responseEntity =
                restTemplate.exchange("/offers/cities" , HttpMethod.GET, request,typeRef);
        
        
        List<String> cities = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(DB_LOCATION_UNIQUE_CITIES_SIZE, cities.size());
        
    }
    
    @Test
    public void testGetAllCulturalOffers() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<List<DTOCulturalOffer>> request = new HttpEntity<List<DTOCulturalOffer>>(headers);
        ParameterizedTypeReference<List<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<List<DTOCulturalOffer>>() {};
        
        ResponseEntity<List<DTOCulturalOffer>> responseEntity =
                restTemplate.exchange("/offers" , HttpMethod.GET, request,typeRef);
        
        
        List<DTOCulturalOffer> culturalOffers = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(DB_CULTURAL_OFFER_SIZE, culturalOffers.size());
        
    }
    
    @Test
    public void testGetOneCulturalOfferByName() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<RestResponsePage<DTOCulturalOffer>> httpEntity = new HttpEntity<RestResponsePage<DTOCulturalOffer>>(headers);
		 ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>>() {
		};
       
        ResponseEntity<RestResponsePage<DTOCulturalOffer>> responseEntity =
                restTemplate.exchange("/offers/search/?name={name}" , HttpMethod.GET, httpEntity, typeRef, DB_CULTURAL_OFFER_NAME_2);
        
        System.out.println(responseEntity.getBody().toString());
        
        Page<DTOCulturalOffer> culturalOffer = responseEntity.getBody();
        System.out.println(culturalOffer);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(DB_CULTURAL_OFFER_NAME_2, culturalOffer.getContent().get(0).getName());
        
    }
    
    @Test
    public void testGetOneCulturalOfferByName_NameDoesntExist() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
       
        ResponseEntity<DTOCulturalOffer> responseEntity =
                restTemplate.exchange("/offers/search/?name={name}" , HttpMethod.GET, request, DTOCulturalOffer.class, NOT_DB_CULTURAL_OFFER_NAME);
        
       
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        
    }
    
    
    @Test
    public void testGetOneCulturalOffer() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
        ParameterizedTypeReference<DTOCulturalOffer> typeRef = new ParameterizedTypeReference<DTOCulturalOffer>() {};
        
        ResponseEntity<DTOCulturalOffer> responseEntity =
                restTemplate.exchange("/offers/" + DB_CULTURAL_OFFER_ID , HttpMethod.GET, request,typeRef);
        
        
        DTOCulturalOffer culturalOffer = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(DB_CULTURAL_OFFER_ID, culturalOffer.getId());
        
    }
    
    @Test
    public void testGetOneCulturalOffer_IdDoesntExist() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<DTOCulturalOffer> request = new HttpEntity<DTOCulturalOffer>(headers);
        ParameterizedTypeReference<DTOCulturalOffer> typeRef = new ParameterizedTypeReference<DTOCulturalOffer>() {};
        
        ResponseEntity<DTOCulturalOffer> responseEntity =
                restTemplate.exchange("/offers/" + NOT_DB_CULTURAL_OFFER_ID , HttpMethod.GET, request,typeRef);
        

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
       
        
    }
    
    @Test
    public void testGetAllCulturalOffersByCategory() {
    	
    	login("test2@gmail.com", "user");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        
        HttpEntity<List<DTOCulturalOffer>> request = new HttpEntity<List<DTOCulturalOffer>>(headers);
        ParameterizedTypeReference<List<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<List<DTOCulturalOffer>>() {};
        
        ResponseEntity<List<DTOCulturalOffer>> responseEntity =
                restTemplate.exchange("/offers/categories/" + DB_CATEGORY_ID , HttpMethod.GET, request,typeRef);
        
        List<DTOCulturalOffer> culturalOffers = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, culturalOffers.size());
        
    }
    
    @Test
    public void testgetAllCulturalOffers() {
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 
		 HttpEntity<RestResponsePage<DTOCulturalOffer>> request = new HttpEntity<RestResponsePage<DTOCulturalOffer>>(headers);
	     ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>>() {};
	        
		 ResponseEntity<RestResponsePage<DTOCulturalOffer>> responseEntity = 
				 restTemplate.exchange("/offers/all-by-page?page=0&size=10", HttpMethod.GET, request,typeRef);
		 
		 
		 Page<DTOCulturalOffer> pageCulturalOffersDTO = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(PAGE_SIZE, pageCulturalOffersDTO.getNumberOfElements());
		 
    }
    
    @Test
    public void testGetAllCulturalOffersByCity() {
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 
	     HttpEntity<RestResponsePage<DTOCulturalOffer>> request = new HttpEntity<RestResponsePage<DTOCulturalOffer>>(headers);
	     ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>>() {};
	        
		 ResponseEntity<RestResponsePage<DTOCulturalOffer>> responseEntity = 
				 restTemplate.exchange("/offers/page-by-city/" + DB_LOCATION_NAME +"?page=0&size=10", HttpMethod.GET, request,typeRef);
		 
		 
		 Page<DTOCulturalOffer> pageCulturalOffersDTO = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CITY, pageCulturalOffersDTO.getNumberOfElements());
   }
    
    @Test
    public void testgetAllCulturalOffersByCategory() {
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 
	     HttpEntity<RestResponsePage<DTOCulturalOffer>> request = new HttpEntity<RestResponsePage<DTOCulturalOffer>>(headers);
	     ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>>() {};
	        
		 ResponseEntity<RestResponsePage<DTOCulturalOffer>> responseEntity = 
				 restTemplate.exchange("/offers/page-by-category/" + DB_CATEGORY_ID +"?page=0&size=10", HttpMethod.GET, request,typeRef);
		 
		 
		 Page<DTOCulturalOffer> pageCulturalOffersDTO = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, pageCulturalOffersDTO.getNumberOfElements());
   }
    
    @Test
    public void testGetAllCulturalOffersByCategoryAndUser() throws ParseException {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 
	     HttpEntity<String> request = new HttpEntity<String>(headers);
	     ParameterizedTypeReference<DTOUsersOffers> typeRef = new ParameterizedTypeReference<DTOUsersOffers>() {};
	        
		 ResponseEntity<DTOUsersOffers> responseEntity = 
				 restTemplate.exchange("/offers/page-by-category-and-user/" + DB_CATEGORY_ID +"?page=0&size=4", HttpMethod.GET, request,typeRef);
		 
		
		 DTOUsersOffers dto = responseEntity.getBody();
		
		 
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(4, dto.getCollectionSize());
		 assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CATEGORY, dto.getOffers().size());

		 
   }
    
    @Test
    public void testGetAllCulturalOffersByCategoryAndLocation() {
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);

	     HttpEntity<RestResponsePage<DTOCulturalOffer>> request = new HttpEntity<RestResponsePage<DTOCulturalOffer>>(headers);
	     ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>> typeRef = new ParameterizedTypeReference<RestResponsePage<DTOCulturalOffer>>() {};
	        
		 ResponseEntity<RestResponsePage<DTOCulturalOffer>> responseEntity = 
				 restTemplate.exchange("/offers/by-page/categories/" + DB_CATEGORY_ID + "/location/" +  DB_LOCATION_NAME +"?page=0&size=10", HttpMethod.GET, request,typeRef);
		 
		 
		 Page<DTOCulturalOffer> pageCulturalOffersDTO = responseEntity.getBody();
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(DB_CULTURAL_OFFER_SIZE_BY_CAT_AND_CITY, pageCulturalOffersDTO.getNumberOfElements());
		 for(DTOCulturalOffer co : pageCulturalOffersDTO) {
			 assertEquals(co.getLocation().getCity(), DB_LOCATION_NAME);
		 }
   }
   

}
