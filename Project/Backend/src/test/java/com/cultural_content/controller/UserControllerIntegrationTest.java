package com.cultural_content.controller;
import static com.cultural_content.constants.UserConstants.DB_USER_EMAIL;
import static com.cultural_content.constants.UserConstants.DB_USER_PASSWORD;
import static com.cultural_content.constants.UserConstants.DB_USER_USERNAME;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cultural_content.dto.DTOLoginResponse;
import com.cultural_content.dto.DTOUser;
import com.cultural_content.dto.DTOUserChange;
import com.cultural_content.dto.DTOUserLogin;
import com.cultural_content.dto.DTOUserTokenState;
import com.cultural_content.helper.UserMapper;
import com.cultural_content.model.User;
import com.cultural_content.repos.UserRepo;
import com.cultural_content.service.UserService;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class UserControllerIntegrationTest {
	
	 @Autowired
	 private TestRestTemplate restTemplate;
	 @Autowired
	 private UserRepo userRepo;
	 @Autowired
	 private UserService userService;
	 

	 private String accessToken;
	 
	 public void login(String username, String password) {
	        ResponseEntity<DTOLoginResponse> responseEntity = restTemplate.postForEntity("/auth/log-in",
	                new DTOUserLogin(username,password), DTOLoginResponse.class);
	      
	        accessToken = "Bearer " + responseEntity.getBody().getDtoUserTokenState().getAccessToken();
	        
	    
	    }
	 
	 @Test
	 @Rollback(true)
	 public void testUpdateUser() {
		 DTOUserChange change = new DTOUserChange();
		 change.setOldPassword(DB_USER_PASSWORD);
		 change.setNewPassword("novaLozinka");
		 
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(change, headers);
		 
		 ResponseEntity<DTOUser> responseEntity = 
				 restTemplate.exchange("/users", HttpMethod.PUT, httpEntity, DTOUser.class);
		 DTOUser updated = responseEntity.getBody();
		
		 assertEquals(HttpStatus.OK,  responseEntity.getStatusCode());
		// assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertNotNull(updated);
		 assertEquals(DB_USER_EMAIL,updated.getEmail() );
		 assertEquals(DB_USER_USERNAME,updated.getUsername());

	//	 assertEquals(NEW_USER_PASSWORD, updated.getPassword());
		 //updated.setPassword(DB_USER_PASSWORD);
		// User u = userMapper.toEntity(updated);
/*		 User u = userService.findById(2);
		 u.setPassword(DB_USER_PASSWORD);
		 userRepo.save(u);
		*/ 
		
	 }
	 
	 @Test
	 public void testUpdateWrongPassword() {
		 
		 DTOUserChange change = new DTOUserChange();
		 change.setOldPassword("nestoBzvz");
		 change.setNewPassword("novaLozinka");
		 
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(change, headers);
		 
		 ResponseEntity<DTOUser> responseEntity = 
				 restTemplate.exchange("/users", HttpMethod.PUT, httpEntity, DTOUser.class);
		
		 assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,  responseEntity.getStatusCode());
	
		
	 }
	 
	 @Test
	 public void testGetUser() {
		 login("test2@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOUser> responseEntity = 
				 restTemplate.exchange("/users/2", HttpMethod.GET, httpEntity, DTOUser.class);
		 DTOUser user = responseEntity.getBody();
		
		 assertEquals(HttpStatus.OK,  responseEntity.getStatusCode());
		
		 assertEquals(DB_USER_EMAIL,user.getEmail() );
		 assertEquals(DB_USER_USERNAME,user.getUsername());
	 }
	 
	 @Test
	 public void testGetUserNotFound() {
		 login("test@gmail.com", "user");
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Authorization", accessToken);
		 HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		 
		 ResponseEntity<DTOUser> responseEntity = 
				 restTemplate.exchange("/users/258", HttpMethod.GET, httpEntity, DTOUser.class);
		
		 assertEquals(HttpStatus.NOT_FOUND,  responseEntity.getStatusCode());
		
	 }
	

}
